using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{
	public static class Vector3DUnityExtentions
	{
		/*public static Vector2D Normalize (Vector3 v)
		{
			float m = (float)Math.Sqrt((double)(v.x*v.x + v.z*v.z)); 
			return m>1E-05f ? new Vector2D(v.x/m, v.z/m) : new Vector2D(0,0);
		}*/

		/*public static Vector3 operator * (Vector3 a, Vector2D b) => new Vector3(a.x*b.x, a.y, a.z*b.z);
		public static Vector3 operator / (Vector3 a, Vector2D b) => new Vector3(a.x/b.x, a.y, a.z/b.z);
		public static Vector3 operator + (Vector3 a, Vector2D b) => new Vector3(a.x+b.x, a.y, a.z+b.z);
		public static Vector3 operator - (Vector3 a, Vector2D b) => new Vector3(a.x-b.x, a.y, a.z-b.z);
		public static Vector3 operator * (Vector2D a, Vector3 b) => new Vector3(a.x*b.x, b.y, a.z*b.z);
		public static Vector3 operator / (Vector2D a, Vector3 b) => new Vector3(a.x/b.x, b.y, a.z/b.z);
		public static Vector3 operator + (Vector2D a, Vector3 b) => new Vector3(a.x+b.x, b.y, a.z+b.z);
		public static Vector3 operator - (Vector2D a, Vector3 b) => new Vector3(a.x-b.x, b.y, a.z-b.z);


		public static explicit operator Vector2D(Vector3 v) => new Vector2D(v.x, v.z);
		public static explicit operator Vector3(Vector2D v) => new Vector3(v.x, 0f, v.z);
		public static explicit operator Vector2D(Vector2 v) => new Vector2D(v.x, v.y);
		public static explicit operator Vector2(Vector2D v) => new Vector3(v.x, v.z);
		public static explicit operator Vector2D(float v) => new Vector2D(v, v);*/

		public static Vector3D Vector3D (this Vector3 v) => new Vector3D(v.x, v.y, v.z);
		public static Vector3 Vector3 (this Vector3D v) => new Vector3(v.x, v.y, v.z);
		public static Vector3D Vector3D(this Vector2 v) => new Vector3D(v.x, v.y, 0);
		public static Vector2 Vector2(this Vector3D v) => new Vector2(v.x, v.y);
		public static Vector3D Vector3D(this float v) => new Vector3D(v, v, v);

	}
}