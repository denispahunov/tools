using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{
	public static class CoordUnityExtentions
	{
		//public static Vector2 operator * (Coord c, Vector2 s) { return  new Vector2(c.x*s.x, c.z*s.y); }
		//public static Vector3 operator * (Coord c, Vector3 s) { return  new Vector3(c.x*s.x, s.y, c.z*s.z); }

		public static Vector4 Vector4(this Coord c) => new Vector4(c.x, 0, c.z, 0);
		public static Vector3 Vector3(this Coord c) => new Vector3(c.x, 0, c.z);
		public static Vector2 Vector2(this Coord c) => new Vector2(c.x, c.z);
		public static Vector2D Vector2D(this Coord c) => new Vector2D(c.x, c.z);

		public static Vector3 Vector3 (this Coord c, float cellSize) => new Vector3(c.x*cellSize, 0, c.z*cellSize);
		public static Vector2 Vector2 (this Coord c, float cellSize) => new Vector2(c.x*cellSize, c.z*cellSize);
		public static Rect Rect (this Coord c, float cellSize) => new Rect(c.x*cellSize, c.z*cellSize, cellSize, cellSize);

			public static Coord Floor (Vector3 v)
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)v.x, (int)(float)v.z);
			}


			public static Coord Round (Vector3 v)
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)(v.x + 0.5f), (int)(float)(v.z + 0.5f));
			}
	}
	
}