﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{
	public static class CoordRectUnityExtentions
	{
		public enum TileMode { Clamp=0, Tile=1, PingPong=2 } //already defined in CoordRect, but requres to be here for compile purpose

		public static Vector3 CenterVector3 (this CoordRect cr) => new Vector3(cr.offset.x + cr.size.x/2f, 0, cr.offset.z + cr.size.z/2f);
		public static Vector4 Vector4 (this CoordRect cr) => new Vector4(cr.offset.x, cr.offset.z, cr.size.x, cr.size.z);

		public static bool Contains (this CoordRect cr, Vector2 pos)
		{ 
			return (pos.x- cr.offset.x >= 0 && pos.x- cr.offset.x < cr.size.x && 
					pos.y- cr.offset.z >= 0 && pos.y- cr.offset.z < cr.size.z); 
		}

		public static bool Contains (this CoordRect cr, Vector3 pos)
		{ 
			return (pos.x- cr.offset.x >= 0 && pos.x- cr.offset.x < cr.size.x && 
					pos.z- cr.offset.z >= 0 && pos.z- cr.offset.z < cr.size.z); 
		}

		public static bool Contains (this CoordRect cr, Vector3D pos)
		{ 
			return (pos.x- cr.offset.x >= 0 && pos.x- cr.offset.x < cr.size.x && 
					pos.z- cr.offset.z >= 0 && pos.z- cr.offset.z < cr.size.z); 
		}

		public static void DrawGizmo (this CoordRect cr)
		{
			#if UNITY_EDITOR
			Vector3 s = cr.size.Vector3();
			Vector3 o = cr.offset.Vector3();
			Gizmos.DrawWireCube(o + s/2, s);
			#endif
		}
	}
}