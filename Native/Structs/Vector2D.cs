using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{

	[Serializable]
	[StructLayout (LayoutKind.Sequential)] //to pass to native
	public struct Vector2D : IEquatable<Vector2D> 
	{
		public float x;
		public float z;

		public const float kEpsilon = 1E-05F;
		public const float kEpsilonNormalSqrt = 1E-15F;

		public float this[int c] { get => c==0 ? x : z;  set { if (x==0) x=value; else z=value; } }

		public Vector2D (float x, float z) { this.x=x; this.z=z; }
		public Vector2D (float v) { this.x=v; this.z=v; }

		public static readonly Vector2D zero = new Vector2D(0,0);
		public static readonly Vector2D one = new Vector2D(1,1);

		public Vector3D Vector3D() => new Vector3D(x, 0, z); //naming this way to prevent usual naming convention between these and Unity vectors

		public static Vector2D Zero { get; } = new Vector2D(0f, 0f);
		public static Vector2D One { get; } = new Vector2D(1f, 1f);
		public static Vector2D PositiveInfinity { get; } = new Vector2D(float.PositiveInfinity, float.PositiveInfinity);
		public static Vector2D NegativeInfinity { get; } = new Vector2D(float.NegativeInfinity, float.NegativeInfinity);

		public float SqrMagnitude => x*x + z*z;
		public float Magnitude => (float)Math.Sqrt((double)(x*x + z*z));
		public Vector2D Normalized 
		{get{ 
			float m = (float)Math.Sqrt((double)(x*x + z*z)); 
			return m>1E-05f ? new Vector2D(x/m, z/m) : new Vector2D(0,0);
		}}

		public void ClampPositive ()
		{
			if (x<0) x=0;
			if (z<0) z=0;
		}

		public static float Dot (Vector2D lhs, Vector2D rhs) 
		{ 
			return lhs.x * rhs.x + lhs.z * rhs.z; 
		}

		public static float Distance (Vector2D a, Vector2D b)
		{
			float num = a.x - b.x;
			float num2 = a.z - b.z;
			return (float)Math.Sqrt((double)(num * num + num2 * num2));
		}

		public static Vector2D Lerp (Vector2D a, Vector2D b, float t)
		{
			if (t>1) t=1; 
			if (t<0) t=0;
			return new Vector2D(a.x + (b.x - a.x) * t, a.z + (b.z - a.z) * t);
		}

		public static Vector2D Min (Vector2D lhs, Vector2D rhs)
		{
			return new Vector2D(lhs.x<rhs.x ? lhs.x : rhs.x, lhs.z<rhs.z ? lhs.z : rhs.z);
		}

		public static Vector2D Max (Vector2D lhs, Vector2D rhs)
		{
			return new Vector2D(lhs.x>rhs.x ? lhs.x : rhs.x, lhs.z>rhs.z ? lhs.z : rhs.z);
		}

/*		public static Vector2D Normalize (Vector3 v)
		{
			float m = (float)Math.Sqrt((double)(v.x*v.x + v.z*v.z)); 
			return m>1E-05f ? new Vector2D(v.x/m, v.z/m) : new Vector2D(0,0);
		}
*/
		public void Normalize ()
		{
			float m = (float)Math.Sqrt((double)(x*x + z*z)); 
			if (m>1E-05f) {x=x/m; z=z/m;}
			else {x=0; z=0;}
		}

		public override int GetHashCode () { return x.GetHashCode() ^ z.GetHashCode() << 2; }
		public bool Equals (Vector2D other) { return x == other.x && z == other.z; }
		public override bool Equals (object other)
		{
			if (!(other is Vector2D otherV)) return false;
			return x == otherV.x && z == otherV.z;
		}

		public static (Vector2D,Vector2D) Intersected (Vector2D pos1, Vector2D size1, Vector2D pos2, Vector2D size2)
		/// Returns pos,size rect of intersection of two other pos,sizes
		/// Copy of CoordRect intersection
		{
			Vector2D pos3 = Vector2D.Max(pos1, pos2);
			Vector2D size3 = Vector2D.Min(pos1+size1, pos2+size2) - pos3;
			size3.ClampPositive();
			return (pos3, size3);
		}

		public static bool Intersects (Vector2D pos1, Vector2D size1, Vector2D pos2, Vector2D size2)
		/// Finds if two world rects intersecting
		{
			Vector2D pos = Vector2D.Max(pos1, pos2);
			Vector2D size = Vector2D.Min(pos1+size1, pos2+size2) - pos;

			if (size.x > 0 && size.z > 0)
				return true;
			else
				return false;
		}

		public static bool Contains (Vector2D pos, Vector2D size, Vector2D pos2)
		/// Checks if pos2 within pos-size rect
		{
			return (pos2.x>pos.x && pos2.x<pos.x+size.x &&
					pos2.z>pos.z && pos2.z<pos.z+size.z);
		}


		public override string ToString() { return string.Format("({0:F1}, {1:F1})", x, z); }

		public static Vector2D operator + (Vector2D a, Vector2D b) => new Vector2D(a.x + b.x, a.z + b.z);
		public static Vector2D operator + (Vector2D a, float b) => new Vector2D(a.x + b, a.z + b);
		public static Vector2D operator - (Vector2D a, Vector2D b) => new Vector2D(a.x - b.x, a.z - b.z);
		public static Vector2D operator - (Vector2D a, float b) => new Vector2D(a.x-b, a.z-b);
		public static Vector2D operator * (Vector2D a, Vector2D b) => new Vector2D(a.x * b.x, a.z * b.z);
		public static Vector2D operator / (Vector2D a, Vector2D b) => new Vector2D(a.x / b.x, a.z / b.z);
		public static Vector2D operator - (Vector2D a) => new Vector2D(0f - a.x, 0f - a.z);
		public static Vector2D operator * (Vector2D a, float d) => new Vector2D(a.x * d, a.z * d);
		public static Vector2D operator * (float d, Vector2D a) => new Vector2D(a.x * d, a.z * d);
		public static Vector2D operator / (Vector2D a, float d) => new Vector2D(a.x / d, a.z / d);
		public static Vector2D operator / (float d, Vector2D a) => new Vector2D(d / a.x, d / a.z);

		public static bool operator == (Vector2D lhs, Vector2D rhs)
		{
			float num = lhs.x - rhs.x;
			float num2 = lhs.z - rhs.z;
			return num * num + num2 * num2 < 9.99999944E-11f;
		}
		public static bool operator != (Vector2D lhs, Vector2D rhs) => !(lhs == rhs);

		//explicit conversion is not possible due to unity-native split
		/*public static explicit operator Vector2D(Vector3 v) => new Vector2D(v.x, v.z);
		public static explicit operator Vector3(Vector2D v) => new Vector3(v.x, 0f, v.z);
		public static explicit operator Vector2D(Vector2 v) => new Vector2D(v.x, v.y);
		public static explicit operator Vector2(Vector2D v) => new Vector3(v.x, v.z);*/

		//using getters instead as extension, but they are under unity assembly 
		/*public static Vector2D Vector2D (this Vector3 v) => new Vector2D(v.x, v.z);
		public static Vector3 Vector3 (this Vector2D v) => new Vector3(v.x, 0f, v.z);
		public static Vector2D Vector2D(this Vector2 v) => new Vector2D(v.x, v.y);
		public static Vector2 Vector2(this Vector2D v) => new Vector3(v.x, v.z);
		public static Vector2D Vector2D(this float v) => new Vector2D(v, v);*/

		public static explicit operator Vector2D(float v) => new Vector2D(v, v);

		public Coord RoundToCoord () { return new Coord( (int)(float)(x<0 ? x-1 : x + 0.5f), (int)(float)(z<0 ? z-1 : z + 0.5f) ); }
	
	    public static Vector2D Perpendicular(Vector2D v) => new Vector2D { x = -v.z, z = v.x };
	}
}