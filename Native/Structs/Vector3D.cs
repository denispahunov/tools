using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Runtime.InteropServices;

namespace Den.Tools 
{

	[Serializable]
	[StructLayout(LayoutKind.Sequential)] //to pass to native
	public struct Vector3D : IEquatable<Vector3D>
	//Proper name for this should be Float3
	//but it's named this way in correspondence with Vector2D
	//which should be renamed to VectorXZ
	{
		public float x;
		public float y;
		public float z;

		public const float kEpsilon = 1E-05F;
		public const float kEpsilonNormalSqrt = 1E-15F;

		public float this[int c] { get => c==0 ? x : c==1 ? y : z; set { if (c==0) x=value; else if (c==1) y=value; else z=value; } }

		public Vector3D (float x, float y, float z) { this.x=x; this.y=y; this.z=z; }
		public Vector3D (float v) { this.x=v; this.y=v; this.z=v; }

		public Vector2D Vector2D() => new Vector2D(x, z); //naming this way to prevent usual naming convention between these and Unity vectors

		public static readonly Vector3D zero = new Vector3D(0, 0, 0);
		public static readonly Vector3D one = new Vector3D(1, 1, 1);
		public static readonly Vector3D up = new Vector3D(0, 1, 0);

		public static Vector3D Zero { get; } = new Vector3D(0f, 0f, 0f);
		public static Vector3D One { get; } = new Vector3D(1f, 1f, 1f);
		public static Vector3D PositiveInfinity { get; } = new Vector3D(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);
		public static Vector3D NegativeInfinity { get; } = new Vector3D(float.NegativeInfinity, float.NegativeInfinity, float.NegativeInfinity);

		public float sqrMagnitude => x*x + y*y + z*z;
		public float magnitude => (float)Math.Sqrt((double)(x*x + y*y + z*z));
		public Vector3D normalized
		{
			get
			{
				float m = (float)Math.Sqrt((double)(x*x + y*y + z*z));
				return m>1E-05f ? new Vector3D(x/m, y/m, z/m) : new Vector3D(0, 0, 0);
			}
		}

		public void ClampPositive ()
		{
			if (x<0) x=0;
			if (y<0) y=0;
			if (z<0) z=0;
		}

		public static float Dot (Vector3D lhs, Vector3D rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		}

		public static float Distance (Vector3D a, Vector3D b)
		{
			float num = a.x - b.x;
			float num2 = a.y - b.y;
			float num3 = a.z - b.z;
			return (float)Math.Sqrt((double)(num * num + num2 * num2 + num3 * num3));
		}

		public static Vector3D Lerp (Vector3D a, Vector3D b, float t)
		{
			//if (t>1) t=1; //Vector2D is clamped, but it's up to user to clamp it
			//if (t<0) t=0;
			return new Vector3D(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t);
		}

		public static Vector3D Min (Vector3D lhs, Vector3D rhs)
		{
			return new Vector3D(lhs.x<rhs.x ? lhs.x : rhs.x, lhs.y<rhs.y ? lhs.y : rhs.y, lhs.z<rhs.z ? lhs.z : rhs.z);
		}

		public static Vector3D Max (Vector3D lhs, Vector3D rhs)
		{
			return new Vector3D(lhs.x>rhs.x ? lhs.x : rhs.x, lhs.y>rhs.y ? lhs.y : rhs.y, lhs.z>rhs.z ? lhs.z : rhs.z);
		}

		public void Normalize ()
		{
			float m = (float)Math.Sqrt((double)(x*x + y*y + z*z));
			if (m>1E-05f) { x=x/m; y=y/m; z=z/m; }
			else { x=0; y=0; z=0; }
		}

		public override int GetHashCode () 
		{
			int hash = x.GetHashCode();
			hash = (hash * 397) ^ y.GetHashCode(); //397 goldilocks prime multiplier
			hash = (hash * 397) ^ z.GetHashCode();
			return hash;
		}


		public bool Equals (Vector3D other) { return x == other.x && y == other.y && z == other.z; }
		public override bool Equals (object other)
		{
			if (!(other is Vector3D otherV)) return false;
			return x == otherV.x && y == otherV.y && z == otherV.z;
		}

		public override string ToString () { return string.Format("({0:F1}, {1:F1}, {2:F1})", x, y, z); }

		public static Vector3D operator + (Vector3D a, Vector3D b) => new Vector3D(a.x + b.x, a.y + b.y, a.z + b.z);
		public static Vector3D operator + (Vector3D a, float b) => new Vector3D(a.x + b, a.y + b, a.z + b);
		public static Vector3D operator - (Vector3D a, Vector3D b) => new Vector3D(a.x - b.x, a.y - b.y, a.z - b.z);
		public static Vector3D operator - (Vector3D a, float b) => new Vector3D(a.x-b, a.y-b, a.z-b);
		public static Vector3D operator * (Vector3D a, Vector3D b) => new Vector3D(a.x * b.x, a.y * b.y, a.z * b.z);
		public static Vector3D operator / (Vector3D a, Vector3D b) => new Vector3D(a.x / b.x, a.y / b.y, a.z / b.z);
		public static Vector3D operator - (Vector3D a) => new Vector3D(0f - a.x, 0f - a.y, 0f - a.z);
		public static Vector3D operator * (Vector3D a, float d) => new Vector3D(a.x * d, a.y * d, a.z * d);
		public static Vector3D operator * (float d, Vector3D a) => new Vector3D(a.x * d, a.y * d, a.z * d);
		public static Vector3D operator / (Vector3D a, float d) => new Vector3D(a.x / d, a.y / d, a.z / d);
		public static Vector3D operator / (float d, Vector3D a) => new Vector3D(d / a.x, d / a.y, d / a.z);

		public static bool operator == (Vector3D lhs, Vector3D rhs)
		{
			float num = lhs.x - rhs.x;
			float num2 = lhs.y - rhs.y;
			float num3 = lhs.z - rhs.z;
			return num * num + num2 * num2 + num3 * num3 < 9.99999944E-11f;
		}
		public static bool operator != (Vector3D lhs, Vector3D rhs) => !(lhs == rhs);

		public static explicit operator Vector3D (float v) => new Vector3D(v, v, v);

		public Coord RoundToCoord () { return new Coord((int)(float)(x<0 ? x-1 : x + 0.5f), (int)(float)(z<0 ? z-1 : z + 0.5f)); }

		public static Vector3D Cross (Vector3D lhs, Vector3D rhs)
		{
			return new Vector3D(
				lhs.y * rhs.z - lhs.z * rhs.y,
				lhs.z * rhs.x - lhs.x * rhs.z,
				lhs.x * rhs.y - lhs.y * rhs.x
			);
		}

		public float DistanceToLine (Vector3D lineStart, Vector3D lineEnd)
		/// Just helper fn for optimize, isn't related with beizer
		{
			Vector3D point = this;

			Vector3D lineDir = lineStart-lineEnd;
			float lineLengthSq = lineDir.x*lineDir.x + lineDir.y*lineDir.y + lineDir.z*lineDir.z;
			float lineLength = (float)Math.Sqrt(lineLengthSq);
			float startDistance = (lineStart-point).magnitude;
			float endDistance = (lineEnd-point).magnitude;

			//finding height of triangle 
			float halfPerimeter = (startDistance + endDistance + lineLength) / 2;
			float square = (float)Math.Sqrt( halfPerimeter*(halfPerimeter-endDistance)*(halfPerimeter-startDistance)*(halfPerimeter-lineLength) );
			float height = 2/lineLength * square;

			//dealing with out of line cases
			float distFromStartSq = startDistance*startDistance - height*height;
			float distFromEndSq = endDistance*endDistance - height*height;

			if (distFromStartSq > lineLengthSq && distFromStartSq > distFromEndSq) return endDistance;
			else if (distFromEndSq > lineLengthSq) return startDistance; 
			else return height;
		}
	}
}