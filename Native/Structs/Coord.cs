using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{
	[System.Serializable]
	[StructLayout (LayoutKind.Sequential)] //to pass to native
	public partial struct Coord
	{
		public int x;
		public int z;

		public int this[int c] { get => c==0 ? x : z;  set { if (x==0) x=value; else z=value; } }

		public static bool operator > (Coord c1, Coord c2) { return c1.x>c2.x && c1.z>c2.z; }
		public static bool operator < (Coord c1, Coord c2) { return c1.x<c2.x && c1.z<c2.z; }
		public static bool operator == (Coord c1, Coord c2) { return c1.x==c2.x && c1.z==c2.z; }
		public static bool operator != (Coord c1, Coord c2) { return c1.x!=c2.x || c1.z!=c2.z; }
		public static Coord operator + (Coord c, int s) { return  new Coord(c.x+s, c.z+s); }
		public static Coord operator + (Coord c1, Coord c2) { return  new Coord(c1.x+c2.x, c1.z+c2.z); }
		public static Coord operator - (Coord c) { return  new Coord(-c.x, -c.z); }
		public static Coord operator - (Coord c, int s) { return  new Coord(c.x-s, c.z-s); }
		public static Coord operator - (Coord c1, Coord c2) { return  new Coord(c1.x-c2.x, c1.z-c2.z); }
		public static Coord operator * (Coord c, int s) { return  new Coord(c.x*s, c.z*s); }
		//public static Vector2 operator * (Coord c, Vector2 s) { return  new Vector2(c.x*s.x, c.z*s.y); }
		//public static Vector3 operator * (Coord c, Vector3 s) { return  new Vector3(c.x*s.x, s.y, c.z*s.z); }
		public static Coord operator * (Coord c1, Coord c2) { return  new Coord((int)(c1.x*c2.x), (int)(c1.z*c2.z)); }
		public static Coord operator * (Coord c, float s) { return  new Coord((int)(c.x*s), (int)(c.z*s)); }
		public static Coord operator / (Coord c, int s) { return  new Coord(c.x/s, c.z/s); }
		public static Coord operator / (Coord c, float s) { return  new Coord((int)(c.x/s), (int)(c.z/s)); }

		public override bool Equals(object obj) { if (obj is Coord co) return co.x==x && co.z==z; return false; }
		public override int GetHashCode() {return x*10000000 + z;}

		public int Minimal {get{ return x<z ? x : z; } }
		public int Maximal {get{ return x>z ? x : z; } }
		public int SqrMagnitude {get{ return x*x + z*z; } }
		public float Magnitude {get{ return (float)Math.Sqrt(x*x + z*z); } }

		//public Vector4 vector4 {get{ return new Vector4(x,0,z,0); } }
		//public Vector3 vector3 {get{ return new Vector3(x,0,z); } }
		//public Vector2 vector2 {get{ return new Vector2(x,z); } }
		public Vector2D vector2d {get{ return new Vector2D(x,z); } }
		public Vector3D Vector3D {get{ return new Vector3D(x,0,z); } }

		public static explicit operator Coord(Vector2D v) => new Coord((int)v.x, (int)v.z); //no flooring, handling negative values as casting to int
		public static explicit operator Vector2D(Coord c) => new Vector2D(c.x, c.z);
		//public static explicit operator Vector3(Coord c) => new Vector3(c.x, 0, c.z);
		public static explicit operator Coord(int i) => new Coord(i, i);
		
		public static Coord zero {get{ return new Coord(0,0); }}

		public Coord (int x, int z) { this.x=x; this.z=z; }
		public Coord (int x) { this.x=x; this.z=x; }

		#region Cell Operations

			public static Coord PickCell (int ix, int iz, int cellRes)
			{
				int x = ix/cellRes;
				if (ix<0 && ix!=x*cellRes) x--;

				int z = iz/cellRes;
				if (iz<0 && iz!=z*cellRes) z--;
				
				return new Coord(x,z);
			}

			public static Coord PickCell (Coord c, int cellRes) { return PickCell(c.x, c.z, cellRes); }

			public static Coord PickCellByPos (float fx, float fz, float cellSize=1)
			{
				int x = (int)(fx/cellSize);
				if (fx<0 && fx!=x*cellSize) x--;

				int z = (int)(fz/cellSize);
				if (fz<0 && fz!=z*cellSize) z--;
				
				return new Coord (x,z);
			}

			public static Coord PickCellByPos (float fx, float fz, Vector2D cellSize)
			{
				int x = (int)(fx/cellSize.x);
				if (fx<0) x--;

				int z = (int)(fz/cellSize.z);
				if (fz<0) z--;
				
				return new Coord (x,z);
			}

//			public static Coord PickCellByPos (Vector3 v, float cellSize=1) { return PickCellByPos(v.x, v.z, cellSize); }


		#endregion

		#region Rounding

			//use v/tileSize instead of special tileSize methods
			
/*			public static Coord Floor (Vector3 v)
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)v.x, (int)(float)v.z);
			}
*/
			public static Coord Floor (Vector2D v)
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)v.x, (int)(float)v.z);
			}

			public static Coord Floor (float x, float z)
			{
				if (x<0) x--;  if (z<0) z--;
				return new Coord((int)(float)x, (int)(float)z);
			}

			public static Coord Ceil (Vector2D v)
			/// Ceil is NOT Floor(v+1):
			/// Math.Ceiling(-2f)=-2,  Math.Floor(-2f+1)=-1,  Math.Floor(-2f)+1=-1
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)(v.x + 1f), (int)(float)(v.z + 1f));
			}

			public static Coord Ceil (float x, float z)
			{
				if (x<0) x--;  if (z<0) z--;
				return new Coord((int)(float)(x + 1f), (int)(float)(z + 1f));
			}

/*			public static Coord Round (Vector3 v)
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)(v.x + 0.5f), (int)(float)(v.z + 0.5f));
			}
*/
			public static Coord Round (Vector2D v)
			{
				if (v.x<0) v.x--;  if (v.z<0) v.z--;
				return new Coord((int)(float)(v.x + 0.5f), (int)(float)(v.z + 0.5f));
			}

			public static Coord Round (float x, float z)
			{
				if (x<0) x--;  if (z<0) z--;
				return new Coord((int)(float)(x + 0.5f), (int)(float)(z + 0.5f));
			}



		#endregion

		public void Clamp (int sizeX, int sizeZ)
		{
			if (x > sizeX) x = sizeX;
			if (z > sizeZ) z = sizeZ;
		}
			
		public void ClampPositive ()
			{ x = Math.Max(0,x); z = Math.Max(0,z); }

		public void ClampByRect (CoordRect rect)
		// Closest coordinate within rect
		{ 
			if (x<rect.offset.x) x = rect.offset.x; if (x>=rect.offset.x+rect.size.x) x = rect.offset.x+rect.size.x-1;
			if (z<rect.offset.z) z = rect.offset.z; if (z>=rect.offset.z+rect.size.z) z = rect.offset.z+rect.size.z-1;
		}

        public void ClampByRectInclusive(CoordRect rect)
        // Same but without the -1
        {
            if (x < rect.offset.x) x = rect.offset.x; if (x > rect.offset.x + rect.size.x) x = rect.offset.x + rect.size.x;
            if (z < rect.offset.z) z = rect.offset.z; if (z > rect.offset.z + rect.size.z) z = rect.offset.z + rect.size.z;
        }

        static public Coord Min (Coord c1, Coord c2) 
		{ 
			//return new Coord(Mathf.Min(c1.x,c2.x), Mathf.Min(c1.z,c2.z)); 
			int minX = c1.x<c2.x? c1.x : c2.x;
			int minZ = c1.z<c2.z? c1.z : c2.z;
			return new Coord(minX, minZ);
		}
		static public Coord Max (Coord c1, Coord c2) 
		{ 
			//return new Coord(Mathf.Max(c1.x,c2.x), Mathf.Max(c1.z,c2.z));
			int maxX = c1.x>c2.x? c1.x : c2.x;
			int maxZ = c1.z>c2.z? c1.z : c2.z;
			return new Coord(maxX, maxZ);

		}

		public Coord BaseFloor (int cellSize) //tested
		{
			return new Coord(
				x>=0 ? x/cellSize : (x+1)/cellSize-1,
				z>=0 ? z/cellSize : (z+1)/cellSize-1 );
		}


		public override string ToString()
		{
			return (base.ToString() + " x:" + x + " z:" + z);
		}

		public string ToStringShort() => x + "," + z;


		public static float Distance (Coord c1, Coord c2)
		/// Standard Euclidean distance
		{
			int distX = c1.x - c2.x; //if (distX < 0) distX = -distX; //there should be a reason I've added this. Don't really remember
			int distZ = c1.z - c2.z; //if (distZ < 0) distZ = -distZ;
			return (float)Math.Sqrt(distX*distX + distZ*distZ);
		}

		public static float DistanceSq (Coord c1, Coord c2)
		{
			int distX = c1.x - c2.x; if (distX < 0) distX = -distX;
			int distZ = c1.z - c2.z; if (distZ < 0) distZ = -distZ;
			return distX*distX + distZ*distZ;
		}

		public static int DistanceAxisAligned (Coord c1, Coord c2)
		/// Chebyshev Max distance
		{
			int distX = c1.x - c2.x; if (distX < 0) distX = -distX;
			int distZ = c1.z - c2.z; if (distZ < 0) distZ = -distZ;
			return distX>distZ? distX : distZ;
		}

		public static int DistanceManhattan (Coord c1, Coord c2)
		{
			int distX = c1.x - c2.x; if (distX < 0) distX = -distX;
			int distZ = c1.z - c2.z; if (distZ < 0) distZ = -distZ;
			return distX+distZ;
		}

		//TODO: test
		public static int DistanceAxisAligned (Coord c, CoordRect rect) //NOT manhattan dist. offset and size are instead of UnityEngine.Rect
		{
			//finding x distance
			int distPosX = rect.offset.x - c.x;
			int distNegX = c.x - rect.offset.x - rect.size.x;
			
			int distX;
			if (distPosX >= 0) distX = distPosX;
			else if (distNegX >= 0) distX = distNegX;
			else distX = 0;

			//finding z distance
			int distPosZ = rect.offset.z - c.z;
			int distNegZ = c.z - rect.offset.z - rect.size.z;
			
			int distZ;
			if (distPosZ >= 0) distZ = distPosZ;
			else if (distNegZ >= 0) distZ = distNegZ;
			else distZ = 0;

			//returning the maximum(!) distance 
			if (distX > distZ) return distX;
			else return distZ;
		}

		public static float DistanceAxisPriority (Coord c1, Coord c2)
		/// Whole number is an axis (Chebyshev) max dist, and remains is a remoteness from axis. Useful to set priority
		{
			int distX = c1.x - c2.x; if (distX < 0) distX = -distX;
			int distZ = c1.z - c2.z; if (distZ < 0) distZ = -distZ;

			int max = distX>distZ? distX : distZ;
			int min = distX<distZ? distX : distZ;

			return max + 1f*min/(max+1);
		}


		public IEnumerable<Coord> DistanceStep (int i, int dist) //4+4 terrains, no need to use separetely
		{
			yield return new Coord(x-i, z-dist);
			yield return new Coord(x-dist, z+i);
			yield return new Coord(x+i, z+dist);
			yield return new Coord(x+dist, z-i);

			yield return new Coord(x+i+1, z-dist);
			yield return new Coord(x-dist, z-i-1);
			yield return new Coord(x-i-1, z+dist);
			yield return new Coord(x+dist, z+i+1);
		}

		public IEnumerable<Coord> DistancePerimeter (int dist) //a circular square border sorted by distance
		{
			for (int i=0; i<dist; i++)
				foreach (Coord c in DistanceStep(i,dist)) yield return c;
		}

		public IEnumerable<Coord> DistanceArea (int maxDist)
		{
			yield return this;
			for (int i=0; i<maxDist; i++)
				foreach (Coord c in DistancePerimeter(i)) yield return c;
		}

		public IEnumerable<Coord> DistanceArea (CoordRect rect) //same as distance are, but clamped by rect
		{
			int maxDist = Math.Max( Math.Max(x-rect.offset.x, rect.Max.x-x), Math.Max(z-rect.offset.z, rect.Max.z-z) ) + 1;

			if (rect.Contains(this)) yield return this;
			for (int i=0; i<maxDist; i++)
				foreach (Coord c in DistancePerimeter(i)) 
					if (rect.Contains(c)) yield return c;
		}

		public static IEnumerable<Coord> MultiDistanceArea (Coord[] coords, int maxDist)
		{
			if (coords.Length==0) yield break;

			for (int c=0; c<coords.Length; c++) yield return coords[c];
			
			for (int dist=0; dist<maxDist; dist++)
				for (int i=0; i<dist; i++)
					for (int c=0; c<coords.Length; c++)
						foreach (Coord c2 in coords[c].DistanceStep(i,dist)) yield return c2;
		}

//		public Vector3 ToVector3 (float cellSize) { return new Vector3(x*cellSize, 0, z*cellSize); }
//		public Vector2 ToVector2 (float cellSize) { return new Vector2(x*cellSize, z*cellSize); }
//		public Rect ToRect (float cellSize) { return new Rect(x*cellSize, z*cellSize, cellSize, cellSize); }
		public CoordRect ToCoordRect (int cellSize) { return new CoordRect(x*cellSize, z*cellSize, cellSize, cellSize); }

		public float GetFalloff (Vector2D center, float radius, float hardness, int smooth=1)
		/// Gets current pixel's falloff percent for stamps
		/// Smooth is iterational
		{
			float distSq = (x-center.x)*(x-center.x) + (z-center.z)*(z-center.z);
			if (distSq > radius*radius) return 0;
			if (distSq < radius*hardness * radius*hardness) return 1;

			float dist = (float)Math.Sqrt(distSq);

			float hardRadius = radius*hardness;
			float fallof = 1 - (dist - hardRadius) / (radius - hardRadius);  //remaining dist / transition, inversed

			for (int s=0; s<smooth; s++)
				fallof = 3*fallof*fallof - 2*fallof*fallof*fallof;
			
			return fallof;
		}

		public float GetInterpolatedPercent (Vector2D pos)
		/// Gets influence of the pos on current coord
		/// Used instead of GetFallof when brush size is lower than one pixel
		{
			float xp = pos.x - x;  if (xp<0) xp = -xp; if (xp>1) return 0;
			float zp = pos.z - z;  if (zp<0) zp = -zp; if (zp>1) return 0;
			return (1-xp)*(1-zp);
		}

		public float GetInterpolatedFalloff (Vector2D center, float radius, float hardness, int smooth=1)
		/// Automatically switches between GetFalloff and GetInterpolatedPercent to get a neat right falloff of this coord
		{
			if (radius > 2) 
				return GetFalloff(center, radius, hardness, smooth);

			else if (radius > 1) return
				GetFalloff(center, radius, hardness, smooth) * ((radius-1)) +
				GetInterpolatedPercent(center) * (2-radius);

			else 
				return GetInterpolatedPercent(center) * radius;
		}

		//serialization
		public string Encode () { return "x=" + x + " z=" + z; }
		public void Decode (string[] lineMembers) { x = int.Parse(lineMembers[2]); z = int.Parse(lineMembers[3]); }
	}
	
}