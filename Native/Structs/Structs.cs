﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{

	/*[System.Serializable]
	public struct PosRect
	{
		public Vector3 offset;
		public Vector3 size;


	}*/




	[System.Serializable]
	[StructLayout(LayoutKind.Explicit)]
	public struct StructArray 
	{ 
		[FieldOffset(0)] public byte b0;  
		[FieldOffset(1)] public byte b1;  
		[FieldOffset(2)] public byte b2;  
		[FieldOffset(3)] public byte b3;  
		[FieldOffset(4)] public byte b4;  
		[FieldOffset(5)] public byte b5;  
		[FieldOffset(6)] public byte b6;  
		[FieldOffset(7)] public byte b7; 
		[FieldOffset(0)] private long l;

		public const int length = 8;
			
		public byte this[int n]
		{
			get { return (byte)((l>>n*8) & 0b_1111_1111); }
			set 
			{ 
				l &= ~(((long)0b_1111_1111) << n*8); //erasing previous val
				l |= ((long)value)<<n*8;  //writing new one
			}
		}

		public float GetFloat (int n)
		{
			long val = (l>>n*8) & 0b_1111_1111;
			return val / 255f;
		}

		public void SetFloat (int n, float f)
		{
			long val = (int)(f*255);
			l &= ~(((long)0b_1111_1111) << n*8);
			l |= val<<n*8;
		}
	}



	public struct StructArrayExtended<T>
	///Stores first 4 T as a struct, and others as ref array
	{ 
		public T i0;  
		public T i1;  
		public T i2;  
		public T i3;  

		public int count;  
		private T[] others;


		public StructArrayExtended (int capacity)
		{
			i0=default; i1=default; i2=default; i3=default;
			others = null;
			count=0;
			Capacity = capacity;
		}


		public T this[int n]
		{
			get 
			{ 
				switch (n)
				{
					case 0: return i0;
					case 1: return i1;
					case 2: return i2;
					case 3: return i3;
					default: return others[n-4];
				}
			}
			set 
			{ 
				switch (n)
				{
					case 0: i0 = value; break;
					case 1: i1 = value; break;
					case 2: i2 = value; break;
					case 3: i3 = value; break;
					default: others[n-4] = value; break;
				}
			}
		}


		public int Capacity
		{
			get{ return 4 + (others!=null ? others.Length : 0); }
		
			set{
				if (value <= 4)
				{
					if (others!=null)
						others = null;
				}

				else
				{
					if (others==null) others = new T[value-4];
					else ArrayTools.Resize(ref others, value-4);
				}
			}
		}


		public void Add (T item)
		{	
			switch (count)
			{
				case 0: i0 = item; break;
				case 1: i1 = item; break;
				case 2: i2 = item; break;
				case 3: i3 = item; break;
				default:
					if (others == null) others = new T[4];
					if (others.Length <= count-4) ArrayTools.Resize(ref others, others.Length*2);
					others[count-4] = item;
					break;
			}
			count++;
		}


		public void AddRange (StructArrayExtended<T> other)
		{
			for (int i=0; i<other.count; i++)
				this.Add(other[i]);
		}


		public int FindIndex (T item)
		{
			if (i0.Equals(item)) return 0;
			if (i1.Equals(item)) return 1;
			if (i2.Equals(item)) return 2;
			if (i3.Equals(item)) return 3;

			if (others != null)
				for (int i=0; i<others.Length; i++)
					if (others[i].Equals(item)) return i+4;

			return -1;
		}
	}


	[System.Serializable]
	[StructLayout(LayoutKind.Explicit)]
	public struct SemVer
	{
		public enum PRT { Release=4, RC=3, Beta=2, Alpha=1 };

		[FieldOffset(0)] public byte major;
		[FieldOffset(1)] public byte minor;
		[FieldOffset(2)] public byte prt;
		[FieldOffset(3)] public byte patch;

		[FieldOffset(0)] private int hash; //could be negative
		[FieldOffset(0)] private uint summary;
		

		public SemVer (byte major, byte minor, PRT prt, byte patch)
		{
			this.hash = 0;
			this.summary = 0;

			this.major = major;
			this.minor = minor;
			this.prt = (byte)prt;
			this.patch = patch;
		}

		public SemVer (byte major, byte minor, byte patch) : this (major, minor, PRT.Release, patch) { }

		public string PRTtoString 
		{get{
			switch (prt)
			{
				default: return "";
				case 3: return "RC";
				case 2: return "B";
				case 1: return "A";
			}
		}}

		public override string ToString () => $"{major}.{minor}.{PRTtoString}{patch}";
		public static explicit operator int (SemVer version) => version.major*10000 + version.minor*100 + version.patch;

		public bool Equals (SemVer obj) => summary == obj.summary;
		public override bool Equals (object obj) => summary == ((SemVer)obj).summary;
		public override int GetHashCode () => hash;

		public static bool operator == (SemVer v1, SemVer v2) => v1.summary == v2.summary;
		public static bool operator != (SemVer v1, SemVer v2) => v1.summary != v2.summary;
		public static bool operator < (SemVer v1, SemVer v2) => v1.summary < v2.summary;
		public static bool operator > (SemVer v1, SemVer v2) => v1.summary > v2.summary;
		public static bool operator <= (SemVer v1, SemVer v2) => v1.summary <= v2.summary;
		public static bool operator >= (SemVer v1, SemVer v2) => v1.summary >= v2.summary;
	}
}