﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{
	[System.Serializable]
	[StructLayout (LayoutKind.Sequential)] //to pass to native
	public struct CoordRect : IEnumerable<Coord>
	{
		public Coord offset;
		public Coord size;

		public enum TileMode { Clamp=0, Tile=1, PingPong=2 } //see Tile region

		//public int radius; //not related with size, because a clamped CoordRect should have non-changed radius

		public CoordRect (Coord offset, Coord size) { this.offset = offset; this.size = size; }
		public CoordRect (int offsetX, int offsetZ, int sizeX, int sizeZ) { this.offset = new Coord(offsetX,offsetZ); this.size = new Coord(sizeX,sizeZ);  }
		public CoordRect (float offsetX, float offsetZ, float sizeX, float sizeZ) { this.offset = new Coord((int)offsetX,(int)offsetZ); this.size = new Coord((int)sizeX,(int)sizeZ);  }
//		public CoordRect (Rect r) { offset = new Coord((int)r.x, (int)r.y); size = new Coord((int)r.width, (int)r.height); }
		public CoordRect (Coord center, int radius) { this.offset = center-radius; this.size = new Coord(radius*2,radius*2); }
		public CoordRect (Vector2D center, float radius) 
		{ 
			Coord centerCoord = Coord.Round(center);
			int radiusInt = (int)(radius+1);
			this.offset = centerCoord-radiusInt;
			this.size = new Coord(radiusInt + 1 + radiusInt); //1 for the dimensions of the centerCoord tile
		}

		public Coord Max { get { return offset+size; } set { size = value-offset; } }
		public int MaxX { get { return offset.x+size.x; } set { size.x = value-offset.x; } }
		public int MaxZ { get { return offset.z+size.z; } set { size.z = value-offset.z; } }
		public Coord Min { get { return offset; } set { offset = value; } }
		public Coord Center { get { return offset + size/2; } } 
//		public Vector3 CenterVector3 { get { return new Vector3(offset.x + size.x/2f, 0, offset.z + size.z/2f); } } 
		public int Count { get {return size.x*size.z;} }


		public override bool Equals(object obj) { return base.Equals(obj); }
		public override int GetHashCode() {return offset.x*100000000 + offset.z*1000000 + size.x*1000+size.z;}

		public int GetPos (Coord c) { return (c.z-offset.z)*size.x + c.x - offset.x; }
		public int GetPos (int x, int z) { return (z-offset.z)*size.x + x - offset.x; }

		public Coord GetCoord (int pos)
		{
			int z = pos/size.x + offset.z;
			int x = pos - (z-offset.z)*size.x + offset.x;
			return new Coord(x,z);
		}

		public static bool operator > (CoordRect c1, CoordRect c2) { return c1.size>c2.size; }
		public static bool operator < (CoordRect c1, CoordRect c2) { return c1.size<c2.size; }
		public static bool operator == (CoordRect c1, CoordRect c2) { return c1.offset==c2.offset && c1.size==c2.size; }
		public static bool operator != (CoordRect c1, CoordRect c2) { return c1.offset!=c2.offset || c1.size!=c2.size; }
		public static CoordRect operator * (CoordRect c, int s) { return  new CoordRect(c.offset*s, c.size*s); }
		public static CoordRect operator * (CoordRect c, float s) { return  new CoordRect(c.offset*s, c.size*s); }
		public static CoordRect operator / (CoordRect c, int s) { return  new CoordRect(c.offset/s, c.size/s); }

//		public Vector4 vector4 {get{ return new Vector4(offset.x,offset.z,size.x,size.z); } }

//		public static explicit operator CoordRect(Vector4 vec) => new CoordRect((int)vec.x, (int)vec.y, (int)vec.z, (int)vec.w);
//		public static explicit operator Vector4(CoordRect cr) => new Vector4(cr.offset.x, cr.offset.z, cr.size.x, cr.size.z);
//		public static explicit operator CoordRect(Rect r) => new CoordRect((int)r.x, (int)r.y, (int)r.width, (int)r.height);
//		public static explicit operator Rect(CoordRect cr) => new Rect(cr.offset.x, cr.offset.z, cr.size.x, cr.size.z);

		public IEnumerator<Coord> GetEnumerator()
		{
			Coord min = offset;
			Coord max = offset+size;
			for (int x = min.x; x < max.x; x++)
				for (int z = min.z; z < max.z; z++)
					yield return new Coord(x,z);
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			Coord min = offset;
			Coord max = offset+size;
			for (int x = min.x; x < max.x; x++)
				for (int z = min.z; z < max.z; z++)
					yield return new Coord(x,z);
		}

		public void Expand (int v) { offset.x-=v; offset.z-=v; size.x+=v*2; size.z+=v*2; }
		public CoordRect Expanded (int v) { return new CoordRect(offset.x-v, offset.z-v, size.x+v*2, size.z+v*2); }
		public void Contract (int v) { offset.x+=v; offset.z+=v; size.x-=v*2; size.z-=v*2; }
		public CoordRect Contracted (int v) { return new CoordRect(offset.x+v, offset.z+v, size.x-v*2, size.z-v*2); }

		public void Clamp (Coord min, Coord max)
		{
			Coord oldMax = Max;
			offset = Coord.Max(min, offset);
			size = Coord.Min(max-offset, oldMax-offset);
			size.ClampPositive();
		}

		/* //did it ever worked at all?
		public void ClampLine (ref Coord from, ref Coord to)
		/// changes from and to so that they are within rect
		{
			if (this.Contains(from) && this.Contains(to)) return;

			Vector2 dir = (from-to).vector2.normalized;
		}*/

		public static CoordRect Intersected (CoordRect c1, CoordRect c2) 
		{ 
			c1.Clamp(c2.Min, c2.Max); 
			return c1; 
		}

		public static CoordRect Intersected (CoordRect c1, CoordRect c2, CoordRect c3) 
		/// Finds the minimum intersection of 3 rects
		{ 
			c1.Clamp(c2.Min, c2.Max); 
			c1.Clamp(c3.Min, c3.Max);
			return c1; 
		}

		public static bool IsIntersecting (CoordRect c1, CoordRect c2) 
		{ 
			if (c2.Contains(c1.offset.x, c1.offset.z) || c2.Contains(c1.offset.x+c1.size.x, c1.offset.z) || c2.Contains(c1.offset.x, c1.offset.z+c1.size.z) || c2.Contains(c1.offset.x+c1.size.x, c1.offset.z+c1.size.z)) return true;
			if (c1.Contains(c2.offset.x, c2.offset.z) || c1.Contains(c2.offset.x+c2.size.x, c2.offset.z) || c1.Contains(c2.offset.x, c2.offset.z+c1.size.z) || c1.Contains(c2.offset.x+c2.size.x, c2.offset.z+c2.size.z)) return true;

			return false;
		}

		public static CoordRect Combined (CoordRect rect1, CoordRect rect2)
		{
			Coord min = Coord.Min(rect1.offset, rect2.offset);
			Coord max = Coord.Max(rect1.Max, rect2.Max);
			return new CoordRect(min, max-min);
		}

		public static CoordRect Combined (CoordRect[] rects)
		{
			Coord min=new Coord(2000000000, 2000000000); Coord max=new Coord(-2000000000, -2000000000); 
			for (int i=0; i<rects.Length; i++)
			{
				if (rects[i].offset.x < min.x) min.x = rects[i].offset.x;
				if (rects[i].offset.z < min.z) min.z = rects[i].offset.z;
				if (rects[i].offset.x + rects[i].size.x > max.x) max.x = rects[i].offset.x + rects[i].size.x;
				if (rects[i].offset.z + rects[i].size.z > max.z) max.z = rects[i].offset.z + rects[i].size.z;
			}
			return new CoordRect(min, max-min);
		}

		public void Encapsulate (Coord coord)
		/// Resizes this rect so that coord is included
		{
			if (coord.x < offset.x) { size.x += offset.x-coord.x; offset.x = coord.x; }
			if (coord.x > offset.x+size.x) { size.x = coord.x-offset.x; }

			if (coord.z < offset.z) { size.z += offset.z-coord.z; offset.z = coord.z; }
			if (coord.z > offset.z+size.z) { size.z = coord.z-offset.z; }
		}


		public static CoordRect WorldToPixel (Vector2D worldPos, Vector2D worldSize, Vector2D pixelSize, bool inclusive=true)
		/// Converts world rect to 0-aligned pixel/grid rect
		/// Can convert to pixels and cells as well
		{
			if (pixelSize.x == 0  ||  pixelSize.z == 0) 
				throw new Exception("Cell size is zero");

			//From Unity source:
			//public static int CeilToInt(float f) { return (int)Math.Ceiling(f); }
			//public static int FloorToInt(float f) { return (int)Math.Floor(f); }
			//public static int RoundToInt(float f) { return (int)Math.Round(f); }

			Coord min; Coord max;
			if (inclusive)
			{
				min = new Coord(
					(int)Math.Floor(worldPos.x/pixelSize.x), //Mathf.FloorToInt(worldPos.x/pixelSize.x),
					(int)Math.Floor(worldPos.z/pixelSize.z) );
				max = new Coord(
					(int)Math.Ceiling((worldPos.x+worldSize.x)/pixelSize.x),
					(int)Math.Ceiling((worldPos.z+worldSize.z)/pixelSize.z) );
			}
			else
			{
				min = new Coord(
					(int)Math.Ceiling(worldPos.x/pixelSize.x),
					(int)Math.Ceiling(worldPos.z/pixelSize.z) );
				max = new Coord(
					(int)Math.Floor((worldPos.x+worldSize.x)/pixelSize.x),
					(int)Math.Floor((worldPos.z+worldSize.z)/pixelSize.z) );
			}
			
			return new CoordRect(min, max-min);
		}



		#region Contains

		public bool Contains (Coord coord)
			/// Checking if coord is within coordrect
			{ 
				return (coord.x >= offset.x && coord.x < offset.x + size.x && 
						coord.z >= offset.z && coord.z < offset.z + size.z); 
			}

			public bool Contains (int x, int z)
			{ 
				return (x- offset.x >= 0 && x- offset.x < size.x && 
						z- offset.z >= 0 && z- offset.z < size.z); 
			}

			public bool Contains (float x, float z)
			{
				return (x- offset.x >= 0 && x- offset.x < size.x && 
					z- offset.z >= 0 && z- offset.z < size.z); 
			}

/*			public bool Contains (Vector2 pos)
			{ 
				return (pos.x- offset.x >= 0 && pos.x- offset.x < size.x && 
						pos.y- offset.z >= 0 && pos.y- offset.z < size.z); 
			}
*/
/*			public bool Contains (Vector3 pos)
			{ 
				return (pos.x- offset.x >= 0 && pos.x- offset.x < size.x && 
						pos.z- offset.z >= 0 && pos.z- offset.z < size.z); 
			}
*/
			public bool Contains (float x, float z, float margins)
			/// Contracts the coordrect by margins and checks contains
			{
				return (x- offset.x >= margins && x- offset.x < size.x-margins &&
						z- offset.z >= margins && z- offset.z < size.z-margins);
			}

			public bool Contains (Coord coord, float margins)
			/// Contracts the coordrect by margins and checks contains
			{
				return (coord.x- offset.x >= margins && coord.x- offset.x < size.x-margins &&
						coord.z- offset.z >= margins && coord.z- offset.z < size.z-margins);
			}

			public bool Contains (CoordRect r) //tested
			{
				return  r.offset.x >= offset.x && r.offset.x+r.size.x <= offset.x+size.x &&
						r.offset.z >= offset.z && r.offset.z+r.size.z <= offset.z+size.z;
			}

			public bool ContainsOrIntersects (CoordRect r) //tested
			{
				return  r.offset.x > offset.x-r.size.x && r.offset.x+r.size.x < offset.x+size.x+r.size.x &&
						r.offset.z > offset.z-r.size.z && r.offset.z+r.size.z < offset.z+size.z+r.size.z;
			}

		#endregion


		#region Tiling

			public Coord Tile (Coord coord, TileMode tileMode)
			/// Returns the corresponding coord within the matrix rect
			{
				//transferring to zero-based coord
				coord.x -= offset.x;
				coord.z -= offset.z;

				switch (tileMode)
				{
					//case TileMode.Once:
					//	if (coord.x < 0 || coord.x >= size.x) coord.x = -1;
					//	if (coord.z < 0 || coord.z >= size.z) coord.z = -1;
					//	break;

					case TileMode.Clamp:
						if (coord.x < 0) coord.x = 0; 
						if (coord.x >= size.x) coord.x = size.x - 1;
						if (coord.z < 0) coord.z = 0; 
						if (coord.z >= size.z) coord.z = size.z - 1;
						break;

					case TileMode.Tile:
						coord.x = coord.x % size.x; 
						if (coord.x < 0) coord.x= size.x + coord.x;
						coord.z = coord.z % size.z; 
						if (coord.z < 0) coord.z= size.z + coord.z;
						break;

					case TileMode.PingPong:
						coord.x = coord.x % (size.x*2); 
						if (coord.x < 0) coord.x = size.x*2 + coord.x; 
						if (coord.x >= size.x) coord.x = size.x*2 - coord.x - 1;

						coord.z = coord.z % (size.z*2); 
						if (coord.z<0) coord.z=size.z*2 + coord.z; 
						if (coord.z>=size.z) coord.z = size.z*2 - coord.z - 1;
						break;
				}
				

				coord.x += offset.x;
				coord.z += offset.z;

				return coord;
			}


			public void Tile (ref Coord coord, TileMode tileMode)
			/// Returns the corresponding coord within the matrix rect
			{
				//transferring to zero-based coord
				coord.x -= offset.x;
				coord.z -= offset.z;

				switch (tileMode)
				{
					//case TileMode.Once:
					//	if (coord.x < 0 || coord.x >= size.x) coord.x = -1;
					//	if (coord.z < 0 || coord.z >= size.z) coord.z = -1;
					//	break;

					case TileMode.Clamp:
						if (coord.x < 0) coord.x = 0; 
						if (coord.x >= size.x) coord.x = size.x - 1;
						if (coord.z < 0) coord.z = 0; 
						if (coord.z >= size.z) coord.z = size.z - 1;
						break;

					case TileMode.Tile:
						coord.x = coord.x % size.x; 
						if (coord.x < 0) coord.x= size.x + coord.x;
						coord.z = coord.z % size.z; 
						if (coord.z < 0) coord.z= size.z + coord.z;
						break;

					case TileMode.PingPong:
						coord.x = coord.x % (size.x*2); 
						if (coord.x < 0) coord.x = size.x*2 + coord.x; 
						if (coord.x >= size.x) coord.x = size.x*2 - coord.x - 1;

						coord.z = coord.z % (size.z*2); 
						if (coord.z<0) coord.z=size.z*2 + coord.z; 
						if (coord.z>=size.z) coord.z = size.z*2 - coord.z - 1;
						break;
				}
				

				coord.x += offset.x;
				coord.z += offset.z;
			}



			/*public Vector2 Tile (Vector2 vec, TileMode tileMode)
			/// Returns the corresponding coord within the matrix rect
			{
				switch (tileMode)
				{
					case TileMode.Clamp | TileMode.Once: return TileClamp(vec);
					case TileMode.Tile: return TileRepeat(vec);
					case TileMode.PingPong: return TilePingPong(vec);
					default: return vec;
				}
			}

			private Coord TileClamp (Coord coord)
			{
				if (coord.x < offset.x) coord.x = offset.x; 
				if (coord.x >= offset.x + size.x) coord.x = offset.x + size.x - 1;

				if (coord.z < offset.z) coord.z = offset.z; 
				if (coord.z >= offset.z + size.z) coord.z = offset.z + size.z - 1;

				return coord;
			}

			private Vector2 TileClamp (Vector2 vec)
			{
				if (vec.x < offset.x) vec.x = offset.x; 
				if (vec.x >= offset.x + size.x) vec.x = offset.x + size.x - 1;

				if (vec.y < offset.z) vec.y = offset.z; 
				if (vec.y >= offset.z + size.z) vec.y = offset.z + size.z - 1;

				return vec;
			}

			private Coord TileRepeat (Coord coord)
			{
				coord.x -= offset.x;
				coord.x = coord.x % size.x; 
				if (coord.x < 0) coord.x= size.x + coord.x;
				coord.x += offset.x;

				coord.z -= offset.z;
				coord.z = coord.z % size.z; 
				if (coord.z < 0) coord.z= size.z + coord.z;
				coord.z += offset.z;

				return coord;
			}

			private Vector2 TileRepeat (Vector2 vec)
			{
				vec.x -= offset.x;
				vec.x = vec.x % size.x; 
				if (vec.x < 0) vec.x= size.x + vec.x;
				vec.x += offset.x;

				vec.y -= offset.z;
				vec.y = vec.y % size.z; 
				if (vec.y < 0) vec.y= size.z + vec.y;
				vec.y += offset.z;

				return vec;
			}

			private Coord TilePingPong (Coord coord)
			{
				coord.x -= offset.x;
				coord.x = coord.x % (size.x*2); 
				if (coord.x < 0) coord.x = size.x*2 + coord.x; 
				if (coord.x >= size.x) coord.x = size.x*2 - coord.x - 1;
				coord.x += offset.x;

				coord.z -= offset.z;
				coord.z = coord.z % (size.z*2); 
				if (coord.z<0) coord.z=size.z*2 + coord.z; 
				if (coord.z>=size.z) coord.z = size.z*2 - coord.z - 1;
				coord.z += offset.z;

				return coord + offset;
			}

			private Vector2 TilePingPong (Vector2 vec)
			{
				vec.x -= offset.x;
				vec.x = vec.x % (size.x*2); 
				if (vec.x < 0) vec.x = size.x*2 + vec.x; 
				if (vec.x >= size.x) vec.x = size.x*2 - vec.x - 1;
				vec.x += offset.x;

				vec.y -= offset.z;
				vec.y = vec.y % (size.z*2); 
				if (vec.y<0) vec.y=size.z*2 + vec.y; 
				if (vec.y>=size.z) vec.y = size.z*2 - vec.y - 1;
				vec.y += offset.z;

				return vec;
			}*/

		#endregion

		public override string ToString()
		{
			return (base.ToString() + ": offsetX:" + offset.x + " offsetZ:" + offset.z + " sizeX:" + size.x + " sizeZ:" + size.z);
		}

/*		public void DrawGizmo ()
		{
			#if UNITY_EDITOR
			Vector3 s = size.ToVector3(1);
			Vector3 o = offset.ToVector3(1);
			Gizmos.DrawWireCube(o + s/2, s);
			#endif
		}
*/

	}


}