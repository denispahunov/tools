using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools 
{
	[System.Serializable]
	[StructLayout (LayoutKind.Sequential)] //to pass to native
	public partial struct Coord3D
	{
		public int x;
		public int y;
		public int z;

		public static bool operator > (Coord3D c1, Coord3D c2) { return c1.x>c2.x && c1.y>c2.y && c1.z>c2.z; }
		public static bool operator < (Coord3D c1, Coord3D c2) { return c1.x<c2.x && c1.y<c2.y && c1.z<c2.z; }
		public static bool operator == (Coord3D c1, Coord3D c2) { return c1.x==c2.x && c1.y==c2.y && c1.z==c2.z; }
		public static bool operator != (Coord3D c1, Coord3D c2) { return c1.x!=c2.x || c1.y!=c2.y || c1.z!=c2.z; }
		public static Coord3D operator + (Coord3D c, int s) { return  new Coord3D(c.x+s, c.y+s, c.z+s); }
		public static Coord3D operator + (Coord3D c1, Coord3D c2) { return  new Coord3D(c1.x+c2.x, c1.y+c2.y, c1.z+c2.z); }
		public static Coord3D operator - (Coord3D c) { return  new Coord3D(-c.x, -c.y, -c.z); }
		public static Coord3D operator - (Coord3D c, int s) { return  new Coord3D(c.x-s, c.y-s, c.z-s); }
		public static Coord3D operator - (Coord3D c1, Coord3D c2) { return  new Coord3D(c1.x-c2.x, c1.y-c2.y, c1.z-c2.z); }
		public static Coord3D operator * (Coord3D c, int s) { return  new Coord3D(c.x*s, c.y*s, c.z*s); }
//		public static Vector3 operator * (Coord3D c, Vector3 s) { return  new Vector3(c.x*s.x, c.y*s.y, c.z*s.z); }
		public static Coord3D operator * (Coord3D c1, Coord3D c2) { return  new Coord3D(c1.x*c2.x, c1.y*c2.y, c1.z*c2.z); }
		public static Coord3D operator * (Coord3D c, float s) { return  new Coord3D((int)(c.x*s), (int)(c.y*s), (int)(c.z*s)); }
		public static Coord3D operator / (Coord3D c, int s) { return  new Coord3D(c.x/s, c.y/s, c.z/s); }
		public static Coord3D operator / (Coord3D c, float s) { return new Coord3D((int)(c.x/s), (int)(c.y/s), (int)(c.z/s)); }

		public static readonly Coord3D up = new Coord3D(0,1,0); //TODO: could be changed externally
		public static readonly Coord3D down = new Coord3D(0,-1,0);
		public static readonly Coord3D front = new Coord3D(0,0,1);
		public static readonly Coord3D back = new Coord3D(0,0,-1);
		public static readonly Coord3D left = new Coord3D(-1,0,0);
		public static readonly Coord3D right = new Coord3D(1,0,0);

		public override bool Equals(object obj) { if (obj is Coord3D co) return co.x==x && co.y==y && co.z==z; return false; }
		public override int GetHashCode() {return x*1000000 + y*1000 + z;}

		public int Minimal {get{ int xz = x<z ? x : z; return xz<y ? xz : y; } }
		public int Maximal {get{ int xz = x>z ? x : z; return xz>y ? xz : y; } }
		public int SqrMagnitude {get{ return x*x + y*y + z*z; } }
		public float Magnitude {get{ return (float)Math.Sqrt(x*x + y*y + z*z); } }

//		public static explicit operator Coord3D(Vector3 v) => new Coord3D((int)v.x, (int)v.y, (int)v.z); //no flooring, handling negative values as casting to int
//		public static explicit operator Vector3(Coord3D c) => new Vector3(c.x, c.y, c.z);
		
		public static Coord3D zero {get{ return new Coord3D(0,0,0); }}

		public Coord3D (int x, int y, int z) { this.x=x; this.y=y; this.z=z; }

		public override string ToString() => $"{base.ToString()} x:{x}, y{y}, z:{z}";
	}
}