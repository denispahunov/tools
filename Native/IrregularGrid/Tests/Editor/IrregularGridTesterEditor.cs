using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
//using static Den.Tools.IrregularGridMesh;

using Den.Tools.Tests;
using Den.Tools.GUI;

namespace Den.Tools.Tests
{
	[CustomEditor(typeof(IrregularGridTester))]
	public class IrregularGridTesterEditor : Editor
	{
		int selectedVert = -1;

		private void OnSceneGUI () 
		{
		HandleUtility.AddDefaultControl( GUIUtility.GetControlID(FocusType.Passive) );

			IrregularGridTester tester = target as IrregularGridTester;
			IrregularGridMesh mesh = tester.mesh;

			if (mesh != null)
			{
				mesh.RefreshQuality();

				DrawMesh(mesh);
				EditMesh(mesh, tester);
			}
		}

		private void DrawMesh (IrregularGridMesh mesh, Color color=new Color())
		{
			if (color.a < 0.001f)
				color = Handles.elementColor;
			Color faceColor = color;
			faceColor.a *= 0.25f;

			Color oldColor = Handles.color;
			Handles.color = color;

			//verts
			if (mesh.verts != null)
				for (int v=0; v<mesh.verts.Length; v++)
				{
					Vector3 pos = mesh.verts[v].Vector3();
					float size = HandleUtility.GetHandleSize(pos);

					Handles.Label(pos, "  "+ v.ToString());
					Handles.DotHandleCap(0, pos, Quaternion.identity, size*0.05f, EventType.Repaint);
				}

			//edges faces
			if (mesh.faces != null)
			{
				Vector2D[] faceVerts2 = new Vector2D[4];
				Vector3[] faceVerts3 = new Vector3[4];
				Vector3[] triVerts3 = new Vector3[3];
				Vector3[] edgeVerts3 = new Vector3[5]; //same as face verts, but closed

				for (int f=0; f<mesh.faces.Length; f++)
				{
					mesh.FaceVertPositions(f, faceVerts2);
					ArrayTools.Convert(faceVerts2, faceVerts3, v=>v.Vector3());

					bool isTriangle = mesh.faces[f].IsTriangle;
					if (isTriangle)
						Array.Copy(faceVerts3, triVerts3, 3);

					//number
					Vector3 center = Vector3.zero;
					for (int i=0; i<(isTriangle?3:4); i++)
						center += faceVerts3[i];
					center /= isTriangle?3:4;
					//Handles.Label(center, f.ToString());
					//if (mesh.quality != null)
					//	Handles.Label(center, mesh.quality[f].ToString());

					//edge
					Handles.color = color;
					Array.Copy(faceVerts3, edgeVerts3, 4);
					edgeVerts3[4] = faceVerts3[0]; //closing
					Handles.DrawPolyLine(edgeVerts3);

					//face
					Handles.color = faceColor;

					if (isTriangle)
						Handles.DrawAAConvexPolygon(triVerts3);
					else
						Handles.DrawAAConvexPolygon(faceVerts3);
				}
			}

			Handles.color = oldColor;
		}


		private void EditMesh (IrregularGridMesh mesh, IrregularGridTester tester)
		{
			if (mesh.verts == null)
				return;

			//selecting
			if (Event.current.isMouse && Event.current.button==0)
			{
				if (Event.current.type==EventType.MouseDown)
					for (int v=0; v<mesh.verts.Length; v++)
					{
						Vector3 pos = mesh.verts[v].Vector3();
						if (HandleUtility.DistanceToCircle(pos, 0) < 10)
							selectedVert = v;
					}

				//preserving selection

			}

			//disabling selection
			if (selectedVert >= 0)
				HandleUtility.AddDefaultControl( GUIUtility.GetControlID(FocusType.Passive) );

			//moving
			if (selectedVert >= 0  &&  selectedVert < mesh.verts.Length)
			{
				Vector3 selectedPos = mesh.verts[selectedVert].Vector3();
				float size = HandleUtility.GetHandleSize(selectedPos);

				Vector3 newPos = Handles.FreeMoveHandle(selectedPos, size*0.2f, Vector3.zero, Handles.SphereHandleCap);

				mesh.verts[selectedVert] = newPos.Vector2D();
			}
		}
	}
}
