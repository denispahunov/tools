using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DelaunayTriangulation : MonoBehaviour
{
    // A class to represent a point in 2D space
    public class Point
    {
        public float x;
        public float y;

        public Point(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }

    // A class to represent a triangle defined by three points
    public class Triangle
    {
        public Point p1;
        public Point p2;
        public Point p3;

        public Triangle(Point p1, Point p2, Point p3)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }
    }

    // A helper function to determine if a point is inside the circumcircle of a triangle
    private static bool IsPointInsideCircumcircle(Point point, Triangle triangle)
    {
        float ax = triangle.p1.x - point.x;
        float ay = triangle.p1.y - point.y;
        float bx = triangle.p2.x - point.x;
        float by = triangle.p2.y - point.y;
        float cx = triangle.p3.x - point.x;
        float cy = triangle.p3.y - point.y;

        float d = (ax * (by - cy) + bx * (cy - ay) + cx * (ay - by)) * 2f;

        if (d <= 0)
            return false;

        float aa = ax * ax + ay * ay;
        float bb = bx * bx + by * by;
        float cc = cx * cx + cy * cy;

        float x = (aa * (by - cy) + bb * (cy - ay) + cc * (ay - by)) / d;
        float y = (aa * (cx - bx) + bb * (ax - cx) + cc * (bx - ax)) / d;

        float radiusSquared = x * x + y * y;

        return radiusSquared <= Mathf.Max(Mathf.Max(aa, bb), cc);
    }

    // Function to perform Delaunay Triangulation
    public static List<Triangle> DelaunayTriangulate(List<Point> points)
    {
        // Create a super-triangle that bounds all the input points
        float minX = float.MaxValue;
        float minY = float.MaxValue;
        float maxX = float.MinValue;
        float maxY = float.MinValue;

        foreach (var point in points)
        {
            if (point.x < minX) minX = point.x;
            if (point.y < minY) minY = point.y;
            if (point.x > maxX) maxX = point.x;
            if (point.y > maxY) maxY = point.y;
        }

        float dx = maxX - minX;
        float dy = maxY - minY;
        float deltaMax = Mathf.Max(dx, dy);
        float midx = (minX + maxX) * 0.5f;
        float midy = (minY + maxY) * 0.5f;

        Point p1 = new Point(midx - 20 * deltaMax, midy - deltaMax);
        Point p2 = new Point(midx, midy + 20 * deltaMax);
        Point p3 = new Point(midx + 20 * deltaMax, midy - deltaMax);

        List<Triangle> triangles = new List<Triangle>();
        triangles.Add(new Triangle(p1, p2, p3));

        // Add each point to the triangulation one at a time
        foreach (var point in points)
        {
            List<Triangle> badTriangles = new List<Triangle>();

            foreach (var triangle in triangles)
            {
                if (IsPointInsideCircumcircle(point, triangle))
                {
                    badTriangles.Add(triangle);
                }
            }

            List<Edge> polygon = new List<Edge>();

            foreach (var triangle in badTriangles)
            {
                foreach (var edge in triangleEdges(triangle))
                {
                    if (!polygon.Contains(edge))
                    {
                        polygon.Add(edge);
                    }
                    else
                    {
                        polygon.Remove(edge);
                    }
                }
            }

            foreach (var triangle in badTriangles)
            {
                triangles.Remove(triangle);
            }

            foreach (var edge in polygon)
            {
                triangles.Add(new Triangle(edge.p1, edge.p2, point));
            }
        }

        // Remove triangles that share edges with the super-triangle
        List<Triangle> trianglesToRemove = new List<Triangle>();
        foreach (var triangle in triangles)
        {
            if (triangle.p1 == p1 || triangle.p1 == p2 || triangle.p1 == p3 ||
                triangle.p2 == p1 || triangle.p2 == p2 || triangle.p2 == p3 ||
                triangle.p3 == p1 || triangle.p3 == p2 || triangle.p3 == p3)
            {
                trianglesToRemove.Add(triangle);
            }
        }

        foreach (var triangle in trianglesToRemove)
        {
            triangles.Remove(triangle);
        }

        return triangles;
    }

    // A class to represent an edge defined by two points
    public class Edge
    {
        public Point p1;
        public Point p2;

        public Edge(Point p1, Point p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        public override bool Equals(object obj)
        {
            Edge other = obj as Edge;
            return (other != null) && ((other.p1 == this.p1 && other.p2 == this.p2) || (other.p1 == this.p2 && other.p2 == this.p1));
        }

        public override int GetHashCode()
        {
            return p1.GetHashCode() + p2.GetHashCode();
        }
    }

    // A helper function to get the edges of a triangle
    private static List<Edge> triangleEdges(Triangle t)
    {
        List<Edge> edges = new List<Edge>
        {
            new Edge(t.p1, t.p2),
            new Edge(t.p2, t.p3),
            new Edge(t.p3, t.p1)
        };
        return edges;
    }

    // Function to visualize the triangulation (for demonstration purposes)
    private void VisualizeTriangles(List<Triangle> triangles)
    {
        foreach (var triangle in triangles)
        {
            Gizmos.DrawLine(new Vector3(triangle.p1.x, 0, triangle.p1.y), new Vector3(triangle.p2.x, 0, triangle.p2.y));
            Gizmos.DrawLine(new Vector3(triangle.p2.x, 0, triangle.p2.y), new Vector3(triangle.p3.x, 0, triangle.p3.y));
            Gizmos.DrawLine(new Vector3(triangle.p3.x, 0, triangle.p3.y), new Vector3(triangle.p1.x, 0, triangle.p1.y));
        }
    }

    // Start is called before the first frame update
    void OnDrawGizmos()
    {
        // Generate some random points for demonstration
        List<Point> points = new List<Point>();
        System.Random rand = new System.Random();
        for (int i = 0; i < 20; i++)
        {
            points.Add(new Point((float)rand.NextDouble() * 10, (float)rand.NextDouble() * 10));
        }

        // Perform Delaunay triangulation
        List<Triangle> triangles = DelaunayTriangulate(points);

        // Visualize the triangulation
        VisualizeTriangles(triangles);
    }
}