using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Den.Tools;
using static Den.Tools.IrregularGridMesh;

namespace Den.Tools.Tests
{
    public class IrregularGridTester : MonoBehaviour
    {
		public IrregularGridMesh mesh;

		public Vector2D from = new Vector2D(0,0);
		public Vector2D to = new Vector2D(10,10);
		public int cellsX = 10;
		public int cellsZ = 10;
		public int seed = 12345;
		public float relax = 1;
		public bool refresh = false;
		public bool debug = false;
		public bool resetDebug = false;
		private IEnumerator<bool> debugEnumerator;

		public bool drawMesh = false;
		public bool drawDebug = false;

		public int idealizeNum = -1;
		public int idealizeVert = 0;

		public void OnDrawGizmos()
		{
			if (mesh == null)
				mesh = new IrregularGridMesh();

			if (refresh)
			{
				refresh = false;

				Noise noise = new Noise(seed);
				mesh.Scatter(from, to, cellsX, cellsZ, noise, relax);
				mesh.DelaunayTriangulate();
				mesh.Quadrify();
				mesh.FixQuadsOrientation();
				mesh.RelaxQuadsQuality();
			}

			if (debug)
			{
				debug = false;

				if (debugEnumerator == null) 
				{
					Noise noise = new Noise(seed);
					mesh.Scatter(from, to, cellsX, cellsZ, noise, relax);
					debugEnumerator = mesh.RelaxQuadsQualityIterative().GetEnumerator();
				}

				if (!debugEnumerator.MoveNext())
					debugEnumerator = null;
			}

			if (resetDebug)
			{
				debug = false;
				resetDebug = false;
				debugEnumerator  = null;
				mesh.faces = new Face[0];
			}

			if (drawMesh)
			{
				Gizmos.color = new Color(0, 1, 0);
				//DrawVerts(mesh.verts);
				//DrawTris(mesh.verts, mesh.faces);
				//DrawEdges(mesh);
			}

			if (drawDebug)
			{ 
				Gizmos.color = new Color(1, 0.5f, 0);
				DrawVerts(mesh.debugVerts);
				DrawTris(mesh.verts, mesh.debugFaces);
				//DrawEdges(mesh.debugVerts, mesh.debugEdges);
			}

			if (idealizeNum >= 0)
				DrawIdealFace(mesh, mesh.faces[idealizeNum]);
		}

		public void DrawVerts (Vector2D[] verts)
		{
			if (verts == null || verts.Length == 0)
				return;

			for (int i=0; i<verts.Length; i++)
				Gizmos.DrawCube(verts[i].Vector3(), Vector3.one*0.1f);
		}

		public void DrawTris (Vector2D[] verts, Face[] faces)
		{
			if (faces == null || faces.Length == 0)
				return;

			Vector2D[] faceVerts = new Vector2D[3];
			Vector3[] faceVerts3D = new Vector3[3];

			for (int i=0; i<faces.Length; i++)
			{
				//GetFacePositions(verts, faces, i, faceVerts);
				//ArrayTools.Convert(faceVerts, faceVerts3D, (f) => (Vector3)f);
				//Gizmos.DrawLineStrip(faceVerts3D, looped:true);
			}
		}


		public void DrawFaces (Vector2D[] verts, Face[] faces)
		{
			if (faces == null || faces.Length == 0)
				return;

			Vector2D[] faceVerts = new Vector2D[4];
			Vector3[] faceVerts3D = new Vector3[4];

			for (int i=0; i<faces.Length; i++)
			{
				//GetFacePositions(verts, faces, i, faceVerts);
				ArrayTools.Convert(faceVerts, faceVerts3D, (f) => f.Vector3());

				Gizmos.DrawLineStrip(faceVerts3D, looped:true);

				Color prewColor = Gizmos.color;
				Gizmos.color = prewColor/2;
				Gizmos.DrawLine(faceVerts3D[0], faceVerts3D[2]);
				Gizmos.DrawLine(faceVerts3D[1], faceVerts3D[3]);
				Gizmos.color = prewColor;
			}
		}

		public void DrawEdges (Vector2D[] verts, Edge[] edges)
		{
			//if (refresh)
			//	edges = mesh.GabrielGraph(); 

			if (edges != null  &&  edges.Length != 0)
				foreach (Edge edge in edges)
					Gizmos.DrawLine(mesh.verts[edge.id1].Vector3(), mesh.verts[edge.id2].Vector3());
		}

		public void DrawIdealFace (IrregularGridMesh mesh, Face face)
		{
			Vector3[] faceVerts3D = new Vector3[4];

			(Vector2D,Vector2D,Vector2D,Vector2D) idealCoords = mesh.IdealizeQuad(face);

			faceVerts3D[0] = idealCoords.Item1.Vector3();
			faceVerts3D[1] = idealCoords.Item2.Vector3();
			faceVerts3D[2] = idealCoords.Item3.Vector3();
			faceVerts3D[3] = idealCoords.Item4.Vector3();

			Gizmos.color = new Color(1,0.5f,0);
			Gizmos.DrawLineStrip(faceVerts3D, looped:true);
			//Gizmos.DrawSphere((Vector3)mesh.verts[face[0]], 0.2f);
			//Gizmos.DrawSphere((Vector3)mesh.verts[face[1]], 0.1f);
			//Gizmos.DrawSphere((Vector3)mesh.verts[face[2]], 0.05f);
			Gizmos.DrawSphere(mesh.verts[face[idealizeVert]].Vector3(), 0.1f);
			Gizmos.DrawSphere(faceVerts3D[idealizeVert], 0.1f);
		}
	}
}
