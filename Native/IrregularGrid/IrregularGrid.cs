using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Den.Tools
{
	public class IrregularGridMesh 
	{
		[Serializable]
		[StructLayout (LayoutKind.Sequential)] //to pass to native
		public struct Face
		{
			public ushort a, b, c, d;

			public Face (ushort ia, ushort ib, ushort ic, ushort id)
				{ a = ia; b=ib; c=ic; d=id; }

			public Face (int ia, int ib, int ic, int id)
				{ a = (ushort)ia; b=(ushort)ib; c=(ushort)ic; d=(ushort)id; }

			public Face (ushort ia, ushort ib, ushort ic)
			///Triangle
				{ a = ia; b=ib; c=ic; d=ia; }

			public Face (int ia, int ib, int ic)
			///Triangle
				{ a = (ushort)ia; b=(ushort)ib; c=(ushort)ic; d=(ushort)ia; }

			public ushort this[int i]
			{
				get
				{
					switch (i)
					{
						case 0: return a;
						case 1: return b;
						case 2: return c;
						case 3: return d;
						case 4: return a; //loop
						case 5: return b;
						default:
							throw new System.ArgumentOutOfRangeException("Trying to get vertex number " + i);
					}
				}

				set
				{
					switch (i)
					{
						case 0: a = value; break;
						case 1: b = value; break;
						case 2: c = value; break;
						case 3: d = value; break;
						case 4: a = value; break;
						case 5: b = value; break;
					}
				}
			}

			public bool IsTriangle => a==d;

			public bool ContainsVert (int v) => a==v || b==v || c==3;
		}


//TODO: GOON generates some tris. Maybe worth making border quadrifying firts or maybe treat border edges as quads
//Mix two relax modes - distance and poly ideal
//Distance mode could be optimized - instead of array of arrays use same approach as quality
//And optimize in general

		public Vector2D[] verts;
		public Face[] faces;

		public int cellsX, cellsZ;
		public int vertsX, vertsZ; //to quickly find border vertices


		///Gets numbers of 4 corners of the mesh
		public (int,int,int,int) GetCornerVerts () =>
			(0, vertsZ-1, vertsX*vertsZ-1, vertsX*vertsZ-vertsZ);


		public Vector2D[] FaceVertPositions (int faceNum, Vector2D[] arrToFill=null) =>
			FaceVertPositions(faces[faceNum], arrToFill);

		public Vector2D[] FaceVertPositions (Face face, Vector2D[] arrToFill=null)
		{
			if (arrToFill == null)
				arrToFill = new Vector2D[4];

			for (int i=0; i<arrToFill.Length; i++)
				arrToFill[i] = verts[face[i]];

			return arrToFill;
		}

		
		public bool IsVertBorderLeft (int v) => v<vertsZ;
		public bool IsVertBorderRight (int v) => v>=vertsX*vertsZ-vertsZ;
		public bool IsVertBorderTop (int v) => v%vertsZ == 0;
		public bool IsVertBorderBottom (int v) => (v+1)%vertsZ == 0;
		public bool IsVertBorder (int v) =>
			v<vertsZ  ||
			v>=vertsX*vertsZ-vertsZ  ||
			v%vertsZ == 0  ||
			(v+1)%vertsZ == 0;

		#region Scattering

			public void Scatter (Vector2D from, Vector2D to, int cellsX, int cellsZ, Noise noise, float relax)
			{
				this.cellsX = cellsX; this.cellsZ = cellsZ;
				vertsX = cellsX+1; vertsZ = cellsZ+1;
				Vector2D cellSize = (to-from) / new Vector2D(cellsX, cellsZ); //dividing with number of cells!

				verts = new Vector2D[vertsX * vertsZ];

				for (int x=0; x<vertsX; x++)
					for (int z=0; z<vertsZ; z++)
					{
						verts[x*vertsZ + z] = new Vector2D(
							from.x + cellSize.x*x,
							from.z + cellSize.z*z);
					}

				for (int x=1; x<vertsX-1; x++)
					for (int z=1; z<vertsZ-1; z++)
						RandomizePoint(x,z, cellSize, noise);

				for (int r=0; r<3; r++)
					for (int x=1; x<vertsX-1; x++) //keeping boundary points in place
						for (int z=1; z<vertsZ-1; z++)
							RelaxPoint(x, z, relax);
			}

			private void RandomizePoint (int x, int z, Vector2D cellSize, Noise noise, float intensity=0.1f)
			{
				int num = x*vertsZ + z;

				Vector2D pos = verts[num];

				pos.x += (noise.Random(x,z,0) - cellSize.x/2)*intensity;
				pos.z += (noise.Random(x,z,1) - cellSize.z/2)*intensity;

				verts[num] = pos;
			}

			private void RelaxPoint (int x, int z, float intensity)
			{
				Vector2D summaryDir = new Vector2D();

				int num = x*vertsZ + z;

				for (int nx=-1; nx<=1; nx++)
					for (int nz=-1; nz<=1; nz++)
					{
						if ( (nx==0 && nz==0) ||
							x+nx < 0  ||  x+nx >= vertsX  ||
							z+nz < 0  ||  z+nz >= vertsZ)
								continue;

						int nnum = (x+nx)*vertsZ + (z+nz);

						Vector2D pos = verts[num];
						Vector2D npos = verts[nnum];

						Vector2D dir = pos-npos;
						dir = dir.Normalized * 1.0f/dir.Magnitude; //closer the point - bigger the dir; TODO fails on same pos

						summaryDir += dir;
					}

				//TODO: point should not cross cell borders to quickly pick it with poshash
				verts[num] += summaryDir * intensity;
			}

		#endregion


		#region Triangulation

			public void DelaunayTriangulate ()
			{
				faces = new Face[0];
				//AppendSuperTriangle();
				AppendSuperQuad();

				List<Face> faceList = new List<Face>();
				faceList.AddRange(faces);

				for (int v=0; v<verts.Length; v++)
					DelaunayTriangulateIteration(v, faceList);

				RemoveBorderTris(faceList);
				ReturnSuperQuad();

				faces = faceList.ToArray();

			}

			public Vector2D[] debugVerts = new Vector2D[0];
			public Face[] debugFaces = new Face[0];
			public Edge[] debugEdges = new Edge[0];

			public IEnumerable<bool> DelaunayTriangulateIterative ()
			{
				faces = new Face[0];
				//AppendSuperTriangle();
				AppendSuperQuad();

				List<Face> faceList = new List<Face>();
				faceList.AddRange(faces);

				for (int v=0; v<verts.Length; v++)
				{
					DelaunayTriangulateIteration(v, faceList);

					faces = faceList.ToArray();
					yield return false;
				}

				RemoveBorderTris(faceList);
				ReturnSuperQuad();

				faces = faceList.ToArray();
			}


			private void DelaunayTriangulateIteration (int vertNum, List<Face> faceList)
			{
				Vector2D vertPos = verts[vertNum];

				//bringing faces to split in a separate list
				List<Face> trisToSplit = new List<Face>();

				for (int f = faceList.Count-1; f >= 0; f--)
				{
					Face tri = faceList[f];

					(Vector2D circCenter, float circRadius) = FindTriCircumcircle(tri);
					//TODO: cache tri circumcircles

					//if inside circumcircle
					if ((circCenter-vertPos).Magnitude <= circRadius) //TODO: should be epsylon, but this seem to do the trick
					{
						trisToSplit.Add(tri);
						faceList.RemoveAt(f);
					}
				}

				//finding border edges of tris to split
				//border are those edges that mentioned in list only once
				HashSet<Edge> borderEdges = new HashSet<Edge>();
				Edge[] triEdges = new Edge[3];
				for (int t = 0; t<trisToSplit.Count; t++)
				{
					Face tri = trisToSplit[t];

					triEdges[0] = new Edge(tri.a, tri.b);	triEdges[0].SortVerts();
					triEdges[1] = new Edge(tri.b, tri.c);	triEdges[1].SortVerts();
					triEdges[2]= new Edge(tri.c, tri.a);	triEdges[2].SortVerts();

					for (int i=0; i<3; i++)
					{
						Edge edge = triEdges[i];

						//first time adding, if found twice - removing
						if (!borderEdges.Contains(edge))
							borderEdges.Add(edge);
						else
							borderEdges.Remove(edge);
					}
				}

				//for each border edge creating a tri with center at vert
				foreach (Edge edge in borderEdges)
				{
					faceList.Add(new Face(edge.id1, edge.id2, vertNum));
				}

				//debugging - remove!
				HashSet<int> debugVertsHash = new HashSet<int>();
				foreach (Edge edge in borderEdges)
				{
					if (!debugVertsHash.Contains(edge.id1))
						debugVertsHash.Add(edge.id1);
					if (!debugVertsHash.Contains(edge.id2))
						debugVertsHash.Add(edge.id2);
				}
				List<Vector2D> debugVertsList = new List<Vector2D>();
				foreach (int dv in debugVertsHash)
					debugVertsList.Add(verts[dv]);
				debugVerts = debugVertsList.ToArray();
			}


			private void AppendSuperQuad ()
			///Creates 2 triangles that contain all the verts
			///Uses existing verts without adding new
			{
				(int,int,int,int) corners = GetCornerVerts();
				ArrayTools.Add(ref faces,
					new Face(corners.Item1, corners.Item2, corners.Item3),
					new Face(corners.Item3, corners.Item4, corners.Item1) );

					
				verts[corners.Item1] -= new Vector2D(0.1f, 0.1f);
				verts[corners.Item2] -= new Vector2D(0.1f, -0.1f);
				verts[corners.Item3] -= new Vector2D(-0.1f, -0.1f);
				verts[corners.Item4] -= new Vector2D(-0.1f, 0.1f);
			}


			private void ReturnSuperQuad ()
			///Moves corner vertices back
			{
				(int,int,int,int) corners = GetCornerVerts();

				verts[corners.Item1] += new Vector2D(0.1f, 0.1f);
				verts[corners.Item2] += new Vector2D(0.1f, -0.1f);
				verts[corners.Item3] += new Vector2D(-0.1f, -0.1f);
				verts[corners.Item4] += new Vector2D(-0.1f, 0.1f);
			}


			private void AppendSuperTriangle ()
			/// Create a supertriangle that contains all the points
			{
				//calculating points
				float minX = float.MaxValue, minY = float.MaxValue, maxX = float.MinValue, maxY = float.MinValue;
				foreach (Vector2D vert in verts)
				{
					minX = Math.Min(minX, vert.x);
					minY = Math.Min(minY, vert.z);
					maxX = Math.Max(maxX, vert.x);
					maxY = Math.Max(maxY, vert.z);
				}
				float dx = maxX - minX;
				float dy = maxY - minY;
				float deltaMax = Math.Max(dx, dy);
				float midx = (minX + maxX) * 0.5f;
				float midy = (minY + maxY) * 0.5f;

				Vector2D p1 = new Vector2D(midx - 20 * deltaMax, midy - deltaMax);
				Vector2D p2 = new Vector2D(midx, midy + 20 * deltaMax);
				Vector2D p3 = new Vector2D(midx + 20 * deltaMax, midy - deltaMax);

				// Add supertriangle vertices to the points list
				
				ArrayTools.Add(ref verts, p1, p2, p3);

				// Initialize the supertriangle as the first triangle in the triangulation
				int vertsCount = verts.Length;
				ArrayTools.Add(ref faces, new Face(vertsCount-3, vertsCount-2, vertsCount-1));
			}


			public (Vector2D center, float radius) FindTriCircumcircle (Face tri)
			{
				Vector2D a = verts[tri.a];
				Vector2D b = verts[tri.b];
				Vector2D c = verts[tri.c];

				Vector2D center = FindCircumCenter(a, b, c);
				float radius = MathF.Sqrt(MathF.Pow(center.x - a.x, 2) + MathF.Pow(center.z - a.z, 2));

				return (center, radius);
			}

			private static Vector2D FindCircumCenter (Vector2D P, Vector2D Q, Vector2D R)
			{
				// Line PQ is represented as ax + by = c
				double a = Q.z - P.z;
				double b = P.x - Q.x;
				double c = a * P.x + b * P.z;

				// Line QR is represented as ex + fy = g
				double e = R.z - Q.z;
				double f = Q.x - R.x;
				double g = e * Q.x + f * Q.z;

				// Converting lines PQ and QR to perpendicular bisectors.
				PerpendicularBisectorFromLine(ref a, ref b, ref c, P, Q);
				PerpendicularBisectorFromLine(ref e, ref f, ref g, Q, R);

				// The point of intersection of the bisectors gives the circumcenter
				Vector2D circumcenter = LineLineIntersection(a, b, c, e, f, g);

				return circumcenter;
			}

			private static void PerpendicularBisectorFromLine (ref double a, ref double b, ref double c, Vector2D P, Vector2D Q)
			{
				Vector2D midPoint = new Vector2D((P.x + Q.x) / 2, (P.z + Q.z) / 2);
				c = -b * midPoint.x + a * midPoint.z;
				double temp = a;
				a = -b;
				b = temp;
			}

			private static Vector2D LineLineIntersection (double a1, double b1, double c1, double a2, double b2, double c2)
			{
				double determinant = a1 * b2 - a2 * b1;
				if (determinant == 0)
				{
					// The lines are parallel, return a point at infinity
					return new Vector2D(float.MaxValue, float.MaxValue);
				}
				else
				{
					float x = (float)((b2 * c1 - b1 * c2) / determinant);
					float y = (float)((a1 * c2 - a2 * c1) / determinant);
					return new Vector2D(x, y);
				}
			}


			private (Vector2D center, float radius) FindTriCircumcircle_Unreliable (Face tri)
			{
				Vector2D A = verts[tri.a];
				Vector2D B = verts[tri.b];
				Vector2D C = verts[tri.c];

				// Calculate midpoints of sides
				Vector2D midAB = new Vector2D((A.x + B.x) / 2, (A.z + B.z) / 2);
				Vector2D midBC = new Vector2D((B.x + C.x) / 2, (B.z + C.z) / 2);

				// Calculate slopes of sides
				double slopeAB = (B.z - A.z) / (B.x - A.x);
				double slopeBC = (C.z - B.z) / (C.x - B.x);

				// Calculate perpendicular slopes
				double perpSlopeAB = -1 / slopeAB;
				double perpSlopeBC = -1 / slopeBC;

				// Calculate y-intercepts of perpendicular bisectors
				double yInterceptAB = midAB.z - perpSlopeAB * midAB.x;
				double yInterceptBC = midBC.z - perpSlopeBC * midBC.x;

				// Calculate circumcenter coordinates
				double centerX = (yInterceptBC - yInterceptAB) / (perpSlopeAB - perpSlopeBC);
				double centerY = perpSlopeAB * centerX + yInterceptAB;

				// Calculate radius
				double radius = Math.Sqrt(Math.Pow(centerX - A.x, 2) + Math.Pow(centerY - A.z, 2));

				// Return the circumcircle
				return (new Vector2D((float)centerX, (float)centerY), (float)radius);
			}

			private void RemoveBorderTris (List<Face> faceList)
			///Clears triangles containing _only_ border verts
			{
				bool[] borderA = new bool[4];
				bool[] borderB = new bool[4];
				bool[] borderC = new bool[4];
				bool[][] borders = new bool[][]{ borderA, borderB, borderC };

				for (int f=faceList.Count-1; f>=0; f--)
				{
					Face face = faceList[f];

					for (int v=0; v<3; v++)
					{
						int vn = face[v];

						bool[] borderV = borders[v];
						borderV[0] = IsVertBorderLeft(vn);
						borderV[1] = IsVertBorderRight(vn);
						borderV[2] = IsVertBorderTop(vn);
						borderV[3] = IsVertBorderBottom(vn);
						//TODO: coninue if not border at all
					}

					bool allSameBorder = false;
					for (int i=0; i<4; i++)
						if (borderA[i]  &&  borderB[i]  &&  borderC[i])
							allSameBorder = true;

					if (allSameBorder)
						faceList.RemoveAt(f);
				}
			}

		#endregion


		#region Edges

			public struct Edge
			//aka Coord tuple
			{ 
				public int id1; 
				public int id2; 
				public Edge(int i1, int i2) {id1=i1; id2=i2;} 

				public void SortVerts ()
				///Making vert with lesser num be first to compare edges
				{
					if (id1 > id2)
					{
						int tmp = id1;
						id1 = id2;
						id2 = tmp;
					}
				}
			}


			public HashSet<Edge> GabrielGraph (int triesPerObj=1000)
			/// Guarantees that all the vertices will be connected by at least 1 link
			{
				triesPerObj = Math.Min(verts.Length-1, triesPerObj);

				//for each vert storing a list of closest points
				int[][] closestMap = new int[verts.Length][]; 

				//filling closest map
				for (int v=0; v<verts.Length; v++)
				{
					Vector2D vert = verts[v];

					int[] closestIds = new int[triesPerObj];
					FindClosestVertsBruteforce(vert, closestIds, ignoreNum:v);

					closestMap[v] = closestIds;
				}

				//connecting
				HashSet<Edge> edges = new HashSet<Edge>();
				for (int v1=0; v1<verts.Length; v1++)
				{
					int[] closest1 = closestMap[v1];

					for (int n1=0; n1<closest1.Length; n1++)
					{
						int v2 = closest1[n1];
	//					if (v2 == 0) continue; // no more objects left during closest map

						int[] closest2 = closestMap[v2];
						int n2 = closest2.Find(v1);

						//if id1 not contains in closestIds2
						if (n2 < 0) continue; 

						//if this link was not created earlier
						Edge edge = new Edge(v1, v2);
						edge.SortVerts();
						if (edges.Contains(edge))
							continue;

						//if there is no common ids before num1 and num2
						bool hasCommonNodeCloser = false; //SNIPPET: the ideal case of using GOTO
						for (int i=0; i<n1; i++)
						{
							for (int j=0; j<n2; j++)
								if (closest1[i] == closest2[j]) 
									{ hasCommonNodeCloser = true; }// break; }
							//if (hasCommonNodeCloser) break;
						}
						if (hasCommonNodeCloser) continue;

						//if the maximum number of connections reached
						/*idToLinksCount.TryGetValue(id1, out int linksCount1);
						idToLinksCount.TryGetValue(id2, out int linksCount2);
						if (linksCount1 >= maxLinks || linksCount2 >= maxLinks)
							continue;*/

						edges.Add(edge);
						//idToLinksCount.ForceAdd(id1, linksCount1 + 1);
						//idToLinksCount.ForceAdd(id2, linksCount2 + 1);
					}
				}

				return edges;
			}


			public HashSet<Edge> GenerateEdges ()
			{
				HashSet<Edge> edges = new HashSet<Edge>();

				//for each vertex finding 2 closest points
				/*int[] closestTwo = new int[3];
				for (int v=0; v<verts.Length; v++)
				{
					FindClosestVertsBruteforce(verts[v], closestTwo, ignoreNum:v);
					foreach (int closest in closestTwo)
					{
						Edge edge = new Edge(v, closest);
					
						edge.SortVerts();

						if (!edges.Contains(edge))
							edges.Add(edge);
					}
				}*/

				for (int i=0; i<100; i++)
				{
					Edge edge = ConnectClosestUnconnectedBruteforce(edges);
					edges.Add(edge);
				}
			
				return edges;
			}


			private void FindClosestVertsBruteforce (Vector2D pos, int[] closestNums, int ignoreNum=-1)
			//TODO: point cannot cross cell borders, so can use poshash to pick them
			//Coord has a method of iterating cells
			//ignoreNum avoids picking itself
			{
				for (int i=0; i<closestNums.Length; i++)
				{
					float minDist = float.MaxValue;
					int minNum = -1;

					for (int v=0; v<verts.Length; v++)
					{
						if (v==ignoreNum)
							continue;

						if (closestNums.Contains(v))
							continue;

						float dist = (verts[v] - pos).Magnitude;
						if (dist < minDist)
						{
							minDist = dist;
							minNum = v;
						}

						closestNums[i] = minNum;
					}
				}
			}

			private Edge ConnectClosestUnconnectedBruteforce (HashSet<Edge> edges)
			/// Finding two closest points (among all the verts) that are not connected with an edge
			{
				float minDist = float.MaxValue;
				Edge minEdge = new Edge();

				for (int v1=0; v1<verts.Length; v1++)
					for (int v2=0; v2<verts.Length; v2++)
					{
						if (v1 == v2)
							continue;

						Edge edge = new Edge(v1, v2);
						edge.SortVerts();

						if (edges.Contains(edge))
							continue;

						//ensuring that these edges 

						float dist = (verts[v1] - verts[v2]).SqrMagnitude;
						if (dist < minDist)
						{
							minDist = dist;
							minEdge = edge;
						}
					}

				return minEdge;
			}

		#endregion


		#region Quaderization

			public float[] quality;

			public void RefreshQuality ()
			{
					quality = new float[faces.Length];
					for (int f=0; f<quality.Length; f++)
					{
						if (faces[f].IsTriangle)
							continue;
						quality[f] = GetQuadQuality(faces[f]);
					}
			}

			public void Quadrify ()
			{
				List<Face> faceList = new List<Face>(faces);
				
				MergeZeroFace(faceList);

				for (int i=0; i<faceList.Count; i++)
				{
					bool merged = QuadrifyIteration(faceList);

					if (!merged)
						break;
				}

				faces = faceList.ToArray();
			}


			public IEnumerable<bool> QuadrifyIterative ()
			{
				List<Face> faceList = new List<Face>(faces);
				
				MergeZeroFace(faceList);
				
				faces = faceList.ToArray();
				yield return false;

				for (int i=0; i<faceList.Count; i++)
				{
					bool merged = QuadrifyIteration(faceList);

					if (!merged)
						break;

					faces = faceList.ToArray();
					yield return false;
				}
			}


			private bool QuadrifyIteration (List<Face> faceList)
			{
				Dictionary<Edge,(int,int)> edgeToFace = CompileEdgeToFacesDict(faceList);

				HashSet<int> candidateTrisToMerge = new HashSet<int>();
				
				//iterating all quads that have borders with trangles
				for (int f=0; f<faceList.Count; f++)
				{
					Face face = faceList[f];

					if (face.IsTriangle)
						continue;

					foreach (int n in GetNeighbourFaces(f, faceList, edgeToFace))
					{
						Face neig = faceList[n];

						if (!neig.IsTriangle)
							continue;

						candidateTrisToMerge.Add(n);
					}
				}

				//estimating what would be the quality to merge candidates
				float maxQuality = 0;
				int maxCandidate1 = -1;
				int maxCandidate2 = -1;

				foreach (int f in candidateTrisToMerge)
				{
					foreach (int n in GetNeighbourFaces(f, faceList, edgeToFace))
					{
						if (!faceList[n].IsTriangle)
							continue;

						HashSet<int> quadsBordering = new HashSet<int>();

						foreach (int n1 in GetNeighbourFaces(f, faceList, edgeToFace))
							if (!faceList[n1].IsTriangle && !quadsBordering.Contains(n1))
								quadsBordering.Add(n1);
								//TODO: this enumeration might go in upper for level

						foreach (int n2 in GetNeighbourFaces(n, faceList, edgeToFace))
							if (!faceList[n2].IsTriangle && !quadsBordering.Contains(n2))
								quadsBordering.Add(n2);

						float quality = quadsBordering.Count;
						
						Face merged = MergeTris(faceList[f], faceList[n]);
						quality += GetQuadQuality(merged);

						if (quality > maxQuality)
						{
							maxQuality = quality;
							maxCandidate1 = f;
							maxCandidate2 = n;
						}
					}
				}

				//no merge variants left
				if (maxCandidate1 < 0 || maxCandidate2 < 0)
					return false;

				//merging
				//TODO: we already have merged faces - unify with maxquality
				MergeTrisModifyList(maxCandidate1, maxCandidate2, faceList);

				return true;
			}


			private void MergeZeroFace (List<Face> faceList)
			{
				int v1 = 0;
				int v2 = 1;
				int v3 = vertsZ;

				//finding 2 faces containing at least 2 of 3 zero verts
				int faceNum1 = -1, faceNum2 = -1;
				for (int f=0; f<faceList.Count; f++)
				{
					Face face = faceList[f];
					
					int numContains = 0;
					if (face.a == v1  ||  face.b == v1  ||  face.c == v1) numContains++;
					if (face.a == v2  ||  face.b == v2  ||  face.c == v2) numContains++;
					if (face.a == v3  ||  face.b == v3  ||  face.c == v3) numContains++;
					
					if (numContains >= 2)
					{
						if (faceNum1 < 0)
							faceNum1 = f;
						else
							faceNum2 = f;
					}
				}

				//merging
				MergeTrisModifyList(faceNum1, faceNum2, faceList);
			}


			private void MergeTrisModifyList (int t1, int t2, List<Face> faceList)
			{
				Face merged = MergeTris(faceList[t1], faceList[t2]);

				if (t1 > t2)
				{
					faceList.RemoveAt(t1);
					faceList.RemoveAt(t2);
				}
				else
				{
					faceList.RemoveAt(t2);
					faceList.RemoveAt(t1);
				}

				faceList.Add(merged);
			}


			private Face MergeTris (Face tri1, Face tri2)
			{
				//TODO: use edges hash instead allVerts

				HashSet<int> allVerts = new HashSet<int>();
				int commonVert1 = -1, commonVert2 = -1;
				int uniqueVert1 = -1;

				for (int v=0; v<3; v++)
					allVerts.Add(tri1[v]);

				for (int v=0; v<3; v++)
				{
					int vert2 = tri2[v];
					if (allVerts.Contains(vert2))
					{
						if (commonVert1 < 0)
							commonVert1 = vert2;
						else
							commonVert2 = vert2;
					}
					else
						uniqueVert1 = vert2;
				}

				int uniqueVert2 = -1;
				for (int v=0; v<3; v++)
				{
					int vert1 = tri1[v];
					if (commonVert1 != vert1  &&  commonVert2 != vert1)
						uniqueVert2 = vert1;
				}

				if (commonVert1 < 0  ||  commonVert2 < 0  ||  uniqueVert1 < 0  ||  uniqueVert2 < 0)
					throw new Exception("Tris do not have common edge to merge");

				return new Face(commonVert1, uniqueVert1, commonVert2, uniqueVert2);
			}


			private Dictionary<Edge,(int,int)> CompileEdgeToFacesDict (List<Face> faceList)
			{
				Dictionary<Edge,(int,int)> edgeToFace = new Dictionary<Edge, (int, int)>();

				for (int f=0; f<faceList.Count; f++)
				{
					Face face = faceList[f];
					
					int numEdges = face.IsTriangle ? 3 : 4;
					for (int e=0; e<numEdges; e++)
					{
						int v1 = face[e];
						int v2 = face[e==numEdges-1 ? 0 : e+1];

						Edge edge = new Edge(v1,v2);
						edge.SortVerts();

						if (!edgeToFace.TryGetValue(edge, out (int,int) faces))
							edgeToFace.Add(edge, (f,-1));
						else
							edgeToFace[edge] = (faces.Item1, f);
					}
				}

				return edgeToFace;
			}


			private IEnumerable<int> GetNeighbourFaces (int faceNum, List<Face> faceList, Dictionary<Edge,(int,int)> edgeToFace)
			{
				Face face = faceList[faceNum];

				int numEdges = face.IsTriangle ? 3 : 4;
				for (int e=0; e<numEdges; e++)
				{
					int v1 = face[e];
					int v2 = face[e==numEdges-1 ? 0 : e+1];

					Edge edge = new Edge(v1,v2);
					edge.SortVerts();

					int n;
					(int,int) faces = edgeToFace[edge];
					if (faces.Item1 != faceNum)
						n = faces.Item1;
					else
						n = faces.Item2;

					if (n >= 0) //border tri
						yield return n;
				}
			}


			private float GetQuadQuality (Face face, Vector2D[] poses=null)
			/// 1 for perfect square, 0 for degradated or concave
			{
				if (poses==null)
					poses = new Vector2D[6]; //looped+1

				FaceVertPositions(face, poses);

				float biggestDot = 0;
				float biggestSide = 0;
				float smallestSide = float.MaxValue;
				for (int p=1; p<5; p++)
				{
					Vector2D vec1 = poses[p-1] - poses[p];
					Vector2D vec2 = poses[p+1] - poses[p];

					float length1 = vec1.Magnitude;
					float length2 = vec2.Magnitude;

					vec1 /= length1;
					vec2 /= length2;
					
					float dot = Vector2D.Dot(vec1, vec2);
					if (dot < 0) dot = -dot;

					if (dot > biggestDot)
						biggestDot = dot;

					if (length1 > biggestSide)
						biggestSide = length1;

					if (length1 < smallestSide)
						smallestSide = length1;
				}


				return (1-biggestDot) * (smallestSide/biggestSide);
			}


			private bool AreNeighbors (Face face1, Face face2)
			{
				int vertsInCommon = 0;

				bool face1Tri = face1.IsTriangle;
				bool face2Tri = face2.IsTriangle;

				if (face1.a == face2.a  ||  face1.a == face2.b  ||  face1.a == face2.c || (!face2Tri && face1.a == face2.d)) vertsInCommon++;
				if (face1.b == face2.a  ||  face1.b == face2.b  ||  face1.b == face2.c || (!face2Tri && face1.b == face2.d)) vertsInCommon++;
				if (face1.c == face2.a  ||  face1.c == face2.b  ||  face1.c == face2.c || (!face2Tri && face1.c == face2.d)) vertsInCommon++;
				
				if (!face1Tri)
				if (face1.d == face2.a  ||  face1.d == face2.b  ||  face1.d == face2.c || (!face2Tri && face1.d == face2.d)) vertsInCommon++;

				if (vertsInCommon >= 2)
					return true;
				else return false;
			}

		#endregion


		#region Relax Quads Distance

			public void RelaxQuads (int iterations=10, float intensity=0.1f)
			{
				HashSet<Edge> edges = CollectAllEdges();
				int[][] neigVerts = CollectNeighVerts(edges);

				for (int i=0; i<iterations; i++)
					RelaxIteration(neigVerts, intensity);
			}


			public IEnumerable<bool> RelaxQuadsIterative (float intensity=0.01f)
			{
				HashSet<Edge> edges = CollectAllEdges();
				int[][] neigVerts = CollectNeighVerts(edges);

				for (int i=0; i<100; i++)
				{
					RelaxIteration(neigVerts, intensity);
					yield return false;
				}
			}


			private void RelaxIteration (int[][] neigVerts, float intensity)
			{
				for (int v=0; v<neigVerts.Length; v++)
				{
					if (IsVertBorder(v))
						continue;

					Vector2D vertPos = verts[v];

					Vector2D moveSum = Vector2D.zero;
					
					int[] nverts = neigVerts[v];
					foreach (int n in nverts)
					{
						Vector2D neigPos = verts[n];
						Vector2D vec = neigPos - vertPos;
						moveSum += vec * vec.Magnitude; //further the vert more the vector
					}

					verts[v] += moveSum*intensity;
				}
			}


			private HashSet<Edge> CollectAllEdges ()
			/// Like CompileEdgeToFacesDict, but just hashset
			{
				HashSet<Edge> edges = new HashSet<Edge>();

				for (int f=0; f<faces.Length; f++)
				{
					Face face = faces[f];
					
					int numEdges = face.IsTriangle ? 3 : 4;
					for (int e=0; e<numEdges; e++)
					{
						int v1 = face[e];
						int v2 = face[e==numEdges-1 ? 0 : e+1];

						Edge edge = new Edge(v1,v2);
						edge.SortVerts();

						if (!edges.Contains(edge))
							edges.Add(edge);
					}
				}

				return edges;
			}

			private int[][] CollectNeighVerts (HashSet<Edge> edges)
			{
				HashSet<int>[] neigVerts = new HashSet<int>[verts.Length];

				//TODO: do not use array of arrays, use struct-list

				//for (int f=0; f<verts.Length; f++)
				//{
				//	neigVerts[f] = new int[4];
				//}

				foreach (Edge edge in edges)
				{
					int v1 = edge.id1;
					int v2 = edge.id2;

					if (neigVerts[v1] == null)
						neigVerts[v1] = new HashSet<int>();

					if (!neigVerts[v1].Contains(v2))
						neigVerts[v1].Add(v2);

					if (neigVerts[v2] == null)
						neigVerts[v2] = new HashSet<int>();

					if (!neigVerts[v2].Contains(v1))
						neigVerts[v2].Add(v1);
				}

				int[][] neigVertsArr = new int[verts.Length][];
				for (int f=0; f<verts.Length; f++)
				{
					neigVertsArr[f] = new int[neigVerts[f].Count];
					neigVerts[f].CopyTo(neigVertsArr[f]);
				}

				return neigVertsArr;
			}

		#endregion


		#region Fix Orientation

			public void FixQuadsOrientation ()
			{
				for (int f=0; f<faces.Length; f++)
				{
					Face face = faces[f];

					int handness = Math.Sign(GetQuadHandness(face));

					if (handness > 0)
						faces[f] = FlipQuad(face);
				}
			}

			private Face FlipQuad (Face face)
			{
				return new Face(face.d, face.c, face.b, face.a);
			}
		
			private float GetQuadHandness (Face face)
			{
				Vector2D vertA = verts[face.a];
				Vector2D vertB = verts[face.b];
				Vector2D vertC = verts[face.c];
				Vector2D vertD = verts[face.d];

				float handness = 
					Math.Sign(GetHandness(vertA,vertB,vertC)) +
					Math.Sign(GetHandness(vertB,vertC,vertD)) +
					Math.Sign(GetHandness(vertC,vertD,vertA)) +
					Math.Sign(GetHandness(vertD,vertA,vertB));
				//in case of concave polygons - checking handness for all the angles

				return handness / 4;
			}
		
			private float GetHandness (Vector2D v1, Vector2D v2, Vector2D v3)
			{
				Vector2D dA = v1-v2;
				Vector2D dB = v2-v3;
				return dA.x * dB.z - dA.z * dB.x; //return UnityEngine.Vector3.Cross((UnityEngine.Vector3)dA, (UnityEngine.Vector3)dB).y;
			}

		#endregion


		#region Relax Quad Quality

			public void RelaxQuadsQuality (int iterations=10, float intensity=1f)
			{
				for (int i=0; i<iterations; i++)
					RelaxIterationQuality(intensity);
			}


			public IEnumerable<bool> RelaxQuadsQualityIterative (float intensity=0.5f)
			{
				for (int i=0; i<100; i++)
				{
					RelaxIterationQuality(intensity);
					yield return false;
				}
			}


			private void RelaxIterationQuality (float intensity)
			{
				Vector2D[] vertMoveVectors = new Vector2D[verts.Length];
				int[] vertMoveFactor = new int[verts.Length];

				for (int f=0; f<faces.Length; f++)
				{
//if (f != 13 && f != 14) continue;

					Face face = faces[f];
					if (face.IsTriangle)
						continue;

					(Vector2D a, Vector2D b, Vector2D c, Vector2D d) ideal = IdealizeQuad(face);

					vertMoveVectors[face.a] += ideal.a - verts[face.a];
					vertMoveVectors[face.b] += ideal.b - verts[face.b];
					vertMoveVectors[face.c] += ideal.c - verts[face.c];
					vertMoveVectors[face.d] += ideal.d - verts[face.d];

					vertMoveFactor[face.a]++; vertMoveFactor[face.b]++; vertMoveFactor[face.c]++; vertMoveFactor[face.d]++;
				}

				for (int v=0; v<verts.Length; v++)
				{
					if (IsVertBorder(v))
						continue;

					if (vertMoveFactor[v] == 0) //happens only during debugging
						continue;

					verts[v] += vertMoveVectors[v] / vertMoveFactor[v] * intensity; 
				}
			}

			public (Vector2D, Vector2D, Vector2D, Vector2D) IdealizeQuad (Face face)
			{
				Vector2D vertA = verts[face.a];
				Vector2D vertB = verts[face.b];
				Vector2D vertC = verts[face.c];
				Vector2D vertD = verts[face.d];

				Vector2D center = (vertA+vertB+vertC+vertD) / 4;
				float extend = ((center-vertA).Magnitude + (center-vertB).Magnitude + (center-vertC).Magnitude + (center-vertD).Magnitude) / 4;

				Vector2D dirAC = (vertA-vertC).Normalized;
				Vector2D dirBD = (vertB-vertD).Normalized;

				Vector2D perpDirAC = Vector2D.Perpendicular(dirAC);
				Vector2D perpDirBD = -Vector2D.Perpendicular(dirBD);

				Vector2D avgDirAC = ( (dirAC+perpDirBD)/2 ).Normalized;
				Vector2D avgDirBD = ( (dirBD+perpDirAC)/2 ).Normalized;

				vertA = center + avgDirAC * extend;
				vertB = center + avgDirBD * extend;
				vertC = center - avgDirAC * extend;
				vertD = center - avgDirBD * extend;

				return (vertA, vertB, vertC, vertD);
			}

		#endregion
	}
}