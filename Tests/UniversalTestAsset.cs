using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

using Den.Tools.Matrices;
using System;


namespace Den.Tools.Tests
{
	[CreateAssetMenu(fileName = "Test", menuName = "Universal Test")]
	public class UniversalTestAsset : ScriptableObject
	/// Remember it should be named same as file, otherwise it won't be serialized on project restart
	{
		public string className;
		public string fnName;
		public Argument obj;
		public Argument[] arguments;
		public Argument result;

		[HideInInspector] public int benchmarkIterations = 1000;
		[HideInInspector] public int benchmarkReferences = 0;
		[HideInInspector] public int benchmarkCurrent = 0;
		
		//[HideInInspector] public bool regressionResult = false;  //stored in inspector and resets after each asset change
		public Argument regressionObj;
		public Argument[] regressionArguments;
		public Argument regressionRes;



		#region Argument

			[Serializable] public class Argument
			{
				public Vector4 vec;
				public CoordRect rect;
				public MatrixWorld matrix;
				public Stripe stripe;

				public string name;
				public enum ArgType { Null, Bool, Int, Float, Vector2, Vector2D, Vector3, Vector4, Rect, Coord, CoordRect, RectInt, Matrix, MatrixWorld, Stripe };
				public ArgType argType;

				public Type GetArgType()
				{
					switch (argType)
					{
						case ArgType.Null: return null;
						case ArgType.Bool: return typeof(bool);
						case ArgType.Int: return typeof(int);
						case ArgType.Float: return typeof(float);
						case ArgType.Vector2: return typeof(Vector2);
						case ArgType.Vector2D: return typeof(Vector2D);
						case ArgType.Vector3: return typeof(Vector3);
						case ArgType.Vector4: return typeof(Vector4);
						case ArgType.Rect: return typeof(Rect);
						case ArgType.Coord: return typeof(Coord);
						case ArgType.CoordRect: return typeof(CoordRect);
						case ArgType.RectInt: return typeof(RectInt);
						case ArgType.Matrix: return typeof(Matrix);
						case ArgType.MatrixWorld: return typeof(MatrixWorld);
						case ArgType.Stripe: return typeof(Stripe);
						default: throw new ArgumentOutOfRangeException();
					}
				}

				public object GetVal()
				{
					switch (argType)
					{
						case ArgType.Null: return null;
						case ArgType.Bool: return rect.offset.x != 0;
						case ArgType.Int: return rect.offset.x;
						case ArgType.Float: return vec.x;
						case ArgType.Vector2: return new Vector2(vec.x,vec.y);
						case ArgType.Vector2D: return new Vector2D(vec.x,vec.z);
						case ArgType.Vector3: return new Vector3(vec.x,vec.y,vec.z);
						case ArgType.Vector4: return vec;
						case ArgType.Rect: return new Rect(vec.x,vec.y,vec.z,vec.w);
						case ArgType.Coord: return new Coord((int)vec.x,(int)vec.y);
						case ArgType.CoordRect: return rect;
						case ArgType.RectInt: return new RectInt((int)vec.x,(int)vec.y,(int)vec.z,(int)vec.w);
						case ArgType.Matrix: return matrix;
						case ArgType.MatrixWorld: return matrix;
						case ArgType.Stripe: return stripe;
						default: throw new ArgumentOutOfRangeException();
					}
				}

				public void SetVal(object val)
				{
					switch (argType)
					{
						case ArgType.Null: break;
						case ArgType.Bool: rect.offset.x = Convert.ToBoolean(val) ? 1 : 0; break;
						case ArgType.Int: rect.offset.x = Convert.ToInt32(val); break;
						case ArgType.Float: vec.x = Convert.ToSingle(val); break;
						case ArgType.Vector2:
							Vector2 v2 = (Vector2)val;
							vec.x = v2.x; vec.y = v2.y;
							break;
						case ArgType.Vector2D:
							Vector2D v2d = (Vector2D)val;
							vec.x = v2d.x; vec.z = v2d.z;
							break;
						case ArgType.Vector3:
							Vector3 v3 = (Vector3)val;
							vec.x = v3.x; vec.y = v3.y; vec.z = v3.z;
							break;
						case ArgType.Vector4: vec = (Vector4)val; break;
						case ArgType.Rect:
							Rect r = (Rect)val;
							vec.x = r.x; vec.y = r.y; vec.z = r.width; vec.w = r.height;
							break;
						case ArgType.Coord:
							Coord c = (Coord)val;
							vec.x = c.x; vec.y = c.z;
							break;
						case ArgType.CoordRect: rect = (CoordRect)val; break;
						case ArgType.RectInt:
							RectInt ri = (RectInt)val;
							vec.x = ri.x; vec.y = ri.y; vec.z = ri.width; vec.w = ri.height;
							break;
						case ArgType.Matrix: matrix = (MatrixWorld)(Matrix)val; break;
						case ArgType.MatrixWorld: matrix = (MatrixWorld)val; break;
						case ArgType.Stripe: stripe = (Stripe)val; break;
						default: throw new ArgumentOutOfRangeException();
					}
				}

				public Argument Copy ()
				{
					Argument arg = new Argument();
					arg.vec = vec;
					arg.rect = rect;
					arg.matrix = new MatrixWorld(matrix);
					arg.stripe = new Stripe(stripe);

					arg.name = name;
					arg.argType = argType;

					return arg;
				}

				public static bool EqualValues (Argument a1, Argument a2)
				{
					return
						a1.vec == a2.vec  &&
						a1.rect == a2.rect  &&
						MatrixWorld.EqualValues(a1.matrix, a2.matrix, delta:0.0001f)  &&
						Stripe.EqualValues(a1.stripe, a2.stripe, delta:0.0001f)  &&
						a1.name == a2.name  &&
						a1.argType == a2.argType;
				}
			}

			private object[] GetArguments (Argument[] arguments) //providing arguments here to use the  same for regression
			{
				object[] args = new object[arguments.Length];
				for (int i=0; i<args.Length; i++)
					args[i] = arguments[i].GetVal();
				return args;
			}

			private Type[] GetArgumentsTypes (Argument[] arguments)
			{
				Type[] types = new Type[arguments.Length];
				for (int i=0; i<arguments.Length; i++)
					types[i] = arguments[i].GetArgType();
				return types;
			}

			private T GetArgument<T> (Argument[] arguments, string name)
			{ 
				int i = Array.FindIndex(arguments, av => av.name==name);
				if (i<0)
					throw new Exception("Could not find additional value of " + name);

				object obj = arguments[i].GetVal();
				if (!(obj is T))
					throw new Exception("Additional value of " + name + " is not " + typeof(T));

				return (T)obj;
			}

		#endregion


		private MethodInfo GetMethod (Argument[] arguments)
		{
			Type testType = ReflectionExtensions.GetTypeFromAllAssemblies(className);
			if (testType == null)
				throw new Exception("Type " + className + " is not found");

			BindingFlags staticFlags = BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public;
			BindingFlags nonstaticFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
			BindingFlags flags = BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
			Type[] types = GetArgumentsTypes(arguments); 

			//isNonStatic = false;
			//isReturnable = false;

			System.Reflection.MethodInfo testMethod = testType.GetMethod(fnName, types);
			if (testMethod == null)
				testMethod = testType.GetMethod(fnName, flags, null, types, null);
			/*if (testMethod == null)
			{
				testMethod = testType.GetMethod(fnName, staticFlags, null, ArrayTools.RemoveLast(types), null);
				isReturnable = true;
			}
			if (testMethod == null)
			{
				testMethod = testType.GetMethod(fnName, nonstaticFlags, null, ArrayTools.RemoveFirst(types), null);
				isNonStatic = true;
			}
			if (testMethod == null)
			{
				testMethod = testType.GetMethod(fnName, nonstaticFlags, null, ArrayTools.RemoveFirst(ArrayTools.RemoveLast(types)), null);
				isReturnable = true;
				isNonStatic = true;
			}*/

			if (testMethod == null)
				throw new Exception("Static method " + fnName + " not found with arguments " + GetArgumentsTypes(arguments).ToStringArray() + " in class " + className);


			return testMethod;
		}


		private void InvokeMethod (MethodInfo method)
		{
			result.SetVal( method.Invoke( obj.GetVal(), GetArguments(arguments) ) );
		}


		public void Test ()
		{
			MethodInfo testMethod = GetMethod(arguments);
			result.SetVal( testMethod.Invoke( obj.GetVal(), GetArguments(arguments) ) );
		}


		public long Benchmark (int iterations)
		/// returns ms
		{
			return 0;
		}


		public bool Regression ()
		{
			//cloning
			Argument newObj = regressionObj.Copy();
			Argument[] newArguments = new Argument[regressionArguments.Length];
			for (int a=0; a<regressionArguments.Length; a++)
				newArguments[a] = regressionArguments[a].Copy();
			Argument newRes = regressionRes.Copy();

			//invoking
			MethodInfo regressionMethod = GetMethod(newArguments);
			regressionRes.SetVal( regressionMethod.Invoke( regressionObj.GetVal(), GetArguments(newArguments) ) );

			//checking difference
			if (!Argument.EqualValues(newObj, regressionObj))
				return false;

			for (int a=0; a<regressionArguments.Length; a++)
				if (!Argument.EqualValues(regressionArguments[a], newArguments[a]))
					return false;

			if (!Argument.EqualValues(newRes, regressionRes))
				return false;

			return true;
		}

		public void StoreRegression ()
		{
			regressionObj = obj.Copy();

			regressionArguments = new Argument[arguments.Length];
			for (int a=0; a<regressionArguments.Length; a++)
				regressionArguments[a] = arguments[a].Copy();

			regressionRes = result.Copy();
		}
	}
}
