using System.Runtime.InteropServices;
using System;
using UnityEngine;
using Den.Tools;
using Den.Tools.Segs;

[CreateAssetMenu(fileName = "Vector3performanceScrObj", menuName = "Test/Vector3 Performance")]
[ExecuteInEditMode]
public class Vector3performanceScrObj : ScriptableObject, ISerializationCallbackReceiver
{
    public bool test;
	public int iterations = 1000;
	public int tries = 10;
	public double testRefTime;
	public double testUnityTime;
	public double testStructTime;



	public void OnBeforeSerialize ()
	{
		if (!test)
			return;
		test = false;

		testUnityTime = float.MaxValue;
		testStructTime = float.MaxValue;

		for (int t=0; t<tries; t++)
		{
			DateTime startUnityTime = DateTime.Now;

				for (int i=0; i<iterations; i++)
				{
					Spline spline = new Spline(42);
					for (int n=0; n<spline.count; n++)
						spline.nodes[n] = new Vector2D(n*n,n).Vector3D();

					SplineSys sys = new SplineSys(new Spline[]{spline});
					float tmp = BeizerOps.ApproxLengthBeizer(spline);
					spline.Split(10, 0.5f);
					spline.Relax(0.5f, 10);
					spline.Optimize(0.1f);

					/*Vector3 vec = new Vector3(1,2,3);
					float length = vec.sqrMagnitude;
					Vector3 unnorm = vec * length;
					unnorm *= 2;
					vec += unnorm - vec;*/
				}

			double unityTime = (DateTime.Now - startUnityTime).TotalMilliseconds;
			if (unityTime < testUnityTime)
				testUnityTime = unityTime;


			DateTime startStructTime = DateTime.Now;

				for (int i=0; i<iterations; i++)
				{
					Vector3D vec = new Vector3D(1,2,3);
					float length = vec.magnitude;
					Vector3D unnorm = vec * length;
					unnorm *= 2;
					vec += unnorm - vec;
				}

			double structTime = (DateTime.Now - startStructTime).TotalMilliseconds;
			if (structTime < testStructTime)
				testStructTime = structTime;


			DateTime startRefTime = DateTime.Now;

			for (int i=0; i<iterations; i++)
			{

			}

			testRefTime = (DateTime.Now - startRefTime).TotalMilliseconds;
		}


		Debug.Log("Test");
	}

	public void OnAfterDeserialize ()
	{
		
	}
}
