using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools.Matrices;
using Den.Tools.GUI;
using static Den.Tools.Matrices.MatrixOps;
using UnityEditor;
using System;


namespace Den.Tools.Tests
{
	[CreateAssetMenu(fileName = "RegressionSet", menuName = "Test/RegressionSet")]
	public class RegressionSet : ScriptableObject
	{
		public UniversalTestAsset[] tests;
		public RegressionSet[] sets;

		public bool result;

		public bool Test (out Dictionary<UniversalTestAsset,bool> testsPassed, out Dictionary<RegressionSet,bool> setsPassed)
		{
			testsPassed = new Dictionary<UniversalTestAsset, bool>();
			setsPassed = new Dictionary<RegressionSet, bool>();
			return TestRecursive(ref testsPassed, ref setsPassed);
		}

		private bool TestRecursive (ref Dictionary<UniversalTestAsset,bool> testsPassed, ref Dictionary<RegressionSet,bool> setsPassed)
		{
			bool res = true;

			if (tests != null)
				foreach (UniversalTestAsset test in tests)
				{
					bool r = test.Regression();
					res = res && r;

					if (!r) //forcing re-write if false
						testsPassed.ForceAdd(test,false);
					else
						testsPassed.TryAdd(test,true);
				}

			if (sets != null)
				foreach (RegressionSet test in sets)
				{
					bool r = test.TestRecursive(ref testsPassed, ref setsPassed);
					res = res && r;

					if (!r)
						setsPassed.ForceAdd(test,false);
					else
						setsPassed.TryAdd(test,true);
				}

			result = res;
			return res;
		}
	}
}