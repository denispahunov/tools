﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools;
using Den.Tools.Tests;
using Den.Tools.Matrices;
using Den.Tools.GUI;
using Den.Tools.Segs;


namespace Den.Tools.Tests
{
	public static class PathfindingTestHelpers
	{
		public static void PrepareWeights (Matrix weights)
		{
			float maxVal = 0;
			for (int i=0; i<weights.arr.Length; i++)
				if (weights.arr[i] < float.MaxValue && weights.arr[i] > maxVal)
					maxVal = weights.arr[i];
			if (maxVal != 0)
				weights.Multiply(1f / maxVal);

			for (int i=0; i<weights.arr.Length; i++)
			{
				if (float.IsNaN(weights.arr[i])) 
					throw new Exception("Is NaN");
			}
		}

		public static void DrawDirs (Matrix2D<Coord> dirs)
		{
			DebugGizmos.Clear("DrawDirs");

			for (int x = dirs.rect.offset.x; x<dirs.rect.offset.x+dirs.rect.size.x; x++)
				for (int z = dirs.rect.offset.z; z<dirs.rect.offset.z+dirs.rect.size.z; z++)
			{
				Coord dir = dirs[x,z];
				if (dir.x==0 && dir.z==0) continue;

				Vector3 center = new Vector3(x, 0, z);
				DebugGizmos.DrawLine("DrawDirs", center, center+dir.Vector3()/2, Color.yellow, additive:true);
				DebugGizmos.DrawDot("DrawDirs", center, 2, Color.yellow, additive:true);
			}
		}
	}


	[Test(category="Pathfinding")]
	public class FactorsStructTest : ITest
	/// Just tests gizmo drawing
	{
		[Val("Incline")] public float incline = 0.25f;
		[Val("Lowland")] public float lowland = 0.25f;
		[Val("Highland")] public float highland = 0;
		[Val("Straighten")] public float straighten = 0.25f;
		[Val("Distance")] public float distance = 0.25f;

		Pathfinding.Factors factors = new Pathfinding.Factors();

		public void Test ()
		{

		}
	}


	[Test(category="Pathfinding")]
	public class MaskOnlyTest : ITest, ISceneGUI
	{
		[Val("From")] public Vector2D from;
		[Val("To")] public Vector2D to;

		[Val("Mask")] public MatrixAsset maskAsset;
		[Val("Iterations")] public int iterations = 10;
		[NonSerialized] private Matrix mask;
		[NonSerialized] private MatrixTextureGizmo gizmo;

		[Val("Mask/Weights")] public bool maskWeightsSwitch;

		[NonSerialized] private Vector3[] vPath;

		public void OnSceneGUI ()
		{
			gizmo?.Draw();

			from = DebugGizmos.PositionHandle( from.Vector3() ).Vector2D();
			to = DebugGizmos.PositionHandle( to.Vector3() ).Vector2D();

			if (vPath!=null) DebugGizmos.DrawPolyLine("PathfindingTest", vPath, color:Color.red);
		}

		public void Test ()
		{
			if (maskAsset?.matrix == null) return;
			mask = maskAsset.matrix;

			if (gizmo==null) gizmo = new MatrixTextureGizmo();
			gizmo.SetOffsetSize(mask.rect.offset.vector2d, mask.rect.size.vector2d);
			gizmo.SetMatrix(mask);

			Matrix2D<Coord> dirs = new Matrix2D<Coord>(mask.rect);
			Pathfinding.FillDirs(dirs, (Coord)to, null, mask, new Pathfinding.Factors(), maxIterations:iterations);
			Coord[] cPath = Pathfinding.DrawPath(dirs, (Coord)from, (Coord)to);

			vPath = Array.ConvertAll(cPath,c=>c.Vector3());
		}
	}


	[Test(category="Pathfinding")]
	public class PathfindingTest : ITest, ISceneGUI
	{
		[Val("From")] public Vector2D from;
		[Val("To")] public Vector2D to;

		[Val("Mask")] public MatrixAsset maskAsset;
		[Val("Height")] public MatrixAsset heightAsset;
		[Val("WorldHeight")] public float heightVal;
		[Val("Max Iterations")] public int iterations = -1;

		[NonSerialized] private Matrix mask;
		[NonSerialized] private Matrix height;
		[NonSerialized] private MatrixTextureGizmo maskGizmo;
		[NonSerialized] private MatrixTextureGizmo heightGizmo;

		public enum Display { Mask, Height };
		[Val("Mask/Height")] public Display display;

		[Val("Incline")] public float incline = 1;
		[NonSerialized] private Pathfinding.Factors factors = new Pathfinding.Factors();

		[NonSerialized] private Vector3[] vPath;

		public void OnSceneGUI ()
		{
			if (display == Display.Mask) maskGizmo?.Draw();
			else heightGizmo?.Draw();

			from = (Vector2D)DebugGizmos.PositionHandle( from.Vector3() ).Vector2D();
			to = (Vector2D)DebugGizmos.PositionHandle( to.Vector3() ).Vector2D();

			if (vPath!=null) DebugGizmos.DrawPolyLine("PathfindingTest", vPath, color:Color.red);
		}

		public void Test ()
		{
			if (maskAsset?.matrix == null && heightAsset.matrix == null) return;

			if (maskAsset?.matrix != null)
			{
				mask = maskAsset.matrix;
				if (maskGizmo==null) maskGizmo = new MatrixTextureGizmo();
				maskGizmo.SetOffsetSize(mask.rect.offset.vector2d, mask.rect.size.vector2d);
				maskGizmo.SetMatrix(mask);
			}

			MatrixWorld heightWorld = null;
			if (heightAsset?.matrix != null)
			{
				height = heightAsset.matrix;
				if (heightGizmo==null) heightGizmo = new MatrixTextureGizmo();
				heightGizmo.SetOffsetSize(height.rect.offset.vector2d, height.rect.size.vector2d);
				heightGizmo.SetMatrix(height);
				heightWorld = new MatrixWorld(height, height.rect.offset.Vector3D, height.rect.size.Vector3D);
				heightWorld.worldSize.y = heightVal;
			}

			factors.incline = incline;

			Matrix2D<Coord> dirs = new Matrix2D<Coord>(mask.rect);
			Pathfinding.FillDirs(dirs, (Coord)to, heightWorld, mask, factors, maxIterations:iterations);
			Coord[] cPath = Pathfinding.DrawPath(dirs, (Coord)from, (Coord)to);

			vPath = Array.ConvertAll(cPath,c=>c.Vector3());
		}
	}


	[Test(category="Pathfinding")]
	public class DrawPathTest : ITest, ISceneGUI
	{
		[Val("From")] public Vector2D from;
		[Val("To")] public Vector2D to;

		[Val("Mask")] public MatrixAsset maskAsset;
		[Val("Iterations")] public int iterations = 10;
		[Val("Snapshot")] public int snapshot = 23;
		[NonSerialized] private Matrix mask;
		[NonSerialized] private MatrixTextureGizmo gizmo;

		[NonSerialized] private Vector3[] vPath;


		public void OnSceneGUI ()
		{
			gizmo?.Draw();

			from = DebugGizmos.PositionHandle( from.Vector3() ).Vector2D();
			to = DebugGizmos.PositionHandle( to.Vector3() ).Vector2D();

			if (vPath!=null) DebugGizmos.DrawPolyLine("PathfindingTest", vPath, color:Color.red);
		}

		public void Test ()
		{
			if (maskAsset?.matrix == null) return;
			mask = maskAsset.matrix;

			if (gizmo==null) gizmo = new MatrixTextureGizmo();
			gizmo.SetOffsetSize(mask.rect.offset.vector2d - new Vector2D(0.5f, 0.5f), mask.rect.size.vector2d);

			Matrix2D<Coord> dirs = new Matrix2D<Coord>(mask.rect);
			Matrix weights = new Matrix(mask.rect);
			Pathfinding.FillDirs(dirs, (Coord)to, null, mask, new Pathfinding.Factors(), weights, maxIterations:iterations);
			Coord[] cPath = Pathfinding.DrawPath(dirs, (Coord)from, (Coord)to);

			vPath = Array.ConvertAll(cPath,c=>c.Vector3());

			PathfindingTestHelpers.PrepareWeights(weights);
			gizmo.SetMatrix(weights);

			PathfindingTestHelpers.DrawDirs(dirs);
		}
	}
}