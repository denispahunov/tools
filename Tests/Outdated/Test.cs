using Den.Tools;
using Den.Tools.Matrices;
using Den.Tools.GUI;
using System;
using UnityEngine;

//Start with this template:
/*

using UnityEngine;

using Den.Tools.Matrices;
using System;

namespace Den.Tools.Tests
{
	[CreateAssetMenu(fileName = "StripeTester", menuName = "Test/StripeTester")]
	public class StripeTester : TestAsset, ITestInspectorGUI, ITestRegression  
	/// Remember it should be named same as file!
	{
		public override void Test ()
		{
			dst.Clear();

			System.Reflection.MethodInfo testMethod = typeof(Stripe).GetMethod(testFunction.ToString());
			testMethod.Invoke(null, new object[] { src, dst} );
		}

		public bool Regression () 
		{}

		public void StoreRegression () 
		{}

		public void OnInspectorGUI () 
		{}
*/


namespace Den.Tools.Tests
{
	public interface ITest { void Test (); }
	public interface IBenchmark { void Benchmark (int iterations, System.Diagnostics.Stopwatch timer); }
	public interface ISceneGUI { void OnSceneGUI (); }
	public interface IInspectorGUI { void OnInspectorGUI (); }

	public interface ITestBenchmark { public abstract long Benchmark (int iterations); } //returns summary iterations time in ms
	public interface ITestSceneGUI { void OnSceneGUI (); }
	public interface ITestInspectorGUI { void OnInspectorGUI (); }
	public interface ITestRegression { bool Regression (); void StoreRegression (); }

	public sealed class TestAttribute : Attribute
	{
		public string category;
	}

	public abstract class TestAsset : ScriptableObject
	{
		public abstract void Test ();
	}
}