﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Reflection;

using Den.Tools.GUI;
using NUnit.Framework.Internal;
using static Den.Tools.Tests.UniversalTestAsset;

namespace Den.Tools.Tests
{
	[CustomEditor(typeof(UniversalTestAsset))]
	[ExecuteInEditMode]
	public class UniversalTestAssetInspector : Editor
	{
		protected UI ui = new UI();
		protected static bool guiBenchmark = false;

		protected string guiSelectedCat;

		Dictionary<TestAttribute,Type> allTestTypes = null;
		Dictionary<string,(Type[],string[])> catTestTypes; //sorted by category, paired with naimes
		string[] catNames;

		private bool regressionTested = false;
		private bool regressionResult = false;

		private static Texture2D regressionUntestedIcon;
		private static Texture2D regressionPassedIcon;
		private static Texture2D regressionFailedIcon;
		public static Texture2D RegressionIcon (bool tested, bool passed)
		{
			if (regressionUntestedIcon == null) regressionUntestedIcon = EditorGUIUtility.IconContent("TestNormal").image as Texture2D;
			if (regressionPassedIcon == null) regressionPassedIcon = EditorGUIUtility.IconContent("TestPassed").image as Texture2D;
			if (regressionFailedIcon == null) regressionFailedIcon = EditorGUIUtility.IconContent("TestFailed").image as Texture2D;

			Texture2D icon;
			if (tested)
				icon = passed ?
					regressionPassedIcon : 
					regressionFailedIcon;
			else
				icon = regressionUntestedIcon;

			return icon;
		}


		#region Test On Scene Change Event
/*
			private static HashSet<UniversalTesterAsset> updatedTesters = new HashSet<UniversalTesterAsset>();

			[RuntimeInitializeOnLoadMethod, UnityEditor.InitializeOnLoadMethod] 
			public static void Subscribe () 
			{ 
				#if UNITY_2019_1_OR_NEWER
				SceneView.duringSceneGui -= OnSceneView;
				SceneView.duringSceneGui += OnSceneView;
				#else
				SceneView.onSceneGUIDelegate -= OnSceneView;
				SceneView.onSceneGUIDelegate += OnSceneView;
				#endif

				UnityEditor.Undo.willFlushUndoRecord-= OnSceneUpdate;
				UnityEditor.Undo.willFlushUndoRecord += OnSceneUpdate;
			}

			public static void OnSceneUpdate ()
			{
				UniversalTesterAsset[] allTesters = FindObjectsOfType<UniversalTesterAsset>();
				//TODO: cache it?
				if (allTesters.Length == 0) return;

				foreach (UniversalTesterAsset tester in allTesters)
				{
					//if (!tester.isActiveAndEnabled) continue;
					
					if (tester.testOnSceneChange) //testing first
						tester.Test();
				}
			}

			public static void OnSceneView (SceneView sceneView) 
			{
				UniversalTesterAsset[] allTesters = FindObjectsOfType<UniversalTesterAsset>();
				//TODO: cache it?
				if (allTesters.Length == 0) return;

				foreach (UniversalTesterAsset tester in allTesters)
				{
					//if (!tester.isActiveAndEnabled) continue;

					if (tester is ISceneGUI sceneTester) //then drawing gui
						sceneTester.OnSceneGUI();
				}
			}

			private static int prevDirtyId = -1;
			private static bool mousePressed = false;

			private static bool IsSceneChanged ()
			{
				//Debug.Log(Event.current.button);

				//if (Event.current.rawType == EventType.MouseDrag) { Debug.Log("MouseDown"); mousePressed = true; }
				//if (Event.current.

				

				//Debug.Log(mousePressed);

				//testing only when dirty id is changed (but drawing all anyways)
				int currDirtyId = UnityEditor.Undo.GetCurrentGroup(); //SceneManager.GetActiveScene().GetDirtyId();
				bool sceneChanged = currDirtyId != prevDirtyId  &&  !mousePressed; //no change could be made while mouse pressed
				if (sceneChanged) prevDirtyId = currDirtyId;
				
				return sceneChanged;
			}
*/
		#endregion


		public virtual void OnSceneGUI()
		{
			/*UniversalTestAsset tester = (UniversalTestAsset)target;
			if (tester.test == null) return;

			if (tester.test is ISceneGUI sceneTester)
				sceneTester.OnSceneGUI();*/
		}


		public override void OnInspectorGUI ()
		{
			EditorGUILayout.PropertyField(serializedObject.FindProperty("className"), true);
			EditorGUILayout.PropertyField(serializedObject.FindProperty("fnName"), true);

			EditorGUILayout.Space();

			SerializedProperty objProperty = serializedObject.FindProperty("obj");
			objProperty.isExpanded = EditorGUILayout.Foldout(objProperty.isExpanded, "Obj");
			if (objProperty.isExpanded)
				EditorGUILayout.PropertyField(objProperty, true);

			EditorGUILayout.PropertyField(serializedObject.FindProperty("arguments"), true);

			SerializedProperty resultProperty = serializedObject.FindProperty("result");
			resultProperty.isExpanded = EditorGUILayout.Foldout(resultProperty.isExpanded, "Result");
			if (resultProperty.isExpanded)
				EditorGUILayout.PropertyField(resultProperty, true);

			EditorGUILayout.Space();

			// Draw regression properties in a foldout
			SerializedProperty regressionProperty = serializedObject.FindProperty("regressionArguments");
			regressionProperty.isExpanded = EditorGUILayout.Foldout(regressionProperty.isExpanded, "Regression");
			if (regressionProperty.isExpanded)
			{
				EditorGUI.indentLevel++;

				SerializedProperty robjProperty = serializedObject.FindProperty("regressionObj");
				robjProperty.isExpanded = EditorGUILayout.Foldout(robjProperty.isExpanded, "Obj");
				if (robjProperty.isExpanded)
					EditorGUILayout.PropertyField(robjProperty, true);

				EditorGUILayout.PropertyField(serializedObject.FindProperty("regressionArguments"), true);

				SerializedProperty rresultProperty = serializedObject.FindProperty("regressionRes");
				rresultProperty.isExpanded = EditorGUILayout.Foldout(rresultProperty.isExpanded, "Result");
				if (rresultProperty.isExpanded)
					EditorGUILayout.PropertyField(rresultProperty, true);

				EditorGUI.indentLevel--;
			}

			// Draw test buttons
			EditorGUILayout.Space();
			DrawTestButtons();

			// Apply changes to the serialized object
			serializedObject.ApplyModifiedProperties();
		}

		private void CheckArgumentsUnique ()
		// array inspector tends to duplicate class on 
		{

		}


		private void DrawGUI ()
		//if at some point will like to convert it to Tools.UI
		{
			UniversalTestAsset tester = (UniversalTestAsset)target;

			if (ui.undo==null) ui.undo = new GUI.Undo();
			ui.undo.undoObject = tester;

			Cell.EmptyLinePx(6);
		}


		protected void DrawTestButtons ()
		{
			UniversalTestAsset test = (UniversalTestAsset)target;

			//Test
			if (GUILayout.Button("Test"))
			{
				test.Test();
				SceneView.RepaintAll();
				Repaint();
				EditorUtility.SetDirty(test);
			}

			//Benchmark
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Benchmark"))
			{
				long elapsedTime = test.Benchmark(test.benchmarkIterations);
				Debug.Log("Benchmark " + target.name + ": " + elapsedTime + "ms" + "(" + (float)elapsedTime/test.benchmarkIterations + " ms per iteration)");
				EditorUtility.SetDirty(test);
			}
			test.benchmarkIterations = EditorGUILayout.IntField(test.benchmarkIterations);
			EditorGUILayout.EndHorizontal();

			//Regression
			using (new EditorGUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Regression"))
				{
					regressionTested = true;
					regressionResult = test.Regression();
					EditorUtility.SetDirty(test);
				}

				if (GUILayout.Button("Store", GUILayout.Width(60)))
				{
					if (EditorUtility.DisplayDialog("Confirmation", "Are you sure you want to store the regression?", "Yes", "No"))
					{
						test.StoreRegression();

						//testing regression to make sure
						regressionTested = true;
						regressionResult = test.Regression();

						EditorUtility.SetDirty(test);
					}
				}



				Texture2D icon;
				if (regressionTested)
					icon = regressionResult ?
						EditorGUIUtility.IconContent("TestPassed").image as Texture2D : 
						EditorGUIUtility.IconContent("TestFailed").image as Texture2D;
				else
					icon = EditorGUIUtility.IconContent("TestNormal").image as Texture2D;

				GUILayout.Label(icon, GUILayout.Width(20), GUILayout.Height(20));
			}
		}
	}



	


	[CustomPropertyDrawer(typeof(UniversalTestAsset.Argument), useForChildren:true)] 
    public class ArgumentPropertyDrawer : PropertyDrawer  
    {
		//UI ui = new UI();
		UniversalTestAsset.Argument argument;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			argument = property.GetTargetObjectOfProperty() as UniversalTestAsset.Argument;
			//argument = property.boxedValue as UniversalTestAsset.Argument;

			//ui.Draw(DrawGUI, inInspector:true);

			//EditorGUI.PropertyField(position, property.FindPropertyRelative("matrix"), new GUIContent(argument.name));
			//return; 

			Rect nameRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), new GUIContent("Name"));

			Rect valueRect = new Rect(position.x,position.y + 2 * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing),position.width,EditorGUIUtility.singleLineHeight);
			Rect typeRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, position.width, EditorGUIUtility.singleLineHeight);
			EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("argType"), new GUIContent("Type"));

			
			switch (argument.argType)
			{
				case UniversalTestAsset.Argument.ArgType.Bool:
					argument.SetVal(EditorGUI.Toggle(valueRect, argument.name,(bool)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Int:
					argument.SetVal(EditorGUI.IntField(valueRect, argument.name,(int)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Float:
					float f = (float)argument.GetVal();
					f = EditorGUI.FloatField(valueRect, argument.name, f);
					argument.SetVal(f);
					break; 
				//case UniversalTestAsset.Argument.ArgType.String:
				//	argument.SetVal(EditorGUI.TextField(valueRect, argument.name,(string)argument.GetVal()));
				//	break;
				//case UniversalTestAsset.Argument.ArgType.Bool:
				//	argument.SetVal(EditorGUI.Toggle(valueRect, argument.name,(bool)argument.GetVal()));
				//	break;
				case UniversalTestAsset.Argument.ArgType.Vector2:
					argument.SetVal(EditorGUI.Vector2Field(valueRect, argument.name,(Vector2)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Vector2D:
					argument.SetVal(new Vector2D(
						EditorGUI.FloatField(new Rect(valueRect.x,valueRect.y,valueRect.width / 2 - 2,valueRect.height),"X",((Vector2D)argument.GetVal()).x),
						EditorGUI.FloatField(new Rect(valueRect.x + valueRect.width / 2 + 2,valueRect.y,valueRect.width / 2 - 2,valueRect.height),"Z",((Vector2D)argument.GetVal()).z)
					));
					break;
				case UniversalTestAsset.Argument.ArgType.Vector3:
					argument.SetVal(EditorGUI.Vector3Field(valueRect, argument.name,(Vector3)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Vector4:
					argument.SetVal(EditorGUI.Vector4Field(valueRect, argument.name,(Vector4)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Rect:
					argument.SetVal(EditorGUI.RectField(valueRect, argument.name,(Rect)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Coord:
					argument.SetVal(new Coord(
					EditorGUI.IntField(new Rect(valueRect.x,valueRect.y,valueRect.width / 2 - 2,valueRect.height),"X",((Coord)argument.GetVal()).x),
					EditorGUI.IntField(new Rect(valueRect.x + valueRect.width / 2 + 2,valueRect.y,valueRect.width / 2 - 2,valueRect.height),"Z",((Coord)argument.GetVal()).z)
					));
					break;
				case UniversalTestAsset.Argument.ArgType.CoordRect:
					argument.SetVal(new CoordRect(
						EditorGUI.IntField(new Rect(valueRect.x,valueRect.y,valueRect.width / 4 - 2,valueRect.height),"Offset X",((CoordRect)argument.GetVal()).offset.x),
						EditorGUI.IntField(new Rect(valueRect.x + valueRect.width / 4 + 2,valueRect.y,valueRect.width / 4 - 2,valueRect.height),"Offset Z",((CoordRect)argument.GetVal()).offset.z),
						EditorGUI.IntField(new Rect(valueRect.x + valueRect.width / 2 + 4,valueRect.y,valueRect.width / 4 - 2,valueRect.height),"Size X",((CoordRect)argument.GetVal()).size.x),
						EditorGUI.IntField(new Rect(valueRect.x + 3 * valueRect.width / 4 + 6,valueRect.y,valueRect.width / 4 - 2,valueRect.height),"Size Z",((CoordRect)argument.GetVal()).size.z)
					));
					break;
				case UniversalTestAsset.Argument.ArgType.RectInt:
					argument.SetVal(EditorGUI.RectIntField(valueRect, argument.name,(RectInt)argument.GetVal()));
					break;
				case UniversalTestAsset.Argument.ArgType.Matrix:
				case UniversalTestAsset.Argument.ArgType.MatrixWorld:
					EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("matrix"), new GUIContent(argument.name));
					// EditorGUI.PropertyField(typeRect, property, label, true);
					break;
				case UniversalTestAsset.Argument.ArgType.Stripe:
					EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("stripe"), new GUIContent(argument.name));
					break;
				
			}
		}


		public override float GetPropertyHeight(SerializedProperty property,GUIContent label)
		{
			//argument = property.GetTargetObjectOfProperty() as UniversalTestAsset.Argument;
			argument = property.boxedValue as UniversalTestAsset.Argument;
			float height = EditorGUIUtility.singleLineHeight*3 + EditorGUIUtility.standardVerticalSpacing*2 + 4;

			switch (argument.argType)
			{
				case UniversalTestAsset.Argument.ArgType.Rect:
				case UniversalTestAsset.Argument.ArgType.RectInt:
					case UniversalTestAsset.Argument.ArgType.CoordRect:
					height += EditorGUIUtility.singleLineHeight;
					break;
				case UniversalTestAsset.Argument.ArgType.Matrix:
				case UniversalTestAsset.Argument.ArgType.MatrixWorld:
					height += EditorGUIUtility.singleLineHeight * 9;
					break;
				case UniversalTestAsset.Argument.ArgType.Stripe:
					height += EditorGUIUtility.singleLineHeight * 3;
					break;
			}

			return height;
		}
	}
}
