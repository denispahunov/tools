﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Den.Tools
{
	[System.Serializable]
	public class DebugGizmosWindow : EditorWindow
	{
		private bool enabled = true;

		private void OnGUI () 
		{
			EditorGUI.BeginChangeCheck();

			enabled = EditorGUILayout.ToggleLeft("Enabled", enabled);

			#if UNITY_2019_1_OR_NEWER
				SceneView.duringSceneGui -= DebugGizmos.Draw;
				if (enabled) SceneView.duringSceneGui += DebugGizmos.Draw;
			#else
				SceneView.onSceneGUIDelegate -= DebugGizmos.Draw;
				if (enabled) SceneView.onSceneGUIDelegate += DebugGizmos.Draw;
			#endif



			EditorGUILayout.Space();

			foreach (var kvp in DebugGizmos.allGizmos)
			{
				string name = kvp.Key;
				DebugGizmos.GizmoGroup group = kvp.Value; 

				using (new EditorGUILayout.HorizontalScope())
				{
					group.enabled = EditorGUILayout.Toggle(group.enabled, GUILayout.Width(20));
					EditorGUILayout.LabelField(name, GUILayout.MinWidth(30));

					EditorGUILayout.Space();

					group.color = EditorGUILayout.ColorField(group.color, GUILayout.Width(50));
					group.colorOverride = EditorGUILayout.Toggle(group.colorOverride, GUILayout.Width(20));

					EditorGUILayout.Space();

					if (GUILayout.Button("Clear", GUILayout.Width(50)))
						group.gizmos.Clear();
				}
			}

			EditorGUILayout.Space();

			if (GUILayout.Button("Clear All", GUILayout.Width(80)))
				DebugGizmos.allGizmos.Clear();

			if (EditorGUI.EndChangeCheck() && SceneView.lastActiveSceneView != null)
				SceneView.lastActiveSceneView.Repaint();
				
		}

		[MenuItem ("Window/Test/Debug Gizmos")]
		public static void ShowWindow ()
		{
			DebugGizmosWindow window = (DebugGizmosWindow)GetWindow(typeof (DebugGizmosWindow));
			window.position = new Rect(100,100,300,800);
			window.titleContent = new GUIContent("Debug Gizmos");
		}
	}
}