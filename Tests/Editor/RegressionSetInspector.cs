using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using NUnit.Framework.Internal;

namespace Den.Tools.Tests
{
	[CustomEditor(typeof(RegressionSet))]
	public class RegressionSetEditor : Editor
	{
		private Dictionary<UniversalTestAsset,bool> testsPassed = new Dictionary<UniversalTestAsset,bool>(); //if not in dict - not tested
		private Dictionary<RegressionSet,bool> setsPassed = new Dictionary<RegressionSet,bool>();
		private bool tested; 
		private bool passed;

		public override void OnInspectorGUI()
		{
			//base.OnInspectorGUI();

			RegressionSet regSet = (RegressionSet)target;

			//arrays
			SerializedProperty testsProperty = serializedObject.FindProperty("tests");
			EditorGUILayout.PropertyField(testsProperty, new GUIContent("Tests"), true);

			SerializedProperty setsProperty = serializedObject.FindProperty("sets");
			EditorGUILayout.PropertyField(setsProperty, new GUIContent("Sets"), true);

			//tests
			EditorGUILayout.Space();
			DrawTestRecursively(testsProperty, setsProperty);

			//button
			EditorGUILayout.Space();
			using (new EditorGUILayout.HorizontalScope())
			{
				if (GUILayout.Button("Test"))
				{
					tested = true;
					passed = regSet.Test(out testsPassed, out setsPassed);
				}
				
				Texture2D icon = UniversalTestAssetInspector.RegressionIcon(tested, passed);
				GUILayout.Label(icon, GUILayout.Width(20), GUILayout.Height(20));
			}

			serializedObject.ApplyModifiedProperties();
		}


		public void DrawTestRecursively (SerializedProperty testsProperty, SerializedProperty setsProperty)
		{
			if (EditorGUI.indentLevel > 10)
				throw new System.Exception("Repeating recursive");

			//tests
			if (testsProperty.isArray)
			{
				
				for (int i = 0; i < testsProperty.arraySize; i++)
				{
					EditorGUILayout.BeginHorizontal();

					SerializedProperty testProperty = testsProperty.GetArrayElementAtIndex(i);
					EditorGUILayout.PropertyField(testProperty);

					bool tested = testsPassed.TryGetValue((UniversalTestAsset)(testProperty.boxedValue), out bool passed);
					Texture2D icon = UniversalTestAssetInspector.RegressionIcon(tested, passed);
					GUILayout.Label(icon, GUILayout.Width(20), GUILayout.Height(20));

					EditorGUILayout.EndHorizontal();
				}
			}

			//sets
			if (setsProperty.isArray)
			{
				for (int i = 0; i < setsProperty.arraySize; i++)
				{
					SerializedProperty setProperty = setsProperty.GetArrayElementAtIndex(i);
					SerializedObject assetObject = new SerializedObject(setProperty.objectReferenceValue);

					SerializedProperty subTestsProperty = assetObject.FindProperty("tests");
					SerializedProperty subSetsProperty = assetObject.FindProperty("sets");

					assetObject.Update();
					using (new EditorGUILayout.HorizontalScope())
					{
						setProperty.isExpanded = EditorGUILayout.Foldout(setProperty.isExpanded, assetObject.targetObject.name);

						bool tested = setsPassed.TryGetValue((RegressionSet)(setProperty.boxedValue), out bool passed);
						Texture2D icon = UniversalTestAssetInspector.RegressionIcon(tested, passed);
						GUILayout.Label(icon, GUILayout.Width(20), GUILayout.Height(20));
					}
					if (setProperty.isExpanded)
					{
						EditorGUI.indentLevel++;
						DrawTestRecursively(subTestsProperty, subSetsProperty);
						EditorGUI.indentLevel--;
					}
				}
			}
		}
	}

	//[CustomPropertyDrawer(typeof(UniversalTestAsset), useForChildren:true)]
	public class TestAssetDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{

		}
	}
}