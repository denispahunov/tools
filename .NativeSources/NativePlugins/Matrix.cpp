
#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#include <iostream>
#include <cstdio>
#include <ctime>
#include <string>
#include <algorithm>
#include <iterator>
#include <math.h>

#include "Matrix.h"
#include "Line.h"
#include "Vector2.h"
#include "Noise.h"

using namespace std;

extern "C"
{
	Matrix::Matrix() { }

	Matrix::Matrix(const CoordRect &r) : rect(r)
	{
		count = rect.size.x*rect.size.z;
		arr = new float[count];
		for (int i = 0; i<count; i++) arr[i] = 0;
	}

	Matrix::Matrix(const CoordRect &r, float a[]) : rect(r), arr(a) 
	{ 
		count = rect.size.x*rect.size.z;
	}

	Matrix::Matrix(const Matrix &m) : rect(m.rect), count(m.count)
	{
		arr = new float[count];
		//copy(arr, arr + sizeof(arr)/sizeof(*arr), resultArr);
		for (int i = 0; i < count; i++)
			arr[i] = m.arr[i];
	}

	//~Matrix() { delete arr; }

	float Matrix::GetVal(int x, int z) const { return arr[(z - rect.offset.z)*rect.size.x + x - rect.offset.x]; }
	void Matrix::SetVal(int x, int z, float val) { arr[(z - rect.offset.z)*rect.size.x + x - rect.offset.x] = val; }

	inline float Matrix::operator[] (Coord c) { return arr[(c.z - rect.offset.z)*rect.size.x + c.x - rect.offset.x]; }
	inline float& Matrix::operator() (int x, int z) { return arr[(z - rect.offset.z)*rect.size.x + x - rect.offset.x]; }
	inline float Matrix::operator() (int x, int z) const { return arr[(z - rect.offset.z)*rect.size.x + x - rect.offset.x]; }

	/*inline Matrix& Matrix::operator= (Matrix& const m)
	{
		rect = m.rect;
		count = m.count;
		if (this != &m)  { delete arr; arr = m.arr; }
		return *this;
	};*/


	//float Matrix::operator[] (int i) const { return arr[i]; }
	//float Matrix::operator() (Coord c) const { return (c.x, c.z); }
	
	//float Matrix::operator() (int x, int z) const { return arr[rect(x,z)]; }
		

	inline float Matrix::GetFloored(float fx, float fz) const
	{
		int ix = (int)(float)fx; if (fx < 0) ix--;
		int iz = (int)(float)fz; if (fz < 0) iz--;

		return arr[(iz - rect.offset.z)*rect.size.x + ix - rect.offset.x];
	}


	inline float Matrix::GetInterpolated(float fx, float fz, bool roundToShort /*false*/) const
	{
		int ix = (int)fx; if (fx < 0) ix--; if (ix == rect.offset.x + rect.size.x) ix--;
		int iz = (int)fz; if (fz < 0) iz--; if (iz == rect.offset.z + rect.size.z) iz--;

		float xPercent = fx - ix;
		float zPercent = fz - iz;

		if (ix < rect.offset.x) ix = rect.offset.x;
		if (iz < rect.offset.z) iz = rect.offset.z;
		int ix1 = ix + 1;  if (ix1 >= rect.offset.x + rect.size.x) ix1 = rect.offset.x + rect.size.x - 1;
		int iz1 = iz + 1;  if (iz1 >= rect.offset.z + rect.size.z) iz1 = rect.offset.z + rect.size.z - 1;

		float val1 = GetVal(ix, iz);
		float val2 = GetVal(ix1, iz);
		//if (roundToShort) { val1 = (float)(int)(val1*32767)/32767; val2 = (float)(int)(val2*32767)/32767; }
		float val3 = val1 * (1 - xPercent) + val2 * xPercent;

		float val4 = GetVal(ix, iz1);
		float val5 = GetVal(ix1, iz1);
		//if (roundToShort) { val3 = (float)(int)(val3*32767)/32767; val4 = (float)(int)(val4*32767)/32767; }
		float val6 = val4 * (1 - xPercent) + val5 * xPercent;

		return val3 * (1 - zPercent) + val6 * zPercent;
	}


	inline float Matrix::GetInterpolatedBicubic(float x, float z, TileMode tileMode) const
	/// Probably not used
	{
		//neig coords - z axis
		int p = (int)z; if (z<0) z--; //because (int)-2.5 gives -2, should be -3 
		int n = p + 1;
		int pp = p - 1;
		int nn = n + 1;

		//blending percent
		float percent = z - p;

		//reading values
		float vp = GetInterpolateCubic(x, p, tileMode);
		float vpp = GetInterpolateCubic(x, pp, tileMode);
		float vn = GetInterpolateCubic(x, n, tileMode);
		float vnn = GetInterpolateCubic(x, nn, tileMode);

		return vp + 0.5f * percent * (vn - vpp + percent * (2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + percent * (3.0f*(vp - vn) + vnn - vpp)));
	}

	inline float Matrix::GetInterpolateCubic(float x, int z, TileMode tileMode) const
	/// Gets interpolated result using a horizontal level only
	{
		//neig coords - x axis
		int p = (int)x; if (x<0) p--; //because (int)-2.5 gives -2, should be -3 
		int n = p + 1;
		int pp = p - 1;
		int nn = n + 1;

		//blending percent
		float percent = x - p;

		//reading values
		float vp = GetTiled(p, z, tileMode);
		float vpp = GetTiled(pp, z, tileMode);
		float vn = GetTiled(n, z, tileMode);
		float vnn = GetTiled(nn, z, tileMode);

		return vp + 0.5f * percent * (vn - vpp + percent * (2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + percent * (3.0f*(vp - vn) + vnn - vpp)));
	}


	inline float Matrix::GetTiled(Coord coord, TileMode tileMode) const
	/// Returns the usual value if coord is in rect, handles tiling if it is not
	{
		if (rect.Contains(coord))
			return GetVal(coord.x, coord.z);

		else if (tileMode != Once)
		{
			Coord tiledCoord = TileCoord(coord, tileMode);
			return GetVal(tiledCoord.x, tiledCoord.z);
		}

		else return 0; // TileMode.Once
	}

	inline float Matrix::GetTiled(int x, int z, TileMode tileMode) const { return GetTiled(Coord(x, z), tileMode); }


	//TODO move to coord:

	inline Coord Matrix::TileCoord(Coord coord, TileMode tileMode) const
	/// Returns the corresponding coord within the matrix rect
	{
		switch (tileMode)
		{
			case Clamp | Once: return TileClamp(coord);
			case Tile: return TileRepeat(coord);
			case PingPong: return TilePingPong(coord);
			default: return coord;
		}
	}

	inline Coord Matrix::TileClamp(Coord coord) const
	{
		Coord result = coord;

		if (coord.x < rect.offset.x) result.x = rect.offset.x;
		if (coord.x >= rect.offset.x + rect.size.x) result.x = rect.offset.x + rect.size.x - 1;

		if (coord.z < rect.offset.z) result.z = rect.offset.z;
		if (coord.z >= rect.offset.z + rect.size.z) result.z = rect.offset.z + rect.size.z - 1;

		return result;
	}

	inline Coord Matrix::TileRepeat(Coord coord) const
	{
		Coord result = coord - rect.offset;

		result.x = result.x % rect.size.x;
		if (result.x < 0) result.x = rect.size.x + result.x;

		result.z = result.z % rect.size.z;
		if (result.z < 0) result.z = rect.size.z + result.z;

		return result + rect.offset;
	}

	inline Coord Matrix::TilePingPong(Coord coord) const
	{
		Coord result = coord - rect.offset;

		result.x = result.x % (rect.size.x * 2);
		if (result.x < 0) result.x = rect.size.x * 2 + result.x;
		if (result.x >= rect.size.x) result.x = rect.size.x * 2 - result.x - 1;

		result.z = result.z % (rect.size.z * 2);
		if (result.z<0) result.z = rect.size.z * 2 + result.z;
		if (result.z >= rect.size.z) result.z = rect.size.z * 2 - result.z - 1;

		return result + rect.offset;
	}


	PLUGIN_API float Matrix_GetInterpolatedBicubicR(CoordRect& rect, float array[], float x, float z)
	//PLUGIN_API float Matrix_GetInterpolatedBicubic(Matrix &matrix, float x, float z)
	{
		Matrix matrix = Matrix(rect, array);
		return matrix.GetInterpolatedBicubic(x,z, Once);
		//return array[0];
	}

	/*PLUGIN_API float Matrix_GetInterpolatedBicubicP(int coordOffsetX, int coordOffsetZ, int coordSizeX, int coordSizeZ, float array[], float x, float z)
	{
		CoordRect rect = CoordRect(coordOffsetX, coordOffsetZ, coordSizeX, coordSizeZ);
		Matrix matrix = Matrix(rect, array);
		return matrix.GetInterpolatedBicubic(x, z, Once);
	}*/

#pragma region Arithmetic

	void Matrix::Fill(float val)
	{
		for (int i = 0; i<count; i++)
			arr[i] = val;
	}
	PLUGIN_API void MatrixFillVal(Matrix &matrix, float val) { matrix.Fill(val); }

	
	void Matrix::Fill(Matrix &m)
	{
		for (int i = 0; i < count; i++)
			arr[i] = m.arr[i];
	}
	PLUGIN_API void MatrixFill(Matrix &matrix, float val) { matrix.Fill(val); }


	void Matrix::Fill(float val, float opacity)
	{
		for (int i = 0; i < count; i++)
			arr[i] = arr[i] * (1 - opacity) + val * opacity;
	}
	PLUGIN_API void MatrixFillOpacity(Matrix &matrix, float val, float opacity) { matrix.Fill(val, opacity); }

	
	void Matrix::Mix(Matrix &m, float opacity)
	{
		float invOpacity = 1 - opacity;
		for (int i = 0; i < count; i++)
			arr[i] = m.arr[i] * opacity + arr[i] * invOpacity;
	}
	PLUGIN_API void MatrixMix(Matrix &thism, Matrix &m, float opacity = 1) { thism.Mix(m, opacity); }


	void Matrix::Mix(Matrix &m, Matrix &mask)
	{
		for (int i = 0; i < count; i++)
			arr[i] = arr[i] * (1 - mask.arr[i]) + m.arr[i] * mask.arr[i];
	}
	PLUGIN_API void MatrixMixMask(Matrix &thism, Matrix &m, Matrix &mask) { thism.Mix(m, mask); }


	void Matrix::Mix(Matrix &m, Matrix &mask, float opacity)
	{
		float invOpacity = 1 - opacity;
		for (int i = 0; i < count; i++)
			arr[i] = arr[i] * invOpacity*(1 - mask.arr[i]) + m.arr[i] * opacity*mask.arr[i];
	}
	PLUGIN_API void MatrixMixMaskOpacity(Matrix &thism, Matrix &m, Matrix &mask, float opacity) { thism.Mix(m, mask, opacity); }


	void Matrix::Mix(Matrix &m, Matrix &mask, float maskMin, float maskMax, bool maskInvert, bool fallof, float opacity)
	{
		for (int i = 0; i < count; i++)
		{
			float percent = mask.arr[i];

			//percent = (percent-maskMin)/(maskMax-maskMin);
			//was a comment "maskMax-maskMin could be 0". Instead using (can test no change, but for some purpose it was changed):

			if (maskMax - maskMin != 0) percent = (percent - maskMin) / (maskMax - maskMin);
			else percent = 0;

			if (percent < 0) percent = 0; if (percent > 1) percent = 1;
			//if (fallof) percent = 3*percent*percent - 2*percent*percent*percent;
			percent *= opacity;
			if (maskInvert) percent = 1 - percent;

			arr[i] = arr[i] * (1 - percent) + m.arr[i] * percent;
		}
	}
	PLUGIN_API void MatrixMixComplex(Matrix &thism, Matrix &m, Matrix &mask, float maskMin, float maskMax, bool maskInvert, bool fallof, float opacity)
		 { thism.Mix(m, mask, maskMin, maskMax, maskInvert, fallof, opacity); }


	void Matrix::InvMix(Matrix &m, Matrix &invMask, float opacity)
		//using inverted mask: mask1 will leave original value, mask0 will use m
	{
		float invOpacity = 1 - opacity;
		for (int i = 0; i < count; i++)
			arr[i] = arr[i] * invOpacity*invMask.arr[i] + m.arr[i] * opacity*(1 - invMask.arr[i]);
	}
	PLUGIN_API void MatrixInvMix(Matrix &thism, Matrix &m, Matrix &invMask, float opacity = 1)
		{ thism.InvMix(m, invMask, opacity); }


	void Matrix::Add(Matrix &add, float opacity)
	{
		for (int i = 0; i < count; i++)
			arr[i] += add.arr[i] * opacity;
	}
	PLUGIN_API void MatrixAdd(Matrix &thism, Matrix &add, float opacity)
		 { thism.Add(add, opacity); }


	void Matrix::Add(Matrix &add, Matrix &mask, float opacity)
	{
		for (int i = 0; i < count; i++)
			arr[i] += add.arr[i] * mask.arr[i] * opacity;
	}
	PLUGIN_API void MatrixAddMask(Matrix &thism, Matrix &add, Matrix &mask, float opacity)
		 { thism.Add(add, mask, opacity); }


	void Matrix::Add(float add)
	{
		for (int i = 0; i < count; i++)
			arr[i] += add;
	}
	PLUGIN_API void MatrixAddVal(Matrix &thism, float add)
		 { thism.Add(add); }


	void Matrix::Blend(Matrix &matrix, Matrix &mask, Matrix &add, Matrix &addMask)
	{
		for (int i = 0; i < count; i++)
		{
			float sum = mask.arr[i] + addMask.arr[i];
			arr[i] = sum != 0 ?
				(matrix.arr[i] * mask.arr[i] + add.arr[i] * addMask.arr[i]) / sum :
				(matrix.arr[i] + add.arr[i]) / 2;
		}
	}
	PLUGIN_API void MatrixBlend(Matrix &thism, Matrix &matrix, Matrix &mask, Matrix &add, Matrix &addMask)
		 { thism.Blend(matrix, mask, add, addMask); }


	void Matrix::Max(Matrix &matrix, Matrix &mask, Matrix &add, Matrix &addMask)
	{
		for (int i = 0; i < count; i++)
			arr[i] = mask.arr[i] > addMask.arr[i] ? matrix.arr[i] : add.arr[i];
	}
	PLUGIN_API void MatrixMaxComplex(Matrix &thism, Matrix &matrix, Matrix &mask, Matrix &add, Matrix &addMask)
		 { thism.Max(matrix, mask, add, addMask); }


	void Matrix::Step(float mid)
	{
		for (int i = 0; i < count; i++)
			arr[i] = arr[i] > mid ? 1 : 0;
	}
	PLUGIN_API void MatrixStep(Matrix &thism, float mid = 0.5f)
		 { thism.Step(mid); }


	void Matrix::Subtract(Matrix &m, float opacity)
	{
		for (int i = 0; i < count; i++)
			arr[i] -= m.arr[i] * opacity;
	}
	PLUGIN_API void MatrixSubtract(Matrix &thism, Matrix &m, float opacity)
		 { thism.Subtract(m, opacity); }


	void Matrix::InvSubtract(Matrix &m, float opacity)
		/// subtracting this matrix from m
	{
		for (int i = 0; i < count; i++)
			arr[i] = m.arr[i] * opacity - arr[i];
	}
	PLUGIN_API void MatrixInvSubtract(Matrix &thism, Matrix &m, float opacity)
		 { thism.InvSubtract(m, opacity); }	


	void Matrix::Multiply(Matrix &m, float opacity)
	{
		float invOpacity = 1 - opacity;
		for (int i = 0; i < count; i++)
			arr[i] *= m.arr[i] * opacity + invOpacity;
	}
	PLUGIN_API void MatrixMultiply(Matrix &thism, Matrix &m, float opacity)
		 { thism.Multiply(m, opacity); }


	void Matrix::Multiply(float m)
	{
		for (int i = 0; i < count; i++)
			arr[i] *= m;
	}
	PLUGIN_API void MatrixMultiplyVal(Matrix &thism, float m)
		 { thism.Multiply(m); }

	void Matrix::MultiplyInv(Matrix &invFactor)
	{
		for (int i = 0; i < count; i++)
			arr[i] *= 1 - invFactor.arr[i];
	}

	PLUGIN_API void MatrixMultiplyInv(Matrix &thism, Matrix &invFactor)
		{ thism.MultiplyInv(invFactor); }

	void Matrix::Contrast(float m)
	/// Leaving 0.5 values untouched, and increasing/shrinking 1-0 range
	{
		for (int i = 0; i < count; i++)
		{
			float val = arr[i] - 0.5f;
			val *= m;
			arr[i] = val + 0.5f;
		}
	}
	PLUGIN_API void MatrixContrast(Matrix &thism, float m)
		 { thism.Contrast(m); }


	void Matrix::Divide(Matrix &m, float opacity)
	{
		float invOpacity = 1 - opacity;
		for (int i = 0; i < count; i++)
			arr[i] *= opacity / m.arr[i] + invOpacity;
	}
	PLUGIN_API void MatrixDivide(Matrix &thism, Matrix &m, float opacity)
		 { thism.Divide(m, opacity); }


	void Matrix::Difference(Matrix &m, float opacity)
	{
		for (int i = 0; i < count; i++)
		{
			float val = arr[i] - m.arr[i] * opacity;
			if (val < 0) val = -val;
			arr[i] = val;
		}
	}
	PLUGIN_API void MatrixDifference(Matrix &thism, Matrix &m, float opacity)
		 { thism.Difference(m, opacity); }


	void Matrix::Overlay(Matrix &m, float opacity)
	{
		for (int i = 0; i < count; i++)
		{
			float a = arr[i];
			float b = m.arr[i];

			b = b * opacity + (0.5f - opacity / 2); //enhancing contrast via levels

			if (a > 0.5f) b = 1 - 2 * (1 - a)*(1 - b);
			else b = 2 * a*b;

			arr[i] = b;// b*opacity + a*(1-opacity); //the same
		}
	}
	PLUGIN_API void MatrixOverlay(Matrix &thism, Matrix &m, float opacity)
		 { thism.Overlay(m, opacity); }


	void Matrix::HardLight(Matrix &m, float opacity)
	/// Same as overlay but estimating b>0.5
	{
		for (int i = 0; i < count; i++)
		{
			float a = arr[i];
			float b = m.arr[i];

			if (b > 0.5f) b = 1 - 2 * (1 - a)*(1 - b);
			else b = 2 * a*b;

			arr[i] = b * opacity + a * (1 - opacity);
		}
	}
	PLUGIN_API void MatrixHardLight(Matrix &thism, Matrix &m, float opacity)
		 { thism.HardLight(m, opacity); }


	void Matrix::SoftLight(Matrix &m, float opacity)
	{
		for (int i = 0; i < count; i++)
		{
			float a = arr[i];
			float b = m.arr[i];
			b = (1 - 2 * b)*a*a + 2 * b*a;
			arr[i] = b * opacity + a * (1 - opacity);
		}
	}
	PLUGIN_API void MatrixSoftLight(Matrix &thism, Matrix &m, float opacity)
		 { thism.SoftLight(m, opacity); }


	void Matrix::Max(Matrix &m, float opacity)
	{
		for (int i = 0; i < count; i++)
		{
			float val = m.arr[i] > arr[i] ? m.arr[i] : arr[i];
			arr[i] = val * opacity + arr[i] * (1 - opacity);
		}
	}
	PLUGIN_API void MatrixMax(Matrix &thism, Matrix &m, float opacity)
		{ thism.Max(m, opacity); }


	void Matrix::Min(Matrix &m, float opacity)
	{
		for (int i = 0; i < count; i++)
		{
			float val = m.arr[i] < arr[i] ? m.arr[i] : arr[i];
			arr[i] = val * opacity + arr[i] * (1 - opacity);
		}
	}
	PLUGIN_API void MatrixMin(Matrix &thism, Matrix &m, float opacity)
		 { thism.Min(m, opacity); }

	void Matrix::Select(float level)
	{
		for (int i = 0; i < count; i++)
			arr[i] = arr[i] > level ? 1 : 0;
	}
	PLUGIN_API void MatrixSelect(Matrix &thism, float level)
	{
		thism.Select(level);
	}

	void Matrix::Invert()
	{
		for (int i = 0; i < count; i++)
			arr[i] = -arr[i];
	}
	PLUGIN_API void MatrixInvert(Matrix &thism)
		 { thism.Invert(); }


	void Matrix::InvertOne()
	{
		for (int i = 0; i < count; i++)
			arr[i] = 1 - arr[i];
	}
	PLUGIN_API void MatrixInvertOne(Matrix &thism)
		 { thism.InvertOne(); }


	void Matrix::SelectRange(float minFrom, float minTo, float maxFrom, float maxTo)
	/// Fill all values within min1-max0 with 1, while min0-1 and max0-1 are filled with blended
	{
		for (int i = 0; i < count; i++)
		{
			float src = arr[i];
			float dst;

			if (src<minFrom || src>maxTo) dst = 0;
			else if (src > minTo && src < maxFrom) dst = 1;
			else
			{
				float minVal = (src - minFrom) / (minTo - minFrom);
				float maxVal = 1 - (src - maxFrom) / (maxTo - maxFrom);
				dst = minVal > maxVal ? maxVal : minVal;
				if (dst < 0) dst = 0; if (dst > 1) dst = 1;
			}

			arr[i] = dst;
		}
	}
	PLUGIN_API void MatrixSelectRange(Matrix &thism, float minFrom, float minTo, float maxFrom, float maxTo)
		{ thism.SelectRange(minFrom, minTo, maxFrom, maxTo); }


	void Matrix::ChangeRange(float fromMin, float fromMax, float toMin, float toMax)
	/// Used to convert matrix from -1 1 range to 0 1 or vice versa
	{
		float fromRange = fromMax - fromMin;
		float toRange = toMax - toMin;

		for (int i = 0; i < count; i++)
		{
			float val = (arr[i] - fromMin) / fromRange;  //converting to 0 1
			arr[i] = val * toRange + toMin;  //converting to to
		}
	}
	PLUGIN_API void MatrixChangeRange(Matrix &thism, float fromMin, float fromMax, float toMin, float toMax)
		 { thism.ChangeRange(fromMin, fromMax, toMin, toMax); }


	void Matrix::Clamp01()
	{
		for (int i = 0; i < count; i++)
		{
			float val = arr[i];
			if (val > 1) arr[i] = 1;
			else if (val < 0) arr[i] = 0;
		}
	}
	PLUGIN_API void MatrixClamp01(Matrix &thism)
		 { thism.Clamp01(); }


	float Matrix::MaxValue()
	{
		float max = std::numeric_limits<float>::min();
		for (int i = 0; i < count; i++)
		{
			float val = arr[i];
			if (val > max) max = val;
		}
		return max;
	}
	PLUGIN_API float MatrixMaxValue(Matrix &thism)
		 { return thism.MaxValue(); }


	float Matrix::MinValue()
	{
		float min = std::numeric_limits<float>::max();
		for (int i = 0; i < count; i++)
		{
			float val = arr[i];
			if (val < min) min = val;
		}
		return min;
	}
	PLUGIN_API float MatrixMinValue(Matrix &thism)
		 { return thism.MinValue(); }


	float Matrix::Average()
	{
		float avg = 0;
		for (int i = 0; i < count; i++)
			avg += arr[i] / count;
		return avg;
	}
	PLUGIN_API float Average(Matrix &thism)
		{ return thism.Average(); }


	bool Matrix::IsEmpty()
	/// Better than MinValue since it can quit if matrix is not empty
	{
		for (int i = 0; i < count; i++)
			if (arr[i] > 0.0001f) return false;
		return true;
	}
	PLUGIN_API bool MatrixIsEmpty(Matrix &thism)
		 { return thism.IsEmpty(); }


	bool Matrix::IsEmpty(float delta)
	{
		for (int i = 0; i < count; i++)
			if (arr[i] > delta) return false;
		return true;
	}
	PLUGIN_API bool MatrixIsEmptyDelta(Matrix &thism, float delta)
		 { return thism.IsEmpty(delta); }


	void Matrix::BlackWhite(float mid)
	/// Sets all values bigger than mid to white (1), and those lower to black (0)
	{
		for (int i = 0; i < count; i++)
		{
			float val = arr[i];
			if (val > mid) arr[i] = 1;
			else arr[i] = 0;
		}
	}
	PLUGIN_API void MatrixBlackWhite(Matrix &thism, float mid)
		 { thism.BlackWhite(mid); }


	void Matrix::BrightnessContrast(float brightness, float contrast)
	{
		//float factor = (1+contrast) / (1-contrast);

		for (int i = 0; i < count; i++)
		{
			float val = arr[i];

			val = ((val - 0.5f)*contrast) + 0.5f;  //contrast
			val += brightness / 2 * (contrast < 1 ? 1 : contrast); //brightness  //this way brightness works in range -1 to 1 both for contrast <1 and >1
			//val += brightness/2 * (1+contrast);  //alt brightness  

			if (val < 0) val = 0; if (val > 1) val = 1;
			arr[i] = val;
		}
	}
	PLUGIN_API void MatrixBrightnessContrast(Matrix &thism, float brightness, float contrast)
		 { thism.BrightnessContrast(brightness, contrast); }


	void Matrix::Terrace(float* terraces, int terraceCount, float steepness)
	{
		float intensity = sqrt(steepness);

		for (int i = 0; i < count; i++)
		{
			float val = arr[i];
			if (val > 0.9999f) continue;	//do nothing with values that are out of range

			int terrNum = 0;
			for (int t = 0; t < terraceCount - 1; t++)
			{
				if (terraces[terrNum + 1] > val || terrNum + 1 == terraceCount) break;
				terrNum++;
			}

			//kinda curve evaluation
			float delta = terraces[terrNum + 1] - terraces[terrNum];
			float relativePos = (val - terraces[terrNum]) / delta;

			float percent = 3 * relativePos*relativePos - 2 * relativePos*relativePos*relativePos;

			percent = (percent - 0.5f) * 2;
			bool minus = percent < 0; 
			if (percent < 0) percent = -percent;

			percent = pow(percent, 1-steepness);

			if (minus) percent = -percent;
			percent = percent / 2 + 0.5f;

			float dstVal = terraces[terrNum] * (1 - percent) + terraces[terrNum + 1] * percent;
			arr[i] = dstVal*intensity + val*(1-intensity);
		}
	}
	PLUGIN_API void MatrixTerrace(Matrix &thism, float* terraces, int terraceCount, float steepness)
		{ thism.Terrace(terraces, terraceCount, steepness); }

	void Matrix::Levels(float inMin, float inMax, float gamma, float outMin, float outMax)
	{
		float inDelta = inMax - inMin;
		float outDelta = outMax - outMin;

		for (int i = 0; i < count; i++)
		{
			float val = arr[i];

			//preliminary clamping
			if (val < inMin) { arr[i] = outMin; continue; }
			if (val > inMax) { arr[i] = outMax; continue; }

			//input
			if (inDelta != 0)
				val = (val - inMin) / inDelta;
			else
				val = inMin;

			//gamma
			if (gamma > 1.00001f || gamma < 0.9999f)  // gamma != 1
			{
				if (gamma < 1) val = pow(val, gamma);
				else val = pow(val, 1 / (2 - gamma));
			}

			//output
			if (outDelta != 0)
				val = outMin + val * outDelta;
			else
				val = outMin;

			arr[i] = val;
		}
	}
	PLUGIN_API void MatrixLevels(Matrix &thism, float inMin, float inMax, float gamma, float outMin, float outMax)
		 { thism.Levels(inMin, inMax, gamma, outMin, outMax); }


	void Matrix::UniformCurve(float* lut, int lutCount)
	/// Applies curve that got curve lut with uniformly placed times
	/// A copy of curve's EvaluateLuted
	{
		float step = 1.0f / (lutCount - 1);

		for (int i = 0; i < count; i++)
		{
			float val = arr[i];

			int prevNum = (int)(val / step);
			int nextNum = prevNum + 1;

			if (prevNum < 0) prevNum = 0; if (prevNum >= lutCount) prevNum = lutCount - 1;
			if (nextNum < 0) nextNum = 0; if (nextNum >= lutCount) nextNum = lutCount - 1;

			float prevX = prevNum * step;
			float percent = (val - prevX) / step;

			float prevY = lut[prevNum];
			float nextY = lut[nextNum];

			arr[i] = prevY * (1 - percent) + nextY * percent;
		}
	}
	PLUGIN_API void MatrixUniformCurve(Matrix &thism, float* lut, int lutCount)
		 { thism.UniformCurve(lut, lutCount); }


	PLUGIN_API void MatrixPow(Matrix &thism, float pow) { thism.Pow(pow); }
	void Matrix::Pow(float powVal)
	{
		for (int i = 0; i < count; i++)
			arr[i] = pow(arr[i], powVal);
	}


	PLUGIN_API void MatrixParallaxMask (Matrix &thisMatrix, float directionX, float directionZ, Matrix &src, Matrix &maskX, Matrix &maskZ, bool useMaskX, bool useMaskZ, int interpolation)
		{thisMatrix.Parallax(directionX, directionZ, src, maskX, maskZ, useMaskX, useMaskZ, interpolation); }
	void Matrix::Parallax(float directionX, float directionZ, Matrix &src, Matrix &maskX, Matrix &maskZ, bool useMaskX, bool useMaskZ, int interpolation)
	{
		Coord min = rect.Min(); Coord max = rect.Max();
		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int pos = (z - rect.offset.z)*rect.size.x + x - rect.offset.x;

				float intensityX = useMaskX ? maskX.arr[pos] : 1;
				float intensityZ = useMaskZ ? maskZ.arr[pos] : 1; //or use intensityX?

				if (intensityX < 0.0001f  &&  intensityZ < 0.0001f)
				{
					arr[pos] = src.arr[pos]; continue;
				}

				float px = x - directionX *intensityX; //negative intensity (ie direction) to READ pixels will create an effect of positive MOVE
				float pz = z - directionZ *intensityZ;

				if (px < min.x) px = min.x; if (px > max.x - 1) px = max.x - 1;
				if (pz < min.z) pz = min.z; if (pz > max.z - 1) pz = max.z - 1;

				float pVal;

				if (interpolation == 0)
					pVal = src.GetFloored(px + 0.5f, pz + 0.5f);

				else if (interpolation == 1)
					pVal = src.GetInterpolated(px, pz);

				else
				{
					if (intensityX > 0.999f  &&  intensityZ > 0.999f) //full intensity - no interpolation
						pVal = src.GetFloored(px + 0.5f, pz + 0.5f);
					else
						pVal = src.GetInterpolated(px, pz);
				}

				arr[pos] = pVal;
			}
	}


	PLUGIN_API void MatrixStroke(Matrix &thism, float posX, float posZ, float radius, float hardness, bool smoothTransition, float bckgVal, float strokeVal)
		{ thism.Stroke(posX, posZ, radius, hardness, smoothTransition, bckgVal, strokeVal); }
	void Matrix::Stroke(float posX, float posZ, float radius, float hardness, bool smoothTransition, float bckgVal, float strokeVal)
	{
		Coord min = rect.Min(); Coord max = rect.Max();
		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int p = (z - rect.offset.z)*rect.size.x + x - rect.offset.x;

				float dist = sqrt((x - posX)*(x - posX) + (z - posZ)*(z - posZ));
				if (dist > radius) { arr[p] = bckgVal; continue; }
				if (dist < radius*hardness) { arr[p] = strokeVal; continue; }

				float fallof = 1 - (dist - radius * hardness) / (radius*(1 - hardness));
				if (fallof > 1) fallof = 1; if (fallof < 0) fallof = 0;
				if (smoothTransition) fallof = 3 * fallof*fallof - 2 * fallof*fallof*fallof;

				arr[p] = bckgVal * (1 - fallof) + strokeVal * fallof;
			}
	}


	PLUGIN_API void MatrixFallof(Matrix& srcBackground, Matrix& srcBrush, Matrix& dst, float posX, float posZ, float radius, float hardness, bool smoothTransition = true)
	{
		CoordRect intersection = CoordRect::Intersected(srcBackground.rect, srcBrush.rect);
		intersection = CoordRect::Intersected(dst.rect, intersection);
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int dstPos = (z - dst.rect.offset.z)*dst.rect.size.x + x - dst.rect.offset.x;
				int brsPos = (z - srcBrush.rect.offset.z)*srcBrush.rect.size.x + x - srcBrush.rect.offset.x;
				int bkgPos = (z - srcBackground.rect.offset.z)*srcBackground.rect.size.x + x - srcBackground.rect.offset.x;

				float dist = sqrt((x - posX)*(x - posX) + (z - posZ)*(z - posZ));
				if (dist > radius) { dst.arr[dstPos] = srcBackground.arr[bkgPos]; continue; }
				if (dist < radius*hardness) { dst.arr[dstPos] = srcBrush.arr[brsPos]; continue; }

				float fallof = 1 - (dist - radius * hardness) / (radius*(1 - hardness));
				if (fallof > 1) fallof = 1; if (fallof < 0) fallof = 0;
				if (smoothTransition) fallof = 3 * fallof*fallof - 2 * fallof*fallof*fallof;

				dst.arr[dstPos] = srcBackground.arr[bkgPos] * (1 - fallof) + srcBrush.arr[brsPos] * fallof;
			}
	}


	PLUGIN_API void MatrixBlendStamped(Matrix &thism, Matrix& src, Matrix& stamp, float centerX, float centerZ, float radius, float transition, bool smoothFallof /*true*/)
		{ thism.BlendStamped(src, stamp, centerX, centerZ, radius, transition, smoothFallof); }
	void Matrix::BlendStamped(Matrix& src, Matrix& stamp, float centerX, float centerZ, float radius, float transition, bool smoothFallof /*true*/)
	{
		CoordRect intersection = CoordRect::Intersected(rect, stamp.rect);
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				float dist = sqrt((x - centerX)*(x - centerX) + (z - centerZ)*(z - centerZ));

				int pos = (z - rect.offset.z)*rect.size.x + x - rect.offset.x;

				int stampPos = (z - stamp.rect.offset.z)*stamp.rect.size.x + x - stamp.rect.offset.x;
				//if (dist < radius) { arr[pos] = stamp.arr[stampPos]; continue; } //not radius, but radius/transition

				//int srcPos = (z-src.rect.offset.z)*src.rect.size.x + x - src.rect.offset.x; //not used
				//if (dist > radius+transition) { arr[pos] = src.arr[srcPos]; continue; }

				float fallof;
				if (transition == 0)
					fallof = dist > radius ? 0 : 1;
				else
				{
					fallof = 1 - (dist - radius) / transition;
					if (fallof > 1) fallof = 1; if (fallof < 0) fallof = 0;
					if (smoothFallof) fallof = 3 * fallof*fallof - 2 * fallof*fallof*fallof;
				}

				arr[pos] = src.arr[pos] * (1 - fallof) + stamp.arr[stampPos] * fallof;
			}
	}


	PLUGIN_API void ReadMatrixFull (Matrix& src, Matrix& dst)
	{
		CoordRect intersection = CoordRect::Intersected(src.rect, dst.rect);
		if (intersection.size.x <= 0 || intersection.size.z <= 0) return;
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int srcPos = (z - src.rect.offset.z)*src.rect.size.x + x - src.rect.offset.x;
				int dstPos = (z - dst.rect.offset.z)*dst.rect.size.x + x - dst.rect.offset.x;

				dst.arr[dstPos] = src.arr[srcPos];
			}
	}

	PLUGIN_API void ReadMatrixTile(Matrix& src, Matrix& dst, int tileMode)
	{
		Coord tmp = Coord(0, 0);
		Coord min = dst.rect.Min(); Coord max = dst.rect.Max();
		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				tmp.x = x; tmp.z = z;
				tmp = src.rect.Tile(tmp, tileMode);
				dst.SetVal(x, z, src.GetVal(tmp.x, tmp.z));
			}
	}

	PLUGIN_API void ReadMatrixRect (Matrix& src, Matrix& dst, CoordRect rect)
	{
		CoordRect intersection = CoordRect::Intersected(rect, dst.rect);
		intersection = CoordRect::Intersected(src.rect, intersection);
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int srcPos = (z - src.rect.offset.z)*src.rect.size.x + x - src.rect.offset.x;
				int dstPos = (z - dst.rect.offset.z)*dst.rect.size.x + x - dst.rect.offset.x;

				dst.arr[dstPos] = src.arr[srcPos];
			}
	}

	PLUGIN_API void MatrixCopyResized(Matrix& src, Matrix& dst,
		Vector2D srcRectPos, Vector2D srcRectSize,
		Coord dstRectPos, Coord dstRectSize)
	{
		CoordRect dstIntersection = CoordRect::Intersected(CoordRect(dstRectPos, dstRectSize), dst.rect);
		Coord min = dstIntersection.Min(); Coord max = dstIntersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				float pX = (float)(x - dstRectPos.x) / dstRectSize.x; //-1?
				float pZ = (float)(z - dstRectPos.z) / dstRectSize.z;

				int srcX = (int)(pX*srcRectSize.x + srcRectPos.x + 0.5f);
				int srcZ = (int)(pZ*srcRectSize.z + srcRectPos.z + 0.5f);
				if (srcX < src.rect.offset.x || srcX >= src.rect.offset.x + src.rect.size.x ||
					srcZ < src.rect.offset.z || srcZ >= src.rect.offset.z + src.rect.size.z)
					continue;

				int srcPos = (srcZ - src.rect.offset.z)*src.rect.size.x + srcX - src.rect.offset.x;
				int dstPos = (z - dst.rect.offset.z)*dst.rect.size.x + x - dst.rect.offset.x;

				dst.arr[dstPos] = src.arr[srcPos];
			}
	}

	PLUGIN_API void Resize(Matrix& src, Matrix& dst)
	/// Uses NN interpolation to copy pixels from src to dst
	{
		float dstToSrcX = (float)src.rect.size.x / (float)dst.rect.size.x; //src/dst - isn't it an error?
		float dstToSrcZ = (float)src.rect.size.z / (float)dst.rect.size.z;

		for (int x = 0; x < dst.rect.size.x; x++)
			for (int z = 0; z < dst.rect.size.z; z++)
			{
				int dstPos = z * dst.rect.size.x + x;

				int srcX = (int)((x + 0.5f)*dstToSrcX);
				int srcZ = (int)((z + 0.5f)*dstToSrcZ);
				int srcPos = srcZ * src.rect.size.x + srcX;

				dst.arr[dstPos] = src.arr[srcPos];
			}
	}


	void Matrix::BlendLayers(Matrix* matrices[], float opacity[], int layersCount)
	/// Changes splatmaps in photoshop layered style so their summary value does not exceed 1
	{
		bool allNulls = true;
		CoordRect rect;
		for (int i=0; i<layersCount; i++)
			if (matrices[i] != nullptr)
			{
				allNulls = false;
				rect = (*matrices)[i].rect;
			}
		if (allNulls) return;

		int rectCount = rect.size.x * rect.size.z;
		for (int pos = 0; pos < rectCount; pos++)
		{
			float left = 1;
			for (int i = layersCount-1; i >= 0; i--) //layer 0 is background, layer Length-1 is the top one
			{
				if (matrices[i] == nullptr) continue;

				float val = (*matrices)[i].arr[pos];

				if (opacity != nullptr) val *= opacity[i];

				val = val * left;
				(*matrices)[i].arr[pos] = val;
				left -= val;

				if (left < 0) break;
			}
		}
	}
	PLUGIN_API void MatrixBlendLayers(Matrix* matrices[], float opacity[], int layersCount)
		 { Matrix::BlendLayers(matrices, opacity, layersCount); }


	void Matrix::NormalizeLayers(Matrix* matrices[], float opacity[], int layersCount)
	/// Just changes splatmaps so their summary value always 1
	{
		bool allNulls = true;
		CoordRect rect;
		for (int i = 0; i < layersCount; i++)
			if (matrices[i] != nullptr)
			{
				allNulls = false;
				rect = (*matrices)[i].rect;
			}
		if (allNulls) return;

		int rectCount = rect.size.x * rect.size.z;
		for (int pos = 0; pos < rectCount; pos++)
		{
			for (int i = 0; i < layersCount; i++) (*matrices)[i].arr[pos] *= opacity[i];

			float sum = 0;
			for (int i = 0; i < layersCount; i++) sum += (*matrices)[i].arr[pos];
			if (sum > 1.0f) for (int i = 0; i < layersCount; i++) (*matrices)[i].arr[pos] /= sum;
		}
	}
	PLUGIN_API void MatrixNormalizeLayers(Matrix &thism, Matrix* matrices[], float opacity[], int layersCount)
		 { Matrix::NormalizeLayers(matrices, opacity, layersCount); }


	void Matrix::NormalizeLayers(Matrix* matrices[], Matrix* masks[], int layersCount)
	/// Just changes splatmaps so their summary value always 1
	{
		bool allNulls = true;
		CoordRect rect;
		for (int i = 0; i < layersCount; i++)
			if (matrices[i] != nullptr)
			{
				allNulls = false;
				rect = (*matrices)[i].rect;
			}
		if (allNulls) return;

		int rectCount = rect.size.x * rect.size.z;
		for (int pos = 0; pos < rectCount; pos++)
		{
			for (int i = 0; i < layersCount; i++) (*matrices)[i].arr[pos] *= (*masks)[i].arr[pos];

			float sum = 0;
			for (int i = 0; i < layersCount; i++) sum += (*matrices)[i].arr[pos];
			if (sum > 1.0f) for (int i = 0; i < layersCount; i++) (*matrices)[i].arr[pos] /= sum;
		}
	}
	PLUGIN_API void MatrixNormalizeLayersMasks(Matrix &thism, Matrix* matrices[], Matrix* masks[], int layersCount)
		 { Matrix::NormalizeLayers(matrices, masks, layersCount); }

#pragma endregion


#pragma region Import

	/*PLUGIN_API void Matrix_ImportColors(Matrix &thism, Color* colors, int colorsLength, Coord colorsOffset, Coord colorsSize, int channel = -1)
	{
		if (colorsLength != colorsSize.x*colorsSize.z)
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(colorsOffset, colorsSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int colorsPos = (z - colorsOffset.z)*colorsSize.x + x - colorsOffset.x;

				float val;
				switch (channel)
				{
				case 0: val = colors[colorsPos].r; break;
				case 1: val = colors[colorsPos].g; break;
				case 2: val = colors[colorsPos].b; break;
				case 3: val = colors[colorsPos].a; break;
				default: val = (colors[colorsPos].r + colors[colorsPos].g + colors[colorsPos].b) / 3; break;
				}

				thism.arr[matrixPos] = val;
			}
	}*/

	PLUGIN_API void Matrix_ImportRawBytes(Matrix &thism, unsigned char* bytes, int bytesLength, Coord bytesOffset, Coord bytesSize, int start, int step)
	{
		if (bytesLength != bytesSize.x*bytesSize.z*step &&
			(bytesLength < bytesSize.x*bytesSize.z*step*1.3f || bytesLength > bytesSize.x*bytesSize.z*step*1.3666f)) //in case of mipmap information
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(bytesOffset, bytesSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int bytesPos = (z - bytesOffset.z)*bytesSize.x + x - bytesOffset.x;
				bytesPos = bytesPos * step + start;

				float val = bytes[bytesPos] / 255.0f;  //matrix has the range 0-1 _inclusive_, it could be 1, so using 255
				thism.arr[matrixPos] = val;
			}
	}


	PLUGIN_API void Matrix_ImportRaw16(Matrix &thism, unsigned char* bytes, int bytesLength, Coord texOffset, Coord texSize)
	{
		if (texSize.x*texSize.z * 2 > bytesLength) //extra bytes could be mipmaps
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(texOffset, texSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int bytesPos = (z - texOffset.z)*texSize.x + x - texOffset.x;
				bytesPos *= 2;

				float val = (bytes[bytesPos + 1] * 256.0f + bytes[bytesPos]) / 65536.0f;  // 65025.0f;
				thism.arr[matrixPos] = val;
			}
	}

	union FloatToBytes
	{
		float f;
		unsigned char b[4];
	};

	PLUGIN_API void Matrix_ImportRawFloat(Matrix &thism, unsigned char* bytes, int bytesLength, Coord texOffset, Coord texSize, float mult = 1)
	{
		int numPixels = texSize.x*texSize.z;
		if (numPixels * 4 > bytesLength) //extra bytes could be mipmaps
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(texOffset, texSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		FloatToBytes converter = FloatToBytes();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int bytesPos = (z - texOffset.z)*texSize.x + x - texOffset.x;
				bytesPos *= 4;

				converter.b[0] = bytes[bytesPos];
				converter.b[1] = bytes[bytesPos + 1];
				converter.b[2] = bytes[bytesPos + 2];
				converter.b[3] = bytes[bytesPos + 3];

				thism.arr[matrixPos] = converter.f * mult;
			}
	}

	PLUGIN_API void Matrix_ImportHeights(Matrix &thism, float* heights, Coord heightsOffset, Coord heightsSize)
	{
		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(heightsOffset, heightsSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int heightsPosX = x - heightsOffset.x;
				int heightsPosZ = z - heightsOffset.z;

				thism.arr[matrixPos] = heights[heightsPosZ*heightsSize.x + heightsPosX];
			}
	}

	PLUGIN_API void Matrix_ImportSplats(Matrix &thism, float* splats, Coord splatsOffset, Coord splatsSize, int numChannels, int channel)
	{
		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(splatsOffset, splatsSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int heightsPosX = x - splatsOffset.x;
				int heightsPosZ = z - splatsOffset.z;

				thism.arr[matrixPos] = splats[heightsPosZ*splatsSize.x*numChannels + heightsPosX * numChannels + channel];
				//thism.arr[matrixPos] = splats[channel*splatsSize.x*splatsSize.z + heightsPosZ * splatsSize.x + heightsPosX];
			}
	}

	PLUGIN_API void Matrix_ImportDetail(Matrix &thism, int* detail, Coord detailOffset, Coord detailSize, float density = 1) //aka Grass
	{
		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(detailOffset, detailSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int detailPosX = x - detailOffset.x;
				int detailPosZ= z - detailOffset.z;

				float val = detail[detailPosZ*detailSize.x + detailPosX];
				thism.arr[matrixPos] = val / density;
			}
	}

#pragma endregion


#pragma region Export

	/*PLUGIN_API void ExportColors(Matrix& thism, Color* colors, int colorsLength, Coord colorsOffset, Coord colorsSize, int channel = -1)
	{
		if (colorsLength != colorsSize.x*colorsSize.z)
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(colorsOffset, colorsSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int colorsPos = (z - colorsOffset.z)*colorsSize.x + x - colorsOffset.x;

				float val = thism.arr[matrixPos];
				//if (float.IsNaN(val))  colors[colorsPos] = new Color(0,0,1,0);
				if (val > 1) colors[colorsPos] = Color(0, 1, 0, 0);
				else if (val < 0) colors[colorsPos] = Color(1, 0, 0, 0);
				else switch (channel)
				{
				case 0: colors[colorsPos].r = val; break;
				case 1: colors[colorsPos].g = val; break;
				case 2: colors[colorsPos].b = val; break;
				case 3: colors[colorsPos].a = val; break;
				default: colors[colorsPos].r = val; colors[colorsPos].g = val; colors[colorsPos].b = val; colors[colorsPos].a = val; break;
				}
			}
	}*/

	PLUGIN_API void Matrix_ExportRawBytes(Matrix& thism, unsigned char* bytes, int bytesLength, Coord bytesOffset, Coord bytesSize, int start, int step)
	{
		if (bytesLength != bytesSize.x*bytesSize.z*step &&
			(bytesLength < bytesSize.x*bytesSize.z*step*1.3f || bytesLength > bytesSize.x*bytesSize.z*step*1.3666f)) //in case of mipmap information
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(bytesOffset, bytesSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int bytesPos = (z - bytesOffset.z)*bytesSize.x + x - bytesOffset.x;
				bytesPos = bytesPos * step + start;

				float val = thism.arr[matrixPos];
				bytes[bytesPos] = (unsigned char)(val * 255.0f); //matrix has the range 0-1 _inclusive_, it could be 1
			}
	}

	PLUGIN_API void Matrix_ExportRaw16(Matrix& thism, unsigned char* bytes, int bytesLength, Coord texOffset, Coord texSize)
	{
		if (texSize.x*texSize.z * 2 != bytesLength)
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(texOffset, texSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int bytesPos = (z - texOffset.z)*texSize.x + x - texOffset.x;
				bytesPos *= 2;

				float val = thism.arr[matrixPos]; //this[x+regionRect.offset.x, z+regionRect.offset.z];

				int intVal = (int)(val * 65536);
				if (intVal >= 65536) intVal = 65535;
				bytes[bytesPos] = (unsigned char)(intVal & 0xFF);
				bytes[bytesPos + 1] = (unsigned char)(intVal >> 8);
			}
	}


	PLUGIN_API void Matrix_ExportRawFloat(Matrix& thism, unsigned char* bytes, int bytesLength, Coord texOffset, Coord texSize, float mult = 1)
	{
		int numPixels = texSize.x*texSize.z;
		if (numPixels * 4 != bytesLength)
			throw out_of_range("Array count does not match texture dimensions");

		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(texOffset, texSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		FloatToBytes converter = FloatToBytes();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int bytesPos = (z - texOffset.z)*texSize.x + x - texOffset.x;
				bytesPos *= 4;

				//if (bytesPos>=bytesLength || bytesPos<0) return texOffset.x;

				converter.f = thism.arr[matrixPos] * mult;
				bytes[bytesPos] = converter.b[0];
				bytes[bytesPos + 1] = converter.b[1];
				bytes[bytesPos + 2] = converter.b[2];
				bytes[bytesPos + 3] = converter.b[3];
			}
	}

	PLUGIN_API void Matrix_ExportHeights(Matrix& thism, float* heights, Coord heightsOffset, Coord heightsSize)
	{
		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(heightsOffset, heightsSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int heightsPosX = x - heightsOffset.x;
				int heightsPosZ = z - heightsOffset.z;

				float val = thism.arr[matrixPos];
				heights[heightsPosZ*heightsSize.x + heightsPosX] = val;
			}
	}

	PLUGIN_API void Matrix_ExportSplats(Matrix& thism, float* splats, Coord splatsOffset, Coord splatsSize, int numChannels, int channel)
	{
		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(splatsOffset, splatsSize));
		Coord min = intersection.Min(); Coord max = intersection.Max();

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int heightsPosX = x - splatsOffset.x;
				int heightsPosZ = z - splatsOffset.z;

				float val = thism.arr[matrixPos];
				splats[heightsPosZ*splatsSize.x*numChannels + heightsPosX * numChannels + channel] = val;
				//splats[channel*splatsSize.x*splatsSize.z + heightsPosZ*splatsSize.x + heightsPosX] = val;
			}
	}

	PLUGIN_API void Matrix_ExportDetail(Matrix& thism, int* detail, Coord detailOffset, Coord detailSize, int channel, Noise& random, float density = 1) //aka Grass
	{
		CoordRect intersection = CoordRect::Intersected(thism.rect, CoordRect(detailOffset, detailSize));
		Coord min = intersection.Min(); Coord max = intersection.Max() - 1;

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int matrixPos = (z - thism.rect.offset.z)*thism.rect.size.x + x - thism.rect.offset.x;
				int detailPosX = x - detailOffset.x;
				int detailPosZ = z - detailOffset.z;

				float val = thism.arr[matrixPos];

				//interpolating value since detail resolution is 512, while height is 513
				//val += matrix.arr[pos+1] + matrix.arr[pos+fullSize] + matrix.arr[pos+fullSize+1]; //margins should prevent reading out of bounds
				//val /= 4;

				//or using minimal interpolation (creates better visual effect - grass isn't growing where it should not)
				float val1 = thism.arr[matrixPos + 1];						if (val1 < val) val = val1;
				float val2 = thism.arr[matrixPos + thism.rect.size.x];		if (val2 < val) val = val2;
				float val3 = thism.arr[matrixPos + thism.rect.size.x + 1];	if (val3 < val) val = val3;

				//multiply with biome
				//if (biomeMask != null) //no empty biomes in list (so no mask == root biome)
				//	val *= biomeMask.arr[pos]; //if mask is not assigned biome was ignored, so only main outs with mask==null left here

				if (val < 0) val = 0; if (val > 1) val = 1;

				//the number of bushes in pixel
				val *= density;//*pixelSize;

				//random
				float rnd = random.Random(channel, x, z);

				//converting to integer with random
				int intVal = (int)val;
				float remain = val - intVal;
				if (remain > rnd) intVal++;

				//detail[detailPosZ*detailSize.x + detailPosX] = intVal;

				//converting to integer with random
				intVal = val > 0.001f ? 1 : 0;

				detail[detailPosZ*detailSize.x + detailPosX] = (int)(val + 0.5f);
			}
	}

#pragma endregion


#pragma region Other

	PLUGIN_API void MatrixLine(Matrix &thism, float startX, float startZ, float endX, float endZ, float valStart = 1, float valEnd = 1, bool antialised = false, bool paddedOnePixel = false, bool endInclusive = false)
		{ thism.Line(startX, startZ, endX, endZ, valStart, valEnd, antialised, paddedOnePixel, endInclusive); }
	void Matrix::Line(float startX, float startZ, float endX, float endZ, float valStart /*1*/, float valEnd /*1*/, bool antialised /*false*/, bool paddedOnePixel /*false*/, bool endInclusive /*false*/)
	/// Strokes the line from start (inclusive) to end (non-inclusive), gradientally filling it with valStart to valEnd. Antialiased adds 2 pixels to line width.
	/// PaddedOnePixel works similarly to AA, but fills border pixels with full value (to create main tex for the mask)
	{
		startX += 0.5f; startZ += 0.5f;  //way more convenient rounding in code below
		endX += 0.5f; endZ += 0.5f;

		int numSteps = max(
			abs(floor(endX) - floor(startX)),  //NOT (int)(end-start)!
			abs(floor(endZ) - floor(startZ)));

		float stepX = (endX - startX) / numSteps;
		float stepZ = (endZ - startZ) / numSteps;

		bool isVertical = abs(stepZ) > abs(stepX); //for antializasing

		int ei = endInclusive ? 1 : 0;
		for (int s = 0; s < numSteps + ei; s++)
		{
			float fx = startX + stepX * s - rect.offset.x;  //neglecting matrix rect, should be from 0 to matrix.rect.size
			float fz = startZ + stepZ * s - rect.offset.z;

			int ix = (int)(float)fx;  //if (fx<0) ix--;  
			int iz = (int)(float)fz;  //if (fz<0) iz--;  

			if (ix<0 || ix>rect.size.x - 1 ||
				iz<0 || iz>rect.size.z - 1)
				continue;

			int pos = iz * rect.size.x + ix;

			float valPercent = (float)s / numSteps;
			float val = valStart * (1 - valPercent) + valEnd * valPercent;

			arr[pos] = val;

			if (antialised)
			{
				if (!isVertical)
				{
					if (iz<1 || iz>rect.size.z - 2) continue;
					float p = fz - iz;
					arr[pos - rect.size.x] = arr[pos - rect.size.x] * p + val * (1 - p); //blending commented out since it create semas while Stroke
					arr[pos + rect.size.x] = arr[pos + rect.size.x] * (1 - p) + val * p;
				}
				else
				{
					if (ix<1 || ix>rect.size.x - 2) continue;
					float p = fx - ix;
					arr[pos - 1] = arr[pos - 1] * p + val * (1 - p);
					arr[pos + 1] = arr[pos + 1] * (1 - p) + val * p;
				}
			}

			if (paddedOnePixel)
			{
				if (!isVertical)
				{
					if (iz<1 || iz>rect.size.z - 2) continue;
					arr[pos - rect.size.x] = val;
					arr[pos + rect.size.x] = val;
				}
				else
				{
					if (ix<1 || ix>rect.size.x - 2) continue;
					arr[pos - 1] = val;
					arr[pos + 1] = val;
				}
			}
		}
	}


#pragma endregion
}
