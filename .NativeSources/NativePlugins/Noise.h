#pragma once

#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#ifndef Noise_H 
#define Noise_H


extern "C"
{
	class Noise
	{
		public:

			int seed; //the seed this noise was initialized with, for informational purpose
			int subSeed; //the seed that could be changed without re-building permutation. Never bigger permCount.

			int permutationCount;
			int permutationCountMinusOne;
			unsigned int * permutation;

			const static int gradX[];
			const static int gradZ[];

			Noise (unsigned int permutation[], int permutationCount);

			float Random(int x);
			float Random(int x, int y);
			float Random(int x, int y, int z);
			float Random(int x, int y, int z, int w);

			float Linear(float x, float z);
			float Perlin(float x, float z);
			float Simplex(float x, float z); // based on Stefan Gustavson's code (http://webstaff.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf)

			float Fractal(float x, float y, float size, int iterations=-1, float detail=0.5f, float turbulence=0, int type=2);
	};
}

#endif
