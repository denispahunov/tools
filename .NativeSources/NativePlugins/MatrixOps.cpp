
#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#include <cstdio>
#include <algorithm>
#include <math.h>

#include "MatrixOps.h"
#include "Vector2.h"

using namespace std;

extern "C"
{
	Stripe::Stripe(float* arr, int length) { this->arr = arr; this->length = length; }
	Stripe::Stripe(int length) { this->length = length; arr = new float[length]; }
	Stripe::Stripe(Stripe& stripe) 
	{ 
		this->arr = new float[stripe.length]; 
		//copy(arr, arr + sizeof(arr)/sizeof(*arr), resultArr);
		for (int i = 0; i < stripe.length; i++)
			arr[i] = stripe.arr[i];
		length = stripe.length; 
	}

	void Stripe::Expand(int newCount) 
	{ 
		length = newCount; 
		float* newArr = new float[newCount];
		for (int i = 0; i < length; i++)
			newArr[i] = arr[i];
		delete(arr);
		arr = newArr;
	}

	void Stripe::Fill(float val) 
		{ for (int i = 0; i < length; i++) arr[i] = val; }

	void Stripe::Copy(Stripe& src, Stripe& dst) 
	{
		for (int i = 0; i<src.length; i++)
			dst.arr[i] = src.arr[i];
		dst.length = src.length; 
	}

	void Stripe::Swap(Stripe& s1, Stripe& s2) 
	{ 
		for (int i = 0; i < s1.length; i++)
		{
			float t = s1.arr[i];
			s1.arr[i] = s2.arr[i];
			s2.arr[i] = t;
		}
	}


	#pragma region Stripe Readings / Writings

	PLUGIN_API void MatrixOps_ReadLine(Stripe& stripe, Matrix& matrix, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int ix = 0; ix < stripe.length; ix++)
			stripe.arr[ix] = matrix.arr[start + ix];
		//Array.Copy(this.arr, start, stripe.arr, 0, stripe.length);
	}

	PLUGIN_API void MatrixOps_WriteLine(Stripe& stripe, Matrix& matrix, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int ix = 0; ix < stripe.length; ix++)
			matrix.arr[start + ix] = stripe.arr[ix];
		//Array.Copy(stripe.arr, 0, this.arr, start, stripe.length);
	}

	PLUGIN_API void MatrixOps_MaxLine(Stripe& stripe, Matrix& matrix, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int ix = 0; ix < stripe.length; ix++)
		{
			float matrixVal = matrix.arr[start + ix];
			float lineVal = stripe.arr[ix];
			if (lineVal > matrixVal) matrix.arr[start + ix] = lineVal;
		}
	}

	PLUGIN_API void MatrixOps_AddLine(Stripe& stripe, Matrix& matrix, int x, int z, float opacity)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int ix = 0; ix < stripe.length; ix++)
			matrix.arr[start + ix] += stripe.arr[ix] * opacity;
	}

	PLUGIN_API void MatrixOps_ReadRow(Stripe& stripe, Matrix& matrix, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
			stripe.arr[iz] = matrix.arr[start + iz * matrix.rect.size.x];
	}

	PLUGIN_API void MatrixOps_WriteRow(Stripe& stripe, Matrix& matrix, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
			matrix.arr[start + iz * matrix.rect.size.x] = stripe.arr[iz];
	}

	PLUGIN_API void MatrixOps_WriteRowMask(Stripe& stripe, Matrix& matrix, int x, int z, float* mask)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
			matrix.arr[start + iz * matrix.rect.size.x] = stripe.arr[iz] * mask[iz];
	}

	PLUGIN_API void MatrixOps_MaxRow(Stripe& stripe, Matrix& matrix, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
		{
			float matrixVal = matrix.arr[start + iz * matrix.rect.size.x];
			float lineVal = stripe.arr[iz];
			if (lineVal > matrixVal) matrix.arr[start + iz * matrix.rect.size.x] = lineVal;
		}
	}

	PLUGIN_API void MatrixOps_AddRow(Stripe& stripe, Matrix& matrix, int x, int z, float opacity = 1)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
			matrix.arr[start + iz * matrix.rect.size.x] += stripe.arr[iz] * opacity;
	}

	PLUGIN_API void MatrixOps_OverlayRow(Stripe& stripe, Matrix& matrix, int x, int z, float opacity = 1)
	//Uses stripe values with range -1 - 1
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
		{
			float valA = matrix.arr[start + iz * matrix.rect.size.x];
			float valB = stripe.arr[iz];

			valB *= opacity;

			matrix.arr[start + iz * matrix.rect.size.x] = 2 * valA*valB + valA + valB;

			//for range 0-1 just in case
			//if (valA < 0.5f) return 2 * valA*valB;
			//return valA = 1 - 2 * (1 - valA)*(1 - valB);
		}
	}

	PLUGIN_API void MatrixOps_MixRow(Matrix& dst, Matrix& matrix, Matrix& matrixMask, Stripe& stripe, Stripe& stripeMask, int x, int z)
	/// Commutative operation, doesn't matter if row applied to mask or vice versa
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
		{
			int pos = start + iz * matrix.rect.size.x;
			float sum = matrixMask.arr[pos] + stripeMask.arr[iz];

			dst.arr[pos] = sum > 0 ?
				(matrix.arr[pos] * matrixMask.arr[pos] + stripe.arr[iz] * stripeMask.arr[iz]) / sum :
				matrix.arr[pos] + stripe.arr[iz];
		}
	}

	PLUGIN_API void MatrixOps_MaxRowMax(Matrix& dst, Matrix& matrix, Matrix& matrixMask, Stripe& stripe, Stripe& stripeMask, int x, int z)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;
		for (int iz = 0; iz < stripe.length; iz++)
		{
			int pos = start + iz * matrix.rect.size.x;
			dst.arr[pos] = matrixMask.arr[pos] > stripeMask.arr[iz] ?
				matrix.arr[pos] :
				stripe.arr[iz];
		}
	}


	PLUGIN_API void MatrixOps_ReadDiagonal(Stripe& stripe, Matrix& matrix, int x, int z, float stepX = 1, float stepZ = 1)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;

		//clamping stripe length to matrix borders
		float xLength = matrix.rect.size.x + matrix.rect.size.z;
		if (stepX > 0.001f) xLength = (matrix.rect.offset.x + matrix.rect.size.x - x) / stepX;
		if (stepX < -0.001f) xLength = (x - matrix.rect.offset.x) / (-stepX);

		float zLength = matrix.rect.size.x + matrix.rect.size.z;
		if (stepZ > 0.001f) zLength = (matrix.rect.offset.z + matrix.rect.size.z - z) / stepZ;
		if (stepZ < -0.001f) zLength = (z - matrix.rect.offset.z) / (-stepZ);

		stripe.length = (int)((xLength < zLength ? xLength : zLength) - 0.5f); //if no 0.5 will exceed the matrix border (checked)

		//reading
		for (int i = 0; i < stripe.length; i++)
		{
			int posX = (int)(i*stepX + 0.5f);
			int posZ = (int)(i*stepZ + 0.5f);

			stripe.arr[i] = matrix.arr[start + posX + posZ * matrix.rect.size.x];
		}
	}

	PLUGIN_API void MatrixOps_WriteDiagonal(Stripe& stripe, Matrix& matrix, int x, int z, float stepX = 1, float stepZ = 1)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;

		for (int i = 0; i < stripe.length; i++)
		{
			int posX = (int)(i*stepX + 0.5f); 
			int posZ = (int)(i*stepZ + 0.5f);

			matrix.arr[start + posX + posZ * matrix.rect.size.x] = stripe.arr[i];
		}
	}

	PLUGIN_API void MatrixOps_MaxDiagonal(Stripe& stripe, Matrix& matrix, int x, int z, float stepX = 1, float stepZ = 1)
	{
		int start = (z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + x;

		for (int i = 0; i < stripe.length; i++)
		{
			int posX = (int)(i*stepX + 0.5f);
			int posZ = (int)(i*stepZ + 0.5f);

			float matrixVal = matrix.arr[start + posX + posZ * matrix.rect.size.x];
			float lineVal = stripe.arr[i];

			if (lineVal > matrixVal)
				matrix.arr[start + posX + posZ * matrix.rect.size.x] = lineVal;
		}
	}


	PLUGIN_API void MatrixOps_ReadInclined(Stripe& stripe, Matrix& matrix, Vector2 start, Vector2 step)
	{
		for (int i = 0; i < stripe.length; i++)
		{
			float x = start.x + step.x*i;
			float z = start.y + step.y*i;

			if (x < 0) x--;  int ix = (int)(float)(x + 0.5f);
			if (z < 0) z--;  int iz = (int)(float)(z + 0.5f);

			if (ix < matrix.rect.offset.x || ix >= matrix.rect.offset.x + matrix.rect.size.x ||
				iz < matrix.rect.offset.z || iz >= matrix.rect.offset.z + matrix.rect.size.z)
				stripe.arr[i] = -0.00001f; //-Mathf.Epsilon;
			else
			{
				int pos = (iz - matrix.rect.offset.z)*matrix.rect.size.x + ix - matrix.rect.offset.x;
				stripe.arr[i] = matrix.arr[pos];
			}
		}
	}


	PLUGIN_API void MatrixOps_WriteInclined(Stripe& stripe, Matrix& matrix, Vector2 start, Vector2 step)
	{
		for (int i = 0; i < stripe.length; i++)
		{
			float x = start.x + step.x*i;
			float z = start.y + step.y*i;

			if (x < 0) x--;  int ix = (int)(float)(x + 0.5f);
			if (z < 0) z--;  int iz = (int)(float)(z + 0.5f);

			if (ix < matrix.rect.offset.x || ix >= matrix.rect.offset.x + matrix.rect.size.x ||
				iz < matrix.rect.offset.z || iz >= matrix.rect.offset.z + matrix.rect.size.z)
				continue;
			else
			{
				int pos = (iz - matrix.rect.offset.z)*matrix.rect.size.x + ix - matrix.rect.offset.x;
				matrix.arr[pos] = stripe.arr[i];
			}
		}
	}


	PLUGIN_API void MatrixOps_ReadStrip(Stripe stripe, Matrix matrix, Vector2 start, Vector2 end)
	{
		Vector2 delta = end - start;
		Vector2 direction = delta.Normalized();

		//making any of the step components equal to 1
		Vector2 posDir = Vector2(
			(direction.x > 0 ? direction.x : -direction.x),
			(direction.y > 0 ? direction.y : -direction.y));
		float max = posDir.x > posDir.y ? posDir.x : posDir.y;
		Vector2 step = direction / max;

		Vector2 absDelta = Vector2(delta.x > 0 ? delta.x : -delta.x, delta.y > 0 ? delta.y : -delta.y);
		int numSteps = (int)(absDelta.x > absDelta.y ? absDelta.x : absDelta.y) + 1;
		stripe.length = numSteps < stripe.length ? numSteps : stripe.length;

		MatrixOps_ReadInclined(stripe, matrix, start, step);
	}


	PLUGIN_API void MatrixOps_WriteStrip(Stripe stripe, Matrix matrix, Vector2 start, Vector2 end)
	{
		Vector2 delta = end - start;
		Vector2 direction = delta.Normalized();

		//making any of the step components equal to 1
		Vector2 posDir = Vector2(
			(direction.x > 0 ? direction.x : -direction.x),
			(direction.y > 0 ? direction.y : -direction.y));
		float max = posDir.x > posDir.y ? posDir.x : posDir.y;
		Vector2 step = direction / max;

		int numSteps = (int)(delta.x > delta.y ? delta.x : delta.y) + 1;
		stripe.length = numSteps < stripe.length ? numSteps : stripe.length;

		MatrixOps_WriteInclined(stripe, matrix, start, step);
	}


	PLUGIN_API void MatrixOps_ReadSquare(Stripe& stripe, Matrix& matrix, Coord center, int radius)
	/// Same as circular, but in form of square. 4 lines one-by-one. Useful for blurs and spreads
	{
		int side = radius * 2 + 1;
		stripe.length = side * 4;

		//resetting line
		for (int i = 0; i < side * 4; i++)
			stripe.arr[i] = -std::numeric_limits<float>::epsilon();

		Coord min = center - radius;
		Coord max = center + radius;

		Coord rectMin = matrix.rect.offset;
		Coord rectMax = matrix.rect.offset + matrix.rect.size;

		int start = (min.z - matrix.rect.offset.z - 1)*matrix.rect.size.x - matrix.rect.offset.x + min.x;	//matrix[min.x, min.z]
		if (min.z - 1 >= rectMin.z  &&  min.z - 1 < rectMax.z)
			for (int x = 0; x < side; x++)
				if (x + min.x >= rectMin.x  &&  x + min.x < rectMax.x)
					stripe.arr[x] = matrix.arr[start + x];

		start = (min.z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + max.x;	//matrix[max.x, min.z]
		if (max.x >= rectMin.x  &&  max.x < rectMax.x)
			for (int z = 0; z < side; z++)
				if (z + min.z >= rectMin.z  &&  z + min.z < rectMax.z)
					stripe.arr[z + side] = matrix.arr[start + z * matrix.rect.size.x];

		start = (max.z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + max.x;	//matrix[max.x-1, max.z]
		if (max.z >= rectMin.z  &&  max.z < rectMax.z)
			for (int x = 0; x < side; x++)
				if (max.x - x >= rectMin.x  &&  max.x - x < rectMax.x)
					stripe.arr[x + side * 2] = matrix.arr[start - x];

		start = (max.z - matrix.rect.offset.z - 1)*matrix.rect.size.x - matrix.rect.offset.x + min.x;	//matrix[min.x-1, max.z-1]
		if (min.x - 1 >= rectMin.x  &&  min.x < rectMax.x)
			for (int z = 0; z < side; z++)
				if (max.z - 1 - z >= rectMin.z  &&  max.z - 1 - z < rectMax.z)
					stripe.arr[z + side * 3] = matrix.arr[start - z * matrix.rect.size.x]; //matrix[min.x, max.z-1-z]

			//closing
		stripe.arr[0] = stripe.arr[stripe.length - 1];
	}


	PLUGIN_API void MatrixOps_WriteSquare(Stripe& stripe, Matrix& matrix, Coord center, int radius)
	/// Same as circular, but in form of square. 4 lines one-by-one. Useful for blurs and spreads
	/// Line length should be radius*8 + 4 corners
	{
		int side = radius * 2 + 1;
		stripe.length = side * 4;

		Coord min = center - radius;
		Coord max = center + radius;

		Coord rectMin = matrix.rect.offset;
		Coord rectMax = matrix.rect.offset + matrix.rect.size;

		int start = (min.z - matrix.rect.offset.z - 1)*matrix.rect.size.x - matrix.rect.offset.x + min.x;	//matrix[min.x, min.z]
		if (min.z - 1 >= rectMin.z  &&  min.z - 1 < rectMax.z)
			for (int x = 0; x < side; x++)
				if (x + min.x >= rectMin.x  &&  x + min.x < rectMax.x)
					matrix.arr[start + x] = stripe.arr[x];

		start = (min.z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + max.x;	//matrix[max.x, min.z]
		if (max.x >= rectMin.x  &&  max.x < rectMax.x)
			for (int z = 0; z < side; z++)
				if (z + min.z >= rectMin.z  &&  z + min.z < rectMax.z)
					matrix.arr[start + z * matrix.rect.size.x] = stripe.arr[z + side];

		start = (max.z - matrix.rect.offset.z)*matrix.rect.size.x - matrix.rect.offset.x + max.x;	//matrix[max.x-1, max.z]
		if (max.z >= rectMin.z  &&  max.z < rectMax.z)
			for (int x = 0; x < side; x++)
				if (max.x - x >= rectMin.x  &&  max.x - x < rectMax.x)
					matrix.arr[start - x] = stripe.arr[x + side * 2];

		start = (max.z - matrix.rect.offset.z - 1)*matrix.rect.size.x - matrix.rect.offset.x + min.x;	//matrix[min.x-1, max.z-1]
		if (min.x >= rectMin.x  &&  min.x < rectMax.x)
			for (int z = 0; z < side; z++)
				if (max.z - 1 - z >= rectMin.z  &&  max.z - 1 - z < rectMax.z)
					matrix.arr[start - z * matrix.rect.size.x] = stripe.arr[z + side * 3]; //matrix[min.x, max.z-1-z]
	}

	#pragma endregion


	#pragma region Free Resize

	void MatrixOps_DownsizeHorizontally(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe)
	{
		srcStripe.length = src.rect.size.x;
		dstStripe.length = dst.rect.size.x;

		for (int z = 0; z < src.rect.size.z; z++) //should match dst.rect.size.z
		{
			MatrixOps_ReadLine(srcStripe, src, src.rect.offset.x, z + src.rect.offset.z);
			MatrixOps_ResampleStripeLinear(srcStripe, dstStripe);
			MatrixOps_WriteLine(dstStripe, dst, dst.rect.offset.x, z + dst.rect.offset.z);
		}
	}


	void MatrixOps_DownsizeVertically(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe)
	{
		srcStripe.length = src.rect.size.z;
		dstStripe.length = dst.rect.size.z;

		for (int x = 0; x < dst.rect.size.x; x++) //should match src.rect.size.x
		{
			MatrixOps_ReadRow(srcStripe, src, x + src.rect.offset.x, src.rect.offset.z);
			MatrixOps_ResampleStripeLinear(srcStripe, dstStripe);
			MatrixOps_WriteRow(dstStripe, dst, x + dst.rect.offset.x, dst.rect.offset.z);
		}
	}


	void MatrixOps_UpsizeHorizontally(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe, float srcOffset, float srcLength)
	{
		srcStripe.length = src.rect.size.x;
		dstStripe.length = dst.rect.size.x;

		for (int z = src.rect.size.z - 1; z >= 0; z--) //from end to start since in some cases we would be using the same array with different rects
		{
			MatrixOps_ReadLine(srcStripe, src, src.rect.offset.x, z + src.rect.offset.z);
			MatrixOps_ResampleStripeCubic(srcStripe, dstStripe, srcOffset, srcLength);
			MatrixOps_WriteLine(dstStripe, dst, dst.rect.offset.x, z + dst.rect.offset.z);
		}
	}


	void MatrixOps_UpsizeVertically(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe, float srcOffset, float srcLength)
	{
		srcStripe.length = src.rect.size.z;
		dstStripe.length = dst.rect.size.z;

		for (int x = src.rect.size.x - 1; x >= 0; x--)
		{
			MatrixOps_ReadRow(srcStripe, src, x + src.rect.offset.x, src.rect.offset.z);
			MatrixOps_ResampleStripeCubic(srcStripe, dstStripe, srcOffset, srcLength);
			MatrixOps_WriteRow(dstStripe, dst, x + dst.rect.offset.x, dst.rect.offset.z);
		}
	}


	void MatrixOps_ResampleStripeCubic(Stripe& src, Stripe& dst, float srcOffset, float srcLength)
	/// Scales the stripe filling dst with interpolated values. Cubic for upscale
	{
		if (srcLength < 1) srcLength = src.length;

		for (int dstX = 0; dstX < dst.length; dstX++)
		{
			float srcX = (float)dstX * srcLength / dst.length + srcOffset;

			int px = (int)srcX;	if (px < 0) px = 0;
			int nx = px + 1;		if (nx > src.length - 1) nx = src.length - 1;
			int ppx = px - 1;		if (ppx < 0) ppx = 0;
			int nnx = nx + 1;		if (nnx > src.length - 1) nnx = src.length - 1;
			int pppx = ppx - 1;	if (pppx < 0) pppx = 0;
			int nnnx = nnx + 1;	if (nnnx > src.length - 1) nnnx = src.length - 1;


			float vp = src.arr[px]; float vpp = src.arr[ppx]; float vppp = src.arr[pppx];
			float vn = src.arr[nx]; float vnn = src.arr[nnx]; float vnnn = src.arr[nnnx];

			float p = srcX - px;
			float ip = 1.0 - p;

			float dpp = vpp - (vppp + vp - vpp * 2) * 0.25f;
			float dp = vp - (vpp + vn - vp * 2) * 0.25f;
			float dn = vn - (vnn + vp - vn * 2) * 0.25f;
			float dnn = vnn - (vnnn + vn - vnn * 2) * 0.25f;

			float tp = (dn - dpp)*0.5f;
			float tn = (dp - dnn)*0.5f;

			float l = vp * ip + vn * p; //linear filtration

			float cp =
				(vp + tp * p) * ip +
				l * p;

			float cn = l * ip +
				(vn + tn * ip) * p;

			dst.arr[dstX] = cp * ip + cn * p;

			//dst.arr[dstX] = vp + 0.5f * p * (vn - vpp + p*(2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + p*(3.0f*(vp - vn) + vnn - vpp)));
				//standard quadratic filtration (here for test purpose)

			if (dst.arr[dstX] > 1) dst.arr[dstX] = 1;
			if (dst.arr[dstX] < 0) dst.arr[dstX] = 0;
		}
	}


	void MatrixOps_ResampleStripeQuadratic(Stripe& src, Stripe& dst)
	/// Scales the stripe filling dst with interpolated values. Faster than cubic, but can result in "grid" look because of the tangents
	{
		for (int dstX = 0; dstX < dst.length; dstX++)
		{
			float srcX = (1.0f*dstX / dst.length) * src.length;

			int px = (int)srcX;	if (px < 0) px = 0;
			int nx = px + 1;		if (nx > src.length - 1) nx = src.length - 1;
			int ppx = px - 1;		if (ppx < 0) ppx = 0;
			int nnx = nx + 1;		if (nnx > src.length - 1) nnx = src.length - 1;

			float vp = src.arr[px]; float vpp = src.arr[ppx];
			float vn = src.arr[nx]; float vnn = src.arr[nnx];

			float p = srcX - px;
			float ip = 1 - p;

			float tp = (vn - vpp)*0.5f;
			float tn = (vp - vnn)*0.5f;

			float l = vp * ip + vn * p; //linear filtration

			float cp =
				(vp + tp*p) * ip +
				l * p;

			float cn = l * ip +
				(vn + tn*ip) * p;

			dst.arr[dstX] = cp*ip + cn*p;

			//dst.arr[dstX] = vp + 0.5f * p * (vn - vpp + p*(2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + p*(3.0f*(vp - vn) + vnn - vpp)));
				//standard quadratic filtration (here for test purpose)

			if (dst.arr[dstX] > 1) dst.arr[dstX] = 1;
			if (dst.arr[dstX] < 0) dst.arr[dstX] = 0;
		}
	}


	void MatrixOps_ResampleStripeLinear(Stripe& src, Stripe& dst)
	/// Scales the line filling dst with interpolated values. Linear for downscale
	{
		for (int dstX = 0; dstX < dst.length; dstX++)
		{
			int srcStartX = (int)(((float)(dstX - 1) / dst.length) * src.length);
			if (srcStartX < 0) srcStartX = 0;

			int srcEndX = (int)(((float)(dstX + 1) / dst.length) * src.length);
			if (srcEndX >= src.length) srcEndX = src.length - 1;

			float val = 0;
			float sum = 0;

			for (int srcX = srcStartX; srcX <= srcEndX; srcX++)
			{
				float refX = ((float)srcX / src.length) * dst.length; //converting back to dst gauge

				float percent = refX - dstX;
				if (percent < 0) percent = -percent;
				percent = 1 - percent;

				val += src.arr[srcX] * percent;
				sum += percent;
			}

			dst.arr[dstX] = sum != 0 ? val / sum : 0;
		}
	}

	#pragma endregion


	#pragma region Free Resize

	void MatrixOps_UpscaleFast(Matrix& src, Matrix& dst)
	/// Only for cases when dst rect size is exactly twice bigger than src
	/// Does not require temp matrix
	{
		//if (dst.rect.size.x != src.rect.size.x * 2 || dst.rect.size.z != src.rect.size.z * 2)
		//	throw new Exception("Matrix Upscale Fast: rect size mismatch: src:" + src.rect.size.ToString() + " dst:" + dst.rect.size.ToString());

		Stripe *srcStripe = new Stripe(dst.rect.size.Maximal());
		Stripe *dstStripe = new Stripe((*srcStripe).length);

		(*srcStripe).length = src.rect.size.x;
		(*dstStripe).length = dst.rect.size.x;
		for (int z = 0; z < src.rect.size.z; z++)
		{
			MatrixOps_ReadLine(*srcStripe, src, src.rect.offset.x, z + src.rect.offset.z);
			MatrixOps_ResampleStripeUpFast(*srcStripe, *dstStripe);
			MatrixOps_WriteLine(*dstStripe, dst, dst.rect.offset.x, z + dst.rect.offset.z);
		}

		(*srcStripe).length = src.rect.size.z;
		(*dstStripe).length = dst.rect.size.z;
		for (int x = dst.rect.size.x - 1; x >= 0; x--) //inverse order to re-use the same array
		{
			MatrixOps_ReadRow(*srcStripe, dst, x + dst.rect.offset.x, dst.rect.offset.z);
			MatrixOps_ResampleStripeUpFast(*srcStripe, *dstStripe);
			MatrixOps_WriteRow(*dstStripe, dst, x + dst.rect.offset.x, dst.rect.offset.z);
		}

		delete srcStripe;
		delete dstStripe;
	}


	/*void MatrixOps_DownscaleFast(Matrix& src, Matrix& dst)
	/// Only for cases when dst rect size is exactly twice smaller than src
	{
		//if (dst.rect.size.x*2 != src.rect.size.x || dst.rect.size.z*2 != src.rect.size.z)
		//	throw new Exception("Matrix Downscale Fast: rect size mismatch: src:" + src.rect.size.ToString() + " dst:" + dst.rect.size.ToString());

		Matrix *tmp = new Matrix( CoordRect(src.rect.offset.x, src.rect.offset.z, src.rect.size.x / 2, src.rect.size.z) );

		Stripe* srcStripe = new Stripe(src.rect.size.Maximal());
		Stripe* dstStripe = new Stripe((*srcStripe).length);

		MatrixOps_DownscaleFastTmp(src, dst, (*tmp), (*srcStripe), (*dstStripe));

		delete tmp;
		delete srcStripe;
		delete dstStripe;
	}*/


	void MatrixOps_DownscaleFastTmp(Matrix& src, Matrix& dst, Matrix& tmp, Stripe& srcStripe, Stripe& dstStripe)
	/// Only for cases when dst rect size is exactly twice smaller than src
	{
		srcStripe.length = src.rect.size.x;
		dstStripe.length = dst.rect.size.x;
		for (int z = 0; z < src.rect.size.z; z++)
		{
			MatrixOps_ReadLine(srcStripe, src, src.rect.offset.x, z + src.rect.offset.z);
			MatrixOps_ResampleStripeDownFast(srcStripe, dstStripe);
			MatrixOps_WriteLine(dstStripe, tmp, tmp.rect.offset.x, z + tmp.rect.offset.z);
		}

		srcStripe.length = src.rect.size.z;
		dstStripe.length = dst.rect.size.z;
		for (int x = 0; x < dst.rect.size.x; x++)
		{
			MatrixOps_ReadRow(srcStripe, tmp, x + tmp.rect.offset.x, tmp.rect.offset.z);
			MatrixOps_ResampleStripeDownFast(srcStripe, dstStripe);
			MatrixOps_WriteRow(dstStripe, dst, x + dst.rect.offset.x, dst.rect.offset.z);
		}
	}


	void MatrixOps_ResampleStripeDownFast(Stripe& src, Stripe& dst)
	/// Like linear, works faster, but requires dst.size = src.size/2
	{
		for (int dstX = 1; dstX < dst.length - 1; dstX++)
		{
			//dst.arr[dstX] = src.arr[dstX*2]*0.5f + src.arr[dstX*2-1]*0.25f + src.arr[dstX*2+1]*0.25f;
			dst.arr[dstX] = src.arr[dstX * 2] * 0.5f + src.arr[dstX * 2 + 1] * 0.5f;
			//surprizingly this way it generates no offset
		}

		dst.arr[0] = src.arr[0] * 0.75f + src.arr[1] * 0.25f;
		dst.arr[dst.length - 1] = src.arr[src.length - 1] * 0.75f + src.arr[src.length - 2] * 0.25f;
	}


	void MatrixOps_ResampleStripeUpFast(Stripe& src, Stripe& dst)
	/// Like cubic, works faster, but requires dst.size = src.size*2
	{
		for (int srcX = 0; srcX < src.length - 1; srcX++)
		{
			dst.arr[srcX * 2] = src.arr[srcX] * 0.5f + src.arr[srcX + 1] * 0.5f;
			dst.arr[srcX * 2 + 1] = src.arr[srcX + 1];  //placing original value to pixel+1, this will downscale lossless with NN
		}

		if (src.length * 2 < dst.length) //duplicating the last pixel if dst is 1-pixel larger than src*2
			dst.arr[dst.length - 1] = src.arr[src.length - 1];

		dst.arr[src.length * 2 - 1] = src.arr[src.length - 1]; //*0.5f + src.arr[src.length-2]*0.5f;
		dst.arr[src.length * 2 - 2] = src.arr[src.length - 1] * 0.5f + src.arr[src.length - 2] * 0.5f;
	}

	void MatrixOps_ResizeNearestNeighbor(Matrix& src, Matrix& dst)
	/// For upsizing and downsizing. Does not actually use Stripe, so might be moved to Matrix
	{
		float dstToSrcX = (float)src.rect.size.x / (float)dst.rect.size.x;
		float dstToSrcZ = (float)src.rect.size.z / (float)dst.rect.size.z;

		for (int x = 0; x < dst.rect.size.x; x++)
			for (int z = 0; z < dst.rect.size.z; z++)
			{
				int dstPos = z * dst.rect.size.x + x;

				int srcX = (int)((x + 0.5f)*dstToSrcX);
				int srcZ = (int)((z + 0.5f)*dstToSrcZ);
				int srcPos = srcZ * src.rect.size.x + srcX;

				dst.arr[dstPos] = src.arr[srcPos];
			}
	}

	#pragma endregion



	#pragma region GaussianBlur

	void MatrixOps_GaussianBlur(Matrix& src, Matrix& dst, float blur)
	/// Blur value is the number of iterations
	{
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		Stripe* pStripe = new Stripe(rect.size.Maximal());
		Stripe stripe = *pStripe;

		stripe.length = rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, rect.offset.x, z);
			MatrixOps_BlurStripe(stripe, blur);
			MatrixOps_WriteLine(stripe, dst, rect.offset.x, z);
		}

		stripe.length = rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, dst, x, rect.offset.z);
			MatrixOps_BlurStripe(stripe, blur);
			MatrixOps_WriteRow(stripe, dst, x, rect.offset.z);
		}

		delete pStripe;
	}


	void MatrixOps_BlurStripe(Stripe& src, float blur)
	{
		int iterations = (int)blur;

		//iteration blur
		for (int i = 0; i < iterations; i++)
			MatrixOps_BlurIteration(src, 1);

		//last iteration - percentage
		float percent = blur - iterations;
		if (percent > 0.0001f)
			MatrixOps_BlurIteration(src, percent);
	}


	void MatrixOps_BlurIteration(Stripe& src, float blur)
	{
		float qBlur = blur * 0.25f;
		float hBlur = 1 - qBlur * 2;

		float vp = src.arr[0];
		float vx = src.arr[1];
		float vn;

		for (int x = 1; x < src.length - 1; x++)
		{
			vn = src.arr[x + 1];

			src.arr[x] = vp * qBlur + vx * hBlur + vn * qBlur;

			vp = vx;
			vx = vn;
		}

		src.arr[0] = src.arr[0] * hBlur + src.arr[1] * qBlur * 2;
		src.arr[src.length - 1] = src.arr[src.length - 1] * hBlur + src.arr[src.length - 2] * qBlur * 2;
	}


	void MatrixOps_BlurIteration05(Stripe& src)
	/// Surprisingly worse performance rather than read array. I expected less bounds check, but...
	{
		float vp = src.arr[0];
		float vx = src.arr[1];
		float vn;

		for (int x = 1; x < src.length - 1; x++)
		{
			vn = src.arr[x + 1];

			src.arr[x] = (vp + vx * 2 + vn - 0.000000000000001f)*0.25f;

			vp = vx;
			vx = vn;
		}

		src.arr[0] = (src.arr[1] + src.arr[0] + src.arr[0] - 0.000000000000001f) / 3;
		src.arr[src.length - 1] = (src.arr[src.length - 2] + src.arr[src.length - 1] + src.arr[src.length - 1] - 0.000000000000001f) / 3;
	}

	#pragma endregion




	#pragma region Downsample Blur

	void MatrixOps_DownsampleBlur(Matrix& src, Matrix& dst, int downsample, float blur)
	/// Blurs matrix by downscaling each line and then upscaling it back
	/// Downsample 1 means no re-scaling
	{
		//int downsamplePot = (int)pow(2, downsample - 1);
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		Stripe* pHiStripe = new Stripe(rect.size.Maximal());
		Stripe* pLoStripe = new Stripe((*pHiStripe).length / downsample);

		Stripe& hiStripe = (*pHiStripe);
		Stripe& loStripe = (*pLoStripe);

		hiStripe.length = rect.size.x;
		loStripe.length = hiStripe.length / downsample;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(hiStripe, src, rect.offset.x, z);

			MatrixOps_ResampleStripeLinear(hiStripe, loStripe);
			MatrixOps_BlurStripe(loStripe, blur);
			MatrixOps_ResampleStripeCubic(loStripe, hiStripe);

			MatrixOps_WriteLine(hiStripe, dst, rect.offset.x, z);
		}

		hiStripe.length = rect.size.z;
		loStripe.length = hiStripe.length / downsample;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(hiStripe, dst, x, rect.offset.z);

			MatrixOps_ResampleStripeLinear(hiStripe, loStripe);
			MatrixOps_BlurStripe(loStripe, blur);
			MatrixOps_ResampleStripeCubic(loStripe, hiStripe);

			MatrixOps_WriteRow(hiStripe, dst, x, rect.offset.z);
		}

		delete pHiStripe;
		delete pLoStripe;
	}

	void MatrixOps_OverblurMippedIteration(Matrix& mip, Matrix& mat, Matrix& tmp, Stripe& srcStripe, Stripe& dstStripe, float escalate)
	{
		srcStripe.length = mip.rect.size.x;
		dstStripe.length = mat.rect.size.x;
		for (int z = mip.rect.offset.z; z < mip.rect.offset.z + mip.rect.size.z; z++)
		{
			MatrixOps_ReadLine(srcStripe, mip, mip.rect.offset.x, z);
			MatrixOps_ResampleStripeUpFast(srcStripe, dstStripe);
			MatrixOps_WriteLine(dstStripe, tmp, mat.rect.offset.x, z);
		}

		srcStripe.length = mip.rect.size.z;
		dstStripe.length = mat.rect.size.z;
		for (int x = mat.rect.offset.x; x < mat.rect.offset.x + mat.rect.size.x; x++)
		{
			MatrixOps_ReadRow(srcStripe, tmp, x, mat.rect.offset.z);
			MatrixOps_ResampleStripeUpFast(srcStripe, dstStripe);
			MatrixOps_OverlayRow(dstStripe, mat, x, mat.rect.offset.z, escalate);
		}
	}

	#pragma endregion


	#pragma region Normals / Delta

	void MatrixOps_NormalsSet(Matrix& src, Matrix& normX, Matrix& normZ, Matrix& normBlue, float pixelSize, float height)
	/// Generates full set of normal map
	{
		Coord min = src.rect.Min(); Coord max = src.rect.Max();

		Stripe* pStripe = new Stripe(src.rect.size.Maximal());
		Stripe stripe = *pStripe;

		stripe.length = src.rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, src.rect.offset.x, z);
			MatrixOps_NormalsStripe(stripe, height);
			MatrixOps_WriteLine(stripe, normX, src.rect.offset.x, z);
		}

		stripe.length = src.rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, src, x, src.rect.offset.z);
			MatrixOps_NormalsStripe(stripe, height);
			MatrixOps_WriteRow(stripe, normZ, x, src.rect.offset.z);
		}

		MatrixOps_NormalizeSet(normX, normZ, normBlue, pixelSize);
	}

	void MatrixOps_NormalizeSet(Matrix& normX, Matrix& normZ, Matrix& normBlue, float pixelSize)
	{
		//Coord min = normX.rect.Min();
        //Coord max = normX.rect.Max();

		for (int i = 0; i < normX.count; i++)
		{
			float nx = normX.arr[i];
			float nz = normZ.arr[i];
			float blue = pixelSize * 2;

			float length = sqrt(nx*nx + blue * blue + nz * nz);

			normX.arr[i] = (nx / length + 1) / 2;
			normZ.arr[i] = (nz / length + 1) / 2;
			//if (normBlue != NULL)
				normBlue.arr[i] = blue / length;
		}
	}

	void MatrixOps_NormalsStripe(Stripe& stripe, float height)
	{
		if (stripe.length < 3) return;

		float prevHeight = stripe.arr[0];
		float currHeight = stripe.arr[1];
		for (int x = 1; x < stripe.length - 2; x++)
		{
			float nextHeight = stripe.arr[x + 1];

			stripe.arr[x] = (prevHeight - nextHeight)*height;

			prevHeight = currHeight;
			currHeight = nextHeight;
		}

		stripe.arr[0] = stripe.arr[1];
		stripe.arr[stripe.length - 1] = stripe.arr[stripe.length - 2];
	}


	void MatrixOps_SetToDir(Matrix& normX, Matrix& normZ, Matrix& normBlue, Matrix& dst, float dirX, float dirY, float dirZ, float intensity /*1*/, float wrapping /*1*/)
	/// Dot-lighting 3-channel normals set depending on direction
	{
		float dir[] = {dirX, dirY, dirZ};
		float normal[] = { 0, 0, 0 };  //Vector3 normal = new Vector3();

		for (int i = 0; i < dst.count; i++)
		{
			normal[0] = normX.arr[i] * 2 - 1;
			normal[1] = normBlue.arr[i] * 2 - 1;
			normal[2] = normZ.arr[i] * 2 - 1;

			//float val = Vector3.Dot(dir, normal);
			//float val = std::inner_product(a, a + sizeof(a) / sizeof(a[0]), b, 0); 
			float val = dir[0]*normal[0] + dir[1]*normal[1] + dir[2]*normal[2];

			val = val * (1 - wrapping) + wrapping / 2;
			val *= intensity;

			if (val < 0) val = 0;
			if (val > 1) val = 1;
			dst.arr[i] = val;
		}
	}


	void MatrixOps_Delta(Matrix& src, Matrix& dst)
	/// Finds the maximum delta (both pos and neg) for the neighbor pixel
	{
		Coord min = src.rect.Min(); Coord max = src.rect.Max();

		Stripe* pStripe = new Stripe(src.rect.size.Maximal());
		Stripe& stripe = *pStripe;

		stripe.length = src.rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, src.rect.offset.x, z);
			MatrixOps_DeltaStripe(stripe);
			MatrixOps_WriteLine(stripe, dst, src.rect.offset.x, z);
		}

		stripe.length = src.rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, src, x, src.rect.offset.z);
			MatrixOps_DeltaStripe(stripe);
			MatrixOps_MaxRow(stripe, dst, x, src.rect.offset.z);
		}

		delete pStripe;
	}

	void MatrixOps_DeltaStripe(Stripe& stripe)
	{
		float prev = stripe.arr[0];
		float curr = stripe.arr[1];

		for (int x = 1; x < stripe.length - 1; x++)
		{
			//float prev = arr[x-1];
			//float curr = arr[x];
			float next = stripe.arr[x + 1];

			float prevDelta = prev - curr; if (prevDelta < 0) prevDelta = -prevDelta;
			float nextDelta = next - curr; if (nextDelta < 0) nextDelta = -nextDelta;
			float delta = prevDelta > nextDelta ? prevDelta : nextDelta;

			stripe.arr[x] = delta;

			prev = curr;
			curr = next;
		}

		stripe.arr[0] = stripe.arr[1];
		stripe.arr[stripe.length - 1] = stripe.arr[stripe.length - 2];
	}

	/*Seems to be outdated
	void MatrixOps_NormalRelief(Matrix& src, Matrix& dst, float horizontalHeight, float verticalHeight)
	/// To generate the normal map of a heightmap
	{
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		Stripe* pStripe = new Stripe(src.rect.size.Maximal());
		Stripe& stripe = *pStripe;

		stripe.length = rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, rect.offset.x, z);
			MatrixOps_NormalStripe(stripe, horizontalHeight);
			MatrixOps_WriteLine(stripe, dst, rect.offset.x, z);
		}

		stripe.length = rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, src, x, rect.offset.z);
			MatrixOps_NormalStripe(stripe, verticalHeight);
			MatrixOps_AddRow(stripe, dst, x, rect.offset.z);
		}

		delete pStripe;
	}


	void MatrixOps_NormalStripe(Stripe& stripe, float height)
	/// Will return normal direction in range -1, 1
	{
		float prev = stripe.arr[0];
		float curr = stripe.arr[1];

		for (int x = 1; x < stripe.length - 1; x++)
		{
			//float prev = arr[x-1];
			//float curr = arr[x];
			float next = stripe.arr[x + 1];

			//Vector3(prev-next, height, 0)).normalized;
			float delta = prev - next;
			float magnitude = sqrt(delta*delta + height * height + delta * delta);
			float normal = delta * magnitude;
			normal = (normal + 1) / 2;

			stripe.arr[x] = normal;

			prev = curr;
			curr = next;
		}

		stripe.arr[0] = stripe.arr[1];
		stripe.arr[stripe.length - 1] = stripe.arr[stripe.length - 2];
	}
	*/

	#pragma endregion

	#pragma region Select

	void MatrixOps_Silhouette (Matrix& src, Matrix& dst, float level, bool antialiasing)
	/// Makes the pixels crossing level 1, others - 0
	{
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		Stripe* pStripe = new Stripe(src.rect.size.Maximal());
		Stripe& stripe = *pStripe;

		stripe.length = rect.size.x;
		for (int z=min.z; z<max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, rect.offset.x, z);
			MatrixOps_SilhouetteStripe(stripe, level, antialiasing);
			MatrixOps_WriteLine(stripe, dst, rect.offset.x, z);
		}

		stripe.length = rect.size.z;
		for (int x=min.x; x<max.x; x++)
		{
			MatrixOps_ReadRow(stripe, src, x, rect.offset.z);
			MatrixOps_SilhouetteStripe(stripe, level, antialiasing);
			MatrixOps_MaxRow(stripe, dst, x, rect.offset.z);
		}
	}

	void MatrixOps_SilhouetteStripe (Stripe& stripe, float level, bool antiAliasing)
	{
		float prevVal = stripe.arr[0];
		for (int x=0; x<stripe.length-1; x++)
		{
			float nextVal = stripe.arr[x+1];

			float prevSelect = 0;
			float nextSelect = 0;
			float percent = -1;

			if (prevVal < level  &&  nextVal >= level) //growing
			{
				float prevAbs = -(prevVal - level);
				float nextAbs = nextVal - level;
				percent = prevAbs / (prevAbs+nextAbs);
			}

			if (prevVal > level  &&  nextVal <= level) //lowering
			{
				float prevAbs = prevVal - level;
				float nextAbs = -(nextVal - level);
				percent = prevAbs / (prevAbs+nextAbs);
			}

			if (percent >= 0) //if growing or lowering
			{
				//prevSelect = 1-percent;
				//nextSelect = percent;
				//making closest pixel always 1, the other one 0-1

				if (percent < 0.5f)
				{ 
					prevSelect = 1; 
					if (antiAliasing) nextSelect = percent*2; 
				}

				else
				{ 
					nextSelect = 1; 
					if (antiAliasing) prevSelect = (1-percent)*2; 
				}
			}

			if (prevSelect > stripe.arr[x]) stripe.arr[x] = prevSelect;
			stripe.arr[x+1] = nextSelect;

			prevVal = nextVal;
		}	
	}
	#pragma endregion


	#pragma region Cavity

	void MatrixOps_Cavity(Matrix& src, Matrix& dst)
	/// Creates a map of curvatures - white for concave pixels and black for convex
	{
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		Stripe* pStripe = new Stripe(rect.size.Maximal());
		Stripe& stripe = *pStripe;

		stripe.length = rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, rect.offset.x, z);
			MatrixOps_CavityStripe(stripe);
			MatrixOps_AddLine(stripe, dst, rect.offset.x, z, 0.5f);
		}

		stripe.length = rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, src, x, rect.offset.z);
			MatrixOps_CavityStripe(stripe);
			MatrixOps_AddRow(stripe, dst, x, rect.offset.z, 0.5f); //apply row additively (with mid-point 0.5)
		}

		delete pStripe;
	}

	
	void MatrixOps_OverSpread(Matrix& src, Matrix& dst, float multiply)
	/// Doesn't work yet
	{
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		Stripe* pStripe = new Stripe(rect.size.Maximal());
		Stripe& stripe = *pStripe;

		Matrix* pPos =  new Matrix(src);
		Matrix& pos = *pPos;
		for (int i = 0; i < pos.count; i++)
			pos.arr[i] = (pos.arr[i] - 0.5f) * 2;

		for (int i = 0; i < 5; i++)
		{
			stripe.length = rect.size.x;
			for (int z = min.z; z < max.z; z++)
			{
				MatrixOps_ReadLine(stripe, pos, rect.offset.x, z);
				MatrixOps_SpreadMultiply(stripe, multiply);
				MatrixOps_WriteLine(stripe, pos, rect.offset.x, z);
			}

			stripe.length = rect.size.z;
			for (int x = min.x; x < max.x; x++)
			{
				MatrixOps_ReadRow(stripe, pos, x, rect.offset.z);
				MatrixOps_SpreadMultiply(stripe, multiply);
				MatrixOps_WriteRow(stripe, pos, x, rect.offset.z); //apply row additively (with mid-point 0.5)
			}
		}

		dst.arr = pos.arr;

		delete pStripe;
		delete pPos;
	}


	void MatrixOps_Invert(Stripe& src, Stripe& dst)
	{
		for (int i = 0; i < src.length; i++)
			dst.arr[i] = -src.arr[i];
	}


	void MatrixOps_CavityStripe(Stripe& stripe)
	{
		float prev = stripe.arr[0];
		float curr = stripe.arr[1];

		for (int x = 1; x < stripe.length - 1; x++)
		{
			float next = stripe.arr[x + 1];

			//float val = curr - (next + prev)/2;
			//float sign = val>0 ? 1 : -1;
			//val = (val*val*sign)*intensity*1000; 
			//val = (val+1) / 2;
			float avg = (next + prev) / 2;
			float val = avg - curr;

			stripe.arr[x] = val;

			prev = curr;
			curr = next;
		}
		stripe.arr[0] = stripe.arr[1];
		stripe.arr[stripe.length - 1] = stripe.arr[stripe.length - 2];
	}

	#pragma endregion


	#pragma region Spread

	void MatrixOps_SpreadLinear(Matrix& src, Matrix& dst, float subtract, bool diagonals, bool quarters, bool bulb)
	/// Smears all white values all over the matrix
	/// Each new pixel is multiplied with multiply and get subtract subtracted
	/// Will overwrite the gray values with the bigger spread values, so couldn't be used as padding
	{
		Coord min = src.rect.Min(); Coord max = src.rect.Max();

		Stripe* pStripe = new Stripe(src.rect.size.x + src.rect.size.z); //+ to process diagonals
		Stripe& stripe = *pStripe;

		//spreading two dimensions independently
		stripe.length = src.rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, src.rect.offset.x, z);
			MatrixOps_SpreadLinearLeft(stripe, subtract, 1);
			MatrixOps_SpreadLinearRight(stripe, subtract, 1);
			MatrixOps_MaxLine(stripe, dst, src.rect.offset.x, z);
		}

		stripe.length = src.rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, src, x, src.rect.offset.z);
			MatrixOps_SpreadLinearLeft(stripe, subtract, 1);
			MatrixOps_SpreadLinearRight(stripe, subtract, 1);
			MatrixOps_MaxRow(stripe, dst, x, dst.rect.offset.z);
		}

		//connecting crosses lines
		if (!diagonals)
		{
			stripe.length = src.rect.size.x;
			for (int z = min.z; z < max.z; z++)
			{
				MatrixOps_ReadLine(stripe, dst, src.rect.offset.x, z);
				MatrixOps_SpreadLinearLeft(stripe, subtract, 0);
				MatrixOps_SpreadLinearRight(stripe, subtract, 0);
				MatrixOps_MaxLine(stripe, dst, src.rect.offset.x, z);
			}

			stripe.length = src.rect.size.z;
			for (int x = min.x; x < max.x; x++)
			{
				MatrixOps_ReadRow(stripe, dst, x, src.rect.offset.z);
				MatrixOps_SpreadLinearLeft(stripe, subtract, 0);
				MatrixOps_SpreadLinearRight(stripe, subtract, 0);
				MatrixOps_MaxRow(stripe, dst, x, dst.rect.offset.z);
			}
		}

		else //diagonals
		{
			if (quarters)
			{
				float step = 0.4142f;  // sin(radians(22.5)) / cos(radians(22.5))
				float factor = 1.082387f;  // (1,step).magnitude;

				for (int z = max.z - 1; z >= min.z; z -= 3)
				{
					MatrixOps_ReadDiagonal(stripe, dst, min.x, z, step, 1);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, min.x, z, step, 1);
				}
				for (int x = min.x; x < max.x; x++)
				{
					MatrixOps_ReadDiagonal(stripe, dst, x, min.z, step, 1);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, x, min.z, step, 1);
				}

				for (int x = min.x; x < max.x; x += 3)
				{
					MatrixOps_ReadDiagonal(stripe, dst, x, min.z, -1, step);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, x, min.z, -1, step);
				}
				for (int z = min.z; z < max.z; z++)
				{
					MatrixOps_ReadDiagonal(stripe, dst, max.x - 1, z, -1, step);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, max.x - 1, z, -1, step);
				}

				for (int x = max.x - 1; x >= min.x; x--)
				{
					MatrixOps_ReadDiagonal(stripe, dst, x, max.z - 1, step, -1);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, x, max.z - 1, step, -1);
				}
				for (int z = max.z - 1; z >= min.z; z -= 3)
				{
					MatrixOps_ReadDiagonal(stripe, dst, min.x, z, step, -1);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, min.x, z, step, -1);
				}

				for (int z = min.z; z < max.z; z++)
				{
					MatrixOps_ReadDiagonal(stripe, dst, max.x - 1, z, -1, -step);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, max.x - 1, z, -1, -step);
				}
				for (int x = max.x - 1; x >= min.x; x -= 3)
				{
					MatrixOps_ReadDiagonal(stripe, dst, x, max.z - 1, -1, -step);
					MatrixOps_SpreadLinearLeft(stripe, subtract*factor, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*factor, 0);
					MatrixOps_MaxDiagonal(stripe, dst, x, max.z - 1, -1, -step);
				}
			}


			for (int z = max.z - 1; z >= min.z; z--)  //quarters leave empty pixels since they apply 1to3, so using diagonals after
			{
				int maxX = z == min.z ? max.x : min.x + 1;
				for (int x = min.x; x < maxX; x++)
				{
					MatrixOps_ReadDiagonal(stripe, dst, x, z, 1, 1);
					MatrixOps_SpreadLinearLeft(stripe, subtract*1.4142f, 0);  //sqrt(2)
					MatrixOps_SpreadLinearRight(stripe, subtract*1.4142f, 0);
					MatrixOps_MaxDiagonal(stripe, dst, x, z, 1, 1);
				}
			}

			for (int x = min.x; x < max.x; x++)
			{
				int maxZ = x == max.x - 1 ? max.z : min.z + 1;
				for (int z = min.z; z < maxZ; z++)
				{
					MatrixOps_ReadDiagonal(stripe, dst, x, z, -1, 1);
					MatrixOps_SpreadLinearLeft(stripe, subtract*1.41421f, 0);
					MatrixOps_SpreadLinearRight(stripe, subtract*1.41421f, 0);
					MatrixOps_MaxDiagonal(stripe, dst, x, z, -1, 1);
				}
			}
		}

		//bulb
		//simulates pseudo-round behaviour on edges
		if (bulb)
		{
			//applying bulb function to enter bulb mode
			for (int i = 0; i < dst.count; i++)
				dst.arr[i] = (float)(sqrt(2 * dst.arr[i] - dst.arr[i] * dst.arr[i])); //*0.5f + dst.arr[i]*0.5f;

			//spreading two dimensions
			stripe.length = src.rect.size.z;
			for (int x = min.x; x < max.x; x++)
			{
				MatrixOps_ReadRow(stripe, dst, x, src.rect.offset.z);
				MatrixOps_SpreadLinearLeft(stripe, subtract);
				MatrixOps_SpreadLinearRight(stripe, subtract);
				MatrixOps_MaxRow(stripe, dst, x, dst.rect.offset.z);
			}


			stripe.length = src.rect.size.x;
			for (int z = min.z; z < max.z; z++)
			{
				MatrixOps_ReadLine(stripe, dst, src.rect.offset.x, z);
				MatrixOps_SpreadLinearLeft(stripe, subtract);
				MatrixOps_SpreadLinearRight(stripe, subtract);
				MatrixOps_MaxLine(stripe, dst, src.rect.offset.x, z);
			}

			//applying bulb inverse and some magic to 'linearize' bulb
			for (int i = 0; i < dst.count; i++)
			{
				float val = dst.arr[i];
				val = (float)(1 - sqrt(1 - val * val));
				val = (float)(1 - sqrt(1 - val * val));

				val = (val - 0.5f) * 2;
				if (val < 0) val = 0;

				dst.arr[i] = val;
			}
		}
	}

	void MatrixOps_SpreadLinearRight(Stripe& stripe, float subtract, float regardSmallerValues)
	{
		if (stripe.length == 0) return; //diagonals

		float prevVal = stripe.arr[0];
		for (int x = 1; x < stripe.length; x++)
		{
			float currVal = stripe.arr[x];

			if (prevVal > currVal)
			{
				float newVal = currVal;

				newVal = prevVal - subtract + (currVal / prevVal)*subtract*regardSmallerValues;
				if (newVal < 0) newVal = 0;

				if (newVal > currVal)
				{
					stripe.arr[x] = newVal;
					currVal = newVal;
				}
			}

			prevVal = currVal;
		}
	}

	void MatrixOps_SpreadLinearLeft(Stripe& stripe, float subtract, float regardSmallerValues)
	{
		if (stripe.length < 3) return; //diagonals

		float prevVal = stripe.arr[stripe.length - 1];
		for (int x = stripe.length - 2; x >= 0; x--)
		{
			float currVal = stripe.arr[x];

			if (prevVal > currVal)
			{
				float newVal = currVal;

				newVal = prevVal - subtract + (currVal / prevVal)*subtract*regardSmallerValues;
				if (newVal < 0) newVal = 0;

				if (newVal > currVal)
				{
					stripe.arr[x] = newVal;
					currVal = newVal;
				}
			}

			prevVal = currVal;
		}
	}


	void MatrixOps_SpreadMultiply(Stripe& stripe, float multiply /*1.0f*/, float regardSmallerValues /*1*/)
	{
		MatrixOps_SpreadMultiplyRight(stripe, multiply, regardSmallerValues);
		MatrixOps_SpreadMultiplyLeft(stripe, multiply, regardSmallerValues);
	}


	void MatrixOps_SpreadMultiplyLeft(Stripe& stripe, float multiply /*1.0f*/, float regardSmallerValues /*1*/)
	{
		float prevVal = stripe.arr[stripe.length - 1];
		for (int x = stripe.length - 2; x >= 0; x--)
		{
			float currVal = stripe.arr[x];

			if (prevVal > currVal)
			{
				currVal = prevVal * multiply + currVal * (1 - multiply)*regardSmallerValues;
				stripe.arr[x] = currVal;
			}

			prevVal = currVal;
		}
	}


	void MatrixOps_SpreadMultiplyRight(Stripe& stripe, float multiply /*1.0f*/, float regardSmallerValues /*1*/)
	{
		float prevVal = stripe.arr[0];
		for (int x = 1; x < stripe.length - 1; x++)
		{
			float currVal = stripe.arr[x];

			if (prevVal > currVal)
			{
				currVal = prevVal * multiply + currVal * (1 - multiply)*regardSmallerValues;
				stripe.arr[x] = currVal;
			}

			prevVal = currVal;
		}
	}

	#pragma endregion


	#pragma region Padding

	void MatrixOps_PaddingOnePixel(Matrix& src, Matrix& srcMask, Matrix& dst, Matrix& dstMask, float intensity /*1*/)
	/// Pads one pixel only in all directions
	/// Supports AA
	{
		Stripe* pMaskStripe = new Stripe(src.rect.size.Maximal());
		Stripe* pStripe = new Stripe(src.rect.size.Maximal());

		Stripe& stripe = *pStripe;
		Stripe& maskStripe = *pMaskStripe;

		Coord min = src.rect.Min(); Coord max = src.rect.Max();

		maskStripe.length = maskStripe.length = src.rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, src, src.rect.offset.x, z);
			MatrixOps_ReadLine(maskStripe, srcMask, srcMask.rect.offset.x, z);

			MatrixOps_PadStripeOnePixel(stripe, maskStripe, intensity, false);
			MatrixOps_PadStripeOnePixel(stripe, maskStripe, intensity, true);

			MatrixOps_WriteLine(stripe, dst, src.rect.offset.x, z);
			MatrixOps_WriteLine(maskStripe, dstMask, srcMask.rect.offset.x, z);
		}

		maskStripe.length = maskStripe.length = src.rect.size.x;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, dst, x, src.rect.offset.z);
			MatrixOps_ReadRow(maskStripe, dstMask, x, srcMask.rect.offset.z);

			MatrixOps_PadStripeOnePixel(stripe, maskStripe, intensity, false);
			MatrixOps_PadStripeOnePixel(stripe, maskStripe, intensity, true);

			MatrixOps_WriteRow(stripe, dst, x, src.rect.offset.z);
			MatrixOps_WriteRow(maskStripe, dstMask, x, srcMask.rect.offset.z);
		}

		delete pStripe;
		delete pMaskStripe;
	}


	void MatrixOps_PadStripeOnePixel(Stripe& stripe, Stripe& mask, float intensity /*1*/, bool toLeft /*false*/)
	/// Moves image one pixel left, blending it with the previous one the way it's done in photoshop
	{
		float prev = stripe.arr[!toLeft ? 0 : stripe.length - 1];
		float prevMask = mask.arr[!toLeft ? 0 : stripe.length - 1];

		for (int x = !toLeft ? 0 : stripe.length - 1;
			!toLeft ? x < stripe.length : x >= 0;
			x += !toLeft ? +1 : -1)
		{
			float val = stripe.arr[x];
			float maskVal = mask.arr[x];

			float maskSum = maskVal + prevMask; if (maskSum == 0) maskSum = 1;
			float modifiedVal = (val*maskVal + prev * prevMask) / maskSum;
			stripe.arr[x] = val * maskVal +  //should maintain original value with original mask anyways
				modifiedVal * (1 - maskVal);

			float newMaskVal = maskVal + prevMask * (1 - maskVal);
			mask.arr[x] = maskVal * (1 - intensity) + newMaskVal * intensity; //intensity is applied only to mask

			prev = val;
			prevMask = maskVal;
		}
	}


	void MatrixOps_DownscaleMaskedFast(Matrix& src, Matrix& srcMask, Matrix& dst, Matrix& dstMask, Matrix& tmp, Matrix& tmpMask)
	{
		CoordRect rect = src.rect;

		int rectSizeMaximal = rect.size.Maximal();
		Stripe* pSrcStripe = new Stripe(rectSizeMaximal); //just the longest dimension
		Stripe* pDstStripe = new Stripe(rectSizeMaximal);
		Stripe* pSrcMaskStripe = new Stripe(rectSizeMaximal);
		Stripe* pDstMaskStripe = new Stripe(rectSizeMaximal);

		Stripe& srcStripe = *pSrcStripe;
		Stripe& dstStripe = *pDstStripe;
		Stripe& srcMaskStripe = *pSrcMaskStripe;
		Stripe& dstMaskStripe = *pDstMaskStripe;

		srcStripe.length = srcMaskStripe.length = src.rect.size.x;
		dstStripe.length = dstMaskStripe.length = dst.rect.size.x;
		for (int z = src.rect.offset.z; z < src.rect.offset.z + src.rect.size.z; z++)
		{
			MatrixOps_ReadLine(srcStripe, src, src.rect.offset.x, z);
			MatrixOps_ReadLine(srcMaskStripe, srcMask, src.rect.offset.x, z);

			MatrixOps_ResampleMaskedStripeDownFast(srcStripe, srcMaskStripe, dstStripe);
			MatrixOps_ResampleStripeDownFast(srcMaskStripe, dstMaskStripe);

			MatrixOps_WriteLine(dstStripe, tmp, src.rect.offset.x, z);
			MatrixOps_WriteLine(dstMaskStripe, tmpMask, src.rect.offset.x, z);
		}

		srcStripe.length = srcMaskStripe.length = src.rect.size.z;
		dstStripe.length = dstMaskStripe.length = dst.rect.size.z;
		for (int x = dst.rect.offset.x; x < dst.rect.offset.x + dst.rect.size.x; x++)
		{
			MatrixOps_ReadRow(srcStripe, tmp, x, src.rect.offset.z);
			MatrixOps_ReadRow(srcMaskStripe, tmpMask, x, src.rect.offset.z);

			MatrixOps_ResampleMaskedStripeDownFast(srcStripe, srcMaskStripe, dstStripe);
			MatrixOps_ResampleStripeDownFast(srcMaskStripe, dstMaskStripe);

			MatrixOps_WriteRow(dstStripe, dst, x, src.rect.offset.z);
			MatrixOps_WriteRow(dstMaskStripe, dstMask, x, src.rect.offset.z);
		}

		delete pSrcStripe;
		delete pDstStripe;
		delete pSrcMaskStripe;
		delete pDstMaskStripe;
	}


	void MatrixOps_ResampleAutomaskedStripeDownFast(Stripe& src, Stripe& dst, float minVal)
	/// Like ResampleStripeDownFast, but ignores black (lower than minVal) values
	{
		float sum; int num;

		for (int dstX = 1; dstX < dst.length - 1; dstX++)
		{
			sum = 0; num = 0;

			if (src.arr[dstX * 2] > minVal) { sum += src.arr[dstX * 2] * 2; num += 2; } //TODO: compare in longs
			if (src.arr[dstX * 2 - 1] > minVal) { sum += src.arr[dstX * 2 - 1]; num++; }
			if (src.arr[dstX * 2 + 1] > minVal) { sum += src.arr[dstX * 2 + 1]; num++; }

			dst.arr[dstX] = num > 0 ? sum / num : 0;
		}

		sum = 0; num = 0;
		if (src.arr[0] > minVal) { sum += src.arr[0] * 3; num += 3; }
		if (src.arr[1] > minVal) { sum += src.arr[1]; num++; }
		dst.arr[0] = num > 0 ? sum / num : 0;

		sum = 0; num = 0;
		if (src.arr[src.length - 1] > minVal) { sum += src.arr[src.length - 1] * 3; num += 3; }
		if (src.arr[src.length - 2] > minVal) { sum += src.arr[src.length - 2]; num++; }
		dst.arr[dst.length - 1] = num > 0 ? sum / num : 0;
	}


	void MatrixOps_ResampleMaskedStripeDownFast(Stripe& src, Stripe& mask, Stripe& dst)
	/// Like ResampleAutomaskedStripeDownFast, but uses mask instead minVal
	{
		float sum; float num;

		for (int dstX = 1; dstX < dst.length - 1; dstX++)
		{
			sum = 0; num = 0;

			sum += src.arr[dstX * 2] * mask.arr[dstX * 2] * 2;  num += mask.arr[dstX * 2] * 2;
			sum += src.arr[dstX * 2 - 1] * mask.arr[dstX * 2 - 1];  num += mask.arr[dstX * 2 - 1];
			sum += src.arr[dstX * 2 + 1] * mask.arr[dstX * 2 + 1];  num += mask.arr[dstX * 2 + 1];

			dst.arr[dstX] = num > 0 ? sum / num : 0;
		}

		sum = 0; num = 0;
		sum += src.arr[0] * mask.arr[0] * 3;  num += mask.arr[0] * 3;
		sum += src.arr[1] * mask.arr[1];	 num += mask.arr[1];
		dst.arr[0] = num > 0 ? sum / num : 0;

		sum = 0; num = 0;
		sum += src.arr[src.length - 1] * mask.arr[src.length - 1] * 3; num += mask.arr[src.length - 1] * 3;
		sum += src.arr[src.length - 2] * mask.arr[src.length - 2];   num += mask.arr[src.length - 2];
		dst.arr[dst.length - 1] = num > 0 ? sum / num : 0;
	}


	void MatrixOps_UpscaleMaskedFast(Matrix& src, Matrix& srcMask, Matrix& dst, Matrix& dstMask)
	{
		//if (dst.rect.size.x/2 != src.rect.size.x || dst.rect.size.z/2 != src.rect.size.z)
		//	throw new Exception("Matrix Upscale Fast: rect size mismatch: src:" + src.rect.size.ToString() + " dst:" + dst.rect.size.ToString());

		int rectSizeMaximal = dst.rect.size.Maximal();
		Stripe* pSrcStripe = new Stripe(rectSizeMaximal); //just the longest dimension
		Stripe* pDstStripe = new Stripe(rectSizeMaximal);
		Stripe* pSrcMaskStripe = new Stripe(rectSizeMaximal);
		Stripe* pDstMaskStripe = new Stripe(rectSizeMaximal);

		Stripe& srcStripe = *pSrcStripe;
		Stripe& dstStripe = *pDstStripe;
		Stripe& srcMaskStripe = *pSrcMaskStripe;
		Stripe& dstMaskStripe = *pDstMaskStripe;

		srcStripe.length = srcMaskStripe.length = src.rect.size.x;
		dstStripe.length = dstMaskStripe.length = dst.rect.size.x;
		for (int z = src.rect.offset.z; z < src.rect.offset.z + src.rect.size.z; z++)
		{
			MatrixOps_ReadLine(srcStripe, src, src.rect.offset.x, z);
			MatrixOps_ReadLine(srcMaskStripe, srcMask, srcMask.rect.offset.x, z);

			MatrixOps_ResampleMaskedStripeUpFast(srcStripe, srcMaskStripe, dstStripe);
			MatrixOps_ResampleStripeUpFast(srcMaskStripe, dstMaskStripe);

			MatrixOps_WriteLine(dstStripe, dst, dst.rect.offset.x, z);
			MatrixOps_WriteLine(dstMaskStripe, dstMask, dst.rect.offset.x, z);
		}

		srcStripe.length = srcMaskStripe.length = src.rect.size.z;
		dstStripe.length = dstMaskStripe.length = dst.rect.size.z;
		for (int x = dst.rect.offset.x; x < dst.rect.offset.x + dst.rect.size.x; x++)
		{
			MatrixOps_ReadRow(srcStripe, dst, x, dst.rect.offset.z);
			MatrixOps_ReadRow(srcMaskStripe, dstMask, x, dst.rect.offset.z);

			MatrixOps_ResampleMaskedStripeUpFast(srcStripe, srcMaskStripe, dstStripe);
			MatrixOps_ResampleStripeUpFast(srcMaskStripe, dstMaskStripe);

			MatrixOps_WriteRow(dstStripe, dst, x, src.rect.offset.z);
			MatrixOps_WriteRow(dstMaskStripe, dstMask, x, src.rect.offset.z);
		}

		delete pSrcStripe;
		delete pDstStripe;
		delete pSrcMaskStripe;
		delete pDstMaskStripe;
	}


	void MatrixOps_ResampleAutomaskedStripeUpFast(Stripe& src, Stripe& dst, float minVal)
	{
		float sum = 0; int num = 0;

		for (int srcX = 0; srcX < src.length - 1; srcX++)
		{
			dst.arr[srcX * 2] = src.arr[srcX];

			sum = 0; num = 0;
			if (src.arr[srcX] > minVal) { sum += src.arr[srcX]; num += 1; }
			if (src.arr[srcX + 1] > minVal) { sum += src.arr[srcX + 1]; num += 1; }
			dst.arr[srcX * 2 + 1] = num > 0 ? sum / num : 0;
		}

		dst.arr[dst.length - 1] = src.arr[src.length - 1];

		sum = 0; num = 0;
		if (src.arr[src.length - 1] > minVal) { sum += src.arr[src.length - 1]; num += 1; }
		if (src.arr[src.length - 2] > minVal) { sum += src.arr[src.length - 2]; num += 1; }
		dst.arr[dst.length - 2] = num > 0 ? sum / num : 0;
	}


	void MatrixOps_ResampleMaskedStripeUpFast(Stripe& src, Stripe& mask, Stripe& dst)
	{
		float sum = 0; float num = 0;

		for (int srcX = 0; srcX < src.length - 1; srcX++)
		{
			dst.arr[srcX * 2] = src.arr[srcX];

			sum = 0; num = 0;
			sum += src.arr[srcX] * mask.arr[srcX]; num += mask.arr[srcX];
			sum += src.arr[srcX + 1] * mask.arr[srcX + 1]; num += mask.arr[srcX + 1];
			dst.arr[srcX * 2 + 1] = num > 0 ? sum / num : 0;
		}

		if (src.length * 2 < dst.length) //duplicating the last pixel if dst is 1-pixel larger than src*2
			dst.arr[dst.length - 1] = src.arr[src.length - 1];

		dst.arr[src.length * 2 - 1] = src.arr[src.length - 1];

		sum = 0; num = 0;
		sum += src.arr[src.length - 1] * mask.arr[src.length - 1]; num += mask.arr[src.length - 1];
		sum += src.arr[src.length - 2] * mask.arr[src.length - 2]; num += mask.arr[src.length - 2];

		if (num == 0)
			num = 0;

		dst.arr[src.length * 2 - 2] = num > 0 ? sum / num : 0;
	}


	#pragma endregion


	#pragma region Predict Padding

	void MatrixOps_PredictPadding(Matrix& src, Matrix& dst, float expandEdge /*0.1f*/, int expandPixels /*50*/)
	/// Fills gaps in matrix. Uses negative values as a mask
	{
		CoordRect rect = src.rect;
		Coord min = rect.Min(); Coord max = rect.Max();

		int stripeLength = rect.size.Maximal();
		Stripe* pLeftStripe = new Stripe(stripeLength);
		Stripe* pRightStripe = new Stripe(stripeLength);
		Stripe* pLeftMask = new Stripe(stripeLength);
		Stripe* pRightMask = new Stripe(stripeLength);
		Stripe& leftStripe = *pLeftStripe;
		Stripe& rightStripe = *pRightStripe;
		Stripe& leftMask = *pLeftMask;
		Stripe& rightMask = *pRightMask;

		for (int i = 0; i < 5; i++)
		{
			leftStripe.length = rect.size.x;		rightStripe.length = rect.size.x;
			leftMask.length = rect.size.x;			rightMask.length = rect.size.x;

			for (int z = min.z; z < max.z; z++)
			{
				MatrixOps_ReadLine(leftStripe, src, rect.offset.x, z);
				MatrixOps_PadStripe(leftStripe, rightStripe, leftMask, rightMask, expandEdge, expandPixels);
				MatrixOps_WriteLine(leftStripe, dst, rect.offset.x, z);
			}

			/*stripe.length = rect.size.z;
			maskStripe.length = rect.size.z;
			for (int x=min.x; x<max.x; x++)
			{
				ReadRow(src, stripe, x, rect.offset.z);
				mask.ReadRow(maskStripe, x, rect.offset.z);
				PadStripe(stripe, maskStripe);
				dst.WriteRow(stripe, x, rect.offset.z); //apply row additively (with mid-point 0.5)
			}*/
		}

		delete pLeftStripe;
		delete pRightStripe;
		delete pLeftMask;
		delete pRightMask;
	}


	void MatrixOps_PadStripe(Stripe& leftStripe, Stripe& rightStripe, Stripe& leftMask, Stripe& rightMask, float expandEdge /*0.1f*/, int expandPixels /*50*/)
	/// Processed values are stored in leftStripe
	{
		//reading left stripe
		for (int x = 0; x < leftStripe.length; x++)
		{
			leftMask.arr[x] = rightMask.arr[x] = leftStripe.arr[x] > 0 ? 1 : 0;
			rightStripe.arr[x] = leftStripe.arr[x];
		}

		//spreading masks
		MatrixOps_SpreadMultiplyLeft(leftMask, 0.9f, 1);
		MatrixOps_SpreadMultiplyRight(rightMask, 0.9f, 1);

		//padding stripes
		MatrixOps_PadStripeLeft(leftStripe);
		MatrixOps_PadStripeRight(rightStripe);

		//applying masks
		for (int x = 0; x < leftStripe.length; x++)
		{
			float lm = leftMask.arr[x];
			float rm = rightMask.arr[x];
			float sum = lm + rm;
			if (sum != 0) { lm /= sum; rm /= sum; }

			float p;
			if (lm > rm)
			{
				p = lm;
				leftStripe.arr[x] = leftStripe.arr[x] * (1 - p) + rightStripe.arr[x] * p;
			}
			else
			{
				p = rm;
				leftStripe.arr[x] = leftStripe.arr[x] * p + rightStripe.arr[x] * (1 - p);
			}
		}
	}


	void MatrixOps_PredictPadStripeLeft(Stripe& stripe, float expandEdge /*0.1f*/, int expandPixels /*50*/)
	{
		//finding stripe start
		int s;
		for (s = 0; s < stripe.length; s++)
			if (stripe.arr[s] >= 0) break;
		if (s >= stripe.length - 1) return;

		float pv = stripe.arr[s];

		int empty = 0;
		//float avg = stripe.arr[0];
		float vector = 0;

		for (int x = s; x < stripe.length; x++)
		{
			float v = stripe.arr[x];
			//float m = maskStripe.arr[x];

			if (v < 0)
			{
				empty++;

				if (empty < expandPixels)
					pv += vector * (1.0f - empty / expandPixels);

				stripe.arr[x] = pv;
			}
			else
			{
				empty = 0;
				vector = vector * (1 - expandEdge) + (v - pv)*expandEdge;
				//avg = avg*0.9f + v*0.1f;

				//stripe.arr[x] = v; 
				pv = v;
			}
		}
	}

	void MatrixOps_PredictPadStripeRight(Stripe& stripe, float expandEdge /*0.1f*/, int expandPixels /*50*/)
	{
		float pv = stripe.arr[stripe.length - 1];

		int empty = 0;
		float vector = 0;

		for (int x = stripe.length - 1; x > 0; x--)
		{
			float v = stripe.arr[x];

			if (v < 0)
			{
				empty++;

				if (empty < expandPixels)
					pv += vector * (1.0f - empty / expandPixels);

				stripe.arr[x] = pv;
			}
			else
			{
				empty = 0;
				vector = vector * (1 - expandEdge) + (v - pv)*expandEdge;
				//avg = avg*0.9f + v*0.1f;

				stripe.arr[x] = v;
				pv = v;
			}
		}
	}

	void MatrixOps_PadStripeLeft(Stripe& stripe)
	{
		float pv = stripe.arr[0];

		for (int x = 0; x < stripe.length; x++)
		{
			float v = stripe.arr[x];

			if (v < 0)
				stripe.arr[x] = pv;
			else
				pv = v;
		}
	}

	void MatrixOps_PadStripeRight(Stripe& stripe)
	{
		float pv = stripe.arr[0];

		for (int x = stripe.length - 1; x > 0; x--)
		{
			float v = stripe.arr[x];

			if (v < 0)
				stripe.arr[x] = pv;
			else
				pv = v;
		}
	}

	#pragma endregion


	#pragma region Lock Padding

	const double PI = 3.141592653589793238463;

	void MatrixOps_ExtendCircular(Matrix& matrix, Coord center, int radius, int extendRange, int expandPixels)
	{
		//resetting area out of radius
		MatrixOps_RemoveOuter(matrix, center, radius);

		//creating radial lines
		int numLines = (int)(ceil(PI * radius)); //using only the half of the needed lines
		float angleStep = PI * 2 / (numLines - 1); //in radians

		Stripe* pStripe = new Stripe(extendRange * 2);
		Stripe* pMaskStripe = new Stripe(extendRange * 2);

		Stripe& stripe = *pStripe;
		Stripe& maskStripe = *pMaskStripe;

		for (int i = 0; i < extendRange; i++)
			maskStripe.arr[i] = 1;

		for (int i = 0; i < numLines; i++)
		{
			float angle = i * angleStep;
			angle -= PI * 3.0f / 4.0f;
			Vector2 direction = Vector2(sin(angle), cos(angle)).Normalized();

			//making any of the step components equal to 1
			Vector2 posDir = Vector2(
				(direction.x > 0 ? direction.x : -direction.x),
				(direction.y > 0 ? direction.y : -direction.y));
			float max = posDir.x > posDir.y ? posDir.x : posDir.y;
			Vector2 step = direction / max;
			//int predictStart = (int)(extendRange * max);

			//finding proper start so that stripe middle be on the edge
			Vector2 start = Vector2((float)center.x, (float)center.z) + direction * radius;
			start -= step * extendRange;

			MatrixOps_ReadInclined(stripe, matrix, start, step);
			MatrixOps_PredictPadStripeLeft(stripe, expandPixels);
			MatrixOps_WriteInclined(stripe, matrix, start, step);
		}

		delete pStripe;
		delete pMaskStripe;

		//creating a diagonal stripe for squares
		{
			Vector2 dir = Vector2(-1, -1);
			Vector2 start = Vector2(center.x,center.z) + dir.Normalized()*radius;

			//Stripe* pDiagStripe = new Stripe(extendRange * 2); //new Stripe( Mathf.Max(matrix.rect.size.x, matrix.rect.size.z) / 2);
			//Stripe& diagStripe = *pDiagStripe;

			MatrixOps_ReadInclined(stripe, matrix, start, dir);
			MatrixOps_PredictPadStripeLeft(stripe, expandPixels);
			MatrixOps_WriteInclined(stripe, matrix, start, dir);

			//delete pDiagStripe;
		}


		//filling gaps between lines
		int strpLength = (radius + extendRange + 1) * 2 * 4 + 1;
		Stripe* pLeftStripe = new Stripe(strpLength);
		Stripe* pRightStripe = new Stripe(strpLength);
		Stripe* pLeftMask = new Stripe(strpLength);
		Stripe* pRightMask = new Stripe(strpLength);

		Stripe& leftStripe = *pLeftStripe;
		Stripe& rightStripe = *pRightStripe;
		Stripe& leftMask = *pLeftMask;
		Stripe& rightMask = *pRightMask;

		for (int i = (int)(radius*0.7f); i < radius + extendRange; i++)
		{
			MatrixOps_ReadSquare(leftStripe, matrix, center, i);
			MatrixOps_PadStripe(leftStripe, rightStripe, leftMask, rightMask, 0, 0);
			MatrixOps_WriteSquare(leftStripe, matrix, center, i);
		}

		delete pLeftStripe;
		delete pRightStripe;
		delete pLeftMask;
		delete pRightMask;

		//blurring
		MatrixOps_BlurCircular(matrix, center, radius + extendRange, extendRange);
		MatrixOps_DownsampleBlurCircular(matrix, center, radius + (int)(extendRange*0.05f), (int)(extendRange*0.95f + 1), 2, 4);
	}


	void MatrixOps_RemoveOuter(Matrix& matrix, Coord coord, int radius)
	/// Fill the outer part of the matrix with negative values so that FillGaps could be used 
	{
		Coord min = matrix.rect.Min(); Coord max = matrix.rect.Max();
		int radiusSq = radius * radius;

		for (int x = min.x; x < max.x; x++)
			for (int z = min.z; z < max.z; z++)
			{
				int pos = (z - matrix.rect.offset.z)*matrix.rect.size.x + x - matrix.rect.offset.x;

				float dist = (x - coord.x)*(x - coord.x) + (z - coord.z)*(z - coord.z);
				if (dist > radiusSq) { matrix.arr[pos] = -1; continue; }
			}
	}


	void MatrixOps_BlurCircular(Matrix& matrix, Coord coord, int radius, int extendRange)
	/// Maybe blur standard and then mask circular?
	{
		CoordRect rect = matrix.rect;
		Coord min = rect.Min(); Coord max = rect.Max();
		int radiusSq = radius * radius;
		//int extendRangeSq = extendRange * extendRange;

		Stripe* pStripe = new Stripe(rect.size.Maximal());
		Stripe& stripe = *pStripe;

		stripe.length = rect.size.x;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(stripe, matrix, rect.offset.x, z);
			MatrixOps_BlurIteration(stripe, 1.5f);
			//BlurIteration(stripe, 1.5f);
			for (int x = min.x; x < max.x; x++)
			{
				int stripePos = x - matrix.rect.offset.x;
				int matrixPos = (z - matrix.rect.offset.z)*matrix.rect.size.x + x - matrix.rect.offset.x;

				float distSq = (x - coord.x)*(x - coord.x) + (z - coord.z)*(z - coord.z);
				if (distSq > radiusSq)
					matrix.arr[matrixPos] = stripe.arr[stripePos];
			}
		}

		stripe.length = rect.size.z;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(stripe, matrix, x, rect.offset.z);
			MatrixOps_BlurIteration(stripe, 1.5f);
			//BlurIteration(stripe, 1.5f);
			for (int z = min.z; z < max.z; z++)
			{
				int stripePos = z - matrix.rect.offset.z;
				int matrixPos = (z - matrix.rect.offset.z)*matrix.rect.size.x + x - matrix.rect.offset.x;

				float distSq = (x - coord.x)*(x - coord.x) + (z - coord.z)*(z - coord.z);
				if (distSq > radiusSq)
					matrix.arr[matrixPos] = stripe.arr[stripePos];
			}
		}

		delete pStripe;
	}

	void MatrixOps_DownsampleBlurCircular(Matrix& matrix, Coord coord, int radius, int extendRange, int downsample, float blur)
	{
		int maxDownsample = (int)log2(matrix.rect.size.x) - 1;
		if (downsample > maxDownsample) downsample = maxDownsample;
		if (downsample == 0) return;

		CoordRect rect = matrix.rect;
		Coord min = rect.Min(); Coord max = rect.Max();
		int radiusSq = radius * radius;
		int extendRangeSq = extendRange * extendRange;

		int stripeLength = rect.size.Maximal();
		Stripe* pHiStripe = new Stripe(stripeLength);
		//Stripe* pSrcStripe = new Stripe(stripeLength);
		Stripe* pLoStripe = new Stripe(stripeLength / 2);
		Stripe& hiStripe = *pHiStripe;
		//Stripe& srcStripe = *pSrcStripe;
		Stripe& loStripe = *pLoStripe;

		hiStripe.length = rect.size.x;
		loStripe.length = hiStripe.length / 2;
		for (int z = min.z; z < max.z; z++)
		{
			MatrixOps_ReadLine(hiStripe, matrix, rect.offset.x, z);

			MatrixOps_ResampleStripeDownFast(hiStripe, loStripe);
			MatrixOps_BlurStripe(loStripe, blur);
			MatrixOps_ResampleStripeUpFast(loStripe, hiStripe);

			for (int x = min.x; x < max.x; x++)
			{
				int stripePos = x - matrix.rect.offset.x;
				int matrixPos = (z - matrix.rect.offset.z)*matrix.rect.size.x + x - matrix.rect.offset.x;

				float distSq = (x - coord.x)*(x - coord.x) + (z - coord.z)*(z - coord.z);
				if (distSq > radiusSq)
				{
					float p = 1 - (distSq - radiusSq) / extendRangeSq;
					if (p > 1) p = 1; if (p < 0) p = 0;
					p *= p;
					matrix.arr[matrixPos] = matrix.arr[matrixPos] * p + hiStripe.arr[stripePos] * (1 - p);
				}
			}
		}


		hiStripe.length = rect.size.z;
		loStripe.length = hiStripe.length / 2;
		for (int x = min.x; x < max.x; x++)
		{
			MatrixOps_ReadRow(hiStripe, matrix, x, rect.offset.z);

			MatrixOps_ResampleStripeDownFast(hiStripe, loStripe);
			MatrixOps_BlurStripe(loStripe, blur);
			MatrixOps_ResampleStripeUpFast(loStripe, hiStripe);

			for (int z = min.z; z < max.z; z++)
			{
				int stripePos = z - matrix.rect.offset.z;
				int matrixPos = (z - matrix.rect.offset.z)*matrix.rect.size.x + x - matrix.rect.offset.x;

				float distSq = (x - coord.x)*(x - coord.x) + (z - coord.z)*(z - coord.z);
				if (distSq > radiusSq)
				{
					float p = 1 - (distSq - radiusSq) / extendRangeSq;
					if (p > 1) p = 1; if (p < 0) p = 0;
					p *= p;
					matrix.arr[matrixPos] = matrix.arr[matrixPos] * p + hiStripe.arr[stripePos] * (1 - p);
				}
			}
		}

		delete pHiStripe;
		//delete pSrcStripe;
		delete pLoStripe;

		/*hiStripe.length = rect.size.z;
		loStripe.length = hiStripe.length / downsample;
		for (int x=min.x; x<max.x; x++)
		{
			matrix.ReadRow(hiStripe, x, rect.offset.z);

			ResampleStripeLinear(hiStripe, loStripe);
			BlurStripe(loStripe, blur);
			ResampleStripeCubic(loStripe, hiStripe);

			matrix.WriteRow(hiStripe, x, rect.offset.z);
		}*/
	}


	float Test(Matrix& thism, float val)
	{
		return val + 0.00001;
	}

	#pragma endregion

}
