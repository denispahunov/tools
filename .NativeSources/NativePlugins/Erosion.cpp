
#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#include <iostream>
#include <cstdio>
#include <ctime>
#include <string>
#include <algorithm>
#include <math.h>

#include "Coord.h"
#include "Matrix.h"

using namespace std;

extern "C"
{
	union Cross
	{
		public:
			struct { float z, x, c, X, Z; };
			float vals[5];

			Cross (const Matrix& m, int i)
			{
									z = m.arr[i-m.rect.size.x];
				x = m.arr[i-1];		c = m.arr[i];					X= m.arr[i+1];
									Z = m.arr[i+m.rect.size.x];
			};
			Cross(const float arr[], int i, int sizeX)
			{
									z = arr[i - sizeX];
				x = arr[i - 1];		c = arr[i];				X = arr[i + 1];
									Z = arr[i + sizeX];
			};
			Cross (float val) { for (int i = 0; i<5; i++) vals[i] = val; }
			Cross () { }

			float operator[] (int i) { return vals[i]; }

			void SetToMatrix(Matrix& m, int i)
			{
								m.arr[i - m.rect.size.x] = z;
				m.arr[i-1] = x;	m.arr[i] = c;					m.arr[i + 1] = X;
								m.arr[i + m.rect.size.x] = Z;
			}

			void AddToMatrix(Matrix& m, int i)
			{
								m.arr[i - m.rect.size.x] += z;
				m.arr[i-1]+=x;	m.arr[i] += c;					m.arr[i + 1] += X;
								m.arr[i + m.rect.size.x] += Z;
			}

			void SetToMatrix(float arr[], int i, int sizeX)
			{
								arr[i - sizeX] = z;
				arr[i - 1] = x;	arr[i] = c;				arr[i + 1] = X;
								arr[i + sizeX] = Z;
			}

			void AddToMatrix(float arr[], int i, int sizeX)
			{
								arr[i - sizeX] += z;
				arr[i - 1]+=x;	arr[i] += c;				arr[i + 1] += X;
								arr[i + sizeX] += Z;
			}

			Cross operator + (const Cross& c2) const { Cross result; for (int i=0; i<5; i++) result.vals[i] = vals[i] + c2.vals[i]; return result; }
			Cross operator + (float f) const { Cross result; for (int i = 0; i<5; i++) result.vals[i] = vals[i] + f; return result; }
			Cross operator - (const Cross& c2) const { Cross result; for (int i = 0; i<5; i++) result.vals[i] = vals[i] - c2.vals[i]; return result; }
			Cross operator - (float f) const { Cross result; for (int i = 0; i<5; i++) result.vals[i] = vals[i] - f; return result; }
			Cross operator * (float f) const { Cross result; for (int i = 0; i<5; i++) result.vals[i] = vals[i] * f; return result; }
			Cross operator / (float f) const { Cross result; for (int i = 0; i<5; i++) result.vals[i] = vals[i] / f; return result; }
        static Cross InvSubtract (float f, const Cross& c2)
        {
            Cross result;
            for (int i = 0; i<5; i++) result.vals[i] = f - c2.vals[i];
            return result;
        }
        
			float Min() const { float min = 2000000000; for (int i = 0; i<5; i++) if (vals[i]<min) min = vals[i]; return min; }
			float MinSides() const { float min = 2000000000; for (int i = 0; i<5; i++) { if (i == 2) continue; if (vals[i]<min) min = vals[i]; } return min; }
			float MaxSides() const { float max = -2000000000; for (int i = 0; i<5; i++) { if (i == 2) continue; if (vals[i]>max) max = vals[i]; } return max; }
			float Sum() const { float sum = 0; for (int i = 0; i<5; i++) sum += vals[i]; return sum; }
			float SumSides() const { float sum = 0; for (int i = 0; i<5; i++) { if (i == 2) continue; sum += vals[i]; } return sum; }
			float Avg() const { return Sum() / 5.0; }
			float AvgSides() const { return  SumSides() / 4.0; }

			Cross ClampMax(float f) const { Cross c; for (int i = 0; i<5; i++) c.vals[i] = max(f, vals[i]); return c; }
			Cross ClampMin(float f) const { Cross c; for (int i = 0; i<5; i++) c.vals[i] = min(f, vals[i]); return c; }

			Cross Pour (float liquid)
			{
				//if (liquid < 0.0000001f) return new Cross(0);

				//initial avg scatter
				float sum = Sum() + liquid;
				float avg = sum / 5;

				Cross pour = Cross(avg);
				pour = pour - *this;
				pour = pour.ClampMax(0);
				//now liquids sum is larger than original

				//lowering all of the liquid cells
				int liquidCellsCount = 0;
				float currentLiquidSum = 0;
				for (int i = 0; i<5; i++)
				{
					float val = pour.vals[i];
					if (val > 0.0001f) liquidCellsCount++;
					currentLiquidSum += val;
				}
				if (liquidCellsCount == 0) return pour; //should not happen
				float lowerAmount = (pour.Sum() - liquid) / liquidCellsCount;
				pour = pour - lowerAmount;
				pour = pour.ClampMax(0);

				//in most cases now the delta is 0, but sometimes it's still needs to be adjusted
				if (abs(pour.Sum() - liquid) > 0.00001f)
				{
					if (abs(pour.Sum()) < 0.000001f) return Cross(0); //this is 100% needed
					float factor = liquid / pour.Sum();
					pour = pour * factor;
				}

				return pour;
			}
	};

    /*Cross operator - (float f, const Cross& c2)
    {
        Cross result;
        for (int i = 0; i<5; i++) result.vals[i] = f - c2.vals[i];
        return result;
    }*/


	union MooreCross
	{
	public:
		struct { float xz, z, Xz, x, c, X, xZ, Z, XZ; };
		float vals[9];

		MooreCross(const Matrix& m, int i)
		{
			xz = m.arr[i-1-m.rect.size.x];		z = m.arr[i - m.rect.size.x];		Xz = m.arr[i+1 - m.rect.size.x];
			x = m.arr[i - 1];					c = m.arr[i];						X = m.arr[i + 1];
			xZ = m.arr[i-1+m.rect.size.x];		Z = m.arr[i + m.rect.size.x];		XZ = m.arr[i+1 + m.rect.size.x];
		};
		MooreCross(const float arr[], int i, int sizeX)
		{
			xz = arr[i-1-sizeX];	z = arr[i - sizeX];		Xz = arr[i+1 - sizeX];
			x = arr[i - 1];			c = arr[i];				X = arr[i + 1];
			xZ = arr[i-1+sizeX];	Z = arr[i + sizeX];		XZ = arr[i + 1 + sizeX];
		};
		MooreCross(float val) { for (int i = 0; i<5; i++) vals[i] = val; }
		MooreCross() { }

		float operator[] (int i) { return vals[i]; }

		void SetToMatrix(Matrix& m, int i)
		{
			m.arr[i-1-m.rect.size.x] = xz;	m.arr[i - m.rect.size.x] = z;	m.arr[i+1 - m.rect.size.x] = Xz;
			m.arr[i - 1] = x;				m.arr[i] = c;					m.arr[i + 1] = X;
			m.arr[i-1+m.rect.size.x] = xZ;	m.arr[i + m.rect.size.x] = Z;	m.arr[i+1+m.rect.size.x] = XZ;
		}

		void AddToMatrix(Matrix& m, int i)
		{
			m.arr[i - 1 - m.rect.size.x] = xz;	m.arr[i - m.rect.size.x] = z;	m.arr[i + 1 - m.rect.size.x] = Xz;
			m.arr[i - 1] = x;				m.arr[i] = c;					m.arr[i + 1] = X;
			m.arr[i - 1 + m.rect.size.x] = xZ;	m.arr[i + m.rect.size.x] = Z;	m.arr[i + 1 + m.rect.size.x] = XZ;
		}

		void SetToMatrix(float arr[], int i, int sizeX)
		{
			arr[i - 1 - sizeX] = xz*0.7F;		arr[i - sizeX] = z;			arr[i + 1 - sizeX] = Xz*0.7F;
			arr[i - 1] = x;						arr[i] = c;					arr[i + 1] = X;
			arr[i - 1 + sizeX] = xZ*0.7F;		arr[i + sizeX] = Z;			arr[i + 1 + sizeX] = XZ*0.7F;
		}

		void AddToMatrix(float arr[], int i, int sizeX)
		{
			arr[i - 1 - sizeX] += xz*0.7F;		arr[i - sizeX] += z;		arr[i + 1 - sizeX] += Xz*0.7F;
			arr[i - 1] += x;					arr[i] += c;					arr[i + 1] += X;
			arr[i - 1 + sizeX] += xZ*0.7F;		arr[i + sizeX] += Z;		arr[i + 1 + sizeX] += XZ*0.7F;
		}

		MooreCross operator + (const MooreCross& c2) const { MooreCross result; for (int i = 0; i<9; i++) result.vals[i] = vals[i] + c2.vals[i]; return result; }
		MooreCross operator + (float f) const { MooreCross result; for (int i = 0; i<9; i++) result.vals[i] = vals[i] + f; return result; }
		MooreCross operator - (const MooreCross& c2) const { MooreCross result; for (int i = 0; i<9; i++) result.vals[i] = vals[i] - c2.vals[i]; return result; }
		MooreCross operator - (float f) const { MooreCross result; for (int i = 0; i<9; i++) result.vals[i] = vals[i] - f; return result; }
		MooreCross operator * (float f) const { MooreCross result; for (int i = 0; i<9; i++) result.vals[i] = vals[i] * f; return result; }
		MooreCross operator / (float f) const { MooreCross result; for (int i = 0; i<9; i++) result.vals[i] = vals[i] / f; return result; }

		float Min() const { float min = 2000000000; for (int i = 0; i<9; i++) if (vals[i]<min) min = vals[i]; return min; }
		float MinSides() const { float min = 2000000000; for (int i = 0; i<9; i++) { if (i == 2) continue; if (vals[i]<min) min = vals[i]; } return min; }
		float MaxSides() const { float max = -2000000000; for (int i = 0; i<9; i++) { if (i == 2) continue; if (vals[i]>max) max = vals[i]; } return max; }
		float Sum() const { float sum = 0; for (int i = 0; i<9; i++) sum += vals[i]; return sum; }
		float SumSides() const { float sum = 0; for (int i = 0; i<9; i++) { if (i == 2) continue; sum += vals[i]; } return sum; }
		float Avg() const { return Sum() / 9.0; }
		float AvgSides() const { return  SumSides() / 8.0; }

		MooreCross ClampMax(float f) const { MooreCross c; for (int i = 0; i<9; i++) c.vals[i] = max(f, vals[i]); return c; }
		MooreCross ClampMin(float f) const { MooreCross c; for (int i = 0; i<9; i++) c.vals[i] = min(f, vals[i]); return c; }

        MooreCross Pour(float liquid)
		{
			//if (liquid < 0.0000001f) return new Cross(0);

			//initial avg scatter
			float sum = Sum() + liquid;
			float avg = sum / 9;

			MooreCross pour = MooreCross(avg);
			pour = pour - *this;
			pour = pour.ClampMax(0);
			//now liquids sum is larger than original

			//lowering all of the liquid cells
			int liquidCellsCount = 0;
			float currentLiquidSum = 0;
			for (int i = 0; i<9; i++)
			{
				float val = pour.vals[i];
				if (val > 0.0001f) liquidCellsCount++;
				currentLiquidSum += val;
			}
			if (liquidCellsCount == 0) return pour; //should not happen
			float lowerAmount = (pour.Sum() - liquid) / liquidCellsCount;
			pour = pour - lowerAmount;
			pour = pour.ClampMax(0);

			//in most cases now the delta is 0, but sometimes it's still needs to be adjusted
			if (abs(pour.Sum() - liquid) > 0.00001f)
			{
				if (abs(pour.Sum()) < 0.000001f) return MooreCross(0); //this is 100% needed
				float factor = liquid / pour.Sum();
				pour = pour * factor;
			}

			return pour;
		}
	};


	PLUGIN_API void SetOrder(Matrix& ref, IntMatrix& order)
	{
		int length = ref.count;
		for (int i=0; i<length; i++) order.arr[i] = i;

		int *refInts = new int[length]();
		for (int i=0; i<length; i++) 
		{
			float val = max(0.0F, min(1.0F,ref.arr[i]));
			int ival = (int)(val * 1073741823); //magic num big enough to distinguish little delta
			refInts[i] = ival;
		}

		class Order
		{
		private:
			int* order;

		public:
			Order(int a[]) : order(a) {}
			bool operator()(int i, int j) const { return order[i]<order[j]; }
		};

		sort(order.arr, order.arr + length, Order(refInts));

		delete[] refInts;

		/*class Order
		{
		private:
			float* order;

		public:
			Order(float a[]) : order(a) {}
			bool operator()(int i, int j) const { return order[i]<order[j]; }
		};

		sort(orderArr, orderArr + length, Order(refArr));*/

		
	}


	PLUGIN_API void MaskBorders(IntMatrix& order)
	{
		for (int j = 0; j<order.count; j++) //TODO: will count be optimized by compiler?
		{
			int pos = order.arr[j];

			int x = pos / order.rect.size.x;
			int z = pos % order.rect.size.x;

			if (x == 0 || z == 0 || x == order.rect.size.x - 1 || z == order.rect.size.z - 1) order.arr[j] = -1;
		}
	}


	PLUGIN_API void CreateTorrents (Matrix& heights, IntMatrix& order, Matrix& torrents)
	{
		int length = heights.count;

		for (int i = 0; i<length; i++) torrents.arr[i] = 1; //casting initial rain

		for (int j = length - 1; j >= 0; j--)
		{
			//finding column ordered by height
			int pos = order.arr[j];
			if (pos<0) continue;


			Cross height = Cross(heights.arr, pos, heights.rect.size.x);
			Cross torrent = Cross(torrents.arr, pos, torrents.rect.size.x); //moore
			if (torrent.c > 200000000) torrent.c = 200000000;

			//Cross delta = height.c - height;
            Cross delta = Cross::InvSubtract(height.c, height);
			delta = delta.ClampMax(0);

			Cross percents = Cross(0);
			float sum = delta.Sum();
			if (sum>0.00001f) percents = delta / sum;

			Cross newTorrent = percents*torrent.c;
			newTorrent.AddToMatrix(torrents.arr, pos, torrents.rect.size.x);
		}
	}

	PLUGIN_API void Erode (Matrix& heights, Matrix& torrents, Matrix& mudflow, IntMatrix& order,
		float erosionDurability = 0.9f, float erosionAmount = 1, float sedimentAmount = 0.5f)
	{
		int length = heights.count;

		for (int i = 0; i<length; i++) mudflow.arr[i] = 0;

		for (int j = length - 1; j >= 0; j--)
		{
			int pos = order.arr[j];
			if (pos<0) continue;


			Cross height (heights.arr, pos, heights.rect.size.x);
			float h_min = height.Min();

			//erosion line
			float erodeLine = (height.c + h_min) / 2.0; //halfway between current and maximum height
			if (height.c < erodeLine) continue;

			//raising soil
			float raised = height.c - erodeLine;
			float maxRaised = raised*(torrents.arr[pos] - 1) * (1 - erosionDurability);
			if (raised > maxRaised) raised = maxRaised;
			raised *= erosionAmount;

			//saving arrays
			heights.arr[pos] -= raised;
			mudflow.arr[pos] += raised * sedimentAmount;
		}
	}

	PLUGIN_API void TransferSettleMudflow(Matrix& heights, Matrix& mudflow, Matrix& sediments, IntMatrix& order, int erosionFluidityIterations = 3)
	{
		int length = heights.count;
		
		for (int i = 0; i<length; i++) sediments.arr[i] = 0;

		//transfering mudflow
		for (int l = 0; l<erosionFluidityIterations; l++)
			for (int j = length - 1; j >= 0; j--)
			{
				int pos = order.arr[j];
				if (pos<0) continue;


				Cross height (heights.arr, pos, heights.rect.size.x);
				Cross sediment (mudflow.arr, pos, mudflow.rect.size.x);

				float sedimentSum = sediment.Sum();
				if (sedimentSum < 0.00001f) continue;

				Cross pour = height.Pour(sedimentSum);

				pour.SetToMatrix(mudflow.arr, pos, mudflow.rect.size.x);
				pour.AddToMatrix(sediments.arr, pos, sediments.rect.size.x);
			}

		//settling mudflow
		for (int j = 0; j<length; j++)
			heights.arr[j] += mudflow.arr[j];
	}
}
