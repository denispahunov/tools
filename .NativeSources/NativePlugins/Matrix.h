#pragma once

#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#ifndef Matrix_H 
#define Matrix_H

#include "Coord.h"
#include "Noise.h"

extern "C"
{
	enum TileMode { Once, Clamp, Tile, PingPong };

	class Matrix
	{
		public:

			CoordRect rect;
			int count;
			float* arr;

			Matrix();
			Matrix(const CoordRect &r);
			Matrix(const CoordRect &r, float a[]);
			Matrix(const Matrix& m);
			//~Matrix();

			float GetVal(int x, int z) const;
			void SetVal(int x, int z, float val);

			inline float operator[] (Coord c);
			inline float operator() (int x, int z) const;
			inline float& operator() (int x, int z);
			//inline Matrix& operator= (Matrix& const m);


			//float operator[] (int i) const;
			//float operator() (Coord c) const;
			//inline float GetVal(int x, int z) const;

			Matrix* Copy() const;

			inline float GetFloored(float fx, float fz) const;
			inline float GetInterpolated(float fx, float fz, bool roundToShort = false) const;
			inline float GetInterpolatedBicubic(float x, float z, TileMode tileMode = Once) const; /// Outdated?
			inline float GetInterpolateCubic(float x, int z, TileMode tileMode = Once) const;  /// Gets interpolated result using a horizontal level only
			inline float GetTiled(Coord coord, TileMode tileMode) const;  /// Returns the usual value if coord is in rect, handles tiling if it is not
			inline float GetTiled(int x, int z, TileMode tileMode) const;

			//TODO: move those to coord?
			inline Coord TileCoord(Coord coord, TileMode tileMode) const;  /// Returns the corresponding coord within the matrix rect
			inline Coord TileClamp(Coord coord) const;
			inline Coord TileRepeat(Coord coord) const;
			inline Coord TilePingPong(Coord coord) const;

			//arithmetic
			void Fill(float val);
			void Fill(Matrix& m);
			void Fill(float val, float opacity);
			void Mix(Matrix& m, float opacity = 1);
			void Mix(Matrix& m, Matrix& mask);
			void Mix(Matrix& m, Matrix& mask, float opacity);
			void Mix(Matrix& m, Matrix& mask, float maskMin, float maskMax, bool maskInvert, bool fallof, float opacity);
			void InvMix(Matrix& m, Matrix& invMask, float opacity = 1);
			void Add(Matrix& add, float opacity = 1);
			void Add(Matrix& add, Matrix& mask, float opacity = 1);
			void Add(float add);
			void Blend(Matrix& matrix, Matrix& mask, Matrix& add, Matrix& addMask);
			void Max(Matrix& matrix, Matrix& mask, Matrix& add, Matrix& addMask);
			void Step(float mid = 0.5f);
			void Subtract(Matrix& m, float opacity = 1);
			void InvSubtract(Matrix& m, float opacity = 1);
			void Multiply(Matrix& m, float opacity = 1);
			void MultiplyInv(Matrix& m);
			void Multiply(float m);
			void Contrast(float m);
			void Divide(Matrix& m, float opacity = 1);
			void Difference(Matrix& m, float opacity = 1);
			void Overlay(Matrix& m, float opacity = 1);
			void HardLight(Matrix& m, float opacity = 1);
			void SoftLight(Matrix& m, float opacity = 1);
			void Max(Matrix& m, float opacity = 1);
			void Min(Matrix& m, float opacity = 1);
			void Select(float level);
			void Invert();
			void InvertOne();
			void SelectRange(float minFrom, float minTo, float maxFrom, float maxTo);
			void ChangeRange(float fromMin, float fromMax, float toMin, float toMax);
			void Clamp01();
			float MaxValue();
			float MinValue();
			float Average();
			bool IsEmpty();
			bool IsEmpty(float delta);
			void BlackWhite(float mid);
			void BrightnessContrast(float brightness, float contrast);
			void Terrace(float* terraces, int terraceCount, float steepness);
			void Levels(float inMin, float inMax, float gamma, float outMin, float outMax);
			void UniformCurve(float* lut, int lutCount);
			void Pow(float powVal);
			void Parallax(float directionX, float directionZ, Matrix &src, Matrix &maskX, Matrix &maskZ, bool useMaskX, bool useMaskZ, int interpolation);
			void Stroke(float posX, float posZ, float radius, float hardness, bool smoothTransition = true, float bckgVal = 0, float strokeVal = 1);
			void BlendStamped(Matrix& src, Matrix& stamp, float centerX, float centerZ, float radius, float transition, bool smoothFallof = true);
			static void BlendLayers(Matrix* matrices[], float opacity[], int layersCount);
			static void NormalizeLayers(Matrix* matrices[], float opacity[], int layersCount);
			static void NormalizeLayers(Matrix* matrices[], Matrix* masks[], int layersCount);
			void Line(float startX, float startZ, float endX, float endZ, float valStart = 1, float valEnd = 1, bool antialised = false, bool paddedOnePixel = false, bool endInclusive = false);
	};

	class IntMatrix
	{
		public:
		CoordRect rect;
		int count;
		int pos;
		int* arr;
	};
}

#endif
