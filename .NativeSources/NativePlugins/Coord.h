#pragma once

#ifndef Coord_H 
#define Coord_H

#include <string>

using namespace std;

extern "C"
{
	struct Coord
	{
		int x;
		int z;

		Coord();
		Coord(int x, int z);

		Coord operator-(Coord c) const;
		Coord operator-(int i) const;
		Coord operator+(Coord c) const;
		Coord operator+(int i) const;
		//Coord operator=(Coord c) const;

		int Maximal() const;

		string ToString() const;

		static Coord Min(Coord c1, Coord c2);
		static Coord Max(Coord c1, Coord c2);
		void ClampPositive();
	};


	struct CoordRect
	{
		Coord offset;
		Coord size;

		CoordRect();
		CoordRect(Coord o, Coord s);
		CoordRect(int offsetX, int offsetZ, int sizeX, int sizeZ);

		//CoordRect operator= (CoordRect cr) const;

		string ToString() const;

		int count() const;
		int operator() (int x, int z) const;

		bool Contains(Coord coord) const;
		bool Contains(int x, int z) const;

		Coord Min() const;
		Coord Max() const;

		void Clamp(Coord min, Coord max);
		static CoordRect Intersected(CoordRect c1, CoordRect c2);

		Coord Tile(Coord coord, int tileMode);
	};
}

#endif


/*#define PLUGIN_API __declspec(dllexport) 

extern "C"
{
	PLUGIN_API void SetOrder (float* refArr, int* orderArr, int length);
}
*/