
#if _MSC_VER
#define PLUGIN_API __declspec(dllexport)
#else
#define PLUGIN_API
#endif

#include <cstdio>
#include <ctime>
#include <string>
#include <algorithm>
#include <math.h>

#include "Coord.h"
#include "Matrix.h"
#include "Noise.h"

using namespace std;

extern "C"
{
	class StopToken
	{
	public:
		bool stop;
		bool restart;
	};


	PLUGIN_API void GeneratorNoise200(Matrix &matrix, Noise &noise, StopToken &stop,
		int type, float intensity, float size, float detail, float turbulence, float offsetX, float offsetZ,
		float worldRectPosX, float worldRectPosZ, float worldRectSizeX, float worldRectSizeZ)
	{
		int iterations = (int)log2(size) + 1; //+1 max size iteration

		Coord min = matrix.rect.Min(); Coord max = matrix.rect.Max();
		for (int x = min.x; x < max.x; x++)
		{
			for (int z = min.z; z < max.z; z++)
			{
				float relativePosX = (float)(x - matrix.rect.offset.x) / (matrix.rect.size.x - 1);
				float relativePosZ = (float)(z - matrix.rect.offset.z) / (matrix.rect.size.z - 1);

				float worldPosX = relativePosX * worldRectSizeX + worldRectPosX;
				float worldPosZ = relativePosZ * worldRectSizeZ + worldRectPosZ;

				float val = noise.Fractal(worldPosX + offsetX, worldPosZ + offsetZ, size, iterations, detail, turbulence, type);
				val *= intensity;

				if (val < 0) val = 0; //before mask?
				if (val > 1) val = 1;

				matrix.SetVal(x, z, val);
			}
			if (stop.stop) return; //checking stop every x line
		}
	}


	PLUGIN_API void GeneratorLedgeStep (Matrix& src, Matrix& mask, Matrix& dst,
		float minFrom, float maxFrom, float minTo, float maxTo,
		float bottomShoulder, float topShoulder)
	{
		for (int i = 0; i < dst.count; i++)
		{
			float heightVal = src.arr[i];
			float maskVal = mask.arr[i];


			if (maskVal < minFrom)
			{
				float p = heightVal / minFrom;
				p = ((p / bottomShoulder) + (bottomShoulder - 1) / (bottomShoulder)) * p + p * (1 - p); //inverse of (p/shoulder)*(1-p) + p*p;
				dst.arr[i] = minTo * p;
			}

			else if (maskVal > maxFrom)
			{
				float p = ((heightVal - maxFrom) / (1 - maxFrom));
				p = (p / topShoulder)*(1 - p) + p * p; //Mathf.Pow(p,topShoulder);
				dst.arr[i] = maxTo + (1 - maxTo) * p;
			}

			else
			{
				float p = (heightVal - minFrom) / (maxFrom - minFrom);
				//float ip = 1 - p;
				float tp = ((p / topShoulder) + (topShoulder - 1) / (topShoulder)) * p + p * (1 - p);
				float lp = (p / bottomShoulder)*(1 - p) + p * p;
				float bp = 3 * p*p - 2 * p*p*p;

				p = lp * (1 - bp) + tp * bp;

				dst.arr[i] = minTo + (maxTo - minTo)*p;
			}
		}
	}


	PLUGIN_API void GeneratorBeachHeight210(Matrix &src, Matrix &dst, Matrix &shoreSpread, Matrix* mask, float level, float height)
	{
		for (int i = 0; i < dst.count; i++)
		{
			float heightVal = src.arr[i];
			float shoreVal = shoreSpread.arr[i];
			float maskVal = mask != NULL ? 1 - (1 - mask->arr[i])*(1 - mask->arr[i]) : 1;

			if (shoreVal > 0.999f)
				dst.arr[i] = level + height / 2;

			if (shoreVal < 0.0001f)
				dst.arr[i] = src.arr[i];

			float percent = shoreVal * maskVal;
			float val = level - height / 2 + percent * height;

			float bottomPercent = (0.5f - percent) * 2; //0->1, 0.5->0
			if (bottomPercent < 0) bottomPercent = 0;
			//bottomPercent = 3*bottomPercent*bottomPercent - 2*bottomPercent*bottomPercent*bottomPercent;
			val = val * (1 - bottomPercent) + heightVal * bottomPercent;

			dst.arr[i] = val;
		}
	}

	PLUGIN_API void GeneratorBeachSand210(Matrix &heights, Matrix &beach, Matrix &sand, float waterLevel, float maxDelta)
	{
		for (int i = 0; i < heights.count; i++)
		{
			float heightVal = heights.arr[i];
			float beachVal = beach.arr[i];

			//for above water
			if (beachVal > waterLevel)
			{
				float delta = beachVal - heightVal;
				float percent = delta / maxDelta;
				if (percent > 1) percent = 1;
				if (percent < 0) percent = 0;

				sand.arr[i] = percent;
			}

			//for shallow underwater
			else if (beachVal > waterLevel - maxDelta / 2)
			{
				if (beachVal > heightVal + 0.0001f)
					sand.arr[i] = 1;
				else
					sand.arr[i] = 0;
			}

			//deep - always in sand
			else
				sand.arr[i] = 1;
		}
	}
}
