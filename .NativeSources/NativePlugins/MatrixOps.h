#pragma once

#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#ifndef MatrixOps_H 
#define MatrixOps_H

#include "Matrix.h"
#include "Vector2.h"

extern "C"
{
	class Stripe
	{
		public:

		int length; //in case array is longer (for re-use)
		float* arr;

		Stripe(float* arr, int length);
		Stripe(int length);
		Stripe(Stripe& stripe);

		void Expand(int newCount);
		void Fill(float val);

		static void Copy(Stripe& src, Stripe& dst);
		static void Swap(Stripe& s1, Stripe& s2);
	};

	/*void MatrixOps_ReadLine(Stripe& stripe, Matrix& matrix, int x, int z);
	void MatrixOps::WriteLine(Stripe& stripe, Matrix& matrix, int x, int z);
	static void MatrixOps::MaxLine(Stripe& stripe, Matrix& matrix, int x, int z);
	static void MatrixOps::AddLine(Stripe& stripe, Matrix& matrix, int x, int z, float opacity = 1);
	static void MatrixOps::OverlayLine(Stripe& stripe, Matrix& matrix, int x, int z);
	static void MatrixOps::ReadRow(Stripe& stripe, Matrix& matrix, int x, int z);
	static void MatrixOps::WriteRow(Stripe& stripe, Matrix& matrix, int x, int z);
	static void MatrixOps::WriteRow(Stripe& stripe, Matrix& matrix, int x, int z, float* mask);
	static void MatrixOps::MaxRow(Stripe& stripe, Matrix& matrix, int x, int z);
	static void MatrixOps::AddRow(Stripe& stripe, Matrix& matrix, int x, int z, float opacity = 1);
	static void MatrixOps::OverlayRow(Stripe& stripe, Matrix& matrix, int x, int z, float opacity = 1);
	static void MatrixOps::MixRow(Matrix& dst, Matrix& matrix, Matrix& matrixMask, Stripe& stripe, Stripe& stripeMask, int x, int z);
	static void MatrixOps::MaxRow(Matrix& dst, Matrix& matrix, Matrix& matrixMask, Stripe& stripe, Stripe& stripeMask, int x, int z);
	static void MatrixOps::ReadDiagonal(Stripe& stripe, Matrix& matrix, int x, int z, int stepX = 1, int stepZ = 1);
	static void MatrixOps::WriteDiagonal(Stripe& stripe, Matrix& matrix, int x, int z, int stepX = 1, int stepZ = 1);
	static void MatrixOps::MaxDiagonal(Stripe& stripe, Matrix& matrix, int x, int z, int stepX = 1, int stepZ = 1);
	static void MatrixOps::ReadInclined(Stripe& stripe, Matrix& matrix, float startX, float startZ, float stepX, float stepZ);
	static void MatrixOps::WriteInclined(Stripe& stripe, Matrix& matrix, float startX, float startZ, float stepX, float stepZ);
	static void MatrixOps::ReadStrip(Stripe& stripe, Matrix& matrix, float startX, float startZ, float endX, float endZ);
	static void MatrixOps::WriteStrip(Stripe& stripe, Matrix& matrix, float startX, float startZ, float endX, float endZ);
	static void MatrixOps::ReadSquare(Stripe& stripe, Matrix& matrix, Coord center, int radius);
	/// Same as circular, but in form of square. 4 lines one-by-one. Useful for blurs and spreads
	static void MatrixOps::WriteSquare(Stripe& stripe, Matrix& matrix, Coord center, int radius);
	/// Same as circular, but in form of square. 4 lines one-by-one. Useful for blurs and spreads
	/// Line length should be radius*8 + 4 corners*/

	/*void MatrixOps_DownsizeHorizontally(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe);
	void MatrixOps_DownsizeVertically(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe);
	void MatrixOps_UpsizeHorizontally(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe);
	void MatrixOps_UpsizeVertically(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe);
	void MatrixOps_ResampleStripeCubic(Stripe& src, Stripe& dst);
	void MatrixOps_ResampleStripeLinear(Stripe& src, Stripe& dst);

	void MatrixOps_UpscaleFast(Matrix& src, Matrix& dst); /// Only for cases when dst rect size is exactly twice bigger than src
	void MatrixOps_DownscaleFast(Matrix& src, Matrix& dst); /// Only for cases when dst rect size is exactly twice smaller than src
	void MatrixOps_DownscaleFastTmp(Matrix& src, Matrix& dst, Matrix& tmp, Stripe& srcStripe, Stripe& dstStripe);
	void MatrixOps_ResampleStripeDownFast(Stripe& src, Stripe& dst); /// Like linear, works faster, but requires dst.size = src.size/2
	void MatrixOps_ResampleStripeUpFast(Stripe& src, Stripe& dst); /// Like cubic, works faster, but requires dst.size = src.size*2*/

	PLUGIN_API void MatrixOps_DownsizeHorizontally(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe);
	PLUGIN_API void MatrixOps_DownsizeVertically(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe);
	PLUGIN_API void MatrixOps_UpsizeHorizontally(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe, float srcOffset = 0, float srcLength = 0);
	PLUGIN_API void MatrixOps_UpsizeVertically(Matrix& src, Matrix& dst, Stripe& srcStripe, Stripe& dstStripe, float srcOffset = 0, float srcLength = 0);
	PLUGIN_API void MatrixOps_ResampleStripeCubic(Stripe& src, Stripe& dst, float srcOffset = 0, float srcLength = 0);
	PLUGIN_API void MatrixOps_ResampleStripeLinear(Stripe& src, Stripe& dst);

	PLUGIN_API void MatrixOps_UpscaleFast(Matrix& src, Matrix& dst);
	PLUGIN_API void MatrixOps_DownscaleFast(Matrix& src, Matrix& dst);
	PLUGIN_API void MatrixOps_DownscaleFastTmp(Matrix& src, Matrix& dst, Matrix& tmp, Stripe& srcStripe, Stripe& dstStripe);
	PLUGIN_API void MatrixOps_ResampleStripeDownFast(Stripe& src, Stripe& dst);
	PLUGIN_API void MatrixOps_ResampleStripeUpFast(Stripe& src, Stripe& dst);

	PLUGIN_API void MatrixOps_GaussianBlur(Matrix& src, Matrix& dst, float blur);
	PLUGIN_API void MatrixOps_BlurStripe(Stripe& src, float blur);
	PLUGIN_API void MatrixOps_BlurIteration(Stripe& src, float blur);
	PLUGIN_API void MatrixOps_BlurIteration05(Stripe& src);

	PLUGIN_API void MatrixOps_DownsampleBlur(Matrix& src, Matrix& dst, int downsample, float blur);
	PLUGIN_API void MatrixOps_OverblurMippedIteration(Matrix& mip, Matrix& mat, Matrix& tmp, Stripe& srcStripe, Stripe& dstStripe, float escalate);

	PLUGIN_API void MatrixOps_NormalsSet(Matrix& src, Matrix& normX, Matrix& normZ, Matrix& normBlue, float pixelSize, float height);
	PLUGIN_API void MatrixOps_NormalizeSet(Matrix& normX, Matrix& normZ, Matrix& normBlue, float pixelSize);
	PLUGIN_API void MatrixOps_NormalsStripe(Stripe& stripe, float height);
	PLUGIN_API void MatrixOps_SetToDir(Matrix& normX, Matrix& normZ, Matrix& normBlue, Matrix& dst, float dirX, float dirY, float dirZ, float intensity = 1, float wrapping = 1);
	PLUGIN_API void MatrixOps_Delta(Matrix& src, Matrix& dst);
	PLUGIN_API void MatrixOps_DeltaStripe(Stripe& stripe);
	//PLUGIN_API void MatrixOps_NormalRelief(Matrix& src, Matrix& dst, float horizontalHeight = 1, float verticalHeight = 1);
	//PLUGIN_API void MatrixOps_NormalStripe(Stripe& stripe, float height);

	PLUGIN_API void MatrixOps_Silhouette(Matrix& src, Matrix& dst, float level, bool antialiasing=false);
	PLUGIN_API void MatrixOps_SilhouetteStripe(Stripe& stripe, float level, bool antiAliasing);

	PLUGIN_API void MatrixOps_Cavity(Matrix& src, Matrix& dst);
	PLUGIN_API void MatrixOps_OverSpread(Matrix& src, Matrix& dst, float multiply);
	PLUGIN_API void MatrixOps_Invert(Stripe& src, Stripe& dst);
	PLUGIN_API void MatrixOps_CavityStripe(Stripe& stripe);

	PLUGIN_API void MatrixOps_SpreadLinear(Matrix& src, Matrix& dst, float subtract = 0.01f, bool diagonals = false, bool quarters = false, bool bulb = false);
	PLUGIN_API void MatrixOps_SpreadLinearRight(Stripe& stripe, float subtract = 0.01f, float regardSmallerValues = 1);
	PLUGIN_API void MatrixOps_SpreadLinearLeft(Stripe& stripe, float subtract = 0.01f, float regardSmallerValues = 1);
	PLUGIN_API void MatrixOps_SpreadMultiply(Stripe& stripe, float multiply = 1.0f, float regardSmallerValues = 1);
	PLUGIN_API void MatrixOps_SpreadMultiplyLeft(Stripe& stripe, float multiply = 1.0f, float regardSmallerValues = 1);
	PLUGIN_API void MatrixOps_SpreadMultiplyRight(Stripe& stripe, float multiply = 1.0f, float regardSmallerValues = 1);

	PLUGIN_API void MatrixOps_PaddingOnePixel(Matrix& src, Matrix& srcMask, Matrix& dst, Matrix& dstMask, float intensity = 1);
	PLUGIN_API void MatrixOps_PadStripeOnePixel(Stripe& stripe, Stripe& mask, float intensity = 1, bool toLeft = false);
	PLUGIN_API void MatrixOps_DownscaleMaskedFast(Matrix& src, Matrix& srcMask, Matrix& dst, Matrix& dstMask, Matrix& tmp, Matrix& tmpMask);
	PLUGIN_API void MatrixOps_ResampleAutomaskedStripeDownFast(Stripe& src, Stripe& dst, float minVal);
	PLUGIN_API void MatrixOps_ResampleMaskedStripeDownFast(Stripe& src, Stripe& mask, Stripe& dst);
	PLUGIN_API void MatrixOps_UpscaleMaskedFast(Matrix& src, Matrix& srcMask, Matrix& dst, Matrix& dstMask);
	PLUGIN_API void MatrixOps_ResampleAutomaskedStripeUpFast(Stripe& src, Stripe& dst, float minVal);
	PLUGIN_API void MatrixOps_ResampleMaskedStripeUpFast(Stripe& src, Stripe& mask, Stripe& dst);
	PLUGIN_API void MatrixOps_ResizeNearestNeighbor(Matrix& src, Matrix& dst);

	PLUGIN_API void MatrixOps_PredictPadding(Matrix& src, Matrix& dst, float expandEdge = 0.1f, int expandPixels = 50);
	PLUGIN_API void MatrixOps_PadStripe(Stripe& leftStripe, Stripe& rightStripe, Stripe& leftMask, Stripe& rightMask, float expandEdge = 0.1f, int expandPixels = 50);
	PLUGIN_API void MatrixOps_PredictPadStripeLeft(Stripe& stripe, float expandEdge = 0.1f, int expandPixels = 50);
	PLUGIN_API void MatrixOps_PredictPadStripeRight(Stripe& stripe, float expandEdge = 0.1f, int expandPixels = 50);
	PLUGIN_API void MatrixOps_PadStripeLeft(Stripe& stripe);
	PLUGIN_API void MatrixOps_PadStripeRight(Stripe& stripe);

	PLUGIN_API void MatrixOps_ExtendCircular(Matrix& matrix, Coord center, int radius, int extendRange, int expandPixels);
	PLUGIN_API void MatrixOps_RemoveOuter(Matrix& matrix, Coord coord, int radius);
	PLUGIN_API void MatrixOps_BlurCircular(Matrix& matrix, Coord coord, int radius, int extendRange);
	PLUGIN_API void MatrixOps_DownsampleBlurCircular(Matrix& matrix, Coord coord, int radius, int extendRange, int downsample, float blur);

	PLUGIN_API float Test(Matrix& thism, float val); //for performance testing: 0.1757 ms per 1000 calls, about 20 times slower than standard fn call. 
}

#endif
