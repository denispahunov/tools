
#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#include <cstdio>
#include <algorithm>
#include <math.h>

#include "Vector2.h"

using namespace std;

extern "C"
{
    Vector2::Vector2() { x = 0; y = 0; }
    Vector2::Vector2(float x, float y) { this->x = x; this->y = y; }

	Vector2 Vector2::operator-(Vector2 c) const { return Vector2(x - c.x, y - c.y); }
	Vector2 Vector2::operator-=(Vector2 c) const { return Vector2(x - c.x, y - c.y); }
	Vector2 Vector2::operator-(float i) const { return Vector2(x - i, y - i); }
	Vector2 Vector2::operator-=(float i) const { return Vector2(x - i, y - i); }
	Vector2 Vector2::operator+(Vector2 c) const { return Vector2(x + c.x, y + c.y); }
	Vector2 Vector2::operator+=(Vector2 c) const { return Vector2(x + c.x, y + c.y); }
	Vector2 Vector2::operator+(float i) const { return Vector2(x + i, y + i); }
	Vector2 Vector2::operator+=(float i) const { return Vector2(x + i, y + i); }
	Vector2 Vector2::operator*(Vector2 c) const { return Vector2(x * c.x, y * c.y); }
	Vector2 Vector2::operator*(float i) const { return Vector2(x * i, y * i); }
	Vector2 Vector2::operator/(Vector2 c) const { return Vector2(x / c.x, y / c.y); }
	Vector2 Vector2::operator/(float i) const { return Vector2(x / i, y / i); }
	//Vector2 Vector2::operator=(Vector2 c) const { return Vector2(c.x, c.y); }

	float Vector2::Length() { return sqrt(x*x + y*y); }
	Vector2 Vector2::Normalized() 
	{ 
		float length = Length(); 
		if (length!=0) 
			return Vector2(x/length, y/length);
		else
			return Vector2(0,0);
	}


	Vector2D::Vector2D() { x = 0; z = 0; }
	Vector2D::Vector2D(float x, float z) { this->x = x; this->z = z; }


    Color::Color() { r=0; g=0; b=0; a=0; }
    Color::Color(float red, float green, float blue) { r=red; g=green; b=blue; a=1; }
    Color::Color(float red, float green, float blue, float alpha) { r = red; g = green; b = blue; a = alpha; }
}
