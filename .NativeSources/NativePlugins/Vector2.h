#pragma once

#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#ifndef Vector2_H 
#define Vector2_H


extern "C"
{
	struct Vector2
	{
		float x;
		float y;

		Vector2();
		Vector2(float x, float y);

		Vector2 operator-(Vector2 c) const;
		Vector2 operator-=(Vector2 c) const;
		Vector2 operator-(float i) const;
		Vector2 operator-=(float i) const;
		Vector2 operator+(Vector2 c) const;
		Vector2 operator+=(Vector2 c) const;
		Vector2 operator+(float i) const;
		Vector2 operator+=(float i) const;
		Vector2 operator/(float i) const;
		Vector2 operator/(Vector2 v) const;
		Vector2 operator*(Vector2 v) const;
		Vector2 operator*(float i) const;
		//Vector2 operator=(Vector2 c) const;

		float Length();
		Vector2 Normalized();
	};

	struct Vector2D
	{
		float x;
		float z;

		Vector2D();
		Vector2D(float x, float y);
	};

	struct Color
	{
		float r;
		float g;
		float b;
		float a;

		Color();
		Color(float r, float g, float b);
		Color(float r, float g, float b, float a);
	};
}

#endif
