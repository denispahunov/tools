#pragma once

#ifndef Line_H 
#define Line_H

#include "Matrix.h"

extern "C"
{
	class Line
	{
		public:

		int length;
		float* arr;

		Line(int length);
		Line(float arr[], int length);
		//~Line()

		// Sampling
		static void ResampleCubic(Line src, Line dst); /// Scales the line filling dst with interpolated values. Cubic for upscale
		static void ResampleLinear(Line src, Line dst); /// Scales the line filling dst with interpolated values. Linear for downscale
		static void DownsampleFast(Line src, Line dst, int ratio); /// Scales the line filling dst with interpolated values. Linear for downscale

		
		float CubicSample(float x);/// Interpolated sampling for upscaling
		float LinearSample(float x, float radius); /// Weighted radius sampling for downscaling
		float AverageSample(int x);

		// Operations
		void Spread(float subtract = 0.01f, float multiply = 1.0f); /// Spreads higher values (whites) over the lower (darker) ones. Or vice versa if invert enabled
		void Cavity(float intensity = 1);
		void Delta();
		void GaussianBlur(float tmp[], float blur);
		
		// Arithmetic
		void Add(float f);
		void Max(Line l);

		// Reading / Writing Matrix (All line readings are zero-based (no offset))
		void ReadLine(const Matrix &matrix, int z);
		void ReadRow(const Matrix &matrix, int x);
		void WriteLine(const Matrix &matrix, int z);
		void WriteRow(const Matrix &matrix, int x);
		void AppendRow(const Matrix &matrix, int x);

	}; //class

}

#endif