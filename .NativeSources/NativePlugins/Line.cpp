
#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#include <iostream>
#include <cstdio>
#include <ctime>
#include <string>
#include <algorithm>
#include <iterator>

#include "Line.h"

using namespace std;

extern "C"
{
	Line::Line(int l) : length(l) { arr = new float[length]; }
	Line::Line(float a[], int l) : arr(a), length(l) { }

	#pragma region Sampling

		void Line::ResampleCubic(Line src, Line dst)
		/// Scales the line filling dst with interpolated values. Cubic for upscale
		{
			int srcLength = src.length;
			int dstLength = dst.length;

			for (int x = 0; x<dst.length; x++)
			{
				float percent = 1.0f * x / dstLength;
				float sx = percent * srcLength;
				dst.arr[x] = src.CubicSample(sx);
			}
		}

		void Line::ResampleLinear(Line src, Line dst)
		/// Scales the line filling dst with interpolated values. Linear for downscale
		{
			int srcLength = src.length;
			int dstLength = dst.length;

			float radius = 1.0f * srcLength / dstLength;

			for (int x = 0; x<dstLength; x++)
			{
				float percent = 1.0f * x / dstLength;
				float sx = percent * src.length;
				dst.arr[x] = src.LinearSample(sx, radius);
			}
		}

		void Line::DownsampleFast(Line src, Line dst, int ratio)
		/// Scales the line filling dst with interpolated values. Linear for downscale
		{
			for (int x = 0; x<dst.length; x++)
			{
				float sumVal = 0;
				for (int ix = 0; ix<ratio; ix++)
					sumVal += src.arr[x*ratio + ix];
				dst.arr[x] = sumVal / ratio;
			}
		}

		float Line::CubicSample(float x)
		/// Interpolated sampling for upscaling
		{
			int p = (int)x;		if (p<0) p = 0;
			int n = p + 1;		if (n>length - 1) n = length - 1;
			int pp = p - 1;		if (pp<0) pp = 0;
			int nn = n + 1;		if (nn>length - 1) nn = length - 1;

			float percent = x - p;

			float vp = arr[p]; float vpp = arr[pp];
			float vn = arr[n]; float vnn = arr[nn];

			return vp + 0.5f * percent * (vn - vpp + percent * (2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + percent * (3.0f*(vp - vn) + vnn - vpp)));
		}

		float Line::LinearSample(float x, float radius)
			/// Weighted radius sampling for downscaling
		{
			int ix = (int)x;
			int iRadius = (int)(radius + 0.5f);

			float factorSum = 0;
			float valueSum = 0;

			for (int rx = ix - iRadius + 1; rx < ix + iRadius + 1; rx++)
				//skipping edge vertices since their factor is 0
				//evaluating +1 point if x is between ix and ix+1
			{
				float dist = x - rx;
				if (dist < 0) dist = -dist;

				float factor = 1 - dist / radius;
				if (factor<0) factor = 0;

				int crx = rx;
				if (crx<0) crx = 0;
				if (crx>length - 1) crx = length - 1;

				factorSum += factor;
				valueSum += arr[crx] * factor;
			}

			return valueSum / factorSum;
		}

		float Line::AverageSample(int x)
		{
			int p = x - 1;		if (p<0) p = 0;
			int n = x + 1;		if (n>length - 1) n = length - 1;

			float vp = arr[p];
			float vx = arr[x];
			float vn = arr[n];

			return vp * 0.25f + vx * 0.5f + vn * 0.25f;
		}

	#pragma endregion


	#pragma region Operations

		void Line::Spread(float subtract, float multiply)
		/// Spreads higher values (whites) over the lower (darker) ones. Or vice versa if invert enabled
		// Warning: spreading with multiply is WRONG! 
		// It will not steer the corner
		{
			//to right
			for (int x = 1; x<length - 1; x++)
			{
				float delta = arr[x - 1] - arr[x];
				if (delta > 0)
					arr[x] += delta * multiply - subtract;
			}

			//to left
			for (int x = length - 2; x >= 0; x--)
			{
				float delta = arr[x + 1] - arr[x];
				if (delta > 0)
					arr[x] += delta * multiply - subtract;
			}
		}


		void Line::Cavity(float intensity)
		{
			float prev = arr[0];
			float curr = arr[1];

			for (int x = 1; x<length - 1; x++)
			{
				//float prev = src.arr[x-1];
				//float curr = src.arr[x];
				float next = arr[x + 1];

				float val = curr - (next + prev) / 2;
				float sign = val>0 ? 1 : -1;
				val = (val*val*sign)*intensity * 1000;
				val = (val + 1) / 2;
				arr[x] = val;

				prev = curr;
				curr = next;
			}
			arr[0] = arr[1];
			arr[length - 1] = arr[length - 2];
		}


		void Line::Delta()
		{
			float prev = arr[0];
			float curr = arr[1];

			for (int x = 1; x<length - 1; x++)
			{
				//float prev = arr[x-1];
				//float curr = arr[x];
				float next = arr[x + 1];

				float prevDelta = prev - curr; if (prevDelta < 0) prevDelta = -prevDelta;
				float nextDelta = next - curr; if (nextDelta < 0) nextDelta = -nextDelta;
				float delta = prevDelta>nextDelta ? prevDelta : nextDelta;

				if (delta > arr[x]) arr[x] = delta;

				prev = curr;
				curr = next;
			}
			arr[0] = arr[1];
			arr[length - 1] = arr[length - 2];
		}


		void Line::GaussianBlur(float tmp[], float blur)
		{
			int iterations = (int)blur;

			Line* srcPtr = new Line(arr, length); //for switching arrays between iterations
			Line* dstPtr = new Line(tmp, length);

			//iteration blur
			for (int i = 0; i<iterations; i++)
			{
				for (int x = 0; x<length; x++)
					dstPtr->arr[x] = srcPtr->AverageSample(x);

				float* t = srcPtr->arr;
				srcPtr->arr = dstPtr->arr;
				dstPtr->arr = t;
			}

			//last iteration - percentage
			float percent = blur - iterations;
			if (percent > 0.0001f)
			{
				for (int x = 0; x<length; x++)
					dstPtr->arr[x] = srcPtr->AverageSample(x)*percent + srcPtr->arr[x] * (1 - percent);

				float* t = srcPtr->arr;
				srcPtr->arr = dstPtr->arr;
				dstPtr->arr = t;
			}

			//copy values to arr for non-even iteration count
			for (int x = 0; x<length; x++)
				dstPtr->arr[x] = srcPtr->arr[x];
		}

	#pragma endregion


	#pragma region Arithmetic

		void Line::Add(float f) { for (int i = 0; i<length; i++) arr[i] += f; }
		void Line::Max(Line l)
		{
			for (int i = 0; i<length; i++)
			{
				float v1 = arr[i];
				float v2 = l.arr[i];
				arr[i] = v1>v2 ? v1 : v2;
			}
		}

	#pragma endregion


	#pragma region Reading / Writing Matrix

		//All line readings are zero-based (no offset)

		void Line::ReadLine(const Matrix &matrix, int z)
		{
			int start = 0;
			int end = start + length;
			for (int x = start; x<end; x++)
				arr[x - start] = matrix.arr[z*matrix.rect.size.x + x];
		}

		void Line::ReadRow(const Matrix &matrix, int x)
		{
			int start = 0;
			int end = start + length;
			for (int z = start; z<end; z++)
				arr[z - start] = matrix.arr[z*matrix.rect.size.x + x];
		}

		void Line::WriteLine(const Matrix &matrix, int z)
		{
			int start = 0;
			int end = start + length;
			for (int x = start; x<end; x++)
				//array[ (z-rect.offset.z)*rect.size.x + x ] = arr[x-start];
				matrix.arr[z*matrix.rect.size.x + x] = arr[x - start];

		}

		void Line::WriteRow(const Matrix &matrix, int x)
		{
			int start = 0;
			int end = start + length;
			for (int z = start; z<end; z++)
				//array[ z*rect.size.x + x - rect.offset.x ] = arr[z-start]; 
				matrix.arr[z*matrix.rect.size.x + x] = arr[z - start];
		}

		void Line::AppendRow(const Matrix &matrix, int x)
		{
			int start = 0;
			int end = start + length;
			for (int z = start; z<end; z++)
				//array[ z*rect.size.x + x - rect.offset.x ] = arr[z-start]; 
				matrix.arr[z*matrix.rect.size.x + x] += arr[z - start];
		}

	#pragma endregion

}
