
#if _MSC_VER
    #define PLUGIN_API __declspec(dllexport)
#else
    #define PLUGIN_API
#endif

#include <iostream>
#include <cstdio>
#include <ctime>
#include <string>
#include <algorithm>

#include "Coord.h"

using namespace std;

extern "C"
{

    Coord::Coord() { x = 0; z = 0; }
    Coord::Coord(int x, int z) { this->x = x; this->z = z; }

	Coord Coord::operator-(Coord c) const { return Coord(x-c.x, z-c.z); }
	Coord Coord::operator-(int i) const { return Coord(x - i, z - i); }
	Coord Coord::operator+(Coord c) const { return Coord(x+c.x, z+c.z); }
	Coord Coord::operator+(int i) const { return Coord(x + i, z + i); }
	//Coord Coord::operator=(Coord c) const { return Coord(c.x, c.z); }

	int Coord::Maximal() const { return x > z ? x : z; }

	string Coord::ToString() const { return to_string(x) + "," + to_string(z); }

	Coord Coord::Min(Coord c1, Coord c2)
	{
		//return new Coord(Mathf.Min(c1.x,c2.x), Mathf.Min(c1.z,c2.z)); 
		int minX = c1.x < c2.x ? c1.x : c2.x;
		int minZ = c1.z < c2.z ? c1.z : c2.z;
		return Coord(minX, minZ);
	}
	Coord Coord::Max(Coord c1, Coord c2)
	{
		//return new Coord(Mathf.Max(c1.x,c2.x), Mathf.Max(c1.z,c2.z));
		int maxX = c1.x > c2.x ? c1.x : c2.x;
		int maxZ = c1.z > c2.z ? c1.z : c2.z;
		return Coord(maxX, maxZ);
	}

	void Coord::ClampPositive() { x = max(0, x); z = max(0, z); }



	CoordRect::CoordRect() : offset(Coord()), size(Coord()) { }
	CoordRect::CoordRect(Coord o, Coord s) : offset(o), size(s) { }
	CoordRect::CoordRect(int offsetX, int offsetZ, int sizeX, int sizeZ) :
		offset(Coord(offsetX, offsetZ)),
		size(Coord(sizeX, sizeZ))
			{ }

	string CoordRect::ToString() const { return "o:" + offset.ToString() + " s:" + size.ToString(); }
	
	int CoordRect::count() const { return size.x * size.z; }

	int CoordRect::operator() (int x, int z) const { return (z-offset.z)*size.x + x - offset.x; }
	//CoordRect CoordRect::operator= (CoordRect const cr) const { return CoordRect(cr.offset, cr.size); }

	bool CoordRect::Contains(Coord coord) const
	{
		return (coord.x >= offset.x && coord.x < offset.x + size.x &&
			coord.z >= offset.z && coord.z < offset.z + size.z);
	}

	bool CoordRect::Contains(int x, int z) const
	{
		return (x >= offset.x && x < offset.x + size.x &&
				z >= offset.z && z < offset.z + size.z);
	}

	Coord CoordRect::Min() const { return offset; }
	Coord CoordRect::Max() const { return offset+size; }

	void CoordRect::Clamp(Coord min, Coord max)
	{
		Coord oldMax = Max();
		offset = Coord::Max(min, offset);
		size = Coord::Min(max - offset, oldMax - offset);
		size.ClampPositive();
	}

	CoordRect CoordRect::Intersected(CoordRect c1, CoordRect c2) { c1.Clamp(c2.Min(), c2.Max()); return c1; }

	Coord CoordRect::Tile(Coord coord, int tileMode)
	/// Returns the corresponding coord within the matrix rect
	{
		//transferring to zero-based coord
		coord.x -= offset.x;
		coord.z -= offset.z;

		switch (tileMode)
		{
			//case TileMode.Once:
			//	if (coord.x < 0 || coord.x >= size.x) coord.x = -1;
			//	if (coord.z < 0 || coord.z >= size.z) coord.z = -1;
			//	break;

		case 0:
			if (coord.x < 0) coord.x = 0;
			if (coord.x >= size.x) coord.x = size.x - 1;
			if (coord.z < 0) coord.z = 0;
			if (coord.z >= size.z) coord.z = size.z - 1;
			break;

		case 1:
			coord.x = coord.x % size.x;
			if (coord.x < 0) coord.x = size.x + coord.x;
			coord.z = coord.z % size.z;
			if (coord.z < 0) coord.z = size.z + coord.z;
			break;

		case 2:
			coord.x = coord.x % (size.x * 2);
			if (coord.x < 0) coord.x = size.x * 2 + coord.x;
			if (coord.x >= size.x) coord.x = size.x * 2 - coord.x - 1;

			coord.z = coord.z % (size.z * 2);
			if (coord.z < 0) coord.z = size.z * 2 + coord.z;
			if (coord.z >= size.z) coord.z = size.z * 2 - coord.z - 1;
			break;
		}

		coord.x += offset.x;
		coord.z += offset.z;

		return coord;
	}
}
