//
//  Test.hpp
//  MapMagicMacNative
//
//  Created by Wraith on 14/01/2020.
//  Copyright © 2020 Wraith. All rights reserved.
//

#ifndef Test_hpp
#define Test_hpp

#include <stdio.h>

extern "C"
{
int TestMe ();
}

#endif
