using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SerializedDataToObjMonobeh : MonoBehaviour
{
	public bool test = false;
	public string data = @"enabled: 1
id: 16963092770002567169
version: 0
draftTime: 0
mainTime: 0
guiPosition: {x: -295.66956, y: -209.3652}
guiSize: {x: 150, y: 1028}
guiPreview: 0
guiAdvanced: 0
guiDebug: 0
position:
  x: 0
  z: 0
radius: 30
hardness: 0.5
prefabval: {fileID: 0}
scriptableObjectval: {fileID: 0}
intval: 42
floatval: 42
doubleval: 42
vector2val: {x: 42, y: 42}
vector3val: {x: 42, y: 42, z: 42}
vector4val: {x: 42, y: 42, z: 42, w: 42}
rectval:
  serializedVersion: 2
  x: 0
  y: 0
  width: 42
  height: 42
intvector2val: {x: 42, y: 42}
intvector3val: {x: 42, y: 42, z: 42}
intvector4val: {x: 42, y: 42, z: 42, w: 42}
stringval: default
boolval: 1
longval: 42
shortval: 42
byteval: 42
charval: 99
uintval: 42
ulongval: 42
ushortval: 42
sbyteval: 42
coordval:
  x: 0
  z: 0
coordRectVal:
  offset:
    x: 0
    z: 0
  size:
    x: 0
    z: 0
vector2Dval:
  x: 0
  z: 0
colorval: {r: 0, g: 0, b: 0, a: 0}
enumVal: 0
rectOffsetVal:
  m_Left: 0
  m_Right: 0
  m_Top: 0
  m_Bottom: 0
intRectVal:
  x: 0
  y: 0
  width: 0
  height: 0";

    void OnDrawGizmos()
    {
        if (test)
		{
			test = false;

			ulong id = SerializedDataReader.GetValueFromSerializedData<ulong>("id", data);
			Debug.Log(id);

			Vector2 guiPosition = SerializedDataReader.GetValueFromSerializedData<Vector2>("guiPosition", data);
			Debug.Log(guiPosition);

			object obj = SerializedDataReader.CreateObjFromSerializedData("\"MapMagic.Tests\"", "MyTempGenerator8", "", data);


		}
    }
}
