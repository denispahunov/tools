using Den.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.XR;

[ExecuteInEditMode]
public class SemaphoreMonobeh : MonoBehaviour
{
    private static Semaphore pool;

	private static System.Random rand = new System.Random();

	public int numThreads = 5;

    public void OnEnable()
    {

        pool = new Semaphore(initialCount:0, maximumCount:3); //initial count of zero, so that the entire semaphore count is initially owned by the main program thread.

        // Create and start five numbered threads. 
        for(int i = 0; i < numThreads; i++)
        {
            Thread t = new Thread(new ParameterizedThreadStart(FibonacciThread));
            t.Start(i); // Start the thread, passing the number.
        }

        Log.AddThreaded("Start!", "Main");
        pool.Release(releaseCount: 3);
		Log.AddThreaded("Done!", "Main");
						
    }

	private void FibonacciThread(object threadContext)
	{
		string num = ((int)threadContext).ToString();
			
		Log.AddThreaded("Begins and waits", num);
		pool.WaitOne();

		Log.AddThreaded("Starting calculation...", num);
		Fibonacci( rand.Next(2000, 4000) );
		Log.AddThreaded("Finished calculation", num);

		int prevCount = pool.Release();
		Log.AddThreaded("Released semaphore", num, ("previous semaphore count:",prevCount));
	}

	private static int Fibonacci (int n)
	{
		if (n <= 1)
			return n;
		return Fibonacci(n - 1);// + Fibonacci(n - 2);
	}
}
