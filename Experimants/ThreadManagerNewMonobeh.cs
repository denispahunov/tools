﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.XR;
using Den.Tools.Tasks;
using System.Timers;
using UnityEditor.PackageManager;
using System.Collections.Concurrent;
using UnityEditor.SearchService;
using UnityEditor;
using UnityEditor.Search;

[ExecuteInEditMode]
public class ThreadManagerNewMonobeh : MonoBehaviour
{
	public static class ThreadManagerNew
	{
		private class ThreadData
		{
			//public Thread thread;
			public string tag; //for debug purpose
			public int priority;
		}

		//private static ConcurrentBag<ThreadData> queue = new ConcurrentBag<ThreadData>();
		private static ConcurrentDictionary<Thread,ThreadData> queue = new ConcurrentDictionary<Thread, ThreadData>();
		private static int activeCount = 0;
		private static object lockObj = new object();

		public static int maxThreads = 3;
		public static int processorThreads = -1;
		public static bool autoMaxThreads = true;
		public static bool useMultithreading = true;
		public static int NumThreads => autoMaxThreads ? processorThreads-1 : maxThreads;

		private static ConcurrentDictionary<Thread,ThreadData> debugActive = new ConcurrentDictionary<Thread,ThreadData>();
		private static ConcurrentDictionary<Thread,ThreadData> debugPaused = new ConcurrentDictionary<Thread,ThreadData>();

		static ThreadManagerNew ()
		{
			processorThreads = SystemInfo.processorCount; //the first time LaunchThreads is called from main thread

			//aborting when exiting playmode
			#if UNITY_EDITOR
			//EditorApplication.playModeStateChanged -= AbortOnPlaymodeChange;
			//Application.wantsToQuit -= AbortOnExit;

			//EditorApplication.playModeStateChanged += AbortOnPlaymodeChange;
			//Application.wantsToQuit += AbortOnExit;
			#endif
		}

		public static void Enqueue (Action action, int priority, string tag=null)
		{
			void ThreadFn(object context) => ThreadAction(action,context);

			Thread t = new Thread(new ParameterizedThreadStart(ThreadFn));
			if (!queue.TryAdd(t, new ThreadData() {tag=tag, priority=priority}))
				Log("Enqueue THREAD ALREADY ADDED TO DICT", isError:true);

			StartNextThread();
		}

		public static void MarkPaused () 
		{
			//activeCount--;
			Interlocked.Decrement(ref activeCount);
			Log("MarkPaused ActiveCount reduced");

			if (!debugActive.TryRemove(Thread.CurrentThread, out ThreadData tmp))
				Log("MarkPaused COULD NOT REMOVE THREAD FROM ACTIVE", isError:true);

			if (!debugPaused.TryAdd(Thread.CurrentThread, tmp))
				Log("MarkPaused COULD NOT ADD THREAD TO PAUSED", isError:true);

			if (activeCount < 0)
				Log("MarkPaused ACTIVE COUNT < 0", isError:true);

			//if (activeCount != debugActive.Count)
			//	Log("MarkPaused ACTIVE COUNT MISMATCH", isError:true);

			StartNextThread();
		}

		public static void MarkResumed () 
		{
			//activeCount++;
			Interlocked.Increment(ref activeCount);
			Log("MarkResumed ActiveCount increased");

			if (!debugPaused.TryRemove(Thread.CurrentThread, out ThreadData tmp))
				Log("MarkPaused COULD NOT REMOVE THREAD FROM ACTIVE", isError:true);

			if (!debugActive.TryAdd(Thread.CurrentThread, tmp))
				Log("MarkResumed COULD NOT ADD THREAD", isError:true);

			//if (activeCount != debugActive.Count)
			//	Log("MarkResumed ACTIVE COUNT MISMATCH", isError:true);
		}


		public static void ThreadAction (Action action, object context)
		{
			string tag = context as string;

			if (activeCount < 0)
				Log("ThreadAction ACTION WHILE ACTIVE==0", isError:true);

			try
			{ 
				Log("ThreadAction Starting action", tag);
				action(); 
				Log("ThreadAction Finished action", tag);
			}

			catch (ThreadAbortException) { }

			#if !MM_DEBUG
			catch (Exception e)
				{ Debug.LogError("Thread failed: " + e); }  //throw exception ignores VS
				//{ throw new Exception("Thread failed: " + e); }
			#endif

			finally
			{
				//activeCount--;
				Interlocked.Decrement(ref activeCount);
				Log("ThreadAction ActiveCount reduced");

				if (activeCount < 0)
					Log("ThreadAction ACTIVE COUNT < 0", isError:true);

				if (!debugActive.TryRemove(Thread.CurrentThread, out ThreadData tmp))
					Log("ThreadAction COULD NOT REMOVE THREAD", isError:true);

				//if (activeCount != debugActive.Count)
				//	Log("ThreadAction ACTIVE COUNT MISMATCH", isError:true);

				StartNextThread();
			}
		}


		private static void StartNextThread ()
		{
			int curMaxThreads = NumThreads;

			if (activeCount < curMaxThreads  &&  queue.Count > 0)
				lock (lockObj) //queue is safe for adding, but here we are picking and comparing. Can pick the same thread twice
					//TODO: can be re-arranged without lock - if !tryGet choose another thread
					if (activeCount < curMaxThreads  &&  queue.Count > 0) //double check - situation might have changed under lock
					{
						Thread topThread = TopPriorityThread();

						if (topThread == null)
							Log("StartNextThread TOP THREAD IS NULL", isError:true);

						queue.TryRemove(topThread, out ThreadData topThreadData);
						//activeCount++;
						Interlocked.Increment(ref activeCount);
						Log("StartNextThread ActiveCount increased");

						if (!debugActive.TryAdd(topThread, topThreadData))
							Log("StartNextThread COULD NOT ADD THREAD", isError:true);

						Log("StartNextThread Starting " + topThreadData.tag);
						topThread.Start();

						StartNextThread();
					}
			else
			{
				if (activeCount >= curMaxThreads)
					Log("StartNextThread Skipping because of thread limit");
				else if (queue.Count <= 0)
					Log("StartNextThread Skipping because of queue empty");
			}

			//if (activeCount != debugActive.Count)
			//	Log("StartNextThread ACTIVE COUNT MISMATCH", isError:true);
		}


		private static Thread TopPriorityThread ()
		{
			int topPriority = int.MinValue;
			Thread topThread = null;
			ThreadData topThreadData = null;

			foreach (var kvp in queue)
			{
				Thread thread = kvp.Key;
				ThreadData threadData = kvp.Value;

				if (threadData.priority > topPriority)
				{
					topPriority = threadData.priority;
					topThreadData = threadData;
					topThread = thread;
				}
			}

			return topThread;
		}

		public static int QueueCount => queue.Count;
		public static int ActiveCount => activeCount;
		public static bool IsActive => activeCount!=0  &&  queue.Count!=0;

		public static void Abort ()
		{

		}


		private static void Log (string msg, string tag=null, bool isError=false)
		{
			Thread thread = Thread.CurrentThread;
			bool inQueue = queue.TryGetValue(thread, out ThreadData threadData);

			string queueInfo = "";
			foreach (var kvp in queue)
			{
				ThreadData data = kvp.Value;
				queueInfo += data.tag + ", ";
			}
			if (queueInfo.Length > 2) queueInfo = queueInfo.Substring(0, queueInfo.Length - 2);

			string activeInfo = "";
			foreach (var kvp in debugActive)
			{
				ThreadData data = kvp.Value;
				activeInfo += data.tag + ", ";
			}
			if (activeInfo.Length > 2) activeInfo = activeInfo.Substring(0, activeInfo.Length - 2);

			string pauseInfo = "";
			foreach (var kvp in debugPaused)
			{
				ThreadData data = kvp.Value;
				pauseInfo += data.tag + ", ";
			}
			if (pauseInfo.Length > 2) pauseInfo = pauseInfo.Substring(0, pauseInfo.Length - 2);

			Den.Tools.Log.AddThreaded("ThreadManager." + msg, tag,
				("Active Count", activeCount),
				("Priority", inQueue ? threadData.priority : 0),
				("In Queue", inQueue),
				("Queue Count", queue.Count),
				("Queue", queueInfo),
				("Active Count (d)", debugActive.Count),
				("Active", activeInfo),
				("Pause Count", debugPaused.Count),
				("Pause", pauseInfo) );

			if (isError)
			{
				error = true;
				throw new Exception(msg + ". This should not happen.");
			}
		}
	}

	public static class ThreadManagerSemaphore
	{
		private class ThreadData
		{
			public string tag; //for debug purpose
			public int priority;
			public bool active;
		}

		private static ConcurrentDictionary<Thread,ThreadData> threads = new ConcurrentDictionary<Thread, ThreadData>(); 
		//higher priority starts first
		//priority can be negative - like distance to camera (the closer - the better, more priority)
		
		private static Semaphore semaphore;

		public static int maxThreads = 3;
		public static int processorThreads = -1;
		public static bool autoMaxThreads = true;
		public static bool useMultithreading = true;
		public static int NumThreads => autoMaxThreads ? processorThreads-1 : maxThreads;


		static ThreadManagerSemaphore ()
		{
			processorThreads = SystemInfo.processorCount; //the first time LaunchThreads is called from main thread
			semaphore = new Semaphore(NumThreads,NumThreads);

			//aborting when exiting playmode
			#if UNITY_EDITOR
			//EditorApplication.playModeStateChanged -= AbortOnPlaymodeChange;
			//Application.wantsToQuit -= AbortOnExit;

			//EditorApplication.playModeStateChanged += AbortOnPlaymodeChange;
			//Application.wantsToQuit += AbortOnExit;
			#endif
		}


		public static void StartThread (Action action, int priority, string tag=null)
		{
			void ThreadFn() => ThreadAction(action);

			Thread t = new Thread(new ThreadStart(ThreadFn));

			if (!threads.TryAdd(t, new ThreadData() {tag=tag, priority=priority}))
				{ error=true; throw new Exception("Thread already added to dict. This should not happen."); }

			t.Start();
		}

		public static void ThreadAction (Action action)
		{
			Thread thread = Thread.CurrentThread; //knows itself. TODO: maybe provide it and data as parametrized start?

			if (!threads.TryGetValue(thread,out ThreadData threadData))
				{ error=true; throw new Exception("Thread not in dict. This should not happen."); }


			semaphore.WaitOne();
			/*semaphore.Release();

			int topPriority = int.MinValue;
			foreach (ThreadData data in threads.Values)
				if (data.priority > topPriority)
					topPriority = data.priority;

			if (threadData.priority > topPriority)
				throw new Exception("Priority is more than top. This should not happen.");

			if (threadData.priority == topPriority)
				semaphore.WaitOne();*/

			threadData.active = true;

			try
			{ 
				Log("TaskThreadAction Starting action");
				action(); 
				Log("TaskThreadAction Finished action");
			}

			catch (ThreadAbortException) { }

			#if !MM_DEBUG
			catch (Exception e)
				{ Debug.LogError("Thread failed: " + e); }  //throw exception ignores VS
				//{ throw new Exception("Thread failed: " + e); }
			#endif

			finally
			{
				if (!threads.TryRemove(Thread.CurrentThread, out ThreadData tmp))
					{ error=true; throw new Exception("Thread couldn not be removed from dict. This should not happen."); }

				threadData.active = false;
				semaphore.Release();
			}
		}

		public static bool IsActive
		{get{
			if (!threads.TryGetValue(Thread.CurrentThread, out ThreadData data))
				return false;
			return data.active;
		}}

		public static int ActiveCount
		{get{
			int num = 0;
			foreach (var td in threads.Values)
				if (td.active)
					num++;
			return num;
		}}

		public static int QueueCount
		{get{
			int num = 0;
			foreach (var td in threads.Values)
				if (!td.active)
					num++;
			return num;
		}}

		public static int Count => threads.Count;

		public static void Abort ()
		{

		}

		public static void SetMaxThreads (int newNumThreads) //if -1 then auto
		{
			int oldNumThreads = NumThreads;

			if (newNumThreads < 0)
			{
				autoMaxThreads = true;
				newNumThreads = processorThreads;
			}
			else
				maxThreads = newNumThreads;

			if (oldNumThreads != newNumThreads)
			{
				//while (semaphore.WaitOne(0))
				//	semaphore.Release();
				semaphore = new Semaphore(newNumThreads, newNumThreads);
			}
		}

		private static void Log (string msg)
		{
			Thread currThread = Thread.CurrentThread;
			bool managed = threads.TryGetValue(currThread, out ThreadData currData);

			string threadsInfo = "";
			int activeNum = 0;
			int queueNum = 0;
			foreach (var kvp in threads)
			{
				ThreadData data = kvp.Value;
				threadsInfo += data.tag + " " + data.priority + " " + (data.active ? "☑" : "☐") + ", ";
				if (data.active)
					activeNum++;
				else
					queueNum++;
			}
			threadsInfo = threadsInfo.Substring(0, threadsInfo.Length - 2);

			Den.Tools.Log.AddThreaded("ThreadManager." + msg, currData?.tag,
				("Managed", managed),
				("Tag", managed ? currData.tag : "NA"),
				("Active", managed ? currData.active : true),
				("Priority", managed ? currData.priority : 0),
				("Count", Count),
				("ActiveCount", activeNum),
				("QueueCount", queueNum),
				("Threads", threads));
		}
	}

	// -graph (+area) - independent of tile, and changed from elsewhere at any moment. Thread does not write here.
	// -products - generated by tile. Checks if thread is stopped before writing to products, so kinda thread-safe
	// -threadToken - per-thread data. One thread - one data. Task  (action and thread), stop, startedREvision

	public class Tile
	{
		public string name;
		public int result;

		public int startedRevision;
		public int generatedRevision;
		public bool inProgress;

		public StopToken stop = new StopToken(); 
		//it's not part of tile, it's part of thread. One tile might have stopped thread and new one ongoing
		//it's part of cluster (GenInfo), not tile!

		public ManualResetEvent mre = new ManualResetEvent(true);

		public ConcurrentBag<Tile> clusters;
		public string startedBy;
		public bool debugInProgress; //started and ended in thread

		//public ThreadManager.Task task; //last relevant task. All other tasks are 
		//new task is assigned, while thread still tends to read tile.task


	}
	public class StopToken { public bool stop=false; }

	public Tile[] tiles;
	public Tile[] clusters;

	private static System.Random rand = new System.Random();
	public int seed = 12345;

	public bool testOne;
	public int numTiles = 10;
	public int numClusters = 4;
	public int maxThreads = 2;
	public int minVal = 100000;
	public int maxVal = 400000;

	public bool testMultiple;
	public int multipleIterations = 100;
	public float sleepTime = 4;

	public int threadsStarted = 0;
	public int threadsFinished = 0;
	public int clustersStarted = 0;
	public int clustersFinished = 0;
	public int threadsUsed = 0;
	public int threadsQueued = 0;
	public int clusterCurrentRev = 0;
	public int clusterShouldBeRev = 0;
	public int revisionShouldBe = 0;

	public bool testPausing;
	public int threadsActiveNow = 0;
	public int threadsQueuedNow  = 0;
	public int threadsPausedNow = 0;

	public static bool error = false;
	public bool testMultipleMultiple;
	public int multipleMultipleIterations=10;

	public void OnDrawGizmos ()
	{
		threadsActiveNow = ThreadManager.ActiveCount;
		threadsQueuedNow = ThreadManager.QueueCount;
		//threadsPausedNow = ThreadManager.Count;

		if (testOne)
		{
			testOne = false;
			TestOne();
		}

		if (testMultiple)
		{
			testMultiple = false;
			TestMultiple();
		}

		if (testMultipleMultiple)
		{
			testMultipleMultiple = false;
			TestMultipleMultiple();
		}
	}

	private void TestPrepare ()
	{
		error = false;
		ThreadManager.error = false;

		ThreadManager.maxThreads = maxThreads;
		ThreadManager.autoMaxThreads = false;
		rand = new System.Random(seed);

		tiles = new Tile[numTiles];
		clusters = new Tile[numClusters];
		for (int c=0; c<clusters.Length; c++)
		{
			clusters[c] = new Tile();
			clusters[c].name = "CL"+c.ToString();
		}
		for (int t=0; t<tiles.Length; t++)
		{
			tiles[t] = new Tile();
			tiles[t].name = t.ToString();

			tiles[t].clusters = new ConcurrentBag<Tile>();

			int cm = rand.Next(1,4);
			for (int c=0; c<cm; c++)
			{
				int num = rand.Next(0,numClusters);
				Tile cluster = clusters[num];

				for (int i=0; i<5; i++)
				{
					bool contains = false;
					foreach (Tile tCluster in tiles[t].clusters)
						if (tCluster == cluster)
							contains = true;

					if (contains)
					{
						num = rand.Next(0,numClusters);
						cluster = clusters[num];
					}
					else
					{
						tiles[t].clusters.Add(cluster);
						break;
					}

					if (i==4)
						{ error=true; throw new Exception("Could not pick random unique cluster after 5 iterations"); }
				}
			}
		}

		threadsStarted = 0;
		threadsFinished = 0;
		clustersStarted = 0;
		clustersFinished = 0;
		threadsUsed = 0;
		threadsQueued = 0;
		revisionShouldBe  = 0;

		Den.Tools.Log.Clear();
		Den.Tools.Log.enabled = true;

		ThreadManager.Abort();
		ThreadManager.autoMaxThreads = false;
		ThreadManager.maxThreads = maxThreads;
	}


	private void TestStats ()
	{
		Thread.Sleep(1000);

		if (threadsStarted != threadsFinished)
			{error=true; Debug.LogError("Multitest finished. Threads num mismatch"); }
		else if (clustersStarted != clustersFinished)
			{error=true; Debug.LogError("Multitest finished. Clusters num mismatch"); }
		else if (ThreadManager.QueueCount != 0)
			{error=true; Debug.LogError("Multitest finished. Threadmanager queue not empty"); }
		else if (error==true)
			Debug.LogError("Multitest finished with local error.");
		else if (ThreadManager.error==true)
			Debug.LogError("Multitest finished with ThreadManager error.");
		else
			Debug.Log("Multitest finished OK");
	}


	public void TestOne ()
	{
		TestPrepare();

		Log("Start!", null, null);
		revisionShouldBe++;

		// Create and start five numbered threads. 
		for (int i = 0; i < tiles.Length; i++)
		{
			tiles[i].startedRevision = revisionShouldBe;

			Tile tile = tiles[i]; //don't use tiles[i] in ()=>, closure!
			Action action = ()=>TileThread(tile,new StopToken());

			//ThreadManager.Task task = new ThreadManager.Task() { 
			//	action = ()=>TileThread(tile,new StopToken()), 
			//	priority = i,
			//	name = tile.name };
			ThreadManager.Enqueue(action,i,tile.name);
			//tile.task = task;
			//threadsStarted++;
			Interlocked.Increment(ref threadsStarted);
		}

		TestStats();
	}

	public void TestMultiple ()
	{
		TestPrepare();

		Dictionary<Tile,StopToken> lastTileThreadData = new Dictionary<Tile,StopToken>();
		for (int t = 0; t < tiles.Length; t++)
			lastTileThreadData.Add(tiles[t], new StopToken());

		for (int i=0; i<multipleIterations; i++)
		{
			revisionShouldBe++;
			Log("Iteration " + revisionShouldBe + " Start", null, null);
			

			//assigning some clusters at random latest revision to avoid restarting
			//will confuse running threads
			/*foreach (Tile cluster in clusters)
			{
				if (!cluster.inProgress  &&  rand.Next(0,3)!=0)
				{
					cluster.startedRevision = revisionShouldBe;
					cluster.generatedRevision = revisionShouldBe;
				}
			}*/

			for (int t = 0; t < tiles.Length; t++)
			{
				Tile tile = tiles[t];

				//if (tile.inProgress)
					lastTileThreadData[tile].stop = true;
				tile.inProgress  = false;

				tile.startedRevision = revisionShouldBe;

				lastTileThreadData[tile] = new StopToken();
				StopToken stop = lastTileThreadData[tile]; //closure var
				Action action = ()=>TileThread(tile,stop);
				ThreadManager.Enqueue(action, t, tile.name); // -priority to start in reverse order

				//threadsStarted++;
				Interlocked.Increment(ref threadsStarted);
			}

			if (i>multipleIterations/2)
			{
				if (ThreadManager.ActiveCount > threadsUsed)
					threadsUsed = ThreadManager.ActiveCount;
				if (ThreadManager.QueueCount > threadsQueued)
					threadsQueued = ThreadManager.QueueCount;
			}

			int numInProgress = 0;
			for (int t=0; t<tiles.Length; t++)
				if (tiles[t].debugInProgress)
					numInProgress++;
			for (int c=0; c<clusters.Length; c++)
				if (clusters[c].debugInProgress)
					numInProgress++;
			if (numInProgress > maxThreads)
				{ error=true; throw new Exception ("Number of threads in progress (" + numInProgress + ") > maxThreads"); }

			double sleepTimeF = rand.NextDouble() * sleepTime;
			int currSleepTime = (int)(sleepTimeF*sleepTimeF*sleepTimeF);
			Log("Sleep" + currSleepTime + "ms", null, null);
			Thread.Sleep(currSleepTime);

		}

		TestStats();
	}


	public void TestMultipleMultiple ()
	{
		for (int i=0; i<multipleMultipleIterations; i++)
		{
			TestMultiple();

			Thread.Sleep(3000);

			if (error || ThreadManager.error)
			{
				Debug.LogError("MultipleMultiple ended with an error");
				return;
			}
		}
	}


	private void TileThread (Tile tile, StopToken stop)
	{
		int rnd = rand.Next(minVal, maxVal);
		Log("TileThread Starting", tile, stop); 
		
		Fibonacci(rnd, stop);

		TileThreadClusters(tile, stop);

		foreach (Tile cluster in tile.clusters)
			if (cluster.generatedRevision < tile.startedRevision  &&  !stop.stop) //if > then it's okay
			{
				Log("TileThread WRONG CL REV", tile, stop); 
				error = true;
				throw new Exception("TileThread WRONG CL REV");
			}

		Fibonacci(rnd, stop);

		//threadsFinished++;
		Interlocked.Increment(ref threadsFinished);
		tile.generatedRevision = tile.startedRevision;
		tile.inProgress = false;
		Log("TileThread Finished", tile, stop); 
	}


	private void TileThreadClusters (Tile tile, StopToken stop)
	{
		Log("TileThreadClusters Start", tile, stop);

		int revision = tile.startedRevision;

		Tile[] tileClusters = tile.clusters.ToArray(); //tile clusters won't change
		bool[] pauseOnCluster = new bool[tileClusters.Length];  
			//checking both: WaitOne(0) and pauseOnCluster. But pauseOnCluster first for performance reasons
			//should be local - depends  on revision
		
		//finding whether need to start clusters
		//and locking them
		for (int c=0; c<tileClusters.Length; c++)
		{
			Tile cluster = tileClusters[c];
			int clusterNum = Array.FindIndex(clusters, e=>e==cluster);

			//if this or later revision ready - do nothing
			//if this or later revision generating - pausing and wait for it to finish
			//if it's generating outdated version - stopping it, forgetting, starting new and pausing
			//if it's (not started) or (outdated and not generating) or (generating outdated revision) - starting new thread and pausing this

			if (cluster.generatedRevision < revision)  //if this or later revision ready - do nothing. But if not - run this
				lock (cluster)
			{
				//Monitor.Enter(cluster, ref clustersLocked[c]);
				//pause, might become ready meanwhile

				if (cluster.generatedRevision < revision) 
				{
					if (cluster.inProgress  &&  cluster.startedRevision >= revision)
					{
						Log("TileThreadClusters Deciding to pause while CL generating CL" + clusterNum, tile, stop);
						pauseOnCluster[c] = true;
					}

					if (cluster.inProgress  &&  cluster.startedRevision < revision)
					{
						Log("TileThreadClusters Stopping previous CL thread CL" + clusterNum, tile, stop);

						cluster.stop.stop = true; //TODO: currently it's sharing same stop token with tile. There might be a problem!
						cluster.stop = new StopToken();
						cluster.inProgress = false;
					}

					//if it's (not started) or (outdated and not generating) or (generating outdated revision) - starting new thread and pausing this
					if (!cluster.inProgress) //not else - it's resetting inProgress
					{
						Log("TileThreadClusters Starting CL thread CL"+clusterNum, tile, stop);

						cluster.mre.Reset();
						cluster.inProgress = true;
						cluster.startedRevision = revision;
						cluster.startedBy = tile.name;
						cluster.stop = new StopToken();
						StopToken clusterStop = cluster.stop;

						//Why separate thread: one tile can require up to 4 (or even more) clusters
						//And it's better to do it with thread manager to start generating all tiles that's using previously generated cluster instead of waiting

						void ThreadFn() => ClusterThread(cluster, clusterStop);

						//New manager
						ThreadManager.Enqueue(ThreadFn, 1000, cluster.name);

						//Starting via thread manager
						//ThreadManager.Task clusterTask = new ThreadManager.Task() { 
						//	action = ThreadFn, 
						//	priority = 1000000, //before draft
						//	name = cluster.name };
						//ThreadManager.Enqueue(clusterTask);

						/*Consider sending (coord, genInfo, productSet, graph) as thread context:
						Thread t = new Thread(new ParameterizedThreadStart(ThreadFn));
						t.Start(threadContext);*/

						//Starting via thread directly
						//Thread t = new Thread(new ThreadStart(ThreadFn));
						//t.Start();

						pauseOnCluster[c] = true;

						//clustersStarted++;
						Interlocked.Increment(ref clustersStarted);
					}
				}
			}
		}

		//pausing thread (several times for each cluster) and resuming locks if any
		for (int c=0; c<tileClusters.Length; c++)
		{
			Tile cluster = tileClusters[c];

			if (pauseOnCluster[c]  &&  !stop.stop   &&  !cluster.mre.WaitOne(0))
				//WaitOne(0) true if the MRE is signaled (non-blocked), false if the MRE is not signaled (blocked).
			{
				int clusterNum = Array.FindIndex(clusters, e=>e==cluster);

				Log("TileThreadClusters Pausing  CL"+clusterNum, tile, stop);
				ThreadManager.MarkPaused();
				cluster.mre.WaitOne(); // Wait until the ManualResetEvent is signaled
				Log("TileThreadClusters Resuming CL"+clusterNum, tile, stop);
				ThreadManager.MarkResumed();
				
			}
		}

		if (!stop.stop)
			Log("TileThreadClusters Done", tile, stop);
		else
			Log("TileThreadClusters Stopped", tile, stop);
	}


	private void ClusterThread (Tile cluster, StopToken stop)
	{
		Log("ClusterThread Starting", cluster, stop);

		int rnd = rand.Next(minVal*2, maxVal*2);
		Fibonacci(rnd, stop);
		//clustersFinished++;
		Interlocked.Increment(ref clustersFinished);

		//mark generated
		lock (cluster)
		{
			int lastGeneratedRevision = cluster.generatedRevision;
			int justGeneratedRevision = cluster.startedRevision;
			if (!stop.stop) //do not mark revision if it's cancelled
			{
					if (lastGeneratedRevision < justGeneratedRevision)
					//common case - just generated new version other than last marked
					{
						//StoreProductsFromData(productSet, graph, data);  //stores products from tile data to cluster
						Log("ClusterThread Storing Products", cluster, stop);
						cluster.generatedRevision = cluster.startedRevision;
					}

					else if (lastGeneratedRevision > justGeneratedRevision)
					//could generate outdated revision after new one stored - doing nothing
						{ }

					else if (lastGeneratedRevision == justGeneratedRevision)
					{
						Log("ClusterThread REVISION EQUALITY", cluster, stop);
						error=true;
						throw new Exception("Revision equality: " + lastGeneratedRevision + " This should not happen");
					}

					Log("ClusterThread Unpausing", cluster, stop);
					cluster.inProgress = false;
					cluster.mre.Set();
			}

			/*else if (!cluster.inProgress) //if stop and !inProgress
			//inProgress is per-cluster, while stop is per-thread. Of course it can change while thread won't
			{
				Log("ClusterThread STOPPED NOT IN PROGRESS", cluster, stop);
				error=true;
				throw new Exception("Cluster stopped while it's not in progress. This should not happen");
			}*/
		}

		Log("ClusterThread All Done", cluster, stop);
	}



	private void Log (string caption, Tile tile, StopToken stop, params (string,object)[] optional)
	{
		(string,object)[] t = null;
		List<(string,object)> l = new List<(string,object)>();

		if (tile !=  null)
		{
			l.Add(("Name",tile.name));
			l.Add(("Started Rev",tile.startedRevision));
			l.Add(("Generated Rev",tile.generatedRevision));
			l.Add(("In Progress",tile.inProgress));
			l.Add(("Stop",stop?.stop));
			l.Add(("Active",ThreadManager.IsWorking()));
			
			if (tile.startedBy != null)
				l.Add(("Started By", tile.startedBy));

			if (tile.clusters != null)
			{
				l.Add(("Num Clusters",tile.clusters.Count));

				foreach (Tile cluster in tile.clusters)
				{
					int n = Array.FindIndex(clusters, e=>e==cluster);

					l.Add(("CL" + n + " Name",cluster.name));
					l.Add(("CL" + n + " Started Rev",cluster.startedRevision));
					l.Add(("CL" + n + " Generated Rev",cluster.generatedRevision));
					l.Add(("CL" + n + " In Progress",cluster.inProgress));
				}
			}
		}
		else
		{
			int n = 0;
			foreach (Tile cluster in clusters)
			{
				l.Add(("CL" + n + " Name",cluster.name));
				l.Add(("CL" + n + " Started Rev",cluster.startedRevision));
				l.Add(("CL" + n + " Generated Rev",cluster.generatedRevision));
				l.Add(("CL" + n + " In Progress",cluster.inProgress));
				n++;
			}
		}

		t = l.ToArray();

		string idName = tile == null ? "Main" : tile.name;
		
		Den.Tools.Log.AddThreaded(caption, idName, t);
	}

	private static int Fibonacci (int n, StopToken stop=null)
	{
		int prev = 1;
		int prevPrev = 1;
		int result = 0;

		for (int i=0; i<n; i++)
		{
			result = prev + prevPrev;

			prevPrev = prev;
			prev = result;

			if (stop != null &&  stop.stop)
				return 0;
		}

		return result;
	}
}
