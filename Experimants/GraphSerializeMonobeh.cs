using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphSerializeMonobeh : MonoBehaviour
{
	//generators
	[System.Serializable]
	public abstract class Generator
	{
		public int baseVal;
	}

	public interface IInlet<out T>
	{
		
	}


	[System.Serializable]
	public class GeneratorA : Generator, IInlet<GraphSerializeMonobeh>
	{
		public int aVal;
	}

	[System.Serializable]
	public class GeneratorB_tmp : Generator
	{
		public int bVal;
	}

	[System.Serializable]
	public class InletB : IInlet<Generator>
	{
		public int bVal;
	}


	[SerializeReference] public Generator[] generators;
	//[SerializeReference]  //not works with generics  arrays
		public IInlet<object>[] inlets;

	[SerializeReference] public IInlet<int> intInlet;
	[SerializeReference] public IInlet<int>[] intInlets;

	public class ClassWithInlet 
	{
		public IInlet<int> inlet;
	}
	[SerializeReference] ClassWithInlet classInlet;


	public bool reset;
	public bool test;
	public void OnDrawGizmos ()
	{
		if (reset) 
		{
			reset = false;

			generators = new Generator[2];
			generators[0] = new GeneratorA() {aVal = 42, baseVal=42};
//			generators[1] = new GeneratorB() {bVal = 3, baseVal=4};

			inlets = new IInlet<object>[2];
			inlets[0] = new GeneratorA() {aVal=42};
			inlets[0] = new InletB() {bVal=5};
		}

		if (test)
		{
			test = false;

			foreach (Generator gen in generators)
			{
				if (gen is GeneratorA aGen)
					Debug.Log("GeneratorA " + aGen.aVal + ", " + gen.baseVal);
//				if (gen is GeneratorB bGen)
//					Debug.Log("GeneratorB " + bGen.bVal + ", " + gen.baseVal);
			}
		}
	}
}
