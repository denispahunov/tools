
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.XR;
using Den.Tools.Tasks;
using System.Timers;
using UnityEditor.PackageManager;
using System.Collections.Concurrent;

[ExecuteInEditMode]
public class TilesClusterMonobeh : MonoBehaviour
{
	// -graph (+area) - independent of tile, and changed from elsewhere at any moment. Thread does not write here.
	// -products - generated by tile. Checks if thread is stopped before writing to products, so kinda thread-safe
	// -threadToken - per-thread data. One thread - one data. Task  (action and thread), stop, startedREvision

	public class Tile
	{
		public string name;
		public int result;

		public int startedRevision;
		public int generatedRevision;
		public bool inProgress;

		public StopToken stop = new StopToken(); 
		//it's not part of tile, it's part of thread. One tile might have stopped thread and new one ongoing
		//it's part of cluster (GenInfo), not tile!

		public ManualResetEvent mre = new ManualResetEvent(true);

		public ConcurrentBag<Tile> clusters;
		public string startedBy;

		public ThreadManagerTasks.Task task; //last relevant task. All other tasks are 
		//new task is assigned, while thread still tends to read tile.task
	}
	public class StopToken { public bool stop=false; }

	public Tile[] tiles;
	public Tile[] clusters;

	private static System.Random rand = new System.Random();
	public int seed = 12345;

	public bool testOne;
	public int numTiles = 10;
	public int numClusters = 4;
	public int maxThreads = 2;
	public int minVal = 100000;
	public int maxVal = 400000;

	public bool testMultiple;
	public int multipleIterations = 100;
	public float sleepTime = 4;

	public int threadsStarted = 0;
	public int threadsFinished = 0;
	public int clustersStarted = 0;
	public int clustersFinished = 0;
	public int threadsUsed = 0;
	public int threadsQueued = 0;
	public int clusterCurrentRev = 0;
	public int clusterShouldBeRev = 0;
	public int revisionShouldBe = 0;

	public bool testPausing;
	public int threadsActiveNow = 0;
	public int threadsQueuedNow  = 0;
	public int threadsPausedNow = 0;

	public void OnDrawGizmos ()
	{
		threadsActiveNow = ThreadManager.ActiveCount;
		threadsQueuedNow = ThreadManager.QueueCount;
		//threadsPausedNow = ThreadManager.Count;

		if (testOne)
		{
			testOne = false;
			TestOne();
		}

		if (testMultiple)
		{
			testMultiple = false;
			TestMultiple();
		}
	}

	private void TestPrepare ()
	{
		rand = new System.Random(seed);

		tiles = new Tile[numTiles];
		clusters = new Tile[numClusters];
		for (int c=0; c<clusters.Length; c++)
		{
			clusters[c] = new Tile();
			clusters[c].name = "CL"+c.ToString();
		}
		for (int t=0; t<tiles.Length; t++)
		{
			tiles[t] = new Tile();
			tiles[t].name = t.ToString();

			tiles[t].clusters = new ConcurrentBag<Tile>();

			int cm = rand.Next(1,4);
			for (int c=0; c<cm; c++)
			{
				int num = rand.Next(0,numClusters);
				Tile cluster = clusters[num];

				for (int i=0; i<5; i++)
				{
					bool contains = false;
					foreach (Tile tCluster in tiles[t].clusters)
						if (tCluster == cluster)
							contains = true;

					if (contains)
					{
						num = rand.Next(0,numClusters);
						cluster = clusters[num];
					}
					else
					{
						tiles[t].clusters.Add(cluster);
						break;
					}

					if (i==4)
						throw new Exception("Could not pick random unique cluster after 5 iterations");
				}
			}
		}

		threadsStarted = 0;
		threadsFinished = 0;
		clustersStarted = 0;
		clustersFinished = 0;
		threadsUsed = 0;
		threadsQueued = 0;
		revisionShouldBe  = 0;

		Den.Tools.Log.Clear();
		Den.Tools.Log.enabled = true;

		ThreadManager.Abort();
		ThreadManager.autoMaxThreads = false;
		ThreadManager.maxThreads = maxThreads;
	}


	private void TestStats ()
	{
		Thread.Sleep(500);

		if (threadsStarted != threadsFinished)
			Debug.LogError("Multitest finished. Threads num mismatch");
		else if (clustersStarted != clustersFinished)
			Debug.LogError("Multitest finished. Clusters num mismatch");
		else if (ThreadManager.QueueCount != 0)
			Debug.LogError("Multitest finished. Threadmanager queue not empty");
		else
			Debug.Log("Multitest finished OK");
	}


	public void TestOne ()
	{
		TestPrepare();

		Log("Start!", null, null);
		revisionShouldBe++;

		// Create and start five numbered threads. 
		for (int i = 0; i < tiles.Length; i++)
		{
			tiles[i].startedRevision = revisionShouldBe;

			Tile tile = tiles[i]; //don't use tiles[i] in ()=>, closure!
			ThreadManagerTasks.Task task = new ThreadManagerTasks.Task() { 
				action = ()=>TileThread(tile,new StopToken()), 
				priority = i,
				name = tile.name };
			ThreadManagerTasks.Enqueue(task);
			tile.task = task;
			threadsStarted++;
		}

		TestStats();
	}

	public void TestMultiple ()
	{
		TestPrepare();

		Dictionary<Tile,StopToken> lastTileThreadData = new Dictionary<Tile,StopToken>();
		for (int t = 0; t < tiles.Length; t++)
			lastTileThreadData.Add(tiles[t], new StopToken());

		for (int i=0; i<multipleIterations; i++)
		{
			revisionShouldBe++;
			Log("Iteration " + revisionShouldBe + " Start", null, null);
			

			//assigning some clusters at random latest revision to avoid restarting
			//will confuse running threads
			/*foreach (Tile cluster in clusters)
			{
				if (!cluster.inProgress  &&  rand.Next(0,3)!=0)
				{
					cluster.startedRevision = revisionShouldBe;
					cluster.generatedRevision = revisionShouldBe;
				}
			}*/

			for (int t = 0; t < tiles.Length; t++)
			{
				Tile tile = tiles[t];

				//if (tile.inProgress)
					lastTileThreadData[tile].stop = true;
				tile.inProgress  = false;

				tile.startedRevision = revisionShouldBe;

				lastTileThreadData[tile] = new StopToken();
				StopToken stop = lastTileThreadData[tile]; //closure var
				ThreadManagerTasks.Task task = new ThreadManagerTasks.Task() { 
					action = ()=>TileThread(tile,stop),  //don't use tiles[i] in ()=>, closure!
					priority = i,
					name = tile.name };
				ThreadManagerTasks.Enqueue(task);
				tile.task = task;

				threadsStarted++;
			}

			if (i>multipleIterations/2)
			{
				if (ThreadManagerTasks.ActiveCount > threadsUsed)
					threadsUsed = ThreadManagerTasks.ActiveCount;
				if (ThreadManagerTasks.QueueCount > threadsQueued)
					threadsQueued = ThreadManagerTasks.QueueCount;
			}

			double sleepTimeF = rand.NextDouble() * sleepTime;
			int currSleepTime = (int)(sleepTimeF*sleepTimeF*sleepTimeF);
			Log("Sleep" + currSleepTime + "ms", null, null);
			Thread.Sleep(currSleepTime);

		}

		TestStats();
	}


	private void TileThread (Tile tile, StopToken stop)
	{
		int rnd = rand.Next(minVal, maxVal);
		Log("TileThread Starting", tile, stop); 
		
		Fibonacci(rnd, stop);

		TileThreadClusters(tile, stop);

		if (!stop.stop)
			foreach (Tile cluster in tile.clusters)
				if (cluster.generatedRevision != tile.startedRevision)
				{
					Log("TileThread WRONG CL REV", tile, stop); 
					throw new Exception("TileThread WRONG CL REV");
				}

		Fibonacci(rnd, stop);

		threadsFinished++;
		tile.generatedRevision = tile.startedRevision;
		tile.inProgress = false;
		Log("TileThread Finished", tile, stop);
	}


	private void TileThreadClusters (Tile tile, StopToken stop)
	{
		Log("TileThreadClusters Start", tile, stop);

		int revision = tile.startedRevision;

		Tile[] tileClusters = tile.clusters.ToArray(); //tile clusters won't change
		bool[] pauseOnCluster = new bool[tileClusters.Length];  
			//checking both: WaitOne(0) and pauseOnCluster. But pauseOnCluster first for performance reasons
			//should be local - depends  on revision
		
		//finding whether need to start clusters
		//and locking them
		for (int c=0; c<tileClusters.Length; c++)
		{
			Tile cluster = tileClusters[c];
			int clusterNum = Array.FindIndex(clusters, e=>e==cluster);

			//if this or later revision ready - do nothing
			//if this or later revision generating - pausing and wait for it to finish
			//if it's generating outdated version - stopping it, forgetting, starting new and pausing
			//if it's (not started) or (outdated and not generating) or (generating outdated revision) - starting new thread and pausing this

			if (cluster.generatedRevision < revision)  //if this or later revision ready - do nothing. But if not - run this
				lock (cluster)
			{
				//Monitor.Enter(cluster, ref clustersLocked[c]);
				//pause, might become ready meanwhile

				if (cluster.generatedRevision < revision) 
				{
					if (cluster.inProgress  &&  cluster.startedRevision >= revision)
					{
						Log("TileThreadClusters Deciding to pause while CL generating CL" + clusterNum, tile, stop);
						pauseOnCluster[c] = true;
					}

					if (cluster.inProgress  &&  cluster.startedRevision < revision)
					{
						Log("TileThreadClusters Stopping previous CL thread CL" + clusterNum, tile, stop);

						cluster.stop.stop = true; //TODO: currently it's sharing same stop token with tile. There might be a problem!
						cluster.stop = new StopToken();
						cluster.inProgress = false;
					}

					//if it's (not started) or (outdated and not generating) or (generating outdated revision) - starting new thread and pausing this
					if (!cluster.inProgress) //not else - it's resetting inProgress
					{
						Log("TileThreadClusters Starting CL thread CL"+clusterNum, tile, stop);

						cluster.mre.Reset();
						cluster.inProgress = true;
						cluster.startedRevision = revision;
						cluster.startedBy = tile.name;
						cluster.stop = new StopToken();
						StopToken clusterStop = cluster.stop;

						//Why separate thread: one tile can require up to 4 (or even more) clusters
						//And it's better to do it with thread manager to start generating all tiles that's using previously generated cluster instead of waiting

						void ThreadFn() => ClusterThread(cluster, clusterStop);

						//Starting via thread manager
						ThreadManagerTasks.Task clusterTask = new ThreadManagerTasks.Task() { 
							action = ThreadFn, 
							priority = 1000000, //before draft
							name = cluster.name };
						ThreadManagerTasks.Enqueue(clusterTask);

						/*Consider sending (coord, genInfo, productSet, graph) as thread context:
						Thread t = new Thread(new ParameterizedThreadStart(ThreadFn));
						t.Start(threadContext);*/

						//Starting via thread directly
						//Thread t = new Thread(new ThreadStart(ThreadFn));
						//t.Start();

						pauseOnCluster[c] = true;

						clustersStarted++;
					}
				}
			}
		}

		//pausing thread (several times for each cluster) and resuming locks if any
		for (int c=0; c<tileClusters.Length; c++)
		{
			Tile cluster = tileClusters[c];

			if (pauseOnCluster[c]  &&  !stop.stop   &&  !cluster.mre.WaitOne(0))
				//WaitOne(0) true if the MRE is signaled (non-blocked), false if the MRE is not signaled (blocked).
			{
				int clusterNum = Array.FindIndex(clusters, e=>e==cluster);

				if (!tile.task.Active)
				{
					Log("TileThreadClusters PAUSING IN NON-ACTIVE CL" + clusterNum, tile, stop);
					Debug.LogError("Pausing in non-active state"); //removes itself from active on exception
				}

				if (cluster.mre.WaitOne(0)) //WaitOne(0) true if the MRE is signaled (non-blocked), false if the MRE is not signaled (blocked).
				{
					Log("TileThreadClusters PAUSE WILL NOT PAUSE CL"+clusterNum, tile, stop);
					Debug.LogError("Pause will not pause");
				}

				Log("TileThreadClusters Pausing  CL"+clusterNum, tile, stop);
				ThreadManagerTasks.MarkPaused(tile.task);
				cluster.mre.WaitOne(); // Wait until the ManualResetEvent is signaled
				Log("TileThreadClusters Resuming CL"+clusterNum, tile, stop);
				ThreadManagerTasks.EnqueuePaused(tile.task);
				
			}
		}

		if (!stop.stop)
			Log("TileThreadClusters Done", tile, stop);
		else
			Log("TileThreadClusters Stopped", tile, stop);
	}


	private void ClusterThread (Tile cluster, StopToken stop)
	{
		Log("ClusterThread Starting", cluster, stop);

		int rnd = rand.Next(minVal*2, maxVal*2);
		Fibonacci(rnd, stop);
		clustersFinished++;

		//mark generated
		lock (cluster)
		{
			int lastGeneratedRevision = cluster.generatedRevision;
			int justGeneratedRevision = cluster.startedRevision;
			if (!stop.stop) //do not mark revision if it's cancelled
			{
					if (lastGeneratedRevision < justGeneratedRevision)
					//common case - just generated new version other than last marked
					{
						//StoreProductsFromData(productSet, graph, data);  //stores products from tile data to cluster
						Log("ClusterThread Storing Products", cluster, stop);
						cluster.generatedRevision = cluster.startedRevision;
					}

					else if (lastGeneratedRevision > justGeneratedRevision)
					//could generate outdated revision after new one stored - doing nothing
						{ }

					else if (lastGeneratedRevision == justGeneratedRevision)
					{
						Log("ClusterThread REVISION EQUALITY", cluster, stop);
						throw new Exception("Revision equality: " + lastGeneratedRevision + " This should not happen");
					}

					Log("ClusterThread Unpausing", cluster, stop);
					cluster.inProgress = false;
					cluster.mre.Set();
			}

			else if (!cluster.inProgress)
			{
				Log("ClusterThread STOPPED NOT IN PROGRESS", cluster, stop);
				throw new Exception("Cluster stopped while it's not in progress. This should not happen");
			}
		}

		Log("ClusterThread All Done", cluster, stop);
	}



	private void Log (string caption, Tile tile, StopToken stop, params (string,object)[] optional)
	{
		(string,object)[] t = null;
		List<(string,object)> l = new List<(string,object)>();

		if (tile !=  null)
		{
			l.Add(("Name",tile.name));
			l.Add(("Started Rev",tile.startedRevision));
			l.Add(("Generated Rev",tile.generatedRevision));
			l.Add(("In Progress",tile.inProgress));
			l.Add(("Stop",stop?.stop));

			if (tile.task != null)
				l.Add(("Active",tile.task.Active));
			
			if (tile.startedBy != null)
				l.Add(("Started By", tile.startedBy));

			if (tile.clusters != null)
			{
				l.Add(("Num Clusters",tile.clusters.Count));

				foreach (Tile cluster in tile.clusters)
				{
					int n = Array.FindIndex(clusters, e=>e==cluster);

					l.Add(("CL" + n + " Name",cluster.name));
					l.Add(("CL" + n + " Started Rev",cluster.startedRevision));
					l.Add(("CL" + n + " Generated Rev",cluster.generatedRevision));
					l.Add(("CL" + n + " In Progress",cluster.inProgress));
				}
			}
		}
		else
		{
			int n = 0;
			foreach (Tile cluster in clusters)
			{
				l.Add(("CL" + n + " Name",cluster.name));
				l.Add(("CL" + n + " Started Rev",cluster.startedRevision));
				l.Add(("CL" + n + " Generated Rev",cluster.generatedRevision));
				l.Add(("CL" + n + " In Progress",cluster.inProgress));
				n++;
			}
		}

		t = l.ToArray();

		string idName = tile == null ? "Main" : tile.name;
		
		Den.Tools.Log.AddThreaded(caption, idName, t);
	}

	private static int Fibonacci (int n, StopToken stop=null)
	{
		int prev = 1;
		int prevPrev = 1;
		int result = 0;

		for (int i=0; i<n; i++)
		{
			result = prev + prevPrev;

			prevPrev = prev;
			prev = result;

			if (stop != null &&  stop.stop)
				return 0;
		}

		return result;
	}
}
