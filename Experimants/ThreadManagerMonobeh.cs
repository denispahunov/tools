using Den.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.XR;
using Den.Tools.Tasks;

[ExecuteInEditMode]
public class ThreadManagerMonobeh : MonoBehaviour
{
	private static ManualResetEvent clusterMre = new ManualResetEvent(true);
	private static AutoResetEvent clusterAre = new AutoResetEvent(true);
	private static Mutex clusterMutex = new Mutex();
	private static System.Random rand = new System.Random();
	public int seed = 12345;

	public bool testOne;
	public bool clusterGenerated = false;
	private bool isClusterGenerating = false;
	public int numThreads = 5;
	public int maxThreads = 2;
	public int minVal = 100000;
	public int maxVal = 400000;

	public bool testMultiple;
	public int multipleIterations = 100;
	public int threadsStarted = 0;
	public int threadsFinished = 0;
	public int clustersStarted = 0;
	public int clustersFinished = 0;
	public int threadsUsed = 0;
	public int threadsQueued = 0;
	public int clusterCurrentRev = 0;
	public int clusterShouldBeRev = 0;

	public bool testPausing;
	public int threadsActiveNow = 0;
	public int threadsQueuedNow  = 0;
	public int threadsPausedNow = 0;

	public void OnDrawGizmos ()
	{
		threadsActiveNow = ThreadManager.ActiveCount;
		threadsQueuedNow = ThreadManager.QueueCount;
		//threadsPausedNow = ThreadManager.Count;

		if (testOne)
		{
			testOne = false;
			TestOne();
		}

		if (testMultiple)
		{
			testMultiple = false;
			TestMultiple();
		}

		if (testPausing)
		{
			testPausing = false;
			TestPausing();
		}
	}

	private void TestPrepare ()
	{
		rand = new System.Random(seed);

		threadsStarted = 0;
		threadsFinished = 0;
		clustersStarted = 0;
		clustersFinished = 0;
		threadsUsed = 0;
		threadsQueued = 0;

		Den.Tools.Log.Clear();
		Den.Tools.Log.enabled = true;

		clusterGenerated = false;
		isClusterGenerating = false;

		ThreadManager.Abort();
		ThreadManager.autoMaxThreads = false;
		ThreadManager.maxThreads = maxThreads;
	}

	public void TestOne ()
	{
		TestPrepare();

		Log("Start!", "Main");

		// Create and start five numbered threads. 
		for (int i = 0; i < numThreads; i++)
		{
			ThreadManager.Enqueue(FibonacciThread, i, i.ToString());

			//Thread t = new Thread(new ParameterizedThreadStart(FibonacciThread));
			//t.Start(i); // Start the thread, passing the number.
		}
	}

	public void TestMultiple ()
	{
		TestPrepare();

		System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
		timer.Start();
		int totalSleepTime = 0;

		for (int i=0; i<multipleIterations; i++)
		{
			Log("Iteration Start", "Main");

			if (clusterGenerated && isClusterGenerating)
				Debug.LogError("Cluster generating while it's generated"); 

			if (i==0 || (clusterGenerated && rand.Next(0,5)==0))
			{
				clusterGenerated = false;
				clustersStarted++;
				clusterShouldBeRev = Math.Max(clusterCurrentRev++, clusterShouldBeRev++); //revision is only increased
			}

			
			ThreadManager.Enqueue(FibonacciThread, i, i.ToString());
			threadsStarted++;

			if (i>multipleIterations/2)
			{
				if (ThreadManager.ActiveCount > threadsUsed)
					threadsUsed = ThreadManager.ActiveCount;
				if (ThreadManager.QueueCount > threadsQueued)
					threadsQueued = ThreadManager.QueueCount;
			}

			double sleepTimeF = rand.NextDouble()  * 2;
			int sleepTime = (int)(sleepTimeF*sleepTimeF*sleepTimeF);
			totalSleepTime += sleepTime;

			Log("Waiting", "Main", ("Sleep",sleepTime));
			Thread.Sleep(sleepTime);

		}

		Thread.Sleep(100);
		totalSleepTime += 100;

		//to generate cluster last time
		Log("Last Iteration", "Main");
		ThreadManager.Enqueue(FibonacciThread, 0, "Last");
		threadsStarted++;

		Thread.Sleep(100);
		totalSleepTime += 100;
		timer.Stop();

		if (threadsStarted != threadsFinished)
			Debug.LogError("Multitest finished. Threads num mismatch");
		else if (clustersStarted != clustersFinished)
			Debug.LogError("Multitest finished. Clusters num mismatch");
		else if (ThreadManager.QueueCount != 0)
			Debug.LogError("Multitest finished. Threadmanager queue not empty");
		else
			Debug.Log("Multitest finished/nTotal non-sleep time: " + (timer.ElapsedMilliseconds - totalSleepTime)/1000f + " s");
	}

	public void TestPausing ()
	{
		TestPrepare();

		for (int i = 0; i < numThreads; i++)
		{
			ThreadManager.Enqueue(FibonacciThreadWithPreparation, i, i.ToString());
		}
	}


	private void FibonacciThread () => FibonacciThread("null");
	private void FibonacciThread (object threadContext)
	{
		string num = threadContext.ToString();
		Log("Starting", num);

		// Calculating cluster in this thread
		/*{
			Monitor.Enter(clusterMre);

			try
			{
				if (!clusterGenerated)
				{
					clusterMre.WaitOne();
					Log("Entered Wait", num);

					if (!clusterGenerated  &&  !isClusterGenerating)  
					{
						clusterMre.Reset();
						isClusterGenerating = true;
						Monitor.Exit(clusterMre);

						ClusterThread(threadContext);
					}
					else
						Monitor.Exit(clusterMre);
				}
				else
					Monitor.Exit(clusterMre);
					
			}

			catch
			{
				Monitor.Exit(clusterMre);
			}
		}*/


		// Starting cluster in separate thread
		if (!clusterGenerated)
		{
			Monitor.Enter(clusterMre);

			try
			{
				if (!clusterGenerated)
				{
					if (!isClusterGenerating)  
					{
						clusterMre.Reset();
						isClusterGenerating = true;

						Log("Starting cluster thread", num);
						Thread t = new Thread(new ParameterizedThreadStart(ClusterThread));
						t.Start(threadContext);
					}

					Monitor.Exit(clusterMre);
					//it should not be locked to the moment of pausing

					Thread.Sleep(rand.Next(0,100));

					Log("Pausing...", num );
					clusterMre.WaitOne(); // Wait until the ManualResetEvent is signaled
					Log("Resuming...", num);
				}

				else
					Monitor.Exit(clusterMre);
			}

			catch
			{
				Monitor.Exit(clusterMre);
			}
		}


		if (clusterCurrentRev < clusterShouldBeRev)
			LogError("Cluster id mismatch", num);

		Log("Starting Fibonacci...", num);
		int rnd = rand.Next(minVal, maxVal);
		Fibonacci(rnd);
		threadsFinished++;
		Log("Finished Fibonacci", num, ("Iter",rnd));
	}


	private void FibonacciThreadWithPreparation () => FibonacciThreadWithPreparation("null");
	private void FibonacciThreadWithPreparation (object threadContext)
	{
		string num = threadContext.ToString();
		Log("Preparing Fibonacci...", num);
		int rnd = rand.Next(minVal/4, maxVal/4);
		Fibonacci(rnd);
		FibonacciThread(num);
	}


	private void ClusterThread (object threadContext)
	{
		string num = (threadContext).ToString();

		Log("Starting Cluster...", "CL "+num);
		int rnd = rand.Next(minVal*2, maxVal*2);
		Fibonacci(rnd);
		Log("Finished Cluster", "CL "+num, ("Iter",rnd));

		clusterGenerated = true;
		isClusterGenerating = false;
		clustersFinished++;
		clusterCurrentRev = clusterShouldBeRev;
		Log("Resuming paused", "CL "+num);

		clusterMre.Set();
		clusterAre.Set();
	}

	private void Log (string caption, string num, params (string,object)[] optional)
	{
		Den.Tools.Log.AddThreaded(caption, num, 
			("Cl Generated",clusterGenerated), 
			("Is Generating",isClusterGenerating), 
			("Cl Id",clusterCurrentRev),
			("Cl Exp Id",clusterShouldBeRev) );
	}

	private void Log (string caption, string num, (string,object) optional)
	{
		Den.Tools.Log.AddThreaded(caption, num, 
			optional,
			("Cl Generated",clusterGenerated), 
			("Is Generating",isClusterGenerating), 
			("Cl Id",clusterCurrentRev),
			("Cl Exp Id",clusterShouldBeRev));
	}

	private void LogError (string caption, string num)
	{
		Den.Tools.Log.AddThreaded(caption, num, Color.red,
			("Cl Generated",clusterGenerated), 
			("Is Generating",isClusterGenerating), 
			("Cl Id",clusterCurrentRev),
			("Cl Exp Id",clusterShouldBeRev) );
		Debug.LogError(caption + ", thread " + num);
	}

	private static int Fibonacci (int n)
	{
		int prev = 1;
		int prevPrev = 1;
		int result = 0;

		for (int i=0; i<n; i++)
		{
			result = prev + prevPrev;

			prevPrev = prev;
			prev = result;
		}

		return result;
	}
}
