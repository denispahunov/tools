using Den.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.XR;

[ExecuteInEditMode]
public class ManualResetEventMonobeh : MonoBehaviour
{
	private static ManualResetEvent clusterMre = new ManualResetEvent(false);
	private static System.Random rand = new System.Random();

	public bool clusterGenerated = false;
	private bool isClusterGenerating = false;
	public int numThreads = 5;

	public void OnEnable ()
	{
		Log.AddThreaded("Start!", "Main");

		// Create and start five numbered threads. 
		for (int i = 0; i < numThreads; i++)
		{
			Thread t = new Thread(new ParameterizedThreadStart(FibonacciThread));
			t.Start(i); // Start the thread, passing the number.
		}
	}

	private void FibonacciThread (object threadContext)
	{
		string num = ((int)threadContext).ToString();
		Log.AddThreaded("Enqueued", num);

		// Pausing on cluster
		//lock (clusterMre)
		{
			if (!clusterGenerated && !isClusterGenerating)
			{
				isClusterGenerating = true;
				clusterMre.Reset();

				Log.AddThreaded("Starting cluster", num);
				Thread t = new Thread(new ParameterizedThreadStart(ClusterThread));
				t.Start(num);
				
			}
		}

		Log.AddThreaded("Pausing...", num);
		clusterMre.WaitOne();
		Log.AddThreaded("Resuming...", num);

		Log.AddThreaded("Starting calculation...", num);
		Fibonacci(rand.Next(2000, 4000));
		Log.AddThreaded("Finished calculation", num);
	}

	private void ClusterThread (object threadContext)
	{
		string num = (threadContext).ToString();

		Log.AddThreaded("Starting calculation...", "CL "+num);
		Fibonacci(rand.Next(5000, 6000));
		Log.AddThreaded("Finished calculation", "CL "+num);

		Log.AddThreaded("Resuming paused", "CL "+num);
		clusterGenerated = true;
		isClusterGenerating = false;
		clusterMre.Set();
	}

	private static int Fibonacci (int n)
	{
		if (n <= 1)
			return n;
		return Fibonacci(n - 1);// + Fibonacci(n - 2);
	}
}
