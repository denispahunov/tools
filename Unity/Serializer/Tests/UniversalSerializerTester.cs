using UnityEngine;
using System.Collections;
using System;
using System.IO;

using Den.Tools.Serialization;

namespace Den.Tools.Tests
{
    [Serializable]
    [CreateAssetMenu(menuName = "MapMagic/Tests/Serializer Tester", fileName = "SerializerTester.asset", order = 113)]
    public class SerializerTester : ScriptableObject
    {
        public class SomeClass
        {
            public string someString;
        }

        public class ChildClass : SomeClass
        {
            public int otherInt;
        }

        public SomeClass c;


        public void Test ()
        {
            Debug.Log("Test");
        }

    }
}