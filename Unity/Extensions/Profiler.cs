using System.Reflection;
using UnityEditor;
using Unity.Profiling;
using static Unity.Profiling.ProfilerMarker;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System;
//using Unity.Collections.LowLevel.Unsafe;
using Unity.Profiling.LowLevel.Unsafe;
using UnityEngine.Profiling;

namespace Den.Tools
{
	static public class ProfilerExt
	{
		//private static ProfilerMarker marker = new ProfilerMarker("Test");

		//[Conditional("MM_DEBUG")]
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Scope Profile (string msg) => new Scope(msg);

		[IgnoredByDeepProfiler]
		public struct Scope : IDisposable
		{
			//private ProfilerMarker marker = new ProfilerMarker("Test"); ??

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			internal Scope (string msg)
			{
				#if MM_DEBUG
				Profiler.BeginSample(msg);
				#endif
			}

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public void Dispose ()
			{
				#if MM_DEBUG
				Profiler.EndSample();
				#endif
			}
		}
	}
}