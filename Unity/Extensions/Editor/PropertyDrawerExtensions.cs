using System.Reflection;
using UnityEditor;

namespace Den.Tools
{
	static public class PropertyDrawerExtensions
	{

		// Helper method to get the target object of a SerializedProperty
		// in case target is not unity object (asset or monobeh)
		// like: //ITest test = (ITest)property.serializedObject; //InvalidCastException: Specified cast is not valid.
		// source: https://discussions.unity.com/t/get-a-general-object-value-from-serializedproperty/581352/2
		public static object GetTargetObjectOfProperty(this SerializedProperty prop) 
		{
			string path = prop.propertyPath.Replace(".Array.data[", "[");
			object obj = prop.serializedObject.targetObject;
			string[] elements = path.Split('.');

			foreach (string element in elements)
			{
				if (element.Contains("["))
				{
					string elementName = element.Substring(0, element.IndexOf("["));
					int index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
					obj = GetValue(obj, elementName, index);
				}
				else
				{
					obj = GetValue(obj, element);
				}
			}
			return obj;
		}


		private static object GetValue(object source, string name)
		{
			if (source == null)
				return null;
			var type = source.GetType();
			var field = type.GetField(name);
			if (field == null)
				return null;
			return field.GetValue(source);
		}

		private static object GetValue(object source, string name, int index)
		{
			var enumerable = GetValue(source, name) as System.Collections.IEnumerable;
			if (enumerable == null) return null;
			var enm = enumerable.GetEnumerator();
			// Iterate to the index or the end of the list, whichever comes first
			for (int i = 0; i <= index && enm.MoveNext(); i++)
			{
				if (i == index)
					return enm.Current;
			}
			return null;
		}
	}
}