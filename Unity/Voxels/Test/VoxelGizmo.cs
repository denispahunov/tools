﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

using Den.Tools.GUI;

namespace Den.Tools.Voxels
{
	public class VoxelGizmo
	{
		public Matrix3D<bool> voxels = new Matrix3D<bool>(1,1,1, new bool[] { true });

		public Vector3[] verts;
		public Face[] faces;

		public Action<VoxelGizmo> customCreateMesh;

		public Mesh mesh;
		public Color color = Color.white;
		public Material mat;
		public Texture2D texture;
		private static Texture2D whiteTex;


		public void Reset (int extent)
		{
			
		}


		public void CreateMesh ()
		{
			if (customCreateMesh != null)
				{ customCreateMesh(this); return; }

			(verts, faces) = VoxelMeshOps.CreateVertsFaces(voxels);
			List<(int,int)> weldLinks = VoxelMeshOps.CreateWeldLinks(faces);
			VoxelMeshOps.JoinWeldLinks(ref verts, faces, weldLinks);

			VoxelMeshOps.Relax(verts, faces, 2, 0.5f);

			//int[] tris = VoxelMeshOps.MakeTris(faces);
			int[] quads = VoxelMeshOps.MakeQuads(faces);

			SetMesh(verts, quads, useQuads:true);
		}


		public void SetMesh (Vector3[] verts, int[] tris, bool useQuads=false) => SetMesh(verts, null, null, tris, useQuads);
		public void SetMesh (Vector3[] verts, Vector2[] uvs, int[] tris, bool useQuads=false) => SetMesh(verts, null, uvs, tris, useQuads);
		public void SetMesh (Vector3[] verts, Vector3[] normals, int[] tris, bool useQuads=false) => SetMesh(verts, normals, null, tris, useQuads);
		public void SetMesh (Vector3[] verts, Vector3[] normals, Vector2[] uvs, int[] tris, bool useQuads=false)
		{
			if (mesh == null) { mesh = new Mesh(); mesh.MarkDynamic(); }
			if (verts.Length < mesh.vertices.Length) mesh.triangles = new int[0]; //otherwise "The supplied vertex array has less vertices than are referenced by the triangles array."

			mesh.vertices = verts;

			if (useQuads) mesh.SetIndices(tris, MeshTopology.Quads, 0);
			else mesh.triangles = tris;

			if (uvs!=null) mesh.uv = uvs;

			if (normals==null) mesh.RecalculateNormals();
			else mesh.normals = normals;

			mesh.RecalculateBounds();
		}


		public void Draw ()
		{
			if (mesh == null) 
				CreateMesh();
		
			if (mat == null) mat = new Material( Shader.Find("Standard") ); 

			if (texture==null) 
			{
				if (whiteTex==null) whiteTex = TextureExtensions.ColorTexture(4,4,Color.white);
				texture = whiteTex;
			}	

			mat.SetColor("_Color", color);
			mat.SetTexture("_MainTex", texture);

			mat.SetPass(0);
			Graphics.DrawMeshNow(mesh, Matrix4x4.identity); // parent==null ? Matrix4x4.identity : parent.localToWorldMatrix);
		}


		public void Edit ()
		{
			DebugGizmos.Clear("VoxelGizmoHighlight");

			#if UNITY_EDITOR
			UnityEditor.SceneView sceneView = UnityEditor.SceneView.lastActiveSceneView;
			if (sceneView != null && sceneView.camera != null)
			{
				//disabling selection
				UnityEditor.HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

				Vector2 mousePos = Event.current.mousePosition;
				mousePos.y = sceneView.camera.pixelHeight - mousePos.y;
				Ray ray = sceneView.camera.ScreenPointToRay(mousePos); //ViewportPointToRay( new Vector3(0.5f, 0.5f, 0.5f) );

				int f = VoxelMeshOps.IntersectedFace(verts, faces, ray);
				
				if (f >= 0)
					DebugGizmos.DrawMultipleLines("VoxelGizmoHighlight", VoxelMeshOps.Wireframe(verts,faces[f]) );
				UnityEditor.SceneView.lastActiveSceneView.Repaint();

				if (f>=0 && Event.current.type == EventType.MouseDown && Event.current.button==0 && !Event.current.alt)
				{
					Coord3D coord = faces[f].coord;
					Dir dir = faces[f].dir;
					Coord3D nCoord = coord+dir.Coord;

					CoordCube newCube = voxels.cube;
					newCube.Encapsulate( new CoordDir(nCoord.x, nCoord.y, nCoord.z) );
					if (newCube != voxels.cube)
					{
						Matrix3D<bool> newVoxels = new Matrix3D<bool>(newCube);
						Matrix3D<bool>.CopyData(voxels, newVoxels);
						voxels = newVoxels;
					}

					voxels[nCoord.x, nCoord.y, nCoord.z] = true;

					CreateMesh();
				}
			}
			#endif
		}
	}

}
