﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

using Den.Tools;
using Den.Tools.Tests;
using Den.Tools.Matrices;
using Den.Tools.GUI;
using Den.Tools.Segs;


namespace Den.Tools.Voxels.Tests
{

	[Test(category="Voxels")]
	public class DrawGizmo : ITest, ISceneGUI
	/// Just tests gizmo drawing
	{
		[Val("Color")] public Color color;

		Matrix3D<bool> matrix;
		VoxelMeshGizmo gizmo = new VoxelMeshGizmo();

		public void OnSceneGUI ()
		{
			if (gizmo==null) return;
			gizmo.Draw(color);
		}

		public void Test ()
		{
			matrix = new Matrix3D<bool>(3,3,3);

			matrix[0,0,0] = true;
			matrix[0,1,0] = true;
			matrix[1,0,0] = true;
			matrix[2,2,2] = true;

			gizmo.SetVoxels(matrix);
		}
	}


	[Test(category="Voxels")]
	public class DrawFaceMesh : ITest, ISceneGUI
	{
		[Val("Color")] public Color color;

		Matrix3D<bool> matrix;
		MeshGizmo gizmo = new MeshGizmo();

		public void OnSceneGUI ()
		{
			gizmo.Draw(color);
		}

		public void Test ()
		{
			matrix = new Matrix3D<bool>(3,3,3);

			matrix[0,0,0] = true;
			matrix[0,1,0] = true;
			matrix[1,0,0] = true;
			matrix[2,2,2] = true;

			(Vector3[] verts, Vector2[] uvs, int[] tris) = VoxelMeshOps.GenerateMesh(matrix);

			gizmo.SetMesh(verts, uvs, tris);
		}
	}


	[Test(category="Voxels")]
	public class DirTest : ITest
	{
		[Val("Src")] public Coord3D src;
		[Val("Dir")] public Dir dir;
		[Val("Dst")] public Coord3D dst;

		public void Test ()
		{
			dir = new Dir(src.x, src.y, src.z);
			dst = dir.Coord;
		}
	}


	[Test(category="Voxels")]
	public class WeldTest : ITest, ISceneGUI
	{
		[Val("Color")] public Color color;
		[Val("Offset")] public float offset;

		Matrix3D<bool> matrix;
		MeshGizmo gizmo = new MeshGizmo();

		public void OnSceneGUI ()
		{
			gizmo.Draw(color, "VoxelCorners");
		}

		public void Test ()
		{
			matrix = new Matrix3D<bool>(32,10,32);

			matrix[0,0,0] = true;
			matrix[0,1,0] = true;
			matrix[1,0,0] = true;
			matrix[2,2,2] = true;

			matrix[13,1,3] = true; matrix[14,1,3] = true; matrix[15,1,3] = true;
			matrix[13,1,4] = true; matrix[14,1,4] = true; matrix[15,1,4] = true;
			matrix[13,1,5] = true; matrix[14,1,5] = true; matrix[15,1,5] = true;
			matrix[13,1,2] = true; matrix[14,1,2] = true; matrix[15,1,2] = true;
			matrix[13,1,1] = true; matrix[14,1,1] = true; matrix[15,1,1] = true;

			matrix[14,2,2] = true; 
			matrix[14,0,2] = true;


			matrix[3,3,1] = true; matrix[4,3,1] = true; matrix[5,3,1] = true;
			matrix[3,4,1] = true; matrix[4,4,1] = true; matrix[5,4,1] = true;
			matrix[3,5,1] = true; matrix[4,5,1] = true; matrix[5,5,1] = true;
			matrix[3,2,1] = true; matrix[4,2,1] = true; matrix[5,2,1] = true;
			matrix[3,1,1] = true; matrix[4,1,1] = true; matrix[5,1,1] = true;
		
			matrix[4,2,2] = true; 
			matrix[4,2,0] = true;

			matrix[20,3,3] = true; matrix[20,4,3] = true; matrix[20,5,3] = true;
			matrix[20,3,4] = true; matrix[20,4,4] = true; matrix[20,5,4] = true;
			matrix[20,3,5] = true; matrix[20,4,5] = true; matrix[20,5,5] = true;
			matrix[20,3,2] = true; matrix[20,4,2] = true; matrix[20,5,2] = true;
			matrix[20,3,1] = true; matrix[20,4,1] = true; matrix[20,5,1] = true;

			matrix[21,4,2] = true; 
			matrix[19,4,2] = true;

			(Vector3[] verts, Vector2[] uvs, int[] tris) = VoxelMeshOps.GenerateMesh(matrix);

			gizmo.SetMesh(verts, uvs, tris);
			gizmo.Randomize(12345, offset);
		}
	}


	[Test(category="Voxels")]
	public class RandomWeldTest : ITest, ISceneGUI, IBenchmark
	{
		[Val("Color")] public Color color = Color.white;
		[Val("Offset")] public float offset;
		[Val("Size")] public int size = 3;
		[Val("Density")] public float density = 0.5f;
		[Val("Seed")] public int seed = 12345;
		[Val("Next")] public bool next;

		[Val("Num verts")] public int numVerts;
		[Val("Num faces")] public int numFaces;

		Matrix3D<bool> matrix;
		MeshGizmo gizmo = new MeshGizmo();

		public void OnSceneGUI ()
		{
			gizmo.Draw(color, "VoxelCorners");
		}

		public void Test ()
		{
			if (next) { seed++; next=false; }

			matrix = new Matrix3D<bool>(size,size,size);
			UnityEngine.Random.InitState(seed);
			for (int i=0; i<matrix.array.Length; i++)
				matrix.array[i] = UnityEngine.Random.value < density;

			(Vector3[] verts, Vector2[] uvs, int[] tris) = VoxelMeshOps.GenerateMesh(matrix);

			gizmo.SetMesh(verts, uvs, tris);
			gizmo.Randomize(12345, offset);

			numVerts = gizmo.mesh.vertexCount;
			numFaces = gizmo.mesh.triangles.Length / 4; //will go for test
		}

		public void Benchmark (int iterations, Stopwatch timer) 
		{
			matrix = new Matrix3D<bool>(size,size,size);
			UnityEngine.Random.InitState(seed);
			for (int i=0; i<matrix.array.Length; i++)
				matrix.array[i] = UnityEngine.Random.value < density;
			
			for (int i=0; i<iterations; i++)
			{
				(Vector3[] verts, Face[] faces) = VoxelMeshOps.CreateVertsFaces(matrix);
				List<(int,int)> weldLinks = VoxelMeshOps.CreateWeldLinks(faces);
				timer.Start();
				VoxelMeshOps.JoinWeldLinks(ref verts, faces, weldLinks);
				timer.Stop();
			}
		}
	}


	[Test(category="Voxels")]
	public class NormalsTest : ITest, ISceneGUI, IBenchmark
	{
		[Val("Color")] public Color color = Color.white;
		[Val("Offset")] public float offset;
		[Val("Size")] public int size = 3;
		[Val("Density")] public float density = 0.5f;
		[Val("Seed")] public int seed = 12345;
		[Val("Next")] public bool next;

		Matrix3D<bool> matrix;
		MeshGizmo gizmo = new MeshGizmo();

		public void OnSceneGUI ()
		{
			gizmo.Draw(color, "VoxelCorners");
		}

		public void Test ()
		{
			if (next) { seed++; next=false; }

			matrix = new Matrix3D<bool>(size,size,size);
			UnityEngine.Random.InitState(seed);
			for (int i=0; i<matrix.array.Length; i++)
				matrix.array[i] = UnityEngine.Random.value < density;

			{
				(Vector3[] verts, Vector2[] uvs, int[] tris) = VoxelMeshOps.GenerateMesh(matrix);
				gizmo.SetMesh(verts, uvs, tris);
				gizmo.Randomize(12345, offset);
			}

			{
				(Vector3[] verts, Face[] faces) = VoxelMeshOps.CreateVertsFaces(matrix);
				Vector3[] faceNormals = new Vector3[faces.Length];
				VoxelMeshOps.FaceNormalsByVerts(verts, faceNormals, faces);
			}
		}

		public void Benchmark (int iterations, Stopwatch timer) 
		{

		}
	}


	[Test(category="Voxels")]
	public class RelaxTest : ITest, ISceneGUI, IBenchmark
	{
		[Val("Color")] public Color color = Color.white;
		[Val("Size")] public int size = 3;
		[Val("Density")] public float density = 0.5f;
		[Val("Seed")] public int seed = 12345;
		[Val("Next")] public bool next;

		[Val("Iterations")] public int iterations=2;
		[Val("Amount")] public float amount=0.5f;

		Matrix3D<bool> matrix;
		MeshGizmo gizmo = new MeshGizmo();
		Vector3[] verts;
		Face[] faces;

		public void OnSceneGUI ()
		{
			gizmo.Draw(color, "VoxelCorners");

			if (verts!=null && faces!=null)
				DebugGizmos.DrawMultipleLines("Wireframe", VoxelMeshOps.Wireframe(verts,faces) );
		}

		public void Test ()
		{
			if (next) { seed++; next=false; }

			matrix = new Matrix3D<bool>(size,size,size);
			UnityEngine.Random.InitState(seed);
			for (int i=0; i<matrix.array.Length; i++)
				matrix.array[i] = UnityEngine.Random.value < density;

			(verts, faces) = VoxelMeshOps.CreateVertsFaces(matrix);
			List<(int,int)> weldLinks = VoxelMeshOps.CreateWeldLinks(faces);
			VoxelMeshOps.JoinWeldLinks(ref verts, faces, weldLinks);

			VoxelMeshOps.Relax(verts, faces, iterations, amount);

			int[] tris = VoxelMeshOps.MakeTris(faces);
			gizmo.SetMesh(verts, tris);
		}

		public void Benchmark (int iterations, Stopwatch timer) 
		{

		}
	}


	[Test(category="Voxels")]
	public class CollisionTest : ITest, ISceneGUI, IBenchmark
	{
		[Val("Color")] public Color color = Color.white;
		[Val("Size")] public int size = 3;
		[Val("Density")] public float density = 0.5f;
		[Val("Seed")] public int seed = 12345;
		[Val("Next")] public bool next;

		[Val("Iterations")] public int iterations=2;
		[Val("Amount")] public float amount=0.5f;

		Matrix3D<bool> matrix;
		MeshGizmo gizmo = new MeshGizmo();
		Vector3[] verts;
		Face[] faces;

		public void OnSceneGUI ()
		{
			gizmo.Draw(color, "VoxelCorners");

			#if UNITY_EDITOR
			UnityEditor.SceneView sceneView = UnityEditor.SceneView.lastActiveSceneView;
			if (sceneView != null)
			{
				Vector2 mousePos = Event.current.mousePosition;
				mousePos.y = sceneView.camera.pixelHeight - mousePos.y;
				Ray ray = sceneView.camera.ScreenPointToRay(mousePos); //ViewportPointToRay( new Vector3(0.5f, 0.5f, 0.5f) );
				int f = VoxelMeshOps.IntersectedFace(verts, faces, ray); 
				if (f >= 0)
					DebugGizmos.DrawMultipleLines("Wireframe", VoxelMeshOps.Wireframe(verts,faces[f]) );
				UnityEditor.SceneView.lastActiveSceneView.Repaint();
			}
			#endif
		}

		public void Test ()
		{
			if (next) { seed++; next=false; }

			matrix = new Matrix3D<bool>(size,size,size);
			UnityEngine.Random.InitState(seed);
			for (int i=0; i<matrix.array.Length; i++)
				matrix.array[i] = UnityEngine.Random.value < density;

			(verts, faces) = VoxelMeshOps.CreateVertsFaces(matrix);
			List<(int,int)> weldLinks = VoxelMeshOps.CreateWeldLinks(faces);
			VoxelMeshOps.JoinWeldLinks(ref verts, faces, weldLinks);

			VoxelMeshOps.Relax(verts, faces, iterations, amount);
			//DebugGizmos.DrawMultipleLines("Wireframe", VoxelMeshOps.Wireframe(verts,faces) );

			int[] tris = VoxelMeshOps.MakeTris(faces);
			gizmo.SetMesh(verts, tris);

			
		}

		public void Benchmark (int iterations, Stopwatch timer) 
		{

		}
	}


	[Test(category="Voxels")]
	public class VoxelGizmoTest : ITest, ISceneGUI, IBenchmark
	{
		public VoxelGizmo gizmo = new VoxelGizmo();

		[Val("Reset")] public bool reset = false;

		public void OnSceneGUI ()
		{
			gizmo.Draw();
			gizmo.Edit();
		}

		public void Test ()
		{
			if (reset)
			{
				reset = false;
				gizmo = new VoxelGizmo();
			}
		}

		public void Benchmark (int iterations, Stopwatch timer) 
		{

		}
	}
}