﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools;
using Den.Tools.Tests;
using Den.Tools.Matrices;
using Den.Tools.GUI;
using Den.Tools.Segs;


namespace Den.Tools.Segs.Tests.Beizer
{
	[Test(category="Beizer")]
	public class BeizerPositionsTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Count")] public int count;

		public void OnSceneGUI ()
		{
			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			foreach (Spline spline in splineObj.splines)
				for (int n=0; n<spline.nodes.Length-1; n++)
					foreach (Vector3D pos in spline.BeizerPositions(n,count))
					{
						DebugGizmos.DrawDot("SplineTest", pos.Vector3(), 3, Color.red, additive:true);
					}

			
			
		}

		public void Test ()
		{
			if (splineObj==null) return;
			foreach (Spline spline in splineObj.splines)
				for (int n=0; n<spline.nodes.Length-1; n++)
					foreach (Vector3D pos in spline.BeizerPositions(n,count))
					{
						continue;
					}
		}
	}


	[Test(category="Beizer")]
	public class UniformPositionsTest : ITest, ISceneGUI
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Count")] public int count;

		private Vector3D[][][] uniformPoses;

		public void OnSceneGUI ()
		{
			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			for (int s=0; s<uniformPoses.Length; s++)
				for (int n=0; n<uniformPoses[s].Length; n++)
					for (int i=0; i<uniformPoses[s][n].Length; i++)
						DebugGizmos.DrawDot("SplineTest", uniformPoses[s][n][i].Vector3(), 3, Color.red, additive:true);
		}

		public void Test ()
		{
			if (splineObj==null) return;

			if (uniformPoses == null || uniformPoses.Length != splineObj.splines.Length)
				uniformPoses = new Vector3D[splineObj.splines.Length][][];
			
			for (int s=0; s<uniformPoses.Length; s++)
			{
				if (uniformPoses[s]==null || uniformPoses[s].Length != splineObj.splines[s].nodes.Length-1)
					uniformPoses[s] = new Vector3D[splineObj.splines[s].nodes.Length-1][];

				for (int n=0; n<uniformPoses[s].Length; n++)
					if (uniformPoses[s][n]==null || uniformPoses[s][n].Length != count)
						uniformPoses[s][n] = new Vector3D[count];
			}

			for (int s=0; s<uniformPoses.Length; s++)
			{
				Spline spline = splineObj.splines[s];

				for (int n=0; n<spline.nodes.Length-1; n++)
				{
					int i=0;
					foreach (Vector3D pos in BeizerOps.UniformPositions(spline, n, count))
					//foreach (Vector3 pos in BeizerOPs.BeizerPositions(spline, n, count))
					{
						uniformPoses[s][n][i] = pos;
						i++;
					}
				}
			}
		}
	}


	[Test(category="Beizer")]
	public class GetAllPointsTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Res per Unit")] public float res = 0.1f;
		[Val("Min Segs")] public int minSegs = 5;
		[Val("Max Segs")] public int maxSegs = 20;

		private Vector3D[][] points;

		public void OnSceneGUI ()
		{
			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			for (int s=0; s<points.Length; s++)
				for (int n=0; n<points[s].Length; n++)
					DebugGizmos.DrawDot("SplineTest", points[s][n].Vector3(), 3, Color.red, additive:true);
		}

		public void Test ()
		{
			if (splineObj==null) return;

			if (points == null || points.Length != splineObj.splines.Length)
				points = new Vector3D[splineObj.splines.Length][];
			
			for (int s=0; s<points.Length; s++)
				points[s] = BeizerOps.GetAllPoints(splineObj.splines[s], res, minSegs, maxSegs); 
		}
	}


	[Test(category="Beizer")]
	public class SubdivideTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Count")] public int count = 5;

		private Spline[] subdivided;

		public void OnSceneGUI ()
		{
			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			for (int s=0; s<subdivided.Length; s++)
			{
				DebugGizmos.DrawPolyLine("SplineTest",  Array.ConvertAll(subdivided[s].nodes, n=>n.Vector3()), Color.red, additive:true);

				for (int n=0; n<subdivided[s].nodes.Length; n++)
					DebugGizmos.DrawDot("SplineTest", subdivided[s].nodes[n].Vector3(), 2, Color.red, additive:true);
			}
		}

		public void Test ()
		{
			if (splineObj==null) return;

			if (subdivided == null || subdivided.Length != splineObj.splines.Length)
				subdivided = new Spline[splineObj.splines.Length];
			
			for (int s=0; s<splineObj.splines.Length; s++)
			{
				subdivided[s] = new Spline(splineObj.splines[s]);
				BeizerOps.Subdivide(subdivided[s], count); 
			}
		}
	}


	[Test(category="Beizer")]
	public class OptimizeTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Deviation")] public float deviation = 5;

		private Spline[] optimized;

		public void OnSceneGUI ()
		{
			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			for (int s=0; s<optimized.Length; s++)
			{
				//DebugGizmos.DrawPolyLine("SplineTest", BeizerOPs.GetAllPoints(splineObj.splines[s]), Color.red, additive:true);

				DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(BeizerOps.GetAllPoints(optimized[s]), n=>n.Vector3()), Color.green, additive:true);

				for (int n=0; n<optimized[s].nodes.Length; n++)
					DebugGizmos.DrawDot("SplineTest", optimized[s].nodes[n].Vector3(), 4, Color.green, additive:true);
			}
		}

		public void Test ()
		{
			if (splineObj==null) return;

			if (optimized == null || optimized.Length != splineObj.splines.Length)
				optimized = new Spline[splineObj.splines.Length];
			
			for (int s=0; s<splineObj.splines.Length; s++)
			{
				optimized[s] = new Spline(splineObj.splines[s]);
				BeizerOps.Optimize(optimized[s], deviation); 
			}
		}
	}
}