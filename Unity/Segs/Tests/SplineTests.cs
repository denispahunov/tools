﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools;
using Den.Tools.Tests;
using Den.Tools.Matrices;
//using Den.Tools.Splines;
using Den.Tools.GUI;
using Den.Tools.Segs;


namespace Den.Tools.Segs.Tests
{
	[Test(category="Segs")]
	public class ClosestToPointTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Point")] public Vector3 point;

		int cn; float cp; float cDist;

		public void OnSceneGUI ()
		{
			Test();

			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			point = UnityEditor.Handles.PositionHandle(point, Quaternion.identity);
			DebugGizmos.DrawDot("SplineTest", point, 10, Color.green, additive: true);

			Vector3 pos = splineObj.splines[0].GetPoint(cn, cp).Vector3();
			DebugGizmos.DrawLine("SplineTest", point, pos, Color.red, additive: true);
		}

		public void Test ()
		{
			if (splineObj==null) return;
			(cn, cp, cDist) = splineObj.splines[0].ClosestToPoint(point.Vector3D());
		}
	}


	[Test(category="Segs")]
	public class ClosestToLineTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Start")] public Vector3 start;
		[Val("End")] public Vector3 end;

		int cn; float cp; float cDist;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			start = UnityEditor.Handles.PositionHandle(start, Quaternion.identity);
			end = UnityEditor.Handles.PositionHandle(end, Quaternion.identity);
			DebugGizmos.DrawDot("SplineTest", start, 10, Color.green, additive: true);
			DebugGizmos.DrawDot("SplineTest", end, 10, Color.green, additive: true);
			DebugGizmos.DrawLine("SplineTest", start, (end-start).normalized*1000 + start, Color.green, additive: true);

			if (splineObj==null) return;
			Test();

			Vector3 pos = splineObj.splines[0].GetPoint(cn, cp).Vector3();
			DebugGizmos.DrawDot("SplineTest", pos, 10, Color.red, additive: true);
		}

		public void Test ()
		{
			if (splineObj==null) return;
			(cn, cp, cDist) = splineObj.splines[0].ClosestToRay(start.Vector3D(), (end-start).normalized.Vector3D());
		}
	}


	[Test(category="Segs")]
	public class CutTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Coordinate")] public float val = 0;
		[Val("Axis")] public Spline.CutAxis axis;
		[Val("Side")] public Spline.CutSide side;
		[Val("Remove")] public bool remove = false;

		private List<Spline> cut;

		public void OnSceneGUI ()
		{
			if (splineObj==null) return;

			DebugGizmos.Clear("SplineTest");

			if (cut == null) cut = new List<Spline>();
			for (int s = 0; s<cut.Count; s++)
			{
				DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(cut[s].nodes, n=>n.Vector3()), Color.green, additive: true);

				for (int n = 0; n<cut[s].nodes.Length; n++)
					DebugGizmos.DrawDot("SplineTest", cut[s].nodes[n].Vector3(), 6, Color.green, additive: true);
			}
		}

		public void Test ()
		{
			if (splineObj==null) return;

			if (cut == null) cut = new List<Spline>();
			cut.Clear();

			for (int s = 0; s<splineObj.splines.Length; s++)
			{
				Spline newSpline = new Spline(splineObj.splines[s]);

				newSpline.CutAA(val, axis);

				if (remove)
					cut.AddRange(newSpline.RemoveOuterAA(val + 0.0001f*(int)side, axis, side));
				else
					cut.Add(newSpline);

			}
		}
	}


	[Test(category="Segs")]
	public class SubdivideDistTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Max Dist")] public float maxDist = 10;

		private Spline subdivided;

		public void OnSceneGUI ()
		{
			if (subdivided==null) return;

			DebugGizmos.Clear("SplineTest");
			DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(subdivided.nodes, n=>n.Vector3()), Color.green, additive: true);
			for (int n = 0; n<subdivided.nodes.Length; n++)
				DebugGizmos.DrawDot("SplineTest", subdivided.nodes[n].Vector3(), 6, Color.green, additive: true);
		}

		public void Test ()
		{
			if (splineObj==null) return;

			subdivided = new Spline(splineObj.splines[0]);
			subdivided.SubdivideDist(maxDist);
		}
	}


	[Test(category="Segs")]
	public class WeldSplineTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Threshold")] public float threshold = 10;

		private Spline welded;

		public void OnSceneGUI ()
		{
			if (welded==null) return;

			DebugGizmos.Clear("SplineTest");
			DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(welded.nodes, n=>n.Vector3()), Color.green, additive: true);
			for (int n = 0; n<welded.nodes.Length; n++)
				DebugGizmos.DrawDot("SplineTest", welded.nodes[n].Vector3(), 6, Color.green, additive: true);
		}

		public void Test ()
		{
			if (splineObj==null) return;

			welded = new Spline(splineObj.splines[0]);
			welded.Weld(threshold);
		}
	}


	[Test(category="Segs")]
	public class InterlinkTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Positions Count")] public int positionsCount = 1;
		[Val("Area")] public int area = 100;
		[Val("MaxLinks")] public int maxLinks = 4;
		[Val("TriesPerObj")] public int triesPerObj = 8;
		private Vector3D[] positions;
		private Spline[] interlinked;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			DebugGizmos.DrawRect("SplineTest", Vector2D.zero, new Vector2D(area, area), Color.gray, additive: true);

			if (interlinked!=null)
				for (int i = 0; i<interlinked.Length; i++)
					DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(interlinked[i].nodes, n=>n.Vector3()), Color.green, additive: true);

			if (positions!=null)
				for (int i = 0; i<positions.Length; i++)
					positions[i] = DebugGizmos.PositionHandle(positions[i].Vector3()).Vector3D();
		}

		public void Test ()
		{
			if (positions==null)
				positions = new Vector3D[0];

			if (positionsCount!=positions.Length)
				ArrayTools.Resize(ref positions, positionsCount);

			PosTab posTab = new PosTab(Vector3.zero, new Vector3(area, 0, area), 16);
			for (int i = 0; i<positions.Length; i++)
				posTab.Add(positions[i].x, positions[i].z);

			interlinked = Spline.GabrielGraph(positions, maxLinks, triesPerObj);
		}
	}


	[Test(category="Segs")]
	public class SmartSubdivideTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Approx Seg Length")] public float approxSegLength = 1;
		[Val("Min Seg Length")] public float minLength = 0;

		[Val("Min Angle")] public float maxAngle = 0;
		[Val("Min Num")] public int maxNum = 0;
		[Val("Num Segs")] public float numSegs = 0;

		private Spline subdivided;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			if (subdivided!=null)
			{
				DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(subdivided.nodes, n=>n.Vector3()), Color.green, additive: true);

				for (int n = 0; n<subdivided.nodes.Length; n++)
					DebugGizmos.DrawDot("SplineTest", subdivided.nodes[n].Vector3(), 6, Color.green, additive: true);

				DebugGizmos.DrawDot("SplineTest", subdivided.nodes[maxNum].Vector3(), 6, Color.red, additive: true);
			}
		}

		public void Test ()
		{
			subdivided = new Spline(splineObj.splines[0]);
			BeizerOps.SmartSubdivide(subdivided, approxSegLength, minSegmentSize: minLength);

			maxAngle = -float.MaxValue;
			for (int n = 1; n<subdivided.nodes.Length-1; n++)
			{
				Vector3 nextVec = subdivided.nodes[n+1].Vector3()-subdivided.nodes[n].Vector3();
				float nextAngle = Mathf.Atan2(nextVec.x, nextVec.z) * Mathf.Rad2Deg;

				Vector3 prevVec = subdivided.nodes[n].Vector3()-subdivided.nodes[n-1].Vector3();
				float prevAngle = Mathf.Atan2(prevVec.x, prevVec.z) * Mathf.Rad2Deg;

				float deltaAngle = Mathf.Abs(Mathf.DeltaAngle(nextAngle, prevAngle));

				if (deltaAngle > maxAngle)
				{
					maxAngle = deltaAngle;
					maxNum = n;
				}
			}

			numSegs = subdivided.nodes.Length;
		}
	}

	[Test(category="Segs")]
	public class HandnessSpTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Pos")] public Vector2D pos;
		[Val("Handness")] public int handness = 0;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");
			pos = DebugGizmos.PositionHandle(pos);

			Color color = handness==1 ? Color.green : Color.red;
			if (handness == 0) color = Color.yellow;

			DebugGizmos.DrawDot("SplineTest", pos.Vector3(), 6, color, additive: true);
		}

		public void Test ()
		{
			if (splineObj==null) return;

			//setting handness
			bool firstHandness = splineObj.splines[0].Handness(pos) > 0;
			handness = firstHandness ? 1 : -1;

			//checking other splines
			foreach (Spline spline in splineObj.splines)
			{
				bool otherHandness = spline.Handness(pos) > 0;
				if (firstHandness != otherHandness)
					handness = 0;
			}
		}
	}

	[Test(category="Segs")]
	public class SplitNearPointsSpTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Points")] public Vector3[] points = new Vector3[0];
		[Val("Ranges")] public float[] ranges = new float[0];
		[Val("Count")] public int count = 0;
		[Val("Horizontal Only")] public bool horizontalOnly = false;

		Spline splitted = null;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			for (int p = 0; p<points.Length; p++)
			{
				points[p] = DebugGizmos.PositionHandle(points[p]);
				DebugGizmos.DrawDot("SplineTest", points[p], 6, color: Color.red, additive: true);
				DebugGizmos.DrawCircle("SplineTest", points[p], ranges[p], color: Color.red, additive: true);
			}

			if (splitted!=null)
			{
				DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(splitted.nodes, n=>n.Vector3()), color: Color.green, additive: true);
				for (int n = 0; n<splitted.nodes.Length; n++)
					DebugGizmos.DrawDot("SplineTest", splitted.nodes[n].Vector3(), 6, Color.green, additive: true);
			}
		}

		public void Test ()
		{
			if (count!=points.Length)
			{
				ArrayTools.Resize(ref points, count, new Vector3(100, 100, 100));
				ArrayTools.Resize(ref ranges, count, 10);
			}

			splitted = new Spline(splineObj.splines[0]);
			splitted.SplitNearPoints(points.Select(p => p.Vector3D()).ToArray(), ranges, horizontalOnly: horizontalOnly);
		}
	}

	[Test(category="Segs")]
	public class WeldLinksSpTest : ITest
	/// Determines whether ball is on left or on right side of the line
	{
		public void Test ()
		{
			(int, int)[] links = new (int, int)[] {
				(1,2), (0,1), (4,3),  (5,6), (2,3), (5,1), (4,6) };

			Vector3[] nodes = new Vector3[] {
				new Vector3(-1,0,0), new Vector3(0,0,0), new Vector3(1,0,1), new Vector3(2,0,2), new Vector3(3,0,3),
				new Vector3(1,0,-1), new Vector3(2,0,-1) };

			Spline[] splines = Spline.WeldLinks(links, nodes.Select(n => n.Vector3D()).ToArray());
		}
	}

	[Test(category="Segs")]
	public class SplitToLinksSpTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		Spline[] welded = null;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			if (welded != null)
			{
				for (int s = 0; s<welded.Length; s++)
				{
					DebugGizmos.DrawDot("SplineTest", welded[s].nodes[0].Vector3(), 6, color: Color.green, additive: true);
					DebugGizmos.DrawDot("SplineTest", welded[s].nodes[welded[s].nodes.Length-1].Vector3(), 6, color: Color.red, additive: true);
					DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(welded[s].nodes, n=>n.Vector3()), color: Color.yellow, additive: true);
				}
			}
		}

		public void Test ()
		{
			((int, int)[] links, Vector3D[] nodes) = Spline.SplitToLinks(splineObj.splines);
			welded = Spline.WeldLinks(links, nodes);
		}
	}
	
	[Test(category="Segs")]
	public class ReWeldSpTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject = true)] public SplineObject splineObj;
		[Val("Threshold")] public float threshold = 10;
		Spline[] welded = null;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			if (welded != null)
			{
				for (int s = 0; s<welded.Length; s++)
				{
					DebugGizmos.DrawDot("SplineTest", welded[s].nodes[0].Vector3(), 6, color: Color.green, additive: true);
					DebugGizmos.DrawDot("SplineTest", welded[s].nodes[welded[s].nodes.Length-1].Vector3(), 6, color: Color.red, additive: true);
					DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(welded[s].nodes, n=>n.Vector3()), color: Color.yellow, additive: true);
				}
			}
		}

		public void Test ()
		{
			welded = Spline.ReWeld(splineObj.splines, threshold);
		}
	}

	[Test(category="Segs")]
	public class WeldCloseSpTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Threshold")] public float threshold = 10;
		Spline[] welded = null;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			if (welded != null)
			{
				for (int s=0; s<welded.Length; s++)
				{
					DebugGizmos.DrawDot("SplineTest", welded[s].nodes[0].Vector3(), 6, color:Color.green, additive:true);
					DebugGizmos.DrawDot("SplineTest", welded[s].nodes[welded[s].nodes.Length-1].Vector3(), 6, color:Color.red, additive:true);
					DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(welded[s].nodes, n=>n.Vector3()), color:Color.yellow, additive:true);
				}
			}
		}

		public void Test ()
		{
			welded = Spline.WeldClose(splineObj.splines, threshold);
		}
	}

	[Test(category="Segs")]
	public class PushSpTest : ITest, ISceneGUI
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Points")] public Vector3[] points = new Vector3[0];
		[Val("Ranges")] public float[] ranges = new float[0];
		[Val("Count")] public int count = 0;
		[Val("Nodes / Objects")] public float nodeObjectsRatio = 0.5f;
		[Val("Iterations")] public int iterations = 1;
		Vector3[] changedPoints = new Vector3[0];
		Spline[] pushed = null;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			for (int p=0; p<points.Length; p++)
			{
				points[p] = DebugGizmos.PositionHandle(points[p]);
				DebugGizmos.DrawDot("SplineTest", changedPoints[p], 6, color:Color.red, additive:true);
				DebugGizmos.DrawCircle("SplineTest", changedPoints[p], ranges[p], color:Color.red, additive:true);
			}

			if (pushed != null)
				for (int s=0; s<pushed.Length; s++)
					DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(pushed[s].nodes, n=>n.Vector3()), color:Color.yellow, additive:true);
		}

		public void Test ()
		{
			if (count!=points.Length)
			{
				ArrayTools.Resize(ref points, count, new Vector3(100,100,100));
				ArrayTools.Resize(ref ranges, count, 10);
			}

			changedPoints = ArrayTools.Copy(points);

			pushed = new Spline[splineObj.splines.Length];
			for (int s=0; s<splineObj.splines.Length; s++)
				pushed[s] = new Spline(splineObj.splines[s]);

			for (int i=0; i<iterations; i++) //first iterations, splines inside
			for (int s=0; s<pushed.Length; s++)
			{
				float intensity = (float)(i+1) / iterations;
				pushed[s].Push(Array.ConvertAll(changedPoints, n=>n.Vector3D()), ranges, intensity, nodesPointsRatio:nodeObjectsRatio, horizontalOnly:true);
			}
		}
	}

	/*
	public class OptimizeTest : ITest
	{
		[Val("Deviation")] public float deviation = 10;

		public void Process (ref SplineSys dst) 
			{ dst.Optimize(deviation); }
	}

	public class CutRectTest : ITest
	{
		[Val("Pos")] public Vector2D pos;
		[Val("Size")] public Vector2D size;
		[Val("Show Rect")] public bool showRect;

		public void Process (ref SplineSys dst) 
		{ 
			if (showRect) DebugGizmos.DrawRect("SplineTesterRect", pos, size, color:Color.green);
			dst.CutByRect((Vector3)pos, (Vector3)size); 
		}
	}

	public class RemoveOuterRectTest : ITest
	{
		[Val("Pos")] public Vector2D pos;
		[Val("Size")] public Vector2D size;
		[Val("Show Rect")] public bool showRect;

		public void Process (ref SplineSys dst) 
		{ 
			if (showRect) DebugGizmos.DrawRect("SplineTesterRect", pos, size, color:Color.green);
			dst.RemoveOuterSegments((Vector3)pos, (Vector3)size); 
		}
	}

	public class ClampRectTest : ITest
	{
		[Val("Pos")] public Vector2D pos;
		[Val("Size")] public Vector2D size;
		[Val("Show Rect")] public bool showRect;

		public void Process (ref SplineSys dst) 
		{ 
			if (showRect) DebugGizmos.DrawRect("SplineTesterRect", pos, size, color:Color.green);
			dst.Clamp((Vector3)pos, (Vector3)size); 
		}
	}

	public class RelaxTest : ITest
	{
		[Val("Iterations")] public int iterations;
		[Val("Blur")] public float blur;

		public void Process (ref SplineSys dst) 
			{ dst.Relax(blur, iterations); }
	}

	public class ClosestSegSegTest : ITest, ISceneGUI
	{
		[Val("Initial Iterations")] public int initialIterations = 6;
		[Val("Refine Iterations")] public int refineIterations = 6;
		[Val("Percent1")] public float percent1;
		[Val("Percent2")] public float percent2;
		[Val("Point1")] public Vector3 point1;
		[Val("Point2")] public Vector3 point2;

		public void Process (ref SplineSys dst) 
		{ 
			if (dst.lines.Length < 2)
				throw new Exception("ClosestTest: Spline should have at least two lines");

			(percent1, percent2) = Segment.ClosestDistance(dst.lines[0].segments[0], dst.lines[1].segments[0], initialIterations, refineIterations);
			point1 = dst.lines[0].segments[0].GetPoint(percent1);
			point2 = dst.lines[1].segments[0].GetPoint(percent2);
		}

		public void OnSceneGUI ()
		{
			#if UNITY_EDITOR
			UnityEditor.Handles.DrawLine(point1, point2);
			#endif
		}
	}

	public class NeatWeldTest : ITest
	{
		[Val("Dist")] public float distance;

		public void Process (ref SplineSys dst) 
		{
			Segment[] segments = new Segment[dst.lines.Length];

			for (int i=0; i<segments.Length; i++)
				segments[i] = dst.lines[i].segments[0];

			for (int s1=0; s1<segments.Length; s1++)
				for (int s2=0; s2<segments.Length; s2++)
				{
					if ((segments[s1].start.pos - segments[s2].start.pos).sqrMagnitude < distance*distance)
						segments[s2].start.pos = segments[s1].start.pos;

					if ((segments[s1].start.pos - segments[s2].end.pos).sqrMagnitude < distance*distance)
						segments[s2].end.pos = segments[s1].start.pos;

					if ((segments[s1].end.pos - segments[s2].start.pos).sqrMagnitude < distance*distance)
						segments[s2].start.pos = segments[s1].end.pos;

					if ((segments[s1].end.pos - segments[s2].end.pos).sqrMagnitude < distance*distance)
						segments[s2].end.pos = segments[s1].end.pos;
				}

			dst.lines = Line.NeatWeldSegments(segments);
		}
	}


	public class PushPointsTest : ITest, ISceneGUI
	{
		public Vector3[] srcPoints;
		public Vector3[] dstPoints;
		public float[] ranges;
		[Val("Range")] public float range;
		[Val("Count")] public int count;
		[Val("Horizontal")] public bool horizontalOnly;
		[Val("Iterations")] public int iterations = 10;

		private void Prepare ()
		{
			if (srcPoints==null || srcPoints.Length!=count) srcPoints = new Vector3[count];
			if (dstPoints==null || dstPoints.Length!=count) dstPoints = new Vector3[count];
			if (ranges==null || ranges.Length!=count) ranges = new float[count];

			for (int p=0; p<count; p++)
				ranges[p] = range;
		}

		public void Process (ref SplineSys dst)
		{
			Prepare();
			for (int p=0; p<count; p++)
				dstPoints[p] = srcPoints[p];

			for (int i=0; i<iterations; i++)
			{
				float distFactor = Mathf.Sqrt((i+1f)/iterations);
				dst.PushPoints(dstPoints, ranges, horizontalOnly, distFactor);
			}
		}

		public void OnSceneGUI ()
		{
			#if UNITY_EDITOR
			Prepare();
			for (int p=0; p<count; p++)
			{
				UnityEditor.Handles.color = Color.red;
				UnityEditor.Handles.SphereHandleCap(0, srcPoints[p], Quaternion.identity, ranges[p]*2, EventType.Repaint);
				UnityEditor.Handles.color = Color.green;
				UnityEditor.Handles.SphereHandleCap(0, dstPoints[p], Quaternion.identity, ranges[p]*2, EventType.Repaint);

				srcPoints[p] = UnityEditor.Handles.PositionHandle(srcPoints[p], Quaternion.identity);
			}

			#endif
		}
	}


	public class AvoidPointsTest : ITest, ISceneGUI
	{
		public Vector3[] points;
		public float[] ranges;
		[Val("Range")] public float range;
		[Val("Count")] public int count;
		[Val("Horizontal")] public bool horizontalOnly;
		[Val("Iterations")] public int iterations = 10;

		private void Prepare ()
		{
			if (points==null || points.Length!=count) points = new Vector3[count];
			if (ranges==null || ranges.Length!=count) ranges = new float[count];

			for (int p=0; p<count; p++)
				{ points[p] = points[p]; ranges[p] = range; }
		}

		public void Process (ref SplineSys dst)
		{
			Prepare();
			dst.SplitNearPoints(points, ranges, horizontalOnly, startEndProximityFactor:1f, maxIterations:iterations);
			dst.PushStartEnd(points, ranges, horizontalOnly, 0.5f);
			dst.PushStartEnd(points, ranges, horizontalOnly, 1.01f);
		}

		public void OnSceneGUI ()
		{
			#if UNITY_EDITOR
			Prepare();
			for (int p=0; p<count; p++)
			{
				UnityEditor.Handles.color = Color.red;
				UnityEditor.Handles.DrawWireDisc(points[p], Vector3.up, ranges[p]);
				points[p] = UnityEditor.Handles.PositionHandle(points[p], Quaternion.identity);
			}

			#endif
		}
	}

	public class AreCodirectedTest : ITest, ISceneGUI
	{
		[Val("Codirected")] public bool codirected = false;

		public void Process (ref SplineSys dst)
		{
			codirected = Line.AreCodirected(dst.lines[0], dst.lines[1]);
		}

		public void OnSceneGUI ()
		{
			#if UNITY_EDITOR
			UnityEditor.Handles.color = codirected ? Color.green : Color.red;
			UnityEditor.Handles.DrawWireDisc(Vector3.zero, Vector3.up, 100);
			#endif
		}
	}

	public class OverlapTest : ITest
	{
		[Val("Threshold")] public float threshold = 30f;
		[Val("Max Iterations")] public int maxIterations = 10;

		public void Process (ref SplineSys dst)
		{
			Line.OverlapLines(dst.lines[0], dst.lines[1], threshold);
		}
	}

	public class CutForWeldTest : ITest
	{
		[Val("Threshold")] public float threshold = 30f;

		public void Process (ref SplineSys dst)
		{
			dst.lines[0].CutSegmentsForWeld(dst.lines[1], threshold);
			dst.lines[1].CutSegmentsForWeld(dst.lines[0], threshold);
		}
	}

	public class WeldCloseTest : ITest
	{
		[Val("Threshold")] public float threshold = 30f;
		[Val("Final Lines Count")] public int linesCount = -1;

		public void Process (ref SplineSys dst)
		{
			//dst.lines = Line.WeldClose(dst.lines[0], dst.lines[1], threshold);
			dst.WeldCloseLines(threshold);
			linesCount = dst.lines.Length;
		}
	}
	*/
}