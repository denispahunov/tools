﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools;
using Den.Tools.Tests;
using Den.Tools.Matrices;
//using Den.Tools.Splines;
using Den.Tools.GUI;
using Den.Tools.Segs;


namespace Den.Tools.Segs.Tests
{
	/*[Serializable]
	public class MatrixTextureInput
	{
		[Val("Src Tex", allowSceneObject=true)] public Texture2D matrixTex;
		[Val("PixelSize")] public float pixelSize = 1;
		[Val("Downscale")] public int downscale = 1;
		[Val("Height")] public float height = 250;

		[NonSerialized] public MatrixWorld matrix;
		[NonSerialized] public MatrixHeightGizmo gizmo;

		public void Refresh ()
		{
			if (matrixTex==null)
				{ matrix=null; gizmo=null; return; }

			if (matrix==null || matrix.rect.size.x!=matrixTex.width || matrix.rect.size.z!=matrixTex.height)
				matrix = new MatrixWorld(new CoordRect(0,0,matrixTex.width,matrixTex.height), new Vector2D(), new Vector2D(), 0); 
			matrix.worldSize = new Vector3(matrixTex.width*pixelSize, height, matrixTex.height*pixelSize);
			matrix.ImportTexture(matrixTex);

			for (int i=1; i<downscale; i++)
			{
				MatrixWorld downscaled = new MatrixWorld(matrix.rect/2, matrix.worldPos, matrix.worldSize);
				MatrixOps.DownscaleFast(matrix, downscaled);
				matrix = downscaled;
			}

			if (gizmo==null) gizmo = new MatrixHeightGizmo();
			gizmo.SetMatrixWorld(matrix, faceted:true);
		}
	}*/



	[Test(category="SplineMatrix")]
	public class SerpentineTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Height", allowSceneObject=true)] public MatrixObject heightObj;

		[Val("Seg Length")] public float segLength = 10;
		[Val("Iterations")] public int iterations = 10;
		[Val("Debug Node")] public int debugNode = 10;

		[Val("Incline Factor")] public float inclineFactor = 0.1f;
		[Val("Length Factor")] public float lengthFactor = 0.8f;
		[Val("Height Factor")] public float heightFactor = 0.1f;

		[NonSerialized] private Spline serpentine;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			if (serpentine != null)
			{
				DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(serpentine.nodes, n=>n.Vector3()), Color.green, additive:true);
				for (int n=0; n<serpentine.nodes.Length; n++)
					DebugGizmos.DrawDot("SplineTest", serpentine.nodes[n].Vector3(), 6, Color.green, additive:true);
			}
		}

		public void Test ()
		{
			if (splineObj==null || heightObj==null) return;
			
			SplineMatrixOps.tmpHighlightNode = debugNode;

			serpentine = new Spline(splineObj.splines[0]);

			SplineMatrixOps.SerpentineFactors factors = new SplineMatrixOps.SerpentineFactors() {
				incline = inclineFactor,
				length = lengthFactor,
				height = heightFactor };

			SplineMatrixOps.Serpentine(heightObj.MatrixWorld, serpentine, segLength, iterations, factors);

			//for (int n=0; n<serpentine.nodes.Length; n++)
			//	serpentine.nodes[n].y = 0;
		}
	}


	[Test(category="SplineMatrix")]
	public class StrokeSpMatTest : ITest
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Matrix", allowSceneObject=true)] public MatrixObject matrixObj;

		[Val("White")] public bool white=false;
		[Val("Intensity")] public float intensity=1;
		[Val("Antialiased")] public bool antialiased=false;
		[Val("Pad One Pixel")] public bool padOnePixel=false;

		public void Test ()
		{
			if (splineObj==null || matrixObj==null) return;

			matrixObj.matrix.Fill(0);
			SplineMatrixOps.Stroke(splineObj.splines[0], matrixObj.MatrixWorld, white, intensity, antialiased, padOnePixel);
			matrixObj.RefreshGizmos();
			matrixObj.RefreshPreview();
		}
	}


	[Test(category="SplineMatrix")]
	public class SilhouetteSpMatTest : ITest
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Spline", allowSceneObject=true)] public SplineObject splineObj;
		[Val("Matrix", allowSceneObject=true)] public MatrixObject matrixObj;

		[Val("White")] public bool white=false;
		[Val("Intensity")] public float intensity=1;
		[Val("Antialiased")] public bool antialiased=false;
		[Val("Pad One Pixel")] public bool padOnePixel=false;

		public void Test ()
		{
			DebugGizmos.Clear("SplineTest");

			if (splineObj==null || matrixObj==null) return;

			matrixObj.matrix.Fill(0);
			SplineMatrixOps.Silhouette(splineObj.splines, matrixObj.MatrixWorld);
			matrixObj.RefreshGizmos();
			matrixObj.RefreshPreview();
		}
	}


	[Test(category="SplineMatrix")]
	public class IsolineSpTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Matrix", allowSceneObject=true)] public MatrixObject matrixObj;
		[Val("Threshold")] public float threshold = 0.5f;
		[NonSerialized] private Spline[] splines;

		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineTest");

			if (splines != null)
				for (int s=0; s<splines.Length; s++)
					DebugGizmos.DrawPolyLine("SplineTest", Array.ConvertAll(splines[s].nodes, n=>n.Vector3()), Color.green, additive:true);
		}

		public void Test ()
		{
			if (matrixObj==null) return;
			splines = SplineMatrixOps.Isoline(matrixObj.MatrixWorld, threshold);
		}
	}


	[Test(category="SplineMatrix")]
	public class WorldToPixelTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Pos")] public Vector2D pos;
		[Val("Matrix", allowSceneObject=true)] public MatrixObject matrixObj;

		public void OnSceneGUI ()
		{
			pos = DebugGizmos.PositionHandle(pos);
		}

		public void Test ()
		{
			if (matrixObj==null) return;

			MatrixWorld mw = matrixObj.MatrixWorld;
			mw.Fill(0);
			
			Vector3D p = mw.WorldToPixelInterpolated(pos.x, pos.z);
			mw[(int)p.x, (int)p.z] = 1f;
			mw[(int)p.x+1, (int)p.z+1] = 0.75f;
			mw[(int)p.x+1, (int)p.z] = 0.75f;
			mw[(int)p.x, (int)p.z+1] = 0.75f;

			matrixObj.RefreshGizmos();
			matrixObj.RefreshPreview();
		}
	}
}