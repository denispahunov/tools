using Den.Tools.GUI;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Den.Tools.Tasks
{
	public class ThreadManagerWindow : EditorWindow
	{
		UI ui = new UI();

		public void OnGUI () => ui.Draw(DrawGUI, inInspector:false);
		
		public void DrawGUI ()
		{
			using (Cell.LineStd) Draw.Toggle(ref ThreadManager.useMultithreading, "Multithreading");
			using (Cell.LineStd) Draw.Field(ref ThreadManager.maxThreads, "Max Threads");
			using (Cell.LineStd) Draw.Toggle(ref ThreadManager.autoMaxThreads, "Auto Max Threads");
			using (Cell.LineStd) Draw.DualLabel("Processor Threads", ThreadManager.processorThreads.ToString());
			using (Cell.LineStd) Draw.DualLabel("IsWorking", ThreadManager.IsWorking().ToString());

			Cell.EmptyLinePx(10);
			using (Cell.LineStd) Draw.DualLabel("Queue", ThreadManager.QueueCount.ToString());
			using (Cell.LineStd) Draw.DualLabel("Queue Tags", ThreadManager.QueueThreadsTags());

			Cell.EmptyLinePx(10);
			using (Cell.LineStd) Draw.DualLabel("Active", ThreadManager.ActiveCount.ToString());
			using (Cell.LineStd) Draw.DualLabel("Active D", ThreadManager.ActiveDebugCount.ToString());
			using (Cell.LineStd) Draw.DualLabel("Active Tags", ThreadManager.ActiveThreadsTags());

			Cell.EmptyLinePx(10);
			using (Cell.LineStd) Draw.DualLabel("Paused", ThreadManager.PausedDebugCount.ToString());
			using (Cell.LineStd) Draw.DualLabel("Paused Tags", ThreadManager.PausedThreadsTags());
		}


		public static void ShowWindow ()
		{
			ThreadManagerWindow window = new ThreadManagerWindow();
			window.position = new Rect(Screen.currentResolution.width/2-100, Screen.currentResolution.height/2-50, 200, 100);
			window.ShowAuxWindow();
		}

		[MenuItem ("Window/MapMagic/Thread Manager")]
		public static void ShowEditor ()
		{
			EditorWindow.GetWindow<ThreadManagerWindow>("Thread Manager");
		}
	}
}