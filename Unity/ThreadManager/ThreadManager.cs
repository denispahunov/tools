﻿using UnityEngine;
using UnityEditor;
using System;
using System.Threading;
using System.Collections.Generic;
//using UnityEngine.Profiling;
using Unity.Jobs;
using System.Collections.Concurrent;
using UnityEditor.PackageManager;

namespace Den.Tools.Tasks
{
	public static class ThreadManager
	{
		private class ThreadData
		{
			public string tag; //for debug purpose
			public int priority;
		}

		private static ConcurrentDictionary<Thread,ThreadData> queue = new ConcurrentDictionary<Thread, ThreadData>();
		private static int activeCount = 0; 
		private static object lockObj = new object();

		public static int maxThreads = 3;
		public static int processorThreads = -1;
		public static bool autoMaxThreads = true;
		public static bool useMultithreading = true;
		public static int NumThreads => autoMaxThreads ? processorThreads-1 : maxThreads;

		private static ConcurrentDictionary<Thread,ThreadData> debugActive = new ConcurrentDictionary<Thread,ThreadData>();
		private static ConcurrentDictionary<Thread,ThreadData> debugPaused = new ConcurrentDictionary<Thread,ThreadData>();
		public static bool error;


		static ThreadManager () 
		{
			//processorThreads = SystemInfo.processorCount; 
			//can be called from MM's serialization (since it the first time ThreadManager called), and processorCount is main thread only
			
			//aborting when exiting playmode
			#if UNITY_EDITOR
			EditorApplication.playModeStateChanged -= AbortOnPlaymodeChange;
			Application.wantsToQuit -= AbortOnExit;

			EditorApplication.playModeStateChanged += AbortOnPlaymodeChange;
			Application.wantsToQuit += AbortOnExit;
			#endif
		}


		public static Thread CreateThread (Action action)
		/// Sometimes we need to assign thread before it starts
		{
			void ThreadFn(object context) => ThreadAction(action,context);
			return new Thread(new ParameterizedThreadStart(ThreadFn)); 
		}
		

		public static void Enqueue (Thread thread, int priority, string tag=null)
		{
			if (processorThreads < 0) 
				processorThreads = SystemInfo.processorCount; //the first time Enqueue is called from main thread

			ThreadData data = new ThreadData() {tag=tag, priority=priority};

			#if MM_DEBUG
			Log("Enqueuing " + tag, tag);
			#endif
			
			if (!queue.TryAdd(thread, data))
				#if MM_DEBUG
				Log("Enqueue THREAD " + thread.ManagedThreadId + " ALREADY ADDED TO DICT", tag, isError:true);
				#else
				{}
				#endif

			StartNextThread();
		}


		public static Thread Enqueue (Action action, int priority, string tag=null)
		{
			Thread thread = CreateThread(action);
			Enqueue(thread, priority, tag);
			return thread;
		}


		public static bool TryDequeue (Thread thread)
		{
			bool dequeued = queue.TryRemove(thread, out ThreadData data);

			#if MM_DEBUG
			Log("TryDequeue Tried remove thread:" + dequeued, data?.tag);
			#endif

			return dequeued;
		}


		public static bool Enqueued (Thread thread)
		{
			return queue.ContainsKey(thread);
		}


		public static void MarkPaused () 
		{
			Interlocked.Decrement(ref activeCount);

			#if MM_DEBUG
				if (!debugActive.TryRemove(Thread.CurrentThread, out ThreadData tmp))
					Log("MarkPaused COULD NOT REMOVE THREAD FROM ACTIVE", tmp?.tag, isError:true);

				if (!debugPaused.TryAdd(Thread.CurrentThread, tmp))
					Log("MarkPaused COULD NOT ADD THREAD TO PAUSED", tmp?.tag, isError:true);

				if (activeCount < 0)
					Log("MarkPaused ACTIVE COUNT < 0", tmp?.tag, isError:true);

				Log("MarkPaused ActiveCount reduced", null); //after taking from active to maintain corresponding count
			#endif

			StartNextThread();
		}

		public static void MarkResumed () 
		{
			Interlocked.Increment(ref activeCount);

			#if MM_DEBUG
				if (!debugPaused.TryRemove(Thread.CurrentThread, out ThreadData tmp))
					Log("MarkPaused COULD NOT REMOVE THREAD FROM ACTIVE", tmp?.tag, isError:true);

				if (!debugActive.TryAdd(Thread.CurrentThread, tmp))
					Log("MarkResumed COULD NOT ADD THREAD TO ACTIVE", tmp?.tag, isError:true);

				Log("MarkResumed ActiveCount increased", null);
			#endif
		}


		public static void ThreadAction (Action action, object context)
		{
			string tag = context as string; 

			if (activeCount < 0)
				Log("ThreadAction ACTION WHILE ACTIVE==0", tag, isError:true);

			try
			{ 
				Log("ThreadAction Starting action", tag);
				action(); 
				Log("ThreadAction Finished action", tag);
			}

			catch (ThreadAbortException) { }

			#if !MM_DEBUG
			catch (Exception e)
				{ Debug.LogError("Thread failed: " + e); }  //throw exception ignores VS
				//{ throw new Exception("Thread failed: " + e); }
			#endif

			finally
			{
				//activeCount--;
				Interlocked.Decrement(ref activeCount);

				if (activeCount < 0)
					Log("ThreadAction ACTIVE COUNT < 0", tag, isError:true);

				if (!debugActive.TryRemove(Thread.CurrentThread, out ThreadData tmp))
					Log("ThreadAction COULD NOT REMOVE THREAD", tag, isError:true);
					
				Log("ThreadAction ActiveCount reduced", tag); //after taking from active to maintain corresponding count

				//if (activeCount != debugActive.Count)
				//	Log("ThreadAction ACTIVE COUNT MISMATCH", isError:true);

				StartNextThread();
			}
		}


		private static void StartNextThread ()
		{
			int curMaxThreads = NumThreads;

			if (activeCount < curMaxThreads  &&  queue.Count > 0)
				lock (lockObj) //queue is safe for adding, but here we are picking and comparing. Can pick the same thread twice
					//TODO: can be re-arranged without lock - if !tryGet choose another thread
					if (activeCount < curMaxThreads  &&  queue.Count > 0) //double check - situation might have changed under lock
					{
						Thread topThread = TopPriorityThread();

						if (topThread == null)
							Log("StartNextThread TOP THREAD IS NULL", null, isError:true);

						queue.TryRemove(topThread, out ThreadData topThreadData);
						//activeCount++; 
						Interlocked.Increment(ref activeCount);

						if (!debugActive.TryAdd(topThread, topThreadData))
							Log("StartNextThread COULD NOT ADD THREAD", topThreadData.tag, isError:true);
							
						Log("StartNextThread ActiveCount increased", topThreadData.tag); //after taking from active to maintain corresponding count


						Log("StartNextThread Starting " + topThreadData.tag, topThreadData.tag);

						if (!topThread.ThreadState.HasFlag(ThreadState.Unstarted))
							Log("StartNextThread THREAD IS ALREADY STARTED", topThreadData.tag, isError:true);

						topThread.Start();

						StartNextThread();
					}
			else
			{
				if (activeCount >= curMaxThreads)
					Log("StartNextThread Skipping because of thread limit", null);
				else if (queue.Count <= 0)
					Log("StartNextThread Skipping because of queue empty", null);
			}

			//if (activeCount != debugActive.Count) 
			//	Log("StartNextThread ACTIVE COUNT MISMATCH", isError:true);
		}


		private static Thread TopPriorityThread ()
		{
			int topPriority = int.MinValue;
			Thread topThread = null;
			ThreadData topThreadData = null;

			foreach (var kvp in queue)
			{
				Thread thread = kvp.Key;
				ThreadData threadData = kvp.Value;

				if (threadData.priority > topPriority)
				{
					topPriority = threadData.priority;
					topThreadData = threadData;
					topThread = thread;
				}
			}

			return topThread;
		}

		public static int QueueCount => queue.Count;
		public static int ActiveCount => activeCount;
		public static int ActiveDebugCount => debugActive.Count;
		public static int PausedDebugCount => debugPaused.Count;
		public static bool IsWorking () => activeCount!=0  &&  queue.Count!=0;
		public static bool IsWorking (string tag) => activeCount!=0  &&  queue.Count!=0;
			//TODO: not taking tag into account now

		public static string ActiveThreadsTags () => ConDictToString(debugActive);
		public static string QueueThreadsTags () => ConDictToString(queue);
		public static string PausedThreadsTags () => ConDictToString(debugPaused);
		private static string ConDictToString (ConcurrentDictionary<Thread,ThreadData> dict)
		{
			string info = "";
			foreach (var kvp in dict)
			{
				ThreadData data = kvp.Value;
				info += data.tag + ", ";
			}
			if (info.Length > 2) info = info.Substring(0, info.Length - 2);
			return info;
		}

		public static void Abort ()
		{
			queue.Clear();

			foreach (var kvp in debugActive)
				kvp.Key.Abort();
			debugActive.Clear();
			activeCount = 0;
		}

		#if UNITY_EDITOR
		static void AbortOnPlaymodeChange (PlayModeStateChange state)
		{
			if (state==PlayModeStateChange.ExitingEditMode || state==PlayModeStateChange.ExitingPlayMode)
				Abort();
		}
		#endif


		static bool AbortOnExit ()
		{
			Abort(); return true;
		}


		private static void Log (string msg, string subjTag, bool isError=false)
		{
			Thread thread = Thread.CurrentThread; //TODO: use subj thread!

			bool inQueue = queue.TryGetValue(thread, out ThreadData threadDataQ);
			bool isActive = debugActive.TryGetValue(thread, out ThreadData threadDataA);
			bool isPaused = debugPaused.TryGetValue(thread, out ThreadData threadDataP);

			ThreadData threadData = null;
			if (inQueue) threadData = threadDataQ;
			if (isActive) threadData = threadDataA;
			if (isPaused) threadData = threadDataP;

			Den.Tools.Log.AddThreaded("ThreadManager." + msg, threadData?.tag, //using thread tag here
				("Subj", subjTag),
				("Thread Id", thread.ManagedThreadId),
				("Thread State", thread.ThreadState),
				("In Queue", inQueue),
				("Is Active", isActive),
				("Is Paused", isPaused),
				("Priority", threadData?.priority),
				("Queue Count", queue.Count),
				("Queue Ids", queue.Keys.ToStringMemberwise(toStringFn:v=>v.ManagedThreadId.ToString())),
				("Queue Tags", QueueThreadsTags()),
				("Active Count", activeCount),
				("Active Count (d)", debugActive.Count),
				("Active Ids", debugActive.Keys.ToStringMemberwise(toStringFn:v=>v.ManagedThreadId.ToString())),
				("Active Tags", ActiveThreadsTags()),
				("Pause Count", debugPaused.Count),
				("Pause Ids", debugPaused.Keys.ToStringMemberwise(toStringFn:v=>v.ManagedThreadId.ToString())),
				("Pause Tags", PausedThreadsTags()) );

			if (isError)
			{
				error = true;
				throw new Exception(msg + ". This should not happen."); 
			}
		}
	}



	public static class ThreadManagerTasks
	{
		//TODO: make Task private. No one should really know what it is
		//Use dictionary thread-task to  find task 
		//Add stop and revision to it?

		//TODO: use semaphore to limit number of threads
		//start all and instantly pause
		//then each thread decided whether it should continue (performance?)
		//will go well with cluster's mre pause

		public class Task
		{
			public string name; //for debug purpose, never used
			public int priority;
			public Action action;
			public Thread thread;
			public bool wasActiveBeforePause;

			public bool Enqueued {get{ return queue.Contains(this); }}
			public bool Active {get{ return active.Contains(this) || paused.Contains(this); }}
			public bool IsAlive {get{ return queue.Contains(this) || active.Contains(this) || paused.Contains(this); }}
		}

		private static List<Task> queue = new List<Task>();
		private static List<Task> active = new List<Task>();
		private static List<Task> paused = new List<Task>();

		public static int maxThreads = 3;
		public static int processorThreads = -1;
		public static bool autoMaxThreads = true;
		public static bool useMultithreading = true;

		static ThreadManagerTasks ()
		{
			#if UNITY_EDITOR
			EditorApplication.playModeStateChanged -= AbortOnPlaymodeChange;
			Application.wantsToQuit -= AbortOnExit;
			//UpdateCaller.Update -= UpdateMain;

			EditorApplication.playModeStateChanged += AbortOnPlaymodeChange;
			Application.wantsToQuit += AbortOnExit;
			//UpdateCaller.Update += UpdateMain; //updating main actions/routines every frame
			#endif
		}


		public static Task Enqueue (Action action, int priority=0, string name=null)
		{
			Task task = new Task() { action=action, priority=priority, name=name };
			Enqueue(task);
			return task;
		}


		public static void Enqueue (ref Task task, Action action, int priority=0, string name=null)
		{
			task = new Task() { action=action, priority=priority, name=name };
			Enqueue(task);
		}


		public static void Enqueue (Task task)
		{
			//if already executing - do nothing
			lock (active)
				if (active.Contains(task))
					return;

			lock (queue)
			{
				//if not in queue - enqueueing
				if (!queue.Contains(task))
					queue.Add(task);

				//if in queue  - do nothing
				else
					return;
			}

			LaunchThreads();
		}


		public static void Dequeue (Task task)
		{
			//if job is in queue and has not been started - just remove from queue
			lock (queue)
				if (queue.Contains(task))
				{
					queue.Remove(task);
					return;
				}
		}


		public static void MarkPaused (Task task)
		//Just marks as paused, actual pause is done via MRE
		//This one just lets other threads go while it's paused
		{
			lock (paused)
			{
				if (paused.Contains(task))
					return; //do nothing if already paused

				paused.Add(task);

				#if MM_DEBUG
				Log("Pause AddedToPause");
				#endif

				//if it's active - suspending
				lock (active)
					if (active.Contains(task))
					{
						active.Remove(task);

						#if MM_DEBUG
						Log("Pause RemovedFromActive");
						#endif
					}

				//if in queue - removing from queue
				lock (queue)
					if (queue.Contains(task))
					{
						queue.Remove(task);

						#if MM_DEBUG
						Log("Pause RemovedFromQueue");
						#endif
					}

				LaunchThreads(); //updating threads to  start new ones after pause
			}
		}


		public static void EnqueuePaused (Task task)
		{
			lock (paused)
			{
				paused.Remove(task);

				lock (active)   //was marked as paused from active thread only
				{
					active.Add(task);

					#if MM_DEBUG
					Log("EnqueuePaused Added back to active");
					#endif
				}
			}
		}



		public static void Pause (Task task)
		{
			bool suspend = false;

			lock (paused)
			{
				if (paused.Contains(task))
					return; //do nothing if already paused

				paused.Add(task);

				#if MM_DEBUG
				//Log.AddThreaded("ThreadManager.Pause AddedToPause");
				#endif

				//if it's active - suspending
				lock (active)
					if (active.Contains(task))
					{
						active.Remove(task);

						#if MM_DEBUG
						//Log.AddThreaded("ThreadManager.Pause RemoveFromActive");
						#endif

						task.wasActiveBeforePause = true;

						suspend = true;
					}

				//if in queue - removing from queue
				lock (queue)
					if (queue.Contains(task))
					{
						queue.Remove(task);

						#if MM_DEBUG
						//Log.AddThreaded("ThreadManager.Pause RemoveFromQueue");
						#endif

						task.wasActiveBeforePause = false;
					}
			}

			if (suspend)
				task.thread.Suspend(); //can't do anything after this
		}

		public static void Resume (Task task)
		{
			lock (paused)
			{
				paused.Remove(task);

				if (task.wasActiveBeforePause)
				{
					lock (active)
					{
						active.Add(task);

						#if MM_DEBUG
						//Log.AddThreaded("ThreadManager.Resume AddedToActive");
						#endif

						task.thread.Resume();
					}
				}
				else
					lock (queue)
					{
						queue.Add(task);

						#if MM_DEBUG
						//Log.AddThreaded("ThreadManager.Resume AddedToQueue");
						#endif
					}
			}
		}


		public static void LaunchThreads ()
		{
			lock (active)
				while (true) //active.Count < maxThreads && queue.Count != 0)
			{
				int curMaxThreads = maxThreads;
				if (autoMaxThreads) 
				{
					if (processorThreads < 0) processorThreads = SystemInfo.processorCount; //the first time LaunchThreads is called from main thread
					curMaxThreads = processorThreads-1;
				}

				if (active.Count >= curMaxThreads) break;

				Task task;
				lock (queue)
				{
					if (queue.Count == 0) break;

					int jobNum = GetMaxPriorityNum(queue);
					task = queue[jobNum]; 
					queue.RemoveAt(jobNum);

					#if MM_DEBUG
					Log("LaunchThreads Picking from queue");
					#endif
				}
		
				active.Add(task);

				#if MM_DEBUG
				Log("LaunchThreads Added to active");
				#endif

				if (useMultithreading)
				{
					Thread thread = new Thread(task.TaskThreadAction);
					lock (task)
						task.thread = thread;
					thread.Start();
				}
				else
					task.TaskThreadAction();
			}
		}


		public static void TaskThreadAction (this Task task)
		{
			try
			{ 
				Log("TaskThreadAction Starting action");
				task.action(); 
				Log("TaskThreadAction Finished action");
			}

			catch (ThreadAbortException) { }

			#if !MM_DEBUG
			catch (Exception e)
				{ Debug.LogError("Thread failed: " + e); }  //throw exception ignores VS
				//{ throw new Exception("Thread failed: " + e); }
			#endif

			finally
			{
				lock (active)
				{
					active.Remove(task); //it SHOULD be in active 
					Log("TaskThreadAction Removed from active as it finished");
				}

				//task.ended = true;
				LaunchThreads(); //updating threads once one done
			}
		}


		public static int GetMaxPriorityNum (List<Task> list)
		{
			int maxPriority = int.MinValue;
			int maxPriorityNum = -1;

			for (int i=list.Count-1; i>=0; i--) //for FIFO
			{
				int priority = list[i].priority;
				if (priority > maxPriority)
				{
					maxPriority = priority;
					maxPriorityNum = i;
				}
			}

//			Debug.Log("Selected " + maxPriority + " from list of " + list.Count + "\n" + list.ToStringMemberwise<Task>(o => o.priority.ToString()));

			return maxPriorityNum;
		}


		public static void Abort ()
		{
			//clearing queue (in that order so no job will pass from queue to active)
			lock (queue)
				queue.Clear();

			//clearing active
			List<Task> activeCopy;
			lock (active)
			{
				activeCopy = new List<Task>(active);
				active.Clear();
			}

			for (int i=0; i<activeCopy.Count; i++)
			{
				Task task = activeCopy[i];

				lock (task)
					if (task.thread != null) 
						task.thread.Abort(); 
			}
		}

		#if UNITY_EDITOR
		static void AbortOnPlaymodeChange (PlayModeStateChange state)
		{
			if (state==PlayModeStateChange.ExitingEditMode || state==PlayModeStateChange.ExitingPlayMode)
				Abort();
		}
		#endif


		static bool AbortOnExit ()
		{
			Abort(); return true;
		}


		public static int Count => queue.Count + active.Count; 
		public static int ActiveCount => active.Count;
		public static int QueueCount => queue.Count;

		public static bool IsWorking {get{ return queue.Count!=0 || active.Count!=0; }}


		public static Task GetCurrentTask ()
		{
			Thread thread = Thread.CurrentThread;

			bool IsCurrent (Task task) => task.thread == thread;

			Task current = null;
			current = active.Find(IsCurrent);
			if (current == null) current = queue.Find(IsCurrent);
			if (current == null) current = paused.Find(IsCurrent);

			return current;
		}

		public static string DebugState ()
		/// returns active thread names and queue names to debug
		{
			string activeNames;
			string queueNames;

			//debug usually happens on border state, so locking 
			lock (active) 
				activeNames = active.ToStringMemberwise<Task>(t => t.name);
			lock (queue)
				queueNames = queue.ToStringMemberwise<Task>(t => t.name);

			return "Thread active: " + activeNames + "\n" + "Thread queue: " + queueNames;
		}

		private static void Log (string msg)
		{
			Den.Tools.Log.AddThreaded("ThreadManager." + msg, 
				("Active",ToStringMemberwise(active)), 
				("Queue",ToStringMemberwise(queue)), 
				("Paused",ToStringMemberwise(paused)) );
		}

			public static string ToStringMemberwise (List<Task> list)
			/// prints list as one, two, three
			{
				string result = "";
				result += list.Count.ToString() + ": ";
				for (int i=0; i<list.Count; i++)
					result += list[i].name + ", ";
				if (result.Length>=2) result = result.Substring(0, result.Length-2);
				return result;
			}
	}
}

