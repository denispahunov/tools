using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;


//ManagedReferenceMissingType consists of:
/*
	public string assemblyName = "MapMagic.Tests"
	public string namespaceName = "";
	public string className = "MyTempGenerator8";
	public long referenceId = 1885265714747801614;
	public string serializedData;
*/

#region UYAML example
/*
id: 9186461431510335489
version: 0
draftTime: 0
mainTime: 0
guiPosition: {x: 15.2, y: 104.8}
guiSize: {x: 150, y: 1068}
guiPreview: 0
guiAdvanced: 0
guiDebug: 0
serializedUnitIds: 0200000000de7c7f0300000000de7c7f0400000000de7c7f0500000000de7c7f
serializedMissingDownloadString: 
serializedMissingInletTypes:
- Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity
- Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity
- Den.Tools.TransitionsList, Den.Tools.Unity
serializedMissingOutletTypes:
- Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity
- Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity
- Den.Tools.TransitionsList, Den.Tools.Unity
serializedMissingInletIds: 0100000000de7c7f0200000000de7c7f0300000000de7c7f
serializedMissingOutletIds: 0100000000de7c7f0400000000de7c7f0500000000de7c7f
position:
  x: 0
  z: 0
radius: 30
hardness: 0.5
prefabval: {fileID: 0}
scriptableObjectval: {fileID: 0}
intval: 42
floatval: 42
doubleval: 42
vector2val: {x: 42, y: 42}
vector3val: {x: 42, y: 42, z: 42}
vector4val: {x: 42, y: 42, z: 42, w: 42}
rectval:
  serializedVersion: 2
  x: 0
  y: 0
  width: 42
  height: 42
serializedMissingDownloadString: 
serializedMissingInletTypes:
- Den.Tools.Matrices.MatrixWorld
- Den.Tools.Matrices.MatrixWorld
- Den.Tools.TransitionsList
intvector2val: {x: 42, y: 42}
intvector3val: {x: 42, y: 42, z: 42}
intvector4val: {x: 42, y: 42, z: 42, w: 42}
stringval: default
boolval: 1
longval: 42
shortval: 42
byteval: 42
charval: 99
uintval: 42
ulongval: 42
ushortval: 42
sbyteval: 42
coordval:
  x: 0
  z: 0
coordRectVal:
  offset:
    x: 0
    z: 0
  size:
    x: 0
    z: 0
vector2Dval:
  x: 0
  z: 0
colorval: {r: 0, g: 0, b: 0, a: 0}
enumVal: 0
rectOffsetVal:
  m_Left: 0
  m_Right: 0
  m_Top: 0
  m_Bottom: 0
intRectVal:
  x: 0
  y: 0
  width: 0
  height: 0
*/
#endregion


#region binary2text example
/*
enabled 1 (UInt8)
id 17709255445471821828 (UInt64)
<LinkedOutletId>k__BackingField 0 (UInt64)
<LinkedGenId>k__BackingField 0 (UInt64)
version 0 (UInt64)
draftTime 0 (double)
mainTime 0 (double)
guiPosition (0 96) (Vector2f)
guiSize (150 1068) (Vector2f)
guiPreview 0 (UInt8)
guiAdvanced 0 (UInt8)
guiDebug 0 (UInt8)
serializedMissingDownloadString "" (string)
serializedMissingInletTypes  (vector)
	size 3 (int)
	data "Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
	data "Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
	data "Den.Tools.TransitionsList, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)

serializedMissingOutletTypes  (vector)
	size 3 (int)
	data "Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
	data "Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
	data "Den.Tools.TransitionsList, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)

serializedMissingInletIds  (vector)
	size 3 (int)
	data (UInt64) #0: 17709255445471821828 17709255445471821829 17709255445471821830
serializedMissingOutletIds  (vector)
	size 3 (int)
	data (UInt64) #0: 17709255445471821828 17709257644495077383 17709257644495077384
position  (Vector2D)
	x 0 (float)
	z 0 (float)
radius 30 (float)
hardness 0.5 (float)
prefabval  (PPtr<$GameObject>)
	m_FileID 0 (int)
	m_PathID 0 (SInt64)
scriptableObjectval  (PPtr<$ScriptableObject>)
	m_FileID 0 (int)
	m_PathID 0 (SInt64)
intval 42 (int)
floatval 42 (float)
doubleval 42 (double)
vector2val (42 42) (Vector2f)
vector3val (42 42 42) (Vector3f)
vector4val (42 42 42 42) (Vector4f)
rectval  (Rectf)
	x 0 (float)
	y 0 (float)
	width 42 (float)
	height 42 (float)
intvector2val (42 42) (int2_storage)
intvector3val (42 42 42) (int3_storage)
intvector4val (42 42 42 42) (Vector4f)
stringval "default" (string)
boolval 1 (UInt8)
longval 42 (SInt64)
shortval 42 (SInt16)
byteval 42 (UInt8)
charval 99 (UInt16)
uintval 42 (unsigned int)
ulongval 42 (UInt64)
ushortval 42 (UInt16)
sbyteval 42 (SInt8)
coordval  (Coord)
	x 0 (int)
	z 0 (int)
coordRectVal  (CoordRect)
	offset  (Coord)
		x 0 (int)
		z 0 (int)
	size  (Coord)
		x 0 (int)
		z 0 (int)
vector2Dval  (Vector2D)
	x 0 (float)
	z 0 (float)
colorval (0 0 0 0) (ColorRGBA)
enumVal 0 (int)
rectOffsetVal (m_Left 0 m_Right 0 m_Top 0 m_Bottom 0) (RectOffset)
intRectVal  (RectInt)
	x 0 (int)
	y 0 (int)
	width 0 (int)
	height 0 (int)
intArray  (vector)
	size 5 (int)
	data (int) #0: 1 2 3 4 5
vectorArray  (vector)
	size 2 (int)
	data (1 2) (Vector2f)
	data (3 4) (Vector2f)

coordArray  (Coord)
	size 2 (int)
	data  (Coord)
		x 1 (int)
		z 2 (int)
	data  (Coord)
		x 3 (int)
		z 4 (int)

emptyString "" (string)
aIn  (MatrixInlet)
	id 17709255445471821829 (UInt64)
	<LinkedOutletId>k__BackingField 0 (UInt64)
	<LinkedGenId>k__BackingField 0 (UInt64)
bIn  (TransitionsInlet)
	id 17709255445471821830 (UInt64)
	<LinkedOutletId>k__BackingField 0 (UInt64)
	<LinkedGenId>k__BackingField 0 (UInt64)
aOut  (MatrixOutlet)
	id 17709257644495077383 (UInt64)
bOut  (TransitionsOutlet)
	id 17709257644495077384 (UInt64)

*/
#endregion


public static class SerializedDataReader
{
    public static object CreateObjFromSerializedData (string assemblyName, string className, string namespaceName, string serializedData)
    {
        Type type = Type.GetType($"{namespaceName}.{className}, {assemblyName}");
        if (type == null)
            throw new Exception($"Type {namespaceName}.{className} not found in assembly {assemblyName}.");

        object instance = Activator.CreateInstance(type);
		var lines = serializedData.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
		int lineNum = 0;

		if (IsUyaml(lines))
			return ParserUyaml.ExtractMultilineValue(lines, instance, type, ref lineNum, 0);
		else
			return ParserBtt.ExtractMultilineValue(lines, instance, type, ref lineNum, 0);
    }


	public static bool TryCreateObjFromSerializedData (string assemblyName, string className, string namespaceName, string serializedData, out object createdObject)
	/// Returns false and outputs an error string as createdObject if something went wrong
	{
		try 
		{
			createdObject = CreateObjFromSerializedData (assemblyName, className, namespaceName, serializedData);
			return true;
		}
		catch (Exception e)
		{
			createdObject = e.Message;
			return false;
		}
	}


	public static T GetValueFromSerializedData<T> (string fieldName, string data)
	{
		string[] lines = data.Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
		bool uyaml = IsUyaml(lines);

		for (int lineNum = 0; lineNum < lines.Length; lineNum++)
		{
			string line = lines[lineNum];

			if (line.StartsWith(fieldName + (uyaml ? ':' : ' ')))
			{
				object val; 

				if (uyaml)
					val = ParserUyaml.ParseValue(lines, typeof(T), ref lineNum, 0);
				else
					val = ParserBtt.ParseValue(lines, typeof(T), ref lineNum, 0);

				return (T)val;
			}
		}

		throw new Exception($"Field '{fieldName}' not found in data.");
	}


	private static class ParserUyaml
	{
		public static object ParseValue (string[] lines, Type type, ref int lineNum, int tabOffset=0)
		{
			string currentLine = lines[lineNum];
			int currentTabCount = CountTabs(currentLine);

			if (currentTabCount != tabOffset)
				throw new InvalidOperationException($"Expected tab offset: {tabOffset}, but got: {currentTabCount}");

			currentLine = currentLine.Trim();

			int separatorIndex = currentLine.IndexOf(':');
			string fieldName = currentLine.Substring(0, separatorIndex).Trim();
			string valueString = currentLine.Substring(separatorIndex + 1).Trim();


			//multiline:
			if (valueString == null || valueString.Length == 0)
			{
				string nextLine = lines[lineNum+1];

				//string array - next line starts with '-'
				if (lineNum < lines.Length  &&  nextLine.Trim().StartsWith('-'))
					return ExtractStringArray(lines, ref lineNum, tabOffset);

				//struct with labbeled fields
				else if (nextLine.Contains(':'))
				{
					object instance = Activator.CreateInstance(type);
					return ExtractMultilineValue(lines, instance, type, ref lineNum, tabOffset+1);
				}

				//null/empty string
				else
					return null;
			}

			//colorval: {r: 0, g: 0, b: 0, a: 0}
			else if (valueString.Contains("{"))
			{
				object instance = Activator.CreateInstance(type);
				return ExtractSinglelineValue(instance, type, valueString);
			}
		
			//serializedUnitIds: 0200000000de7c7f0300000000de7c7f0400000000de7c7f0500000000de7c7f
			else if (type.IsArray  &&  type.GetElementType().IsPrimitive) //string goes multiline
				return ExtractBinaryArray(type.GetElementType(), valueString);
		
			//x: 0
			else
				return ExtractPrimitiveValue(type, valueString);

		}

		private static (string fieldName, string valueString) ParseFieldValue(string line)
		/// Note two spaces in coordval  (Coord)
		{
			var parts = line.Split(':',2);

			if (parts.Length < 2)
				throw new FormatException($"Invalid line format: {line}");

			string fieldName = parts[0].Trim();
			string valueString = parts[1].Trim();

			return (fieldName, valueString);
		}



		private static object ExtractSinglelineValue (object instance, Type instanceType, string valueString)
		{
			string trim = valueString.Trim('{', '}');
			string[] nLines = trim.Split(',');

			foreach (string nLine in nLines)
			{
				string[] nParts = nLine.Split(':');
				string nFieldName = nParts[0].Trim();
				string nValueString = nParts[1].Trim();

				FieldInfo nFieldInfo = instanceType.GetField(nFieldName);
				Type nType = nFieldInfo.FieldType;

				object nVal = ExtractPrimitiveValue(nType, nValueString);

				nFieldInfo.SetValue(instance, nVal);
			}
			return instance;
		}

		public static object ExtractMultilineValue (string[] lines, object instance, Type instanceType, ref int lineNum, int tabOffset=0)
		{
			lineNum++;

			while (lineNum < lines.Length)
			{
				string nextLine = lines[lineNum];
				int nextTabCount = CountTabs(nextLine);
				if (nextTabCount <= tabOffset) break; // Reached the end of the current struct

				string[] nParts = nextLine.Split(':');
				string nFieldName = nParts[0].Trim();
				FieldInfo nFieldInfo = instanceType.GetField(nFieldName);
				Type nType = nFieldInfo.FieldType;

				object nVal = ParseValue(lines, nType, ref lineNum, tabOffset+1);
				
				nFieldInfo.SetValue(instance, nVal);
			}

			lineNum--; //returning line num to the last line of this struct
			return instance;
		}

		private static string[] ExtractStringArray (string[] lines, ref int lineNum, int tabOffset=0)
		{
			List<string> list = new List<string>();

			while (lineNum < lines.Length)
			{
				string nextLine = lines[lineNum+1];
				int nextTabCount = CountTabs(nextLine);

				if (!nextLine.Trim().StartsWith('-')  ||  nextTabCount < tabOffset)
					break;

				string element = nextLine.Trim('-', ' ');
				list.Add(element);

				lineNum++;
			}

			return list.ToArray();
		}

		static Array ExtractBinaryArray (Type elementType, string hex)
		{
			if (hex.Length % 2 != 0)
				throw new ArgumentException("Hex string must have an even length.");

			// string to bytes
			byte[] byteSequence = new byte[hex.Length / 2];
			for (int i = 0; i < hex.Length; i += 2)
				byteSequence[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);

			// decoding bytes
			int bytesPerElement = 8;
			switch (elementType)
			{
				case Type t when t == typeof(ulong): bytesPerElement = 8; break;
				case Type t when t == typeof(uint): bytesPerElement = 4; break;
				case Type t when t == typeof(ushort): bytesPerElement = 2; break;
				case Type t when t == typeof(byte): bytesPerElement = 1; break;
				case Type t when t == typeof(float): bytesPerElement = sizeof(float); break;
				case Type t when t == typeof(double): bytesPerElement = sizeof(double); break;
				case Type t when t == typeof(long): bytesPerElement = 8; break;
				case Type t when t == typeof(int): bytesPerElement = 4; break;
				case Type t when t == typeof(short): bytesPerElement = 2; break;
				case Type t when t == typeof(sbyte): bytesPerElement = 1; break;
				case Type t when t == typeof(char): bytesPerElement = sizeof(char); break;
				case Type t when t == typeof(bool): bytesPerElement = sizeof(bool); break;
				case Type t when t == typeof(decimal): bytesPerElement = sizeof(decimal); break;
				default:
					throw new InvalidOperationException("Unsupported element type");
			}

			int elementCount = byteSequence.Length / bytesPerElement;
			Array elementArray = Array.CreateInstance(elementType, elementCount);

			if (elementType == typeof(ulong))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToUInt64(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(long))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToInt64(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(uint))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToUInt32(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(int))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToInt32(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(ushort))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToUInt16(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(short))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToInt16(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(float))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToSingle(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(double))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToDouble(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(byte))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(byteSequence[i * bytesPerElement], i);
			else if (elementType == typeof(sbyte))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue((sbyte)byteSequence[i * bytesPerElement], i);
			else if (elementType == typeof(char))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToChar(byteSequence, i * bytesPerElement), i);
			else if (elementType == typeof(bool))
				for (int i = 0; i < elementArray.Length; i++)
					elementArray.SetValue(BitConverter.ToBoolean(byteSequence, i * bytesPerElement), i);

			return elementArray;
		}
	}


	private static class ParserBtt
	{
		public static object ParseValue (string[] lines, Type type, ref int lineNum, int tabOffset=0)
		{
			string currentLine = lines[lineNum];
			int currentTabCount = CountTabs(currentLine);

			if (currentTabCount != tabOffset)
				throw new InvalidOperationException($"Expected tab offset: {tabOffset}, but got: {currentTabCount}");

			currentLine = currentLine.Trim();

			var parts = currentLine.Split(' ');
			if (parts.Length < 3)
				throw new FormatException($"Invalid line format: {currentLine}");

			string fieldName = parts[0];
			string valueString = parts[1];
			string fieldType = parts[2].Trim('(', ')'); // Not used - it's a copy of UYAML decoder

			//multiline:
			if (valueString == null || valueString.Length == 0)
			{
				string nextLine = lines[lineNum+1];
				//string nextNextLine = lineNum+2<lines.Length ? lines[lineNum+2] : "";

				//array (vector)
				if (fieldType == "vector")
					return ExtractArray(lines, type, ref lineNum, tabOffset);

				//struct with labbeled fields - one more level of tabs
				else if (CountTabs(nextLine) == currentTabCount+1)
				{
					object instance = Activator.CreateInstance(type);
					return ExtractMultilineValue(lines, instance, type, ref lineNum, tabOffset+1);
				}

				//null/empty string
				else
					return null;
			}

			//vector4val (42 42 42 42) (Vector4f)
			else if (valueString.Contains("("))
			{
				object instance = Activator.CreateInstance(type);
				return ExtractSinglelineValue(instance, type, valueString, fieldType);
			}
		
			//serializedUnitIds: 0200000000de7c7f0300000000de7c7f0400000000de7c7f0500000000de7c7f
			//else if (type.IsArray  &&  type.GetElementType().IsPrimitive) //string goes multiline
			//	return ExtractBinaryArray(type.GetElementType(), valueString);
		
			//longval 42 (SInt64)
			else
				return ExtractPrimitiveValue(type, valueString);
		}

		private static object ExtractSinglelineValue (object instance, Type instanceType, string valueString, string typeString)
		//vector4val (42 42 42 42) (Vector4f)
		//rectOffsetVal (m_Left 0 m_Right 0 m_Top 0 m_Bottom 0) (RectOffset)
		{
			string trim = valueString.Trim('(', ')');
			string[] nLines = trim.Split(' ');

			for (int n=0; n<nLines.Length; n++)
			{
				string nLine = nLines[n];

				string nFieldName = null;
				string nValueString;
				if (!nLine.Contains(' '))
				{
					if (typeString != "ColorRGBA")
					//vector4val (42 42 42 42) (Vector4f)
						switch (n)
						{
							case 0: nFieldName = "x"; break;
							case 1: nFieldName = "y"; break;
							case 2: nFieldName = "z"; break;
							case 3: nFieldName = "w"; break;
						}

					else
					//colorval (0 0 0 0) (ColorRGBA)
						switch (n)
						{
							case 0: nFieldName = "r"; break;
							case 1: nFieldName = "g"; break;
							case 2: nFieldName = "b"; break;
							case 3: nFieldName = "a"; break;
						}

					nValueString = nLine;
				}

				else
				//rectOffsetVal (m_Left 0 m_Right 0 m_Top 0 m_Bottom 0) (RectOffset)
				{
					string[] nParts = nLine.Split(' ');
					nFieldName = nParts[0].Trim();
					nValueString = nParts[1].Trim();
				}

				FieldInfo nFieldInfo = instanceType.GetField(nFieldName);
				Type nType = nFieldInfo.FieldType;

				object nVal = ExtractPrimitiveValue(nType, nValueString);

				nFieldInfo.SetValue(instance, nVal);
			}

			return instance;
		}


		public static object ExtractMultilineValue (string[] lines, object instance, Type instanceType, ref int lineNum, int tabOffset=0)
		//coordval  (Coord)
		//	x 0 (int)
		//	z 0 (int)
		{
			lineNum++;

			while (lineNum < lines.Length)
			{
				string nextLine = lines[lineNum];
				int nextTabCount = CountTabs(nextLine);
				if (nextTabCount <= tabOffset) break; // Reached the end of the current struct

				string[] nParts = nextLine.Split(' ');
				string nFieldName = nParts[0].Trim();
				FieldInfo nFieldInfo = instanceType.GetField(nFieldName);
				Type nType = nFieldInfo.FieldType;

				object nVal = ParseValue(lines, nType, ref lineNum, tabOffset+1);
				
				nFieldInfo.SetValue(instance, nVal);
			}

			lineNum--; //returning line num to the last line of this struct
			return instance;
		}


		private static Array ExtractArray (string[] lines, Type arrayType, ref int lineNum, int tabOffset=0)
		//serializedMissingOutletTypes  (vector)
		//	size 3 (int)
		//	data "Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
		//	data "Den.Tools.Matrices.MatrixWorld, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
		//	data "Den.Tools.TransitionsList, Den.Tools.Unity, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null" (string)
		//intArray  (vector)
		//	size 5 (int)
		//	data (int) #0: 1 2 3 4 5
		{
			Type elementType = arrayType.GetElementType();

			lineNum++;
			int length = int.Parse(lines[lineNum].Trim().Split(' ')[1], CultureInfo.InvariantCulture);

			Array array = Array.CreateInstance(elementType, length);

			if (!lines[lineNum+1].Contains("\"")  &&  lines[lineNum+1].Contains(") #0: "))
			//data (int) #0: 1 2 3 4 5
			//and not string to avoid accidentally ) #0:  in string
			{
				string nextLine = lines[lineNum+1].Trim();
				string[] valStrings = nextLine.Split(' ');

				for (int i=3; i<valStrings.Length; i++)
				{
					object val = ExtractPrimitiveValue(elementType, valStrings[i]);
					array.SetValue(val, i-3);
				}
			}

			else
			{
				int i=0;
				while (lineNum < lines.Length)
				{
					string nextLine = lines[lineNum+1];
					int nextTabCount = CountTabs(nextLine);

					if (nextTabCount <= tabOffset)
						break;

					string[] elements;  //field, val, type
					if (nextLine.Contains('"')) //if string with spaces
						elements = nextLine.Split('"');
					else
						elements = nextLine.Split(' ');
					object val = ExtractPrimitiveValue(elementType, elements[1]);
					array.SetValue(val, i);

					lineNum++;
					i++;
				}
			}

			return array;
		}
	}


	private static object ExtractPrimitiveValue(Type type, string valueString)
	{
		if (type == typeof(int)) return int.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(float)) return float.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(double)) return double.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(bool)) return valueString == "1";
		if (type == typeof(string)) return valueString;
		if (type == typeof(ulong)) return ulong.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(long)) return long.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(short)) return short.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(byte)) return byte.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(char)) return (char)ushort.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(uint)) return uint.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(sbyte)) return sbyte.Parse(valueString, CultureInfo.InvariantCulture);
		if (type == typeof(ushort)) return ushort.Parse(valueString, CultureInfo.InvariantCulture);

		throw new NotSupportedException($"Unsupported primitive type: {type}");
	}


	private static int CountTabs(string line)
	{
		int count = 0;
		foreach (var ch in line)
		{
			if (ch == '\t') count++;
			else break;
		}
		return count;
	}


	private static bool IsUyaml (string[] lines)
	//b2t doesn't use ':' unless it's string
	//each line of uyaml use either ':' or '-'
	{
		foreach (var line in lines)
		{
			if (line.Contains(':')  &&  !line.Contains('\"')  &&   !line.Contains('\''))
				return true;

			if (!line.Contains(':')  &&  !line.Contains('-'))
				return false;
		}

		return false;
	}
}