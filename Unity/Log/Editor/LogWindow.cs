﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Den.Tools.GUI;
using UnityEngine.UIElements;
using Den.Tools.SceneEdit;
using static UnityEditor.Search.SearchColumn;
using UnityEngine.Profiling;
using Unity.Profiling;
using System.Xml.Linq;
using UnityEditor.Hardware;

namespace Den.Tools
{
	public class LogWindow : EditorWindow
	{
		UI toolbarUI = new UI();
		UI previewUI = UI.ScrolledUI( new Vector2(0,60) ); 
		ScrollZoom customZoom = new ScrollZoom();
		float millisecondHeight = 0.0001f;
		float zoom = 1;


		const int toolbarWidth = 260; //128+4
		int columnWidth = 30;

		long lastEntryTime; //to draw entries in order

		enum GropupBy { None, Id, Thread };
		GropupBy groupBy = GropupBy.Id;

		//per-frame cache
		[NonSerialized] List<Log.Entry> entries = new List<Log.Entry>(); //cached entities to avoid enumerator for each column

		class ColumnInfo
		{
			public string id = "";  //whether threadNum.ToString or idName, based on what is currently chosen
			public string info = "";  //other data (if name selected - then threads, or vice versa)
			public bool selected = false;
		}
		[NonSerialized] List<ColumnInfo> columnInfos = new List<ColumnInfo>();

		[NonSerialized] List<int> posYs = new List<int>(); //for each entity calculate it's Y coordinate beforehand
		[NonSerialized] List<int> timePosYs = new List<int>(); //and also time-based pos //where it should be placed without offset
		[NonSerialized] int firstEntryOnScreen = 0;
		[NonSerialized] int lastEntryOnScreen = 0;

		long firstTimeEntry = 0;
		int lastHeight = 0; //to go to bottom
		
		bool timeBased = false;
		bool columnsId;
		bool isAnyColumnSelected = false;
		Rect displayRect;
		GUIStyle blackLabel;
		GUIStyle blackCenterLabel;
		GUIStyle frameStyle; 
		GUIStyle frameStyleDisabled;

		string search;




		private void OnGUI () 
		{
			titleContent = new GUIContent("Thread Log");

			previewUI.Draw(DrawMain, inInspector:false);
			toolbarUI.Draw(DrawToolbar, inInspector:false);
		}

		[System.NonSerialized] private bool testLogCreated = false;
		private void CreateTestLog ()
		{
			long time = DateTime.Now.Ticks - System.Diagnostics.Process.GetCurrentProcess().StartTime.Ticks;

			Log.enabled = true;
			Log.Add("I'm test entry!", idName:null, obj:null);
			Log.Add("Another entry!", idName:null, obj:null);
			//Log.Add("Entry with fields", idName:null, obj:null, new object[] {("Field1", 1), ("Field2", 2)});
			Log.Add("Threaded entry!", idName:"Thread 1", obj:null);
			Log.Add("Another thread", idName:"Thread 2", obj:null);
			Log.enabled = false;
		}

		private void DrawToolbar ()
		{
			using (Cell.LinePx(20))
			{
				Draw.Button();

				//Graph graph = CurrentGraph;
				//Graph rootGraph = mapMagic.graph;

				//if (mapMagic != null  &&  mapMagic.graph!=graph  &&  mapMagic.graph!=rootGraph) mapMagic = null;

				UI.current.styles.Resize(0.9f);  //shrinking all font sizes

				Draw.Element(UI.current.styles.toolbar);

				using (Cell.RowPx(60))
				{
					Draw.CheckButton(ref Log.enabled, style:UI.current.styles.toolbarButton);
					Draw.Label("Record", style:UI.current.styles.boldLabel);
				}

				using (Cell.RowPx(60))
				{
					if (Draw.Button("", style:UI.current.styles.toolbarButton))
						Log.Clear();
					Draw.Label("Clear", style:UI.current.styles.label);
				}

				using (Cell.RowPx(60))
					Draw.CheckButton(ref timeBased, "Time-based", style:UI.current.styles.toolbarButton);

				using (Cell.RowPx(40))
					if (Draw.Button("Test", style:UI.current.styles.toolbarButton))
						CreateTestLog();

				using (Cell.RowPx(160))
					Draw.Toggle(ref columnsId, "Columns Id");

				using (Cell.RowPx(160))
					Draw.Field(ref groupBy, "Group By");

				using (Cell.RowPx(60))
					Draw.Field(ref columnWidth, "Column Width");

				using (Cell.RowPx(160))
					Draw.Field(ref search);
				using (Cell.RowPx(30))
					if (Draw.Button("🔍"))
						Search(search);

				//using (Cell.RowPx(30)) Draw.Label("Zoom");
				//using (Cell.RowPx(100)) Draw.Field(ref previewUI.scrollZoom.zoom.x, "Hor"); 
				//using (Cell.RowPx(100)) Draw.Field(ref previewUI.scrollZoom.zoom.x, "Ver"); 

				Cell.EmptyRow();

				using (Cell.RowPx(40))
					if (Draw.Button("To Top", style:UI.current.styles.toolbarButton))
					{
						float scrollX = previewUI.scrollZoom.scroll.x;
						previewUI.scrollZoom.FocusWindowOn( new Vector2(0,0), position.size);
						previewUI.scrollZoom.scroll.x = scrollX;
					}

				using (Cell.RowPx(50))
					if (Draw.Button("To Bottom", style:UI.current.styles.toolbarButton))
					{
						float scrollX = previewUI.scrollZoom.scroll.x;
						previewUI.scrollZoom.FocusWindowOn( new Vector2(0,lastHeight), position.size);
						previewUI.scrollZoom.scroll.x = scrollX;
					}
			}
		}


		private void DrawMain ()
		{
			if (Log.root.subs == null)
				return;

			if (Event.current.isScrollWheel)
				ScrollZoom(ref UI.current.scrollZoom.scroll, ref zoom);

			//caching
			Profiler.BeginSample("Caching");
			{
				entries.Clear();
				foreach (Log.Entry entry in Log.AllEntries())
					entries.Add(entry);

				UpdateColumnInfos();

				isAnyColumnSelected = false;
				foreach (ColumnInfo columnInfo in columnInfos)
					if (columnInfo.selected)
						isAnyColumnSelected = true;

				CalculateHeights();
			}
			Profiler.EndSample();

			//styles
			previewUI.styles.OverridePro(false);
			displayRect = new Rect(0, 0, Screen.width, Screen.height);
			blackLabel = new GUIStyle(UnityEditor.EditorStyles.label);
			blackLabel.active.textColor = blackLabel.normal.textColor =  blackLabel.focused.textColor = Color.black;
			blackCenterLabel = new GUIStyle(blackLabel);
			blackCenterLabel.alignment = TextAnchor.MiddleCenter;

			//background
			Profiler.BeginSample("Background");
			{
				float backgroundColor = 0.3f;
				Draw.Background(displayRect, new Color(backgroundColor,backgroundColor,backgroundColor));
			}
			Profiler.EndSample();
			
			//dragging
			Profiler.BeginSample("Dragging");
				for (int c=0; c<columnInfos.Count; c++)
					DragColumn(c);

				string dragColumnId = null;
				if (DragDrop.obj is string dragString)
					dragColumnId = dragString;
			Profiler.EndSample();

			//lines
			Profiler.BeginSample("Lines");
			{
				for (int c=0; c<columnInfos.Count; c++)
				{
					int posX = (int)(columnWidth*c);

					if (posX + UI.current.scrollZoom.scroll.x + columnWidth + 100 < 0)
						continue;
					if (posX + UI.current.scrollZoom.scroll.x > Screen.width)
						continue;

					Draw.StaticAxis(displayRect, posX+columnWidth, true, Color.gray);
					Draw.StaticAxis(displayRect, posX, true, Color.gray);
				}
			}
			Profiler.EndSample();

			//drawing
			Profiler.BeginSample("Drawing");
			{
				for (int c=columnInfos.Count-1; c>=0; c--) //reverse order to draw time line links
				{
					if (columnInfos[c].id != dragColumnId)
						DrawColumn(c);
				}
			}
			Profiler.EndSample();

			//drawing ids
			Profiler.BeginSample("Drawing Ids");
			{
				for (int c=columnInfos.Count-1; c>=0; c--) //reverse order to draw time line links
				{
					if (columnInfos[c].id != dragColumnId)
						DrawColumn(c, idOnly:true);
				}
			}
			Profiler.EndSample();

			//drawing draged
			Profiler.BeginSample("Drawing Dragged");
			{
				if (dragColumnId != null)
					for (int c=0; c<columnInfos.Count; c++)
						if (columnInfos[c].id == dragColumnId)
							DrawColumn(c, DragDrop.totalDelta.x);
			}
			Profiler.EndSample();
		}


		private void DragColumn (int num)
		{
			ColumnInfo columnInfo = columnInfos[num];
			string id = columnInfo.id;
			string info = columnInfo.info;

			Vector2 posScreen = UI.current.scrollZoom.ToScreen( new Vector2(columnWidth*num, 40) );
			float columnWidthScreen = columnWidth * UI.current.scrollZoom.zoom.x;


			if (DragDrop.TryDrag(name, UI.current.mousePos))
			{
				//Cell.current.pixelOffset += DragDrop.totalDelta; //offsetting cell position with the mouse
			}

			if (DragDrop.TryRelease(name))
			{
				float releasedColumnPos = columnWidth*num + DragDrop.totalDelta.x;
				if (DragDrop.totalDelta.x<0) releasedColumnPos +=  + columnWidth;
				int flooredPos = (int)(releasedColumnPos/columnWidth); 
				if (flooredPos<0) flooredPos=0;
				if (flooredPos>=columnInfos.Count) flooredPos = columnInfos.Count-1;
				
				columnInfos.RemoveAt(num);
				columnInfos.Insert(flooredPos, columnInfo);
			}

			Vector2 pos = new Vector2(columnWidth*num, 20);
			pos.y = UI.current.scrollZoom.ToInternal(pos).y;
			DragDrop.TryStart(name, UI.current.mousePos, new Rect(pos, new Vector2(columnWidthScreen,20)));
		}


		private void DrawColumn (int num, float offset=0, bool idOnly=false) //offset is for dragging
		{
			ColumnInfo columnInfo = columnInfos[num];
			string id = columnInfo.id;
			string info = columnInfo.info;
			

			float posX = columnWidth*num + offset;

			//optimizing
			if (posX + UI.current.scrollZoom.scroll.x + columnWidth + 100 < 0)
				return;
			if (posX + UI.current.scrollZoom.scroll.x > Screen.width)
				return;

			Vector2 posScreen = UI.current.scrollZoom.ToScreen( new Vector2(posX, 20) );
			float columnWidthScreen = columnWidth * UI.current.scrollZoom.zoom.x;
			
			//entries
			bool selected = true;
			if (isAnyColumnSelected)
				selected = columnInfo.selected;

			if (!idOnly)
				for (int e=firstEntryOnScreen; e<lastEntryOnScreen; e++)
				{
					Log.Entry entry = entries[e];

					bool drawEntry = IfDrawEntry(entry, id);

					if (drawEntry)
						DrawEntry(entry, (int)posX, posYs[e], timePosYs[e], columnInfo.selected );
				}

			//selection
			if (!idOnly)
			using (Cell.Static(posScreen.x, 35, columnWidthScreen, 1000))
				if (Draw.Button(visible:false))
					columnInfo.selected = !columnInfo.selected;

			//id
			using (Cell.Static(posScreen.x, 20, columnWidthScreen, 20))
			//using (Cell.Custom(posX, posY, columnWidth, 20))
			{
				Draw.Rect(Cell.current.InternalRect.Extended(1), new Color(0.25f, 0.25f, 0.25f, 1));
				Draw.Label(id, blackCenterLabel);
			}

			//printing additional info
			using (Cell.Static(posScreen.x, 40, columnWidthScreen, 20))
			{
				Draw.Rect(Cell.current.InternalRect.Extended(1), new Color(0.25f, 0.25f, 0.25f, 1));
				Draw.Label(info, blackCenterLabel, tooltip:info);
			}
		}


		private int DrawEntry (Log.Entry entry, int posX, int posY, int timePosY, bool selected)
		///Returns the height of the entry
		{
			Vector2 pos = UI.current.scrollZoom.ToScreen( new Vector2(posX, posY) );
			Vector2 timePos = UI.current.scrollZoom.ToScreen( new Vector2(posX, timePosY) );

			float labelWidth = UnityEditor.EditorStyles.label.CalcSize( new GUIContent(entry.name) ).x;

			if (timeBased)
			{
				using (Cell.Static(timePos.x+6, timePos.y, 3, 2.99f))
					Draw.Rect(Color.black);

				using (Cell.Static(timePos.x+7, timePos.y, 0.9f, pos.y-timePos.y)) 
					Draw.Rect(Color.black);
			}

			using (Cell.Static(pos.x+2, pos.y, labelWidth+20, 20))
			//using (Cell.LineStd)
			//	using (Cell.Custom(posX+2, 2, width, 20))
			{
				if (!UI.current.layout)
				{
					if (frameStyle == null  ||  frameStyle.normal.background == null)
						frameStyle = UI.current.textures.GetElementStyle("LogEntry", 
							borders: new RectOffset(10,2,6,2),
							overflow: new RectOffset(0,0,0,0) );

					if (frameStyleDisabled == null ||  frameStyleDisabled.normal.background == null)
						frameStyleDisabled = UI.current.textures.GetElementStyle("LogEntryDisabled", 
							borders: new RectOffset(10,2,6,2),
							overflow: new RectOffset(0,0,0,0) );

					Draw.Element(selected ? frameStyle : frameStyleDisabled);
				}

				using (Cell.Padded(2, 2, 2, 0))
					
				{
					if (entry.fieldValues == null  ||  entry.fieldValues.Length == 0)
					{
						using (Cell.LinePx(18 / UI.current.scrollZoom.zoom.y))
							Draw.Label(entry.name, blackLabel);
					}

					else
					{
						using (Cell.LinePx(18 / UI.current.scrollZoom.zoom.y))
							Draw.FoldoutLeft(ref entry.guiExpanded, entry.name, style:blackLabel);

						if (entry.guiExpanded)
						{
							foreach ((string n,string v) in entry.fieldValues)
								using (Cell.LinePx(17  / UI.current.scrollZoom.zoom.y))
								{
									using (Cell.Row) Draw.Label(n, blackLabel);
									using (Cell.Row) Draw.Label(v, blackLabel, tooltip:v);
								}
						}
					}
				}

				return (int)Cell.current.pixelSize.y;
			}
		}

		private int GetEntryHeight (Log.Entry entry)
		{
			int guiHeight = 20;
			if (entry.guiExpanded)
				guiHeight += entry.fieldValues.Length*17;
			return (int)(guiHeight / UI.current.scrollZoom.zoom.y);
		}

		private string GetEntryColumnId (Log.Entry entry)
		{
			return groupBy switch
			{
				GropupBy.Id => entry.idName ?? "Null",
				GropupBy.Thread => entry.thread.ToString(),
				_ => "Main"
			};
		}

		private string GetEntryColumnAdditionalInfo (Log.Entry entry)
		{
			return groupBy switch
			{
				GropupBy.Id => entry.thread.ToString(),
				GropupBy.Thread => entry.idName ?? "Null",
				_ => "Main"
			};
		}

		private bool IfDrawEntry (Log.Entry entry, string columnId)
		{
			bool drawEntry = groupBy switch
			{
				GropupBy.Id => entry.idName==columnId,
				GropupBy.Thread => entry.thread.ToString()==columnId,
				_ => true
			};

			if (columnId=="Null" && groupBy==GropupBy.Id && entry.idName==null)
				drawEntry = true;

			return drawEntry;
		}


		private void UpdateColumnInfos ()
		{
			Profiler.BeginSample("UpdateColumnNames");

			//columnNames.Clear();
			//this will ruin custom columns order

			/*HashSet<string> usedIds = new HashSet<string>(); //names used currently
			HashSet<string> usedColumnNames = new HashSet<string>(); //names used previously, in columns, may contain names currently non-exist
			foreach (ColumnInfo info in columnInfos)
				usedColumnNames.Add(info.id);*/

			HashSet<string> idsToAdd = new HashSet<string>();
			HashSet<string> idsExisting = new HashSet<string>();
			foreach (ColumnInfo info in columnInfos)
				idsExisting.Add(info.id); //adding already existing ids - they should not create new columns

			HashSet<string> idsToRemove = new HashSet<string>();
			foreach (ColumnInfo info in columnInfos)
				idsToRemove.Add(info.id);

			Dictionary<string,HashSet<string>> idToInfo = new Dictionary<string, HashSet<string>>(); //storing secondary info in hashset btw

			//gathering ids
			foreach (Log.Entry sub in entries)
			{
				string id = GetEntryColumnId(sub);
				string info = GetEntryColumnAdditionalInfo(sub);

				if (!idsExisting.Contains(id))
					idsToAdd.Add(id);

				if (idsToRemove.Contains(id))
					idsToRemove.Remove(id);

				if (!idToInfo.TryGetValue(id, out HashSet<string> infos))
				{ 
					infos = new HashSet<string>(); 
					idToInfo.Add(id, infos);
				}
				if (!infos.Contains(info))
					infos.Add(info);
			}

			//adding new columns
			foreach (string id in idsToAdd)
				columnInfos.Add( new ColumnInfo() { id=id, info=null  } );

			//removing non-existing names
			for (int i=columnInfos.Count-1; i>=0; i--)
			{
				if (idsToRemove.Contains(columnInfos[i].id))
					columnInfos.RemoveAt(i);
			}

			//updating infos
			foreach (ColumnInfo columnInfo in columnInfos)
			{
				HashSet<string> infos = idToInfo[columnInfo.id];
				columnInfo.info = infos.ToStringMemberwise();
			}

			Profiler.EndSample();
		}


		private void CalculateHeights ()
		{
			Profiler.BeginSample("CalculateHeights");

			if (timeBased) //TODO: why just don't take first?
			{
				firstTimeEntry = long.MaxValue;
				foreach (Log.Entry entry in entries)
					firstTimeEntry = Math.Min(firstTimeEntry,entry.startTicks);
			}

			posYs.Clear();
			timePosYs.Clear();

			firstEntryOnScreen = 0;
			lastEntryOnScreen = entries.Count;

			int posY = 60;
			int timePosY = posY;
			for (int e=0; e<entries.Count; e++)
			{
				Log.Entry entry = entries[e];

				if (timeBased)
				{
					timePosY = (int)((entry.startTicks-firstTimeEntry)*millisecondHeight*zoom)+5; //where it should be placed without offset
					posY = Math.Max(posY, timePosY);
				}

				posYs.Add(posY);
				timePosYs.Add(timePosY);
					
				int height = GetEntryHeight(entry);
				posY += height;

				//min and max on screen
				if (posY+height + UI.current.scrollZoom.scroll.y < 0  &&   timePosY+height + UI.current.scrollZoom.scroll.y < 0)
					firstEntryOnScreen = e;

				if (posY + UI.current.scrollZoom.scroll.y < Screen.height  &&   timePosY + UI.current.scrollZoom.scroll.y < Screen.height)
					lastEntryOnScreen = e;
			}


			Profiler.EndSample();
		}


		private void Search (string search)
		{
				Log.Entry foundEntry = null;
				int searchHeight = 0;

				//finding entry and height
				int i = 0;
				foreach (Log.Entry entry in entries)
				{
					if (entry.name.ToLower().Contains(search.ToLower()))
					{
						foundEntry = entry;
						searchHeight = posYs[i];
						break;
					}
					i++;
				}
				if (foundEntry == null)
				{
					Debug.Log("No entry found");
					return;
				}

				//finding column
				int c=0;
				for (c=0; c<columnInfos.Count; c++)
				{
					if (IfDrawEntry(foundEntry, columnInfos[c].id))
						break;
				}

				previewUI.scrollZoom.scroll.y = -searchHeight;
				previewUI.scrollZoom.scroll.x = -c*columnWidth;
				ScrollZoom(ref UI.current.scrollZoom.scroll, ref zoom);
		}


		public static void ScrollZoom (ref Vector2 scroll, ref float zoom, float step=1.25f, float minZoom=0.0025f, float maxZoom=2)
		{
			if (Event.current == null) 
				return;

			//reading control
			#if UNITY_EDITOR_OSX
			bool control = Event.current.command;
			#else
			bool control = Event.current.control;
			#endif

			float delta = 0;
			if (Event.current.type == EventType.ScrollWheel) delta = Event.current.delta.y / 3f;
			if (Mathf.Abs(delta) < 0.001f) 
				return;

			//zooming
			if (Event.current.control)
			{
				scroll.y -= Event.current.mousePosition.y;

				if (delta > 0)
				{
					scroll.y /= step;
					zoom /= step;
				}
				else
				{
					scroll.y *= step;
					zoom *= step;
				}

				scroll.y += Event.current.mousePosition.y;
			}

			//scrolling
			else
			{
				if (delta > 0)
					scroll.y -= 20;

				else
					scroll.y += 20;
			}

			#if UNITY_EDITOR
			//UnityEditor.EditorWindow.focusedWindow?.Repaint();
			UI.current?.editorWindow?.Repaint(); 
			#endif
		}


		[MenuItem ("Window/MapMagic/Log")]
		public static void ShowEditor ()
		{
			EditorWindow.GetWindow<LogWindow>("Log");
		}
	}
}