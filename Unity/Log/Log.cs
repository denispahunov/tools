﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Reflection;
using UnityEngine;

namespace Den.Tools
{
	public static class Log
	{
		public class Entry : IDisposable
		{
			public string name; //caption, actually
			public int thread;
			public string idName; //id should be unique, while name shouldn't. Like id=thread, idName=coord for tile
			public long startTicks;
			public long disposeTicks;
			public (string,string)[] fieldValues;
			public ConcurrentQueue<Entry> subs;
			public bool guiExpanded;
			public Color color; //do display errors or warnings

			public void Dispose () => Log.DisposeGroup();

			public int Count
			{get{
				int count = 1;
				if (subs!=null) 
					foreach (Entry sub in subs)
						count += sub.Count;
				return count;
			}}

			public IEnumerable<Entry> SubsRecursive ()
			{
				if (subs==null) yield break;
				foreach (Entry sub in subs)
				{
					yield return sub;

					if (sub.subs != null)
						foreach (Entry subSub in sub.SubsRecursive())
							yield return subSub;
				}
			}
		}

		public static bool enabled = false; 
		private static long unityStartTime = 0;

		public static Entry root = new Entry() {name="Root"};
		private static Entry activeGroup = root; //not among openedGroups  //TODO: make a dictionary thread->group
		private static List<Entry> openedGroups = new List<Entry>();

		private static Entry tempGroup = new Entry(); //to return when recording disabled

		public const string defaultId = "Default";

		
		public static void AddThreaded (string name, string idName=defaultId, Color color=new Color(), params (string,object)[] additional) => 
			Add(name, idName,  Thread.CurrentThread.ManagedThreadId, null, color, additional);

		public static void AddThreaded (string name, string idName=defaultId, params (string,object)[] additional) => 
			Add(name, idName,  Thread.CurrentThread.ManagedThreadId, null, new Color(), additional);

		public static void AddThreaded (string name, params (string,object)[] additional) => 
			Add(name, null,  Thread.CurrentThread.ManagedThreadId, null, new Color(), additional);


		public static void Add (string name, string idName=defaultId, params (string,object)[] additional) => 
			Add(name, idName, Thread.CurrentThread.ManagedThreadId, null, new Color(), additional);

		public static void Add (string name, params (string,object)[] additional) => 
			Add(name, null, Thread.CurrentThread.ManagedThreadId, null, new Color(), additional);

		public static void Add (string name, string idName=defaultId, int thread=-1, object obj=null, Color color=new Color(), params (string,object)[] additional)
		{
			if (!enabled) return;

			Entry entry = new Entry() {name=name, thread=thread};

			if (thread < 0)
				thread =  Thread.CurrentThread.ManagedThreadId;

			if (unityStartTime == 0)
				unityStartTime = System.Diagnostics.Process.GetCurrentProcess().StartTime.Ticks;
			long currentTime = DateTime.Now.Ticks; //todo: minimize operations after DateTime.Now
			entry.startTicks = currentTime - unityStartTime;

			entry.idName = idName;
			entry.color = color;

			if (obj != null)
				entry.fieldValues = ReadValues(obj);

			if (additional != null)
			{
				(string,string)[] sVals = new (string, string)[additional.Length];
				for (int i=0; i<sVals.Length; i++)
				{
					sVals[i] = new (additional[i].Item1, null);
					object item2 = additional[i].Item2;
					if (item2 is string sItem2)
						sVals[i].Item2 = sItem2;
					else
						sVals[i].Item2 = item2?.ToString();
				}
				if (entry.fieldValues==null || entry.fieldValues.Length==0) 
					entry.fieldValues = sVals;
				else
					entry.fieldValues = ArrayTools.Append(entry.fieldValues, sVals);
			}
			
			lock (root)
			{
				if (activeGroup.subs == null) activeGroup.subs = new ConcurrentQueue<Entry>();
				activeGroup.subs.Enqueue(entry);
			}
		}


		public static Entry Group (string name, string id=defaultId)
		{
			if (!enabled) return tempGroup;

			Entry entry = new Entry() {name=name, idName=id};

			if (activeGroup.subs == null) activeGroup.subs = new ConcurrentQueue<Entry>();
			activeGroup.subs.Enqueue(entry);

			openedGroups.Add(activeGroup);
			activeGroup = entry;

			if (unityStartTime == 0)
				unityStartTime = System.Diagnostics.Process.GetCurrentProcess().StartTime.Ticks;
			long currentTime = DateTime.Now.Ticks; //todo: minimize operations after DateTime.Now
			entry.startTicks = currentTime - unityStartTime;

			return entry;
		}


		private static void DisposeGroup ()
		{
			if (!enabled) return;

			long currentTime = DateTime.Now.Ticks;
			long unityStartTime = System.Diagnostics.Process.GetCurrentProcess().StartTime.Ticks;
			activeGroup.disposeTicks = currentTime - unityStartTime;

			activeGroup = openedGroups[openedGroups.Count-1];
			openedGroups.RemoveAt(openedGroups.Count-1);
		}


		private static (string,string)[] ReadValues (object obj)
		{
			Type type = obj.GetType();
			FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

			(string,string)[] fieldValues = new (string,string)[fields.Length];

			for (int i=0; i<fields.Length; i++)
			{
				string name = fields[i].Name;
				string value = fields[i].GetValue(obj).ToString();

				fieldValues[i] = (name,value);
			}

			return fieldValues;
		}


		public static void Clear () 
		{
			if (root.subs != null)
				root.subs.Clear();
		}

		public static int Count => root.Count;

		public static IEnumerable AllEntries ()  //all except root
		{
			foreach (Entry sub in root.SubsRecursive())
				yield return sub;
		}
	}
}