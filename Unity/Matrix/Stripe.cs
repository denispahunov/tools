using System;
using System.Collections;

/// Matrix Stripe Operations

using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UIElements;
using static UnityEditor.PlayerSettings;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Den.Tools.Tests")]


namespace Den.Tools.Matrices 
{
	[Serializable, StructLayout (LayoutKind.Sequential)] //to pass to native
	public class Stripe : IComparable
	{
		//for c++ compatibility. Remove length hell out here!
		public int length;
		public float[] arr; //can be any count for resize, but more than ceil(length)
		public float offset;
		public float size;

		//replace with this
		//public float offset;
		//public float size;
		//public float[] arr; //can be any count for resize, but more than ceil(length)

		public int Length => length; //{get{ int i=(int)size; return i==size ? i : i+1; }} 
			//adding 0.1 epsilon to array length. 
			//Size 24 -> arr length 24, 24.01 -> 25, 23.99 -> 24
			//it should not be 25 when size 24, not (int)(size+1)
		const int one = 1; //converts pixels to grid-anchored, like heightmap 513

		public Stripe () { arr = new float[0]; size = 0; }
		public Stripe (float[] arr) { this.arr = arr; length = arr.Length; size = arr.Length; }
		public Stripe (int length) { this.length = length; this.size = length; arr = new float[length]; }
		public Stripe (float[] arr, int length) { this.arr = arr; this.length = length; this.size = length; if (arr.Length < Length) Resize(Length); }
		public Stripe (float[] arr, float offset, float size) { this.arr = arr; this.length = arr.Length; this.offset = offset; this.size = size; if (arr.Length < length) Resize(length); }
		public Stripe (float[] arr, int length, float offset, float size) { this.arr = arr; this.length = length; this.offset = offset; this.size = size; if (arr.Length < length) Resize(length); }
		public Stripe (Stripe stripe) { this.arr = new float[stripe.arr.Length]; Array.Copy(stripe.arr, arr, stripe.arr.Length); length=stripe.length; offset = stripe.offset; size = stripe.size; }

		public void Resize (int newLength) { length = newLength; ArrayTools.Resize(ref arr, length); }
		public void Fill (float val) { for (int i=0; i<arr.Length; i++) arr[i] = val; }
		public void Fill (float val, int start, int length) { for (int i=0; i<length; i++) arr[i+start] = val; }
		public void Clear () { for (int i=0; i<arr.Length; i++) arr[i] = 0; }
		
		public int CompareTo (object o) 
		{
			if (o is Stripe s  &&  s.length == length  &&  CompareFloats(s.offset, offset)  &&  CompareFloats(s.size, size))
			{
				for (int i=0; i<length; i++)
					if (!CompareFloats(s.arr[i], arr[i]))
						return -1;
				return 1;
			}
			return -1;
		} 
		private static bool CompareFloats (float f1, float f2) => Math.Abs(f1 - (float)f2) <= 0.001f;

		public static void Copy (Stripe src, Stripe dst)
		{ 
			if (dst.arr.Length != src.arr.Length)
				dst.arr = new float[src.arr.Length];
			Array.Copy(src.arr, dst.arr, src.arr.Length); 
			dst.length = src.length;
			dst.offset = src.offset; dst.size = src.size; 
		}

		public static bool EqualValues (Stripe s1, Stripe s2) 
		{
			return 
				s1.length == s2.length  &&
				s1.offset == s2.offset  &&
				s1.size == s2.size &&
				s1.arr.AsSpan().SequenceEqual(s2.arr); //1.5 times faster than for loop
		}

		public static bool EqualValues (Stripe s1, Stripe s2, float delta) 
		{
			return 
				s1.length == s2.length  &&
				s1.offset == s2.offset  &&
				s1.size == s2.size &&
				ArrayTools.EqualValues(s1.arr, s2.arr, delta);
		}

		public static void Swap (Stripe s1, Stripe s2) { float[] t=s1.arr; s1.arr=s2.arr; s2.arr=t; }

		public float PixelToWorld (float pix)
		{
			//double pixelSize = (double)length / size;
			//return (float)((pix + 0.5f )/pixelSize) + offset; //+- 0.5 is a half-pixel offset. It makes matrices behave the same way resizing works in Photoshop
			float pixelSize = size / (length-one);
			return pix*pixelSize + offset;
		}

		/*public float PixelToWorldInterpolated (float pix)
		{
			double pixelSize = (double)length / size;
			return (float)((pix + 0.5f)/pixelSize)  + offset; //+- 0.5 is a half-pixel offset. It makes matrices behave the same way resizing works in Photoshop
		}*/

		public float WorldToPixelInterpolated (float pos)
		{
			float pixelSize = size / (length-one);
			return (pos-offset)/pixelSize;
		}

		public int WorldToPixelInt (float pos)
		{
			float pixelSize = size / (length-one);
			float pix = (pos-offset)/pixelSize;
			return (int)(pix+0.5f);
			//double pixelSize = (double)length / size;
			//return (int)((pos-offset)*pixelSize);
		}




		#region Resampling

			#if MM_NATIVE && (UNITY_EDITOR || !UNITY_ANDROID && !ENABLE_IL2CPP)
				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "MatrixOps_ResampleStripeCubic")]
				internal static extern void ResampleStripeCubic (Stripe src, Stripe dst, float srcOffset=0, float srcLength=0);
				/// Scales the stripe filling dst with interpolated values. Cubic for upscale
			#else
				internal static void ResampleStripeCubic (Stripe src, Stripe dst, float srcOffset=0, float srcLength=0)
				/// Scales the stripe filling dst with interpolated values. Cubic for upscale
				{
					if (srcLength<1) srcLength = src.length;

					for (int dstX=0; dstX<dst.length; dstX++)
					{
						float srcX = (float)dstX * srcLength / dst.length  + srcOffset;

						int px = (int)srcX;	if (px<0) px=0;
						int nx = px+1;		if (nx>src.length-1) nx = src.length-1;
						int ppx = px-1;		if (ppx<0) ppx = 0;
						int nnx = nx+1;		if (nnx>src.length-1) nnx = src.length-1;
						int pppx = ppx-1;	if (pppx<0) pppx = 0;
						int nnnx = nnx+1;	if (nnnx>src.length-1) nnnx = src.length-1;


						float vp = src.arr[px]; float vpp = src.arr[ppx]; float vppp = src.arr[pppx];
						float vn = src.arr[nx]; float vnn = src.arr[nnx]; float vnnn = src.arr[nnnx];

						float p = srcX-px;
						float ip = 1f-p;

						float dpp = vpp - (vppp+vp-vpp*2) * 0.25f;
						float dp = vp - (vpp+vn-vp*2) * 0.25f;
						float dn = vn - (vnn+vp-vn*2) * 0.25f;
						float dnn = vnn - (vnnn+vn-vnn*2) * 0.25f;

						float tp = (dn-dpp)*0.5f;
						float tn = (dp-dnn)*0.5f;

						float l = vp*ip + vn*p; //linear filtration

						float cp = 
							(vp + tp*p) * ip + 
							l * p;

						float cn = l * ip + 
							(vn + tn*ip) * p;

						dst.arr[dstX] =  cp*ip + cn*p;

						//dst.arr[dstX] = vp + 0.5f * p * (vn - vpp + p*(2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + p*(3.0f*(vp - vn) + vnn - vpp)));
							//standard quadratic filtration (here for test purpose)

						if (dst.arr[dstX] > 1) dst.arr[dstX] = 1;
						if (dst.arr[dstX] < 0) dst.arr[dstX] = 0;
					}
				}
			#endif


			#if MM_NATIVE && (UNITY_EDITOR || !UNITY_ANDROID && !ENABLE_IL2CPP)
				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "MatrixOps_ResampleStripeQuadratic")]
				private static extern void ResampleStripeQuadratic (Stripe src, Stripe dst);
				/// Scales the stripe filling dst with interpolated values. Faster than cubic, but can result in "grid" look because of the tangents
			#else
				private static void ResampleStripeQuadratic (Stripe src, Stripe dst)
				/// Scales the stripe filling dst with interpolated values. Faster than cubic, but can result in "grid" look because of the tangents
				{
					for (int dstX=0; dstX<dst.length; dstX++)
					{
						float srcX = (1.0f*dstX / dst.length) * src.length;

						int px = (int)srcX;	if (px<0) px=0;
						int nx = px+1;		if (nx>src.length-1) nx = src.length-1;
						int ppx = px-1;		if (ppx<0) ppx = 0;
						int nnx = nx+1;		if (nnx>src.length-1) nnx = src.length-1;

						float vp = src.arr[px]; float vpp = src.arr[ppx];
						float vn = src.arr[nx]; float vnn = src.arr[nnx];

						float p = srcX-px;
						float ip = 1-p;

						float tp = (vn-vpp)*0.5f;
						float tn = (vp-vnn)*0.5f;

						float l = vp*ip + vn*p; //linear filtration

						float cp = 
							(vp + tp*p) * ip + 
							l * p;

						float cn = l * ip + 
							(vn + tn*ip) * p;

						dst.arr[dstX] =  cp*ip + cn*p;

						//dst.arr[dstX] = vp + 0.5f * p * (vn - vpp + p*(2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + p*(3.0f*(vp - vn) + vnn - vpp)));
							//standard quadratic filtration (here for test purpose)

						if (dst.arr[dstX] > 1) dst.arr[dstX] = 1;
						if (dst.arr[dstX] < 0) dst.arr[dstX] = 0;
					}
				}
			#endif


			#if MM_NATIVE && (UNITY_EDITOR || !UNITY_ANDROID && !ENABLE_IL2CPP)
				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "MatrixOps_ResampleStripeLinear")]
				internal static extern void ResampleStripeLinear (Stripe src, Stripe dst);
				/// Scales the line filling dst with ALL src interpolated values. Linear for downscale
			#else
				internal static void ResampleStripeLinear (Stripe src, Stripe dst)
				/// Scales the line filling dst with interpolated values. Linear for downscale
				{
					float dstToSrc = (float)src.length / (float)dst.length;

					for (int dstX=0; dstX<dst.length; dstX++)
					{
						int srcStartX = (int)((dstX-1) * dstToSrc - 1);
						if (srcStartX < 0) srcStartX = 0;

						int srcEndX = (int)((dstX+1) * dstToSrc + 2);
						if (srcEndX >= src.length) srcEndX = src.length-1;

						float val = 0;
						float sum = 0;

						for (int srcX = srcStartX; srcX <= srcEndX; srcX++)
						{
							float refX = srcX / dstToSrc; //converting back to dst gauge
							
							float percent = (refX - dstX) * dstToSrc;
							if (percent > 1) percent = 1;
							if (percent < -1) percent = -1;
							if (percent < 0) percent = -percent;
							percent = 1-percent;

							val += src.arr[srcX] * percent;
							sum += percent;
						}

						dst.arr[dstX] = sum!=0 ? val/sum : 0;	
					}
				}
			#endif

		#endregion

		#region New Resampling


				private static int DstToSrcPix (int dstPix, double srcOffset, double srcSize,  double dstOffset, double dstSize)
				{
					double dstPos = dstPix + dstOffset;
					double srcPos = dstPos * (srcSize / dstSize);
					int srcPix = (int)(srcPos - srcOffset);

					if (srcPix < 0) srcPix = 0;
					if (srcPix > srcSize-1) srcPix = (int)(srcSize-1); //Tested

					return srcPix; 
				}


				private static double SrcToDstPixInterpolated (double srcPix, double srcOffset, double srcSize,  double dstOffset, double dstSize)
				{
					double srcPos = srcPix + srcOffset - 0.5f; //+- 0.5 is a half-pixel offset. It makes matrices behave the same way resizing works in Photoshop
					double dstPos = srcPos * (dstSize / srcSize);
					double dstPix = dstPos - dstOffset; // - 0.5f;

					return dstPix;
				}


				private static double DstToSrcPixInterpolated (double dstPix, double srcOffset, double srcSize,  double dstOffset, double dstSize)
				{
					double dstPos = dstPix + dstOffset + 0.5f;
					double srcPos = dstPos * (srcSize / dstSize);
					double srcPix = srcPos - srcOffset - 0.5f;

					return srcPix;
				}


				public static void ResampleNN (Stripe src, Stripe dst)
				{
					float srcPixelSize = src.size/(src.length-1);
					float dstPixelSize = dst.size/(dst.length-1);

					for (int dstPix=0; dstPix<dst.length; dstPix++)
					{
						//float pos = dst.PixelToWorld(dstPix);
						float pos = dstPix*dstPixelSize + dst.offset;
						
						//float srcPix = src.WorldToPixelInterpolated(pos);
						float srcPix = (pos-src.offset) / srcPixelSize;

						int srcPixInt = (int)(srcPix+0.5f);

						if (srcPix < 0) srcPix = 0;
						if (srcPix > src.length-1) srcPix = (int)(src.length-1);
						//if (dstPix < 0) dstPix = 0;
						//if (dstPix > dst.size-1) dstPix = (int)(dst.size-1);

						dst.arr[dstPix] = src.arr[srcPixInt]; 
					}
				}


				public static void UpsampleLinear (Stripe src, Stripe dst)
				{
					float srcPixelSize = src.size/(src.length-1);
					float dstPixelSize = dst.size/(dst.length-1);

					for (int dstPix=0; dstPix<dst.length; dstPix++)
					{
						//float pos = dst.PixelToWorld(dstPix);
						float pos = dstPix*dstPixelSize + dst.offset;
						
						//float srcPix = src.WorldToPixelInterpolated(pos);
						float srcPix = (pos-src.offset) / srcPixelSize;

						int prevSrcPix = (int)srcPix;
						if (srcPix < 0) //srcPix here, flooring and clamping at the same tile
							prevSrcPix = 0;
						if (prevSrcPix >= src.length) 
							prevSrcPix = src.length-1;

						int nextSrcPix = (int)(srcPix+1); 
						if (srcPix+1 < 0) 
							nextSrcPix = 0;
						if (nextSrcPix >= src.length) 
							nextSrcPix = src.length-1;

						float percent = srcPix - prevSrcPix;

						dst.arr[dstPix] = src.arr[prevSrcPix]*(1-percent) + src.arr[nextSrcPix]*percent;
					}
				}


				public static void DownsampleLinear (Stripe src, Stripe dst)
				/// Works the other way round than Upsample - it iterates in source pixels and writes to dst
				/// For explanation see DownsampleLinear_SumArray
				{
					float srcPixelSize = src.size/(src.length-1);
					float dstPixelSize = dst.size/(dst.length-1);

					//finding where on src to start and where to stop
					//float posStart = dst.PixelToWorld(-1);
					//int srcPixStart = Mathf.FloorToInt(src.WorldToPixelInterpolated(posStart));
					//srcPixStart = Mathf.Clamp(srcPixStart, 0, src.length-1);
					float posStart = (-1)*dstPixelSize + dst.offset;
					float srcPixStartF = (posStart-src.offset) / srcPixelSize;
					int srcPixStart = (int)srcPixStartF;
					if (srcPixStartF < 0) //using float here, flooring and clamping at the same time
						srcPixStart = 0;
					if (srcPixStart >= src.length)
						srcPixStart = src.length - 1;

					//float posEnd = dst.PixelToWorld(dst.length-1);
					//int srcPixEnd = Mathf.CeilToInt(src.WorldToPixelInterpolated(posEnd));
					//srcPixEnd = Mathf.Clamp(srcPixEnd, 0, src.length-1);
					float posEnd = (dst.length-1)*dstPixelSize + dst.offset;
					float srcPixEndF = (posEnd-src.offset) / srcPixelSize;
					int srcPixEnd = (int)(srcPixEndF+1);
					if (srcPixEnd < 0)
						srcPixEnd = 0;
					if (srcPixEnd >= src.length)
						srcPixEnd = src.length - 1;

					//downsampling
					int prevDstPixStart = -int.MaxValue;

					float dstPixStartSum = 0;
					float dstPixEndSum = 0;
					float dstPixStartVal = 0;
					float dstPixEndVal = 0;

					for (int srcPix=srcPixStart; srcPix<=srcPixEnd; srcPix++)
					{
						//float pos = src.PixelToWorld(srcPix);
						float pos = srcPix*srcPixelSize + src.offset;

						//float dstPix = dst.WorldToPixelInterpolated(pos);
						float dstPix = (pos-dst.offset) / dstPixelSize;

						int dstPixStart = (int)(dstPix + 0.0001f);  if (dstPix < 0) dstPixStart--;
						int dstPixEnd = dstPixStart + 1;

						//step sum on next dst pixel
						if (dstPixStart != prevDstPixStart) 
						{
							if (prevDstPixStart >= 0  &&  prevDstPixStart < dst.Length  &&  dstPixStartSum != 0)
								dst.arr[prevDstPixStart] = dstPixStartVal / dstPixStartSum; //note that first goes (processed) end, then goes start

							dstPixStartVal = dstPixEndVal;
							dstPixStartSum = dstPixEndSum;

							dstPixEndVal = 0;
							dstPixEndSum = 0;

							prevDstPixStart = dstPixStart;
						}

						float percent = 1-(dstPix-dstPixStart);
						float val = src.arr[srcPix];

						dstPixStartVal += val * percent;
						dstPixStartSum += percent;

						dstPixEndVal += (float)(val*(1-percent));
						dstPixEndSum += 1-percent;
					}

					if (prevDstPixStart >= 0  &&  prevDstPixStart < dst.Length  &&  dstPixStartSum != 0)
								dst.arr[prevDstPixStart] = dstPixStartVal / dstPixStartSum;
					if (prevDstPixStart >= 0  &&  prevDstPixStart+1 < dst.Length  &&  dstPixEndSum != 0)
								dst.arr[prevDstPixStart+1] = dstPixEndVal / dstPixEndSum;

					//filling all other stripe area with beginning and ending values
					{
						//float srcPosStart = src.PixelToWorld(0);
						//float srcPosEnd = src.PixelToWorld(src.length-1);
						//int dstPixStart = Mathf.FloorToInt(dst.WorldToPixelInterpolated(srcPosStart));
						//int dstPixEnd = Mathf.CeilToInt(dst.WorldToPixelInterpolated(srcPosEnd));
						
						float srcPosStart = src.offset;
						float dstPixStartF = (srcPosStart-dst.offset) / dstPixelSize;
						int dstPixStart = (int)dstPixStartF;
						if (dstPixStartF < 0) dstPixStart--;

						float srcPosEnd = (src.length-1)*srcPixelSize + src.offset;
						float dstPixEndF = (posEnd-dst.offset) / dstPixelSize;
						int dstPixEnd = (int)(dstPixEndF+1);
						if (dstPixEnd < 0) dstPixEnd--;

						if (dstPixStart > 0  &&  dstPixStart < dst.length)
							dst.Fill(dst.arr[dstPixStart], 0, dstPixStart);

						if (dstPixEnd < dst.length-1  &&  dstPixEnd > 0)
							dst.Fill(dst.arr[dstPixEnd], dstPixEnd, dst.length-dstPixEnd);
					}
				}


				[Obsolete] public static void DownsampleLinear_SumArray (Stripe src, Stripe dst)
				/// Exactly the same as DownsampleLinear, but uses temporary array
				/// Mostly here for the debug purpose, and as explanation on how things work
				{
					//resetting dst since we'll fill it additively
					for (int dstPix=0; dstPix<dst.length; dstPix++)
						dst.arr[dstPix] = 0;

					float[] sums = new float[dst.arr.Length]; //replaced with step-by-step floats, but leaving here for debug purpose

					for (int srcPix=0; srcPix<src.length; srcPix++)
					{
						float pos = src.PixelToWorld(srcPix);
						float dstPix = dst.WorldToPixelInterpolated(pos);

						int dstPixStart = (int)(dstPix + 0.0001f);  if (dstPix < 0) dstPixStart--;
						int dstPixEnd = dstPixStart + 1;

						float percent = 1-(dstPix-dstPixStart);

						if (dstPixStart >= 0  &&  dstPixStart < dst.Length)
						{
							dst.arr[dstPixStart] += (float)(src.arr[srcPix]*percent);
							sums[dstPixStart] += percent;
						}

						if (dstPixEnd >= 0  &&  dstPixEnd < dst.Length) 
						{
							dst.arr[dstPixEnd] += (float)(src.arr[srcPix]*(1-percent));
							sums[dstPixEnd] += 1-percent;
						}
					}

					for (int dstPix=0; dstPix<dst.length; dstPix++)
						if (sums[dstPix] > 0.001f)
							dst.arr[dstPix] /= sums[dstPix];
				}


				[Obsolete] public static void DownsampleLinear_LastWorking (Stripe src, Stripe dst)
				/// Doesn't deal well with dst start and end
				/// Produces different result, but hmmmm...
				{
					double valueReduceRatio = (double)(dst.length-one) / (double)(src.length-one);
					//double srcPixelSize = (double)src.size / src.length;
					//double dstPixelSize = (double)dst.size / dst.length;
					//double valueReduceRatio = srcPixelSize / dstPixelSize;
					//same

					//resetting dst since we'll fill it additively
					for (int dstPix=0; dstPix<dst.length; dstPix++)
						dst.arr[dstPix] = 0;

					for (int srcPix=0; srcPix<src.length; srcPix++)
					{
						float pos = src.PixelToWorld(srcPix);
						float dstPix = dst.WorldToPixelInterpolated(pos);

						float posStart = src.PixelToWorld(srcPix-0.5f);
						float dstPixStart = dst.WorldToPixelInterpolated(posStart) + 0.5f;

						float posEnd = src.PixelToWorld(srcPix+0.5f);
						float dstPixEnd = dst.WorldToPixelInterpolated(posEnd) + 0.5f;

						int dstPixStartInt = (int)dstPixStart;
						int dstPixEndInt = (int)dstPixEnd;

						if (dstPixStartInt<0)
							dstPixStartInt=0;
						if (dstPixStartInt>=dst.length)
							dstPixStartInt=dst.length-1;
						if (dstPixEndInt<0)
							dstPixEndInt=0;
						if (dstPixEndInt>=dst.length)
							dstPixEndInt=dst.length-1;

						if (dstPixStartInt == dstPixEndInt)
							dst.arr[dstPixStartInt] += (float)(src.arr[srcPix] * valueReduceRatio);

						else
						{
							float rightHand = dstPixEnd - dstPixEndInt;
							float leftHand = dstPixEndInt - dstPixStart;

							float percent = rightHand / (rightHand+leftHand); //or /(dstPixEnd-dstPixStart)

							if (dstPixStartInt >= 0)
								dst.arr[dstPixStartInt] += (float)(src.arr[srcPix]*(1-percent) * valueReduceRatio);
							if (dstPixEndInt < dst.Length) 
								dst.arr[dstPixEndInt] += (float)(src.arr[srcPix]*percent * valueReduceRatio);
						}
					}
				}


				[Obsolete] public static void DownsampleLinear_Smooth (Stripe src, Stripe dst)
				/// Older variant of DownsampleLinear - theoretically more correct, but produces result different than photoshop/gimp
				/// Doesn't work
				{
					float valueReduceRatio = dst.size / src.size;

					for (int dstPix=0; dstPix<dst.size; dstPix++)
						dst.arr[dstPix] = 0;

					for (int srcPix=0; srcPix<src.size; srcPix++)
					{
						float pos = src.PixelToWorld(srcPix);
						float dstPix = dst.WorldToPixelInterpolated(pos);

						int prevDstPix = Mathf.FloorToInt(dstPix-0.5f);
						if (prevDstPix < 0) 
							prevDstPix = 0;

						int nextDstPix = prevDstPix+1; //Mathf.CeilToInt( (float)SrcToDstPixInterpolated(srcPix+0.5f, srcOffset, srcSize, dstOffset, dstSize) );
						if (nextDstPix >= dst.Length) 
							nextDstPix = dst.Length-1;

						float percent = dstPix - (prevDstPix+0.5f); //distance to center of prevDstPix
						//float percent = dstPix - prevDstPix;
						//if (percent < 0)
						//	percent = -percent;
						//percent = 1-percent;

						dst.arr[prevDstPix] += src.arr[srcPix]*(1-percent) * valueReduceRatio;
						dst.arr[nextDstPix] += src.arr[srcPix]*percent * valueReduceRatio;
					}
				}


				private static float CubicInterpolateClassic(float vpp, float vp, float vn, float vnn, float percent)
				{
					float P = (vnn - vn) - (vpp - vp);
					float Q = (vpp - vp) - P;
					float R = vn - vpp;
					float S = vp;

					return P*percent*percent*percent + Q*percent*percent + R*percent  +  S;
					//generated result is over-curvy. For linear gradient pixels it tends to create non-linear curves
				}


				private static float CubicInterpolateMM (float vpp, float vp, float vn, float vnn, float p)
				{
					float ip = 1-p;

					float tp = (vn-vpp)*0.5f;
					float tn = (vp-vnn)*0.5f;

					float l = vp*ip + vn*p; //linear filtration

					float cp = 
						(vp + tp*p) * ip + 
						l * p;

					float cn = l * ip + 
						(vn + tn*ip) * p;

					return  cp*ip + cn*p;
				}


				private static float CubicInterpolate (float vpp, float vp, float vn, float vnn, float p)
				{
						return vp + 0.5f * p * (vn - vpp + p*(2.0f*vpp - 5.0f*vp + 4.0f*vn - vnn + p*(3.0f*(vp - vn) + vnn - vpp)));
						//old code comments this out and says "s tandard quadratic filtration (here for test purpose)"
						//but it's not quadratic!
						//and generates same as MM. Probably MM more optimized
				}


				public static void UpsampleCubic (Stripe src, Stripe dst)
				{
					float srcPixelSize = src.size/(src.length-1);
					float dstPixelSize = dst.size/(dst.length-1);

					//int dstPixStart = (int)(dst.WorldToPixelInterpolated(src.offset));
					int dstPixStart = (int)( (src.offset - dst.offset) / dstPixelSize );
					if (dstPixStart < 0) dstPixStart = 0;  //Math.Max(0, dstPixStart);

					//int dstPixEnd = (int)(dst.WorldToPixelInterpolated(src.offset+src.size)+1); //+1 to make inclusive
					int dstPixEnd = (int)( ((src.offset+src.size) - dst.offset) / dstPixelSize + 1 );
					if (dstPixEnd >= dst.length) dstPixEnd = dst.length-1;

					for (int dstPix = dstPixStart; dstPix <= dstPixEnd; dstPix++)
					{
						//float pos = dst.PixelToWorld(dstPix);
						float pos = dstPix*dstPixelSize + dst.offset;

						//float srcPix = src.WorldToPixelInterpolated(pos);
						float srcPix = (pos-src.offset) / srcPixelSize;

						int srcIdx = (int)srcPix;

						float p = srcPix - srcIdx;

						int idxpp = srcIdx-1;
						int idxp = srcIdx;
						int idxn = srcIdx + 1;
						int idxnn = srcIdx + 2;

						//getting index values - using virtual line continuation on start and end
						float vpp = idxpp >= 0 ? 
							src.arr[idxpp] : 
							src.arr[0]*2 - src.arr[1];  //if it is out of range - using virtual point that continues stripe in minus area: [0] - ([1]-[0])
						float vp = idxp >= 0 ?  src.arr[idxp] : src.arr[0];
						float vn = idxn < src.Length ? 
							src.arr[idxn] : 
							src.arr[src.Length-1]*2 - src.arr[src.Length-2]; //using virtual point at the end (idx2 is idx+1)
						float vnn = idxnn < src.Length ?
							src.arr[idxnn] :
							src.arr[src.Length-1]*2 - src.arr[src.Length-2];
							//idx3 < src.Length+1 ?
							//	src.arr[src.Length-1]*2 - src.arr[src.Length-2] :
							//	src.arr[src.Length-1] + (src.arr[src.Length-1]-src.arr[src.Length-2])*2; //theoretically we should go even further, to 2nd virtual point. But practice shows it's not worth it

						//interpolating
						//dst.arr[dstPix] = CubicInterpolateMM(vpp, vp, vn, vnn, p);
						{
							float ip = 1-p;

							float tp = (vn-vpp)*0.5f;
							float tn = (vp-vnn)*0.5f;

							float l = vp*ip + vn*p; //linear filtration

							float cp = 
								(vp + tp*p) * ip + 
								l * p;

							float cn = l * ip + 
								(vn + tn*ip) * p;

							dst.arr[dstPix] = cp*ip + cn*p;
						}
					}

					//filling all other stripe area with beginning and ending values
					if (dstPixStart < dst.arr.Length)
						dst.Fill(dst.arr[dstPixStart], 0, dstPixStart);
					if (dstPixEnd >= 0)
						dst.Fill(dst.arr[dstPixEnd], dstPixEnd, dst.length-dstPixEnd);
				}


				public static void UpsampleCubic_DoubleTest (Stripe src1, Stripe src2, Stripe dst, bool firstEnabled, bool secondEnabled)
				/// Test to check how it upsamples the seam between two stripes
				/// src2 should start where src1 ends
				{
					dst.Clear();

					if (firstEnabled)
						UpsampleCubic (src1, dst);

					if (secondEnabled)
						UpsampleCubic (src2, dst);
				}

				public static void UpsampleCubic_DoubleMarginsTest (Stripe src1, Stripe src2, Stripe dst1, Stripe dst2)
				/// Test to check how it upsamples the seam between two stripes
				/// src2 should start where src1 ends
				{
					dst1.Clear();
					dst2.Clear();
					UpsampleCubic (src1, dst1);
					UpsampleCubic (src2, dst2);
				}

		#endregion

	} //stripe
} //namespace