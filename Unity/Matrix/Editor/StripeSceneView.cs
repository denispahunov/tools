using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEditor;
using UnityEditor.Overlays;
using UnityEngine.UIElements;
using UnityEditor.EditorTools;

//using Den.Tools.GUI;
using Den.Tools.Matrices;
using Den.Tools.Matrices.Window;
using System.CodeDom;


namespace Den.Tools.Matrices
{

	// Tagging a class with the EditorTool attribute and no target type registers a global tool. Global tools are valid for any selection, and are accessible through the top left toolbar in the editor.
	[EditorTool("Platform Tool")]
	class PlatformTool : EditorTool
	{
		// Serialize this value to set a default value in the Inspector.
		[SerializeField]
		Texture2D m_ToolIcon;

		GUIContent m_IconContent;

		void OnEnable()
		{
			m_IconContent = new GUIContent()
			{
				image = m_ToolIcon,
				text = "Platform Tool",
				tooltip = "Platform Tool"
			};
		}

		public override GUIContent toolbarIcon
		{
			get { return m_IconContent; }
		}

		// This is called for each window that your tool is active in. Put the functionality of your tool here.
		public override void OnToolGUI(EditorWindow window)
		{
			EditorGUI.BeginChangeCheck();

			Vector3 position = UnityEditor.Tools.handlePosition;

			using (new Handles.DrawingScope(Color.green))
			{
				position = Handles.Slider(position, Vector3.right);
			}

			if (EditorGUI.EndChangeCheck())
			{
				Vector3 delta = position - UnityEditor.Tools.handlePosition;

				Undo.RecordObjects(Selection.transforms, "Move Platform");

				foreach (var transform in Selection.transforms)
					transform.position += delta;
			}
		}
	}


	public static class StripeSceneView
    {
		private static Stripe stripe;
		private static StripeOverlay overlay;

		public static void ExposeStripe (Stripe stripe)
		{
			StripeSceneView.stripe = stripe;

			SceneView.duringSceneGui -= Draw;
			SceneView.duringSceneGui += Draw;

			overlay = new StripeOverlay();
			SceneView.AddOverlayToActiveView(overlay);
			overlay.displayed = true;
		}

		public static void DisableView ()
		{
			StripeSceneView.stripe = null;
			SceneView.duringSceneGui -= Draw;

			//if (overlay != null)
				SceneView.RemoveOverlayFromActiveView(overlay);
		}

		static void Draw (SceneView sceneView) 
		{
			if (stripe == null)
				return;

			Handles.color = Color.red;
			Handles.SphereHandleCap(0, Vector3.zero, Quaternion.identity, 1f, EventType.Repaint);

			/*Handles.BeginGUI();
			GUILayout.BeginArea(new Rect(sceneView.position.width - 110, sceneView.position.height - 40, 100, 30));
			if (GUILayout.Button("Disable"))
			{
				DisableView();
			}
			GUILayout.EndArea();
			Handles.EndGUI();*/
		}
	

	//[Overlay(typeof(SceneView), "Stripe", defaultDisplay=false)]
	class StripeOverlay : Overlay
	{
		public override VisualElement CreatePanelContent()
		{
			var root = new VisualElement();
			root.Add(new Button(() => StripeSceneView.DisableView()) { text = "Move Right" });
			root.Add(new Button(() => StripeSceneView.DisableView()) { text = "Move Up" });
			root.Add(new Button(() => StripeSceneView.DisableView()) { text = "Move Forward" });
			return root;
		}
	}
}
}