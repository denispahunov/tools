using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEditor;
using Den.Tools.GUI;
using Den.Tools.Matrices;
using Den.Tools.Matrices.Window;


namespace Den.Tools.GUI
{
	public static partial class Draw
	{
		private static void Matrix (Matrix matrix, bool tmp, string windowName)
		{
			using (Cell.LineStd) 
			{
				CoordRect rect = matrix.rect;
				Draw.Field(ref rect, "Rect");
				if (Cell.current.valChanged)
				{
					if (rect.size != matrix.rect.size)
						matrix.Resize(rect.size);
					matrix.rect = rect;
				}
			}

			if (matrix is MatrixWorld mw)
			{
				using (Cell.LineStd) Draw.Field(ref mw.worldPos, "World Pos");
				using (Cell.LineStd) Draw.Field(ref mw.worldSize, "World Size");
			}

			using (Cell.LineStd) 
			{
				using (Cell.Row)
					if (Draw.Button("To Window"))
						MatrixWindow.OpenUpdate(matrix, windowName);
				using (Cell.Row)
					if (Draw.Button("New Window"))
						MatrixWindow.OpenUpdate(matrix, windowName);
				using (Cell.Row)
					if (Draw.Button("To Scene"))
						MatrixSceneView.ExposeMatrix(matrix, windowName, UI.current.undo.undoObject);
			}
		}

		public static void Matrix (Matrix matrix)
		{
			Draw.Matrix(matrix, false, "Matrix");
		}

		public static void Matrix (Matrix matrix, string label)
		{
			using (Cell.RowRel(1-Cell.current.fieldWidth))
			{
				using (Cell.LineStd) Draw.Label(label.Nicify());
				Cell.EmptyLine(); //to avoid placing label at the center
			}

			using (Cell.RowRel(Cell.current.fieldWidth))
				Draw.Matrix(matrix, true, label);
		}
	}
}


namespace Den.Tools.Matrices
{

	[CustomPropertyDrawer(typeof(Matrix), useForChildren:true)] 
    public class MatrixPropertyDrawer : PropertyDrawer  
    {
		Matrix matrix;
		string matrixName;
		//UI ui = new UI();  //only one instance of MatrixPropertyDrawer exists, and re-used for each property. If cell is dragged in one - value will be changed everywhere
		Dictionary<Matrix,UI> uis = new Dictionary<Matrix, UI>();

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			matrix = property.GetTargetObjectOfProperty() as Matrix;
			matrixName = property.propertyPath;

			bool found = uis.TryGetValue(matrix, out UI ui);
			if (!found)
			{
				ui = new UI();
				ui.undo = new GUI.Undo();
				uis.Add(matrix,ui);
			}

			ui.undo.undoObject = property.serializedObject.targetObject;
			ui.Draw(DrawGUI, isProperty:true, customRect:position);
		}

		private void DrawGUI () => Draw.Matrix(matrix, matrixName);

		//public override float GetPropertyHeight(SerializedProperty property, GUIContent label) => Cell.lineHeight*4;  //EditorGUIUtility.singleLineHeight * 4;

		[Obsolete]
        public  void OnGUI_bac(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            Rect lengthFieldRect = new Rect(position.x, position.y, position.width - 100f, position.height);
            Rect addButtonRect = new Rect(position.x + position.width - 100f, position.y, 75f, position.height);
            Rect plusButtonRect = new Rect(position.x + position.width - 25f, position.y, 25f, position.height);

            //MatrixOps.Stripe stripe = property.boxedValue as MatrixOps.Stripe;
            //MatrixOps.Stripe stripe = fieldInfo.GetValue(property.serializedObject.targetObject) as MatrixOps.Stripe;
            Matrix matrix = property.GetTargetObjectOfProperty() as Matrix;

			// Calculate the rects for the three lines
			Rect[] lineRects = new Rect[3];
			for (int i=0; i<lineRects.Length; i++)
				lineRects[i] = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight*i, position.width, EditorGUIUtility.singleLineHeight-2);

			SerializedProperty rectProperty = property.FindPropertyRelative("rect");
			EditorGUI.PropertyField(lineRects[0], rectProperty, new GUIContent("Rect"));
			if (rectProperty.hasMultipleDifferentValues || rectProperty.serializedObject.ApplyModifiedProperties())
			{
				// Do something when rect is changed
				Debug.Log("Rect property has been changed.");
			}


			//EditorGUI.PropertyField(lineRects[0], property.FindPropertyRelative("offset"), new GUIContent("Offset"));
			//EditorGUI.PropertyField(lineRects[1], property.FindPropertyRelative("size"), new GUIContent("Size"));

			//buttons
			Rect[] buttonRects = new Rect[3];
			for (int i=0; i<buttonRects.Length; i++)
				buttonRects[i] = new Rect(
					lineRects[lineRects.Length-1].position.x + position.width/buttonRects.Length, 
					lineRects[lineRects.Length-1].position.y, 
					position.width/buttonRects.Length, 
					EditorGUIUtility.singleLineHeight-2);

			if (UnityEngine.GUI.Button(buttonRects[0], "To Window"))
				MatrixWindow.OpenUpdate(matrix, name:property.name);


            EditorGUI.EndProperty();
        }
    }


    /*[CustomPropertyDrawer(typeof(StripeTester.Tester), true)]
    public class TesterDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true) + EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
        
            Rect propertyRect = position;
            propertyRect.height -= EditorGUIUtility.singleLineHeight;

            // Draw the default inspector fields
            EditorGUI.PropertyField(propertyRect, property, true);
        
            Rect buttonRect = new Rect(position.x, position.yMax - EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
            if (UnityEngine.GUI.Button(buttonRect, "Test"))
            {
                StripeTester.Tester tester = fieldInfo.GetValue(property.serializedObject.targetObject) as StripeTester.Tester;
                if (tester != null)
                {
                    tester.Test();
                }
            }

            EditorGUI.EndProperty();
        }
    }*/
}