using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEditor;
//using Den.Tools.GUI;
using Den.Tools.Matrices;
using Den.Tools.Matrices.Window;
using System.Linq;


namespace Den.Tools.Matrices
{
	[CustomPropertyDrawer(typeof(Stripe))]
    public class StripePropertyDrawer : PropertyDrawer  
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            Rect lengthFieldRect = new Rect(position.x, position.y, position.width - 100f, position.height);
            Rect addButtonRect = new Rect(position.x + position.width - 100f, position.y, 75f, position.height);
            Rect plusButtonRect = new Rect(position.x + position.width - 25f, position.y, 25f, position.height);

            //MatrixOps.Stripe stripe = property.boxedValue as MatrixOps.Stripe;
            //MatrixOps.Stripe stripe = fieldInfo.GetValue(property.serializedObject.targetObject) as MatrixOps.Stripe;
            Stripe stripe = property.GetTargetObjectOfProperty() as Stripe;

			// Calculate the rects for the three lines
			Rect lengthRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight-2);
			Rect offsetRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight-2);
			Rect sizeRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight*2, position.width, EditorGUIUtility.singleLineHeight-2);
			Rect button1Rect = new Rect(position.x, position.y + 3 * EditorGUIUtility.singleLineHeight, position.width/3 - 2, EditorGUIUtility.singleLineHeight-2);
			Rect button2Rect = new Rect(position.x + position.width/3, position.y + 3 * EditorGUIUtility.singleLineHeight, position.width/3, EditorGUIUtility.singleLineHeight-2);
			Rect button3Rect = new Rect(position.x + position.width/3 * 2 + 2, position.y + 3 * EditorGUIUtility.singleLineHeight, position.width/3-2, EditorGUIUtility.singleLineHeight-2);


			int newLength = EditorGUI.DelayedIntField(lengthRect, new GUIContent("Length"), stripe.length);
			if (newLength != stripe.length)
			{
				stripe.Resize(newLength);
				EditorUtility.SetDirty(property.serializedObject.targetObject);
			}

			EditorGUI.PropertyField(offsetRect, property.FindPropertyRelative("offset"), new GUIContent("Offset"));
			EditorGUI.PropertyField(sizeRect, property.FindPropertyRelative("size"), new GUIContent("Size"));

			//Showing in window
			//string[] pathSegments = property.propertyPath.Split('.');
			//string propertyPath = string.Join(".", pathSegments.Skip(Math.Max(0, pathSegments.Length - 3)));
			string propertyPath = property.propertyPath + "." + label.text;

			if (UnityEngine.GUI.Button(button1Rect, "To Window"))
			{
				StripeWindow.Show(stripe, newWindow:false, name:propertyPath);
			}

			if (UnityEngine.GUI.Button(button2Rect, "New Window"))
			{
				StripeWindow win = StripeWindow.Show(stripe, newWindow:true, name:propertyPath);
			}

			if (UnityEngine.GUI.Button(button3Rect, "To Scene"))
			{
				StripeSceneView.ExposeStripe(stripe);
				SceneView.RepaintAll();
			}

			//updating stripe if it is already in window
			StripeWindow.ReassignStripe(stripe, name:propertyPath);
			//StripeWindow.Show(stripe, newWindow:false, openWindow:false, name:property.propertyPath);
			//will add hell a lot of stripes if names differ. Better reset window and open new on pressing Test

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 4;
        }

		public void Refresh ()
		{
			Debug.Log("Refresh");
		}
    }


    /*[CustomPropertyDrawer(typeof(StripeTester.Tester), true)]
    public class TesterDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true) + EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
        
            Rect propertyRect = position;
            propertyRect.height -= EditorGUIUtility.singleLineHeight;

            // Draw the default inspector fields
            EditorGUI.PropertyField(propertyRect, property, true);
        
            Rect buttonRect = new Rect(position.x, position.yMax - EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
            if (UnityEngine.GUI.Button(buttonRect, "Test"))
            {
                StripeTester.Tester tester = fieldInfo.GetValue(property.serializedObject.targetObject) as StripeTester.Tester;
                if (tester != null)
                {
                    tester.Test();
                }
            }

            EditorGUI.EndProperty();
        }
    }*/
}