using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEditor;
using UnityEditor.Overlays;
using UnityEngine.UIElements;
using UnityEditor.EditorTools;

//using Den.Tools.GUI;
using Den.Tools.Matrices;
using Den.Tools.Matrices.Window;
using System.CodeDom;
using System.Linq;
using UnityEditor.UIElements;
using NUnit.Framework.Internal;


namespace Den.Tools.Matrices
{
	public static class MatrixSceneView
	{
		private class MatrixTuple
		{
			public Matrix matrix;
			public Mesh mesh;
			public float[] prevArr; //to update mesh on change
			public string label;
			public bool expanded;
			public bool visible = true;
			public bool editable = true;
			public Color color = Color.white;
			public HashSet<Coord> selectedCoords;
			public UnityEngine.Object undoObject;
		}

		private static MatrixTuple[] matrices;

		private static int FindMatrixNum (Matrix matrix, string label)
		{
			for (int m=0; m<matrices.Length; m++)
				if (matrices[m].matrix == matrix)
					return m;
			
			for (int m=0; m<matrices.Length; m++)
				if (matrices[m].label == label)
					return m;

			return -1;
		}

		private const float selectionHandleSize = 0.1f;
		private const float matrixHeight = 10f;
		private static float softSelection = 0f;

		private static MatrixOverlay overlay;

		private static bool selectionRectActivated = false;
		private static Vector2 selectionFrameStart;
		private static Rect selectionRect;
		private static Coord cursorCoord;

		private static RenderParams renderParams;
		private static Material material;
		
		private static Vector3D MatrixToWorld (Vector3D pos, Matrix m, float height=1)
		{
			if (m is MatrixWorld mw)
			{
				pos = new Vector3D(pos.x*mw.worldSize.x/(mw.rect.size.x-1), pos.y*mw.worldSize.y*height, pos.z*mw.worldSize.z/(mw.rect.size.z-1));
				pos += mw.worldPos;
			}
			else
				pos.y *= height;
			return pos;
		}

		private static Vector3D WorldToMatrix (Vector3D pos, Matrix m, float height=1)
		{
			if (m is MatrixWorld mw)
			{
				pos -= mw.worldPos;
				pos = new Vector3D(pos.x*(mw.rect.size.x-1)/mw.worldSize.x, pos.y/(height*mw.worldSize.y), pos.z*(mw.rect.size.z-1)/mw.worldSize.z);
			}
			else
				pos.y /= height;
			return pos;
		}


		public static void ExposeMatrix (Matrix matrix, string label, UnityEngine.Object undoObject)
		{
			if (matrices == null)
				matrices = new MatrixTuple[0];

			int num = FindMatrixNum(matrix, label);
			if (num < 0)
			{
				MatrixTuple mt = new MatrixTuple() {matrix=matrix, label=label, undoObject=undoObject};

				if (matrices.Length != 0)
					mt.editable = false;
				if (matrices.Length == 1)
					mt.color = Color.green;
				if (matrices.Length == 2)
					mt.color = Color.red;

				ArrayTools.Add(ref matrices, mt); 
			}
			else
			{
				matrices[num].matrix = matrix;
				matrices[num].label = label;
				UpdateMatrixMesh(matrices[num].matrix, ref matrices[num].mesh);
			}
			
			if (overlay == null)
			{
				SceneView.duringSceneGui -= Draw;
				SceneView.duringSceneGui += Draw;

				overlay = new MatrixOverlay();
				SceneView.AddOverlayToActiveView(overlay);
				overlay.displayed = true;
			}
		}

		public static void DisableView ()
		{
			matrices = null;
			SceneView.duringSceneGui -= Draw;

			if (overlay != null)
				SceneView.RemoveOverlayFromActiveView(overlay);
			overlay = null;
		}

		static void Draw (SceneView sceneView) 
		{
			if (matrices == null)
				return;

			//UpdateSelectionFrame();

			//Handles.color = Color.red; 
			//Handles.SphereHandleCap(0, Vector3.zero, Quaternion.identity, 1f, EventType.Repaint);

			for (int i=0; i<matrices.Length; i++)
			{
				if (matrices[i].visible)
					DrawMatrixMesh(matrices[i], matrixHeight);
				if (matrices[i].visible && matrices[i].editable)
				{
					EditMatrix(matrices[i]); 
					SelectMatrix(matrices[i]);
				}
			}
			
			
			

			/*Handles.BeginGUI();
			GUILayout.BeginArea(new Rect(sceneView.position.width - 110, sceneView.position.height - 40, 100, 30));
			if (GUILayout.Button("Disable"))
			{
				DisableView();
			}
			GUILayout.EndArea();
			Handles.EndGUI();*/

			//System.Type editorWindowType = typeof(Editor).Assembly.GetType("UnityEditor.SceneView");
			//System.Reflection.FieldInfo selectionRectField = editorWindowType.GetField("s_DraggingRect", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
			//Debug.Log( selectionRectField.GetValue(null) );
		}



		private static void DrawMatrixMesh (MatrixTuple matrixTuple, float height=10)
		{
			if (matrixTuple.mesh == null  ||  
				matrixTuple.mesh.vertexCount != (matrixTuple.matrix.rect.size.x-1)*(matrixTuple.matrix.rect.size.z-1)*4 ||
				matrixTuple.prevArr==null || matrixTuple.prevArr.Length != matrixTuple.matrix.arr.Length)
			{
				matrixTuple.mesh = CreateMatrixMesh(matrixTuple.matrix);
				UpdateMatrixMesh(matrixTuple.matrix, ref matrixTuple.mesh);

				matrixTuple.prevArr = new float[matrixTuple.matrix.arr.Length];
				Array.Copy(matrixTuple.matrix.arr, matrixTuple.prevArr, matrixTuple.matrix.arr.Length);
			}

			bool arraysEqual = matrixTuple.prevArr.AsSpan().SequenceEqual(matrixTuple.matrix.arr);
			if (!arraysEqual)
			{
				UpdateMatrixMesh(matrixTuple.matrix, ref matrixTuple.mesh);
				Array.Copy(matrixTuple.matrix.arr, matrixTuple.prevArr, matrixTuple.matrix.arr.Length);
			}

			if (renderParams.material == null)
			{
				if (material == null)
					material = new Material(Shader.Find("Specular"));
				renderParams = new RenderParams(material);
			}

			Matrix4x4 tfm = Matrix4x4.identity;
			if (matrixTuple.matrix is MatrixWorld mw)
			{
				Vector3D pos = MatrixToWorld(Vector3D.zero, mw, height);
				Vector3D max = MatrixToWorld(new Vector3D(mw.rect.size.x,0,mw.rect.size.z), mw, height);
				Vector3D size = new Vector3D(mw.worldSize.x / (mw.rect.size.x-1), mw.worldSize.y * height, mw.worldSize.z / (mw.rect.size.z-1)); //max-pos;
				tfm = Matrix4x4.TRS(mw.worldPos.Vector3(), Quaternion.identity, size.Vector3());
			}

			renderParams.material.color = matrixTuple.color;
			renderParams.material.SetPass(0);
			Graphics.DrawMeshNow(matrixTuple.mesh, tfm, 0);
			//IF MATRIX IS BLACK ON PROJECT START - SHADER NOT INIT. CREATE CUBE!
			//Graphics.DrawMesh(matrixTuple.mesh, tfm, renderParams.material, 0); //renders mesh several times in scene view
		}


		private static void DrawMatrixGizmos (MatrixTuple matrixTuple, float height=10)
		{
			Matrix matrix = matrixTuple.matrix;

			Vector3[] vertices = new Vector3[4];
			Coord[] vertAdd = new Coord[] { new Coord(0,0), new Coord(1,0), new Coord(1,1), new Coord(0,1) };

			Coord min = matrix.rect.Min; Coord max = matrix.rect.Max;
			for (int x=min.x; x<max.x-1; x++)
				for (int z=min.z; z<max.z-1; z++)
				{
					for (int v=0; v<vertices.Length; v++)
					{
						Coord vertCoord = new Coord(x,z) + vertAdd[v];
						vertices[v] = vertCoord.Vector3();
						vertices[v] = MatrixToWorld(vertices[v].Vector3D(), matrix, height).Vector3();
					}

					//drawing
					Vector3 normal = Vector3.Cross(vertices[1] - vertices[0], vertices[3] - vertices[0]).normalized +
									  Vector3.Cross(vertices[2] - vertices[1], vertices[0] - vertices[1]).normalized +
									  Vector3.Cross(vertices[3] - vertices[2], vertices[1] - vertices[2]).normalized +
									  Vector3.Cross(vertices[0] - vertices[3], vertices[2] - vertices[3]).normalized;
					normal = -normal;
					normal.Normalize();

					//Vector3 midPoint = (vertices[0] + vertices[1] + vertices[2] + vertices[3]) / 4;
					//Handles.color = Color.cyan;
					//Handles.DrawLine(midPoint, midPoint + normal);

					float dotUp = Mathf.Pow( Vector3.Dot(normal, Vector3.up), 2);
					float dotRight = Mathf.Pow(Vector3.Dot(normal, Vector3.right), 2);
					float dotForward = Mathf.Pow(Vector3.Dot(normal, Vector3.forward), 2);

					Color color = Color.white*dotUp + Color.red*Mathf.Abs(dotRight) + Color.red*Mathf.Abs(dotForward);
					color = color / (dotUp + Mathf.Abs(dotRight) + Mathf.Abs(dotForward));
					
					Handles.color = color;
					Handles.DrawSolidRectangleWithOutline(vertices, new Color(color.r, color.g, color.b, 0.25f), color);
					Handles.DrawAAConvexPolygon(vertices);
				}
		}


		private static void SelectMatrix (MatrixTuple matrixTuple, float height=10, UnityEngine.Object undoObject=null)
		{
			Matrix matrix = matrixTuple.matrix;
			HashSet<Coord> selected = matrixTuple.selectedCoords;

			if (matrixTuple.selectedCoords == null)
				matrixTuple.selectedCoords = new HashSet<Coord>();

			//finding cursor coord on XZ plane
			Coord hitCoord = new Coord();
			Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
			if (new Plane(Vector3.up, Vector3.zero).Raycast(ray, out float enter))
			{
				Vector3 hitPoint = ray.GetPoint(enter);
				hitPoint = WorldToMatrix(hitPoint.Vector3D(), matrix, height).Vector3();
				hitCoord = Coord.Round(hitPoint.x, hitPoint.z);

				if (hitCoord != cursorCoord)
				{
					cursorCoord = hitCoord;
					SceneView.RepaintAll();

					//Debug.Log(cursorCoord);
				}
			}

			//drawing handles around XZ coord only
			Coord min = hitCoord-3;  min.ClampByRectInclusive(matrix.rect);
			Coord max = hitCoord+3;  max.ClampByRectInclusive(matrix.rect);
			//Coord min = matrix.rect.Min; Coord max = matrix.rect.Max;
			
			for (int x=min.x; x<max.x; x++)
				for (int z=min.z; z<max.z; z++)
					DrawSelectionHandle(matrixTuple, new Coord(x,z), Color.grey, height);

			//drawing selected handles
			foreach (Coord coord in matrixTuple.selectedCoords)
				DrawSelectionHandle(matrixTuple, coord, Color.yellow, height);

			//deselecting
			if (Event.current.type == EventType.MouseDown && 
				Event.current.button == 0 && 
				!selectionRect.Contains(Event.current.mousePosition)  &&  
				!(Event.current.control  ||  Event.current.shift))
				{
					matrixTuple.selectedCoords.Clear();
					SceneView.RepaintAll();
				}

		}


		private static void DrawSelectionHandle (MatrixTuple matrixTuple, Coord vertCoord, Color color, float height=10)
		{
			Vector3D vertPos = new Vector3D(vertCoord.x, matrixTuple.matrix[vertCoord], vertCoord.z);
			vertPos = MatrixToWorld(vertPos, matrixTuple.matrix, height);

			Handles.color = color; //Color.grey;
			//if (matrixTuple.selectedCoords.Contains(vertCoord))
			//	Handles.color = Color.yellow;

			float handleSize = HandleUtility.GetHandleSize(vertPos.Vector3()) * selectionHandleSize;

			if (Handles.Button(vertPos.Vector3(), Quaternion.identity, handleSize, handleSize, Handles.SphereHandleCap))
			{
				if (Event.current.control  ||  Event.current.shift)
				{
					if (!matrixTuple.selectedCoords.Contains(vertCoord))
						matrixTuple.selectedCoords.Add(vertCoord);
					else
						matrixTuple.selectedCoords.Remove(vertCoord);
				}

				else
				{
					matrixTuple.selectedCoords.Clear();
					matrixTuple.selectedCoords.Add(vertCoord);
				}

				//Event.current.Use();
				SceneView.RepaintAll();
			}

			if (selectionRectActivated)
			{
				Vector2 screenPoint = HandleUtility.WorldToGUIPoint(vertPos.Vector3());
				if (selectionRect.Contains(screenPoint))
				{
					matrixTuple.selectedCoords.Add(vertCoord);
					SceneView.RepaintAll();
				}
			}
		}


		private static void EditMatrix (MatrixTuple matrixTuple, float height=10)
		{
			Matrix matrix = matrixTuple.matrix;
			HashSet<Coord> selected = matrixTuple.selectedCoords;

			if (matrixTuple.selectedCoords==null  ||  matrixTuple.selectedCoords.Count == 0)
				return;

			//selection center
			Vector2D sum = Vector2D.zero; float hei=0; int num=0;  //pidän muuttujan nimestä
			foreach (Coord c in selected)
			{
				Vector2D pos = new Vector2D(c.x, c.z);
				sum += new Vector2D(pos.x, pos.z);
				hei += matrix[c.x, c.z];
				num ++;
			}
			Vector2D center = sum / num;
			float cenHei = hei / num;
			Vector3 center3 = new Vector3(center.x, cenHei, center.z);
			center3 = MatrixToWorld(center3.Vector3D(), matrixTuple.matrix, height).Vector3();

			//editing
			EditorGUI.BeginChangeCheck();
			Vector3 newPos = Handles.Slider(center3, Vector3.up);
			if (EditorGUI.EndChangeCheck())
			{
				if (matrixTuple.undoObject != null)
					Undo.RecordObject(matrixTuple.undoObject, "Edit Matrix");

				newPos = WorldToMatrix(newPos.Vector3D(), matrixTuple.matrix, height).Vector3();

				float delta = newPos.y-cenHei;
	
				foreach (Coord c in selected)
				{
					matrix[c.x, c.z] += delta;

					if (softSelection > 1)
					{
						for (int sx = c.x - (int)softSelection; sx <= c.x + (int)softSelection; sx++)
						for (int sz = c.z - (int)softSelection; sz <= c.z + (int)softSelection; sz++)
						{
							if (sx==c.x && sz==c.z)
								continue;

							//soft selection
							if (matrix.rect.Contains(new Coord(sx,sz)))
							{
								float distance = Vector2D.Distance(new Vector2D(c.x, c.z),new Vector2D(sx,sz));
								if (distance <= softSelection)
								{
									float adjustedDelta = delta * (1 - (distance / softSelection));
									matrix[sx,sz] += adjustedDelta;
								}
								}
							}
					}
				}

				EditorUtility.SetDirty(matrixTuple.undoObject);

				Event.current.Use();
				UpdateMatrixMesh(matrixTuple.matrix, ref matrixTuple.mesh);
			}

			SceneView.RepaintAll();
		}

	
		//Overlay GUI
		//[Overlay(typeof(SceneView), "Stripe", defaultDisplay=false)]
		class MatrixOverlay : Overlay
		{
			public override VisualElement CreatePanelContent()
			{
				var root = new VisualElement();

				// Add a button using VisualElement
				root.Add(new Button(() => DisableView()) { text = "Disable" });

				// Add IMGUIContainer to use standard EditorGUI
				var imguiContainer = new IMGUIContainer(OnIMGUI);
				root.Add(imguiContainer);
				root.style.width = 100;

				return root;
			}

			private void OnIMGUI()
			{
				float ss = softSelection*softSelection;
				EditorGUILayout.LabelField("Soft Selection");
				ss = EditorGUILayout.Slider(ss, 0, 100);
				softSelection = Mathf.Sqrt(ss);

				for (int i = 0; i < matrices.Length; i++)
				{
					var matrixTuple = matrices[i];

					matrixTuple.expanded = EditorGUILayout.Foldout(matrixTuple.expanded,matrixTuple.label);
					if (matrixTuple.expanded)
					{
						EditorGUI.indentLevel++;

						matrixTuple.visible = EditorGUILayout.ToggleLeft("Visible",matrixTuple.visible);
						matrixTuple.editable = EditorGUILayout.ToggleLeft("Editable",matrixTuple.editable);
						matrixTuple.color = EditorGUILayout.ColorField("Color",matrixTuple.color);

						if (GUILayout.Button("Remove"))
						{
							ArrayTools.RemoveAt(ref matrices,i);
							if (matrices.Length == 0)
								DisableView();
						}

						EditorGUI.indentLevel--;
					}
				}
			}

			private void OnVisibleToggled(int i,bool v)
			{
				matrices[i].visible = v;
			}
		}


		private static Mesh CreateMatrixMesh (Matrix matrix)
		{
			Mesh mesh = new Mesh();
			
			int sizeX = matrix.rect.size.x; int sizeZ = matrix.rect.size.z;
			int numFaces = (sizeX-1)*(sizeZ-1);

			mesh.vertices = new Vector3[numFaces*4];
			mesh.normals = new Vector3[numFaces*4];

			int[] tris = new int[(sizeX-1) * (sizeZ-1) * 2 * 3];

			for (int x=0; x<sizeX-1; x++)
				for (int z=0; z<sizeZ-1; z++)
				{
					int faceNum = z*(sizeX-1) + x;
					int triNum = faceNum*6;
					int vertNum = faceNum*4;

					tris[triNum] = vertNum;		//vertNum;
					tris[triNum+1] = vertNum+2;	//vertNum+sizeX+1;
					tris[triNum+2] = vertNum+1;	//vertNum+sizeX;
					tris[triNum+3] = vertNum;	//vertNum+1;
					tris[triNum+4] = vertNum+3;	//vertNum+sizeX+1;
					tris[triNum+5] = vertNum+2;

					//mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
				}

			mesh.triangles = tris;

			Vector3 boundsSize = new Vector3(sizeX, 10000, sizeZ);
			mesh.bounds = new Bounds(boundsSize/2, boundsSize);

			return mesh;
		}




		private static void UpdateMatrixMesh (Matrix matrix, ref Mesh mesh)
		{
			if (mesh == null  ||  mesh.vertexCount != (matrix.rect.size.x-1)*(matrix.rect.size.z-1)*4)
				mesh = CreateMatrixMesh(matrix);

			int sizeX = matrix.rect.size.x; int sizeZ = matrix.rect.size.z;
			int numFaces = (sizeX-1)*(sizeZ-1);

			Vector3[] vertices = new Vector3[numFaces*4];
			Vector3[] normals = new Vector3[numFaces*4];

			Coord[] vertAdd = new Coord[] { new Coord(0,0), new Coord(1,0), new Coord(1,1), new Coord(0,1) };

			for (int x=0; x<sizeX-1; x++)
				for (int z=0; z<sizeZ-1; z++)
				{
					int faceNum = z*(sizeX-1) + x;
					int vertNum = faceNum*4;

					//vertex position
					for (int v=0; v<4; v++)
					{
						Coord vertCoord = new Coord(x,z) + vertAdd[v];
						vertices[v+vertNum] = vertCoord.Vector3();
						vertices[v+vertNum].y = matrix[vertCoord];
					}

					//face normal
					Vector3 normal = Vector3.Cross(vertices[vertNum+1] - vertices[vertNum+0], vertices[vertNum+3] - vertices[vertNum+0]).normalized +
									  Vector3.Cross(vertices[vertNum+2] - vertices[vertNum+1], vertices[vertNum+0] - vertices[vertNum+1]).normalized +
									  Vector3.Cross(vertices[vertNum+3] - vertices[vertNum+2], vertices[vertNum+1] - vertices[vertNum+2]).normalized +
									  Vector3.Cross(vertices[vertNum+0] - vertices[vertNum+3], vertices[vertNum+2] - vertices[vertNum+3]).normalized;
					normal = -normal;
					normal.Normalize();

					for (int v=vertNum; v<vertNum+4; v++)
						normals[v] = normal;
				}

			mesh.vertices = vertices;
			mesh.normals = normals;
		}



		private static void UpdateSelectionFrame ()
		{
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
			{
				selectionFrameStart = Event.current.mousePosition;
				selectionRect.width = 0;
				selectionRect.height = 0;
			}

			if (Event.current.type == EventType.MouseDrag && Event.current.button == 0)
			{
				selectionRect = new Rect(
					Mathf.Min(selectionFrameStart.x, Event.current.mousePosition.x),
					Mathf.Min(selectionFrameStart.y, Event.current.mousePosition.y),
					Mathf.Abs(selectionFrameStart.x - Event.current.mousePosition.x),
					Mathf.Abs(selectionFrameStart.y - Event.current.mousePosition.y) );
				
			}

			//if (Event.current.type != EventType.Repaint  &&  Event.current.type != EventType.Layout)
			if (Event.current.type == EventType.Used)
				Debug.Log(Event.current.mousePosition);

			var sceneViewType = typeof(SceneView);
			//var selectionRectField = sceneViewType.GetField("m_SelectionRect", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
			//Debug.Log(selectionRectField);

			/*if (Event.current.type == EventType.Repaint)
			{
				System.Type editorGUIUtilityType = typeof(EditorGUIUtility);
				System.Reflection.MethodInfo getLastRectMethod = editorGUIUtilityType.GetMethod("GetLastRect", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
				Rect lastRect = (Rect)getLastRectMethod.Invoke(null, null);
				//Debug.Log(lastRect);
			}*/
		}

	}
	



	//Not used but kept if we want to use matrix edit tool
	//[EditorTool("Matrix Tool")]
	class MatrixTool : EditorTool
	{
		[SerializeField] Texture2D m_ToolIcon;

		GUIContent m_IconContent;

		void OnEnable()
		{
			m_IconContent = new GUIContent()
			{
				image = m_ToolIcon,
				text = "Platform Tool",
				tooltip = "Platform Tool"
			};
		}

		public override GUIContent toolbarIcon => m_IconContent;

		// This is called for each window that your tool is active in. Put the functionality of your tool here.
		public override void OnToolGUI(EditorWindow window)
		{
			EditorGUI.BeginChangeCheck();

			Vector3 position = UnityEditor.Tools.handlePosition;

			using (new Handles.DrawingScope(Color.green))
				position = Handles.Slider(position, Vector3.right);

			if (EditorGUI.EndChangeCheck())
			{
				Vector3 delta = position - UnityEditor.Tools.handlePosition;
				Undo.RecordObjects(Selection.transforms, "Move Platform");
				foreach (var transform in Selection.transforms)
					transform.position += delta;
			}
		}
	}
}
