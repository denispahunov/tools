using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using UnityEngine.Profiling;

using Den.Tools;
using Den.Tools.Matrices;
using Den.Tools.GUI;
//using System.Numerics;


namespace Den.Tools.Matrices.Window
{
	//A window with toolbar and scrollzoom
	public class StripeWindow : EditorWindow
	{
		//public Stripe stripe;
		[Serializable] 
		public class StripeDisplay
		{
			public Stripe stripe;
			//public Vector2 offset;
			//public Vector2 scale = new Vector2(1,1);
			public string name;
		}

		public StripeDisplay[] stripes = new StripeDisplay[0];
		public int selected = 0;
		public bool anchorPoints = true;

		public int FindStripeIndex (Stripe stripe) => Array.FindIndex(stripes, s => s.stripe == stripe);
		public int FindNameIndex (string name) => Array.FindIndex(stripes, s => s.name == name);

		public int pixelSelected = -1;

		UI toolbarUI = new UI();
		UI previewUI = UI.ScrolledZoomedUI(maxZoomStage:32, minZoomStage:-8, defaultScroll:new Vector2(0,100)); 

		const int winToolbarWidth = 180; //128+4

		private static Color[] colors = new Color[] { Color.white, Color.red, Color.green, Color.blue };

		private void OnGUI () 
		{
			previewUI.Draw(DrawPreview, inInspector:false);
			toolbarUI.Draw(DrawWinToolbar, inInspector:false);
		}

		private const float Width = 10;
		private const float Height = 100;

		private float LengthMod => anchorPoints ? 1 : 0; //heightmap is 513, but it is perfectly fit in 512 grid. Because of -1.
		private float CellWidth (Stripe stripe) =>  stripe.size / (stripe.length - LengthMod) * Width;
		private float CellPos (Stripe stripe, int pixelNum) => (pixelNum+stripe.offset)*CellWidth(stripe);
		private Rect CellRect (Stripe stripe, int pixelNum) => new Rect( CellPos(stripe,pixelNum), -Height, CellWidth(stripe), Height );

		protected virtual void DrawPreview ()
		{
			if (selected >= stripes.Length)
				selected = 0;

			//background
			Rect displayRect = new Rect(0, 0, Screen.width, Screen.height);
			float gridBackgroundColor = !UI.current.styles.isPro ? 0.45f : 0.2f;
			float gridColor = !UI.current.styles.isPro ? 0.5f : 0.23f;
			Draw.StaticGrid(
				displayRect: displayRect,
				cellSize:new Vector2(Width, Width),//new Vector2(CellWidth(stripes[selected].stripe), 10),
				cellOffset:new Vector2(0,0),//new Vector2(stripes[selected].stripe.offset, 0),
				color:new Color(gridColor,gridColor,gridColor), 
				background:new Color(gridBackgroundColor,gridBackgroundColor,gridBackgroundColor),
				fadeWithZoom:true);

			//using (Cell.Custom(MatrixRect))
			//	Draw.MatrixPreviewTexture(PreviewTexture, colorize:colorize, relief:relief, min:min, max:max);


			//stripe bounds
			for (int s=0; s<stripes.Length; s++)
			{
				float ghostsAlpha = 0.25f;
				Color white = s==selected ? Color.white : new Color(1,1,1,ghostsAlpha);
				Color black = s==selected ? Color.black : new Color(0,0,0,ghostsAlpha);
				Color red = s==selected ? Color.red : new Color(1,0,0,ghostsAlpha);
				Color green = s==selected ? Color.green : new Color(0,1,0,ghostsAlpha);

				StripeDisplay disp = stripes[s];
				Stripe stripe = disp.stripe;

				Draw.StaticAxis(displayRect, 0, false, black);
				Draw.StaticAxis(displayRect, -100, false, white);

				Draw.StaticAxis(displayRect, (int)(stripe.offset*Width), true, red);
				Draw.StaticAxis(displayRect, (int)(stripe.offset*Width + CellWidth(disp.stripe)*(stripe.length-LengthMod)), true, green);
			}


			//stripes
			void DrawStripe (StripeDisplay disp, int num, bool selected)
			{
				Stripe stripe = disp.stripe;

				Color color = num>=colors.Length ? Color.yellow : colors[num];
				color.a = selected ? 0.5f : 0.25f;

				if (anchorPoints)
				{
					using (Cell.Custom(displayRect))
					{
						Vector2 prevPointPos = Vector2.zero;
						Vector2 prevFlooredPos = Vector2.zero;

						for (int i=0; i<stripe.Length; i++)
						{
							float currHeight = stripe.arr[i]*100;
							float currOffset = stripe.offset * Width  +  i * CellWidth(stripe);

							Vector2 pointPos = new Vector2(currOffset,-currHeight);
							pointPos = UI.current.scrollZoom.ToScreen(pointPos);

							Vector2 flooredPos = new Vector2(currOffset,-currHeight);
							flooredPos = UI.current.scrollZoom.ToScreen(flooredPos);

							Handles.color = color;
							Handles.DrawSolidDisc((Vector3)pointPos, Vector3.forward, 5); 

							if (i!=0)
								Handles.DrawAAConvexPolygon(pointPos, flooredPos, prevFlooredPos, prevPointPos);

							prevFlooredPos = flooredPos;
							prevPointPos = pointPos;
						}

					}
				}

				else
					for (int i=0; i<stripe.Length; i++)
					{
						float currHeight = disp.stripe.arr[i]*100;
					
						using (Cell.Custom(CellPos(stripe,i), -currHeight, CellWidth(disp.stripe), currHeight))
							Draw.Rect(color);
						using (Cell.Custom(CellPos(stripe,i), -currHeight, CellWidth(disp.stripe), 1))
							Draw.Rect(color);
					}
			}

			for (int s=0; s<stripes.Length; s++) //ghosts
			{
				if (s==selected)
					continue;

				DrawStripe(stripes[s], s, false); 
			}

			DrawStripe(stripes[selected], selected, true); //selected


			//picking pixel
			{
				StripeDisplay disp = stripes[selected];
				Stripe stripe = disp.stripe;

				for (int i=0; i<stripe.Length; i++)
				{
					using (Cell.Custom(CellPos(stripe,i), -Height, CellWidth(disp.stripe), Height))
						if (Draw.Button(visible:false))
						{
							Vector2 clickPos = UI.current.mousePos;
							float val = -clickPos.y / Height;

							if (Event.current.alt)
								val = 0;

							stripe.arr[i] = val;

							Repaint();
						}
				}
			}

			//foreach (IPlugin plugin in plugins)
			//	plugin.DrawWindow(Matrix, this);
		}


		protected virtual void DrawWinToolbar ()
		{
			if (selected >= stripes.Length)
				selected = 0;
			StripeDisplay display = stripes[selected];
			Stripe stripe = display.stripe;

			Cell.EmptyRow();
			using (Cell.RowPx(winToolbarWidth)) 
			{
				Cell.EmptyLinePx(6);
				int removeAt = -1;

				using (Cell.LineStd)
				{
					bool tmp = true;
					using (new Draw.FoldoutGroup(ref tmp, "General", isLeft:false, style:UI.current.styles.foldoutOpaque, backgroundWhileClosed:true))
					{
						using (Cell.LineStd) Draw.Toggle(ref anchorPoints, "Draw Anchor Points");
					}
				}

				for (int s=0; s<stripes.Length; s++)
				{
					bool enabled = s==selected;
					bool newEnabled = enabled;

					using (Cell.LineStd)
						using (new Draw.FoldoutGroup(ref newEnabled, stripes[s].name, isLeft:false, style:UI.current.styles.foldoutOpaque, backgroundWhileClosed:true))
							if (newEnabled)
							{
								if (!enabled)
									{ selected=s; stripe=stripes[s].stripe; }

								using (Cell.LineStd) Draw.Label("Ctrl-click to change values");
								using (Cell.LineStd) Draw.Label("Alt-click to reset");
								using (Cell.LineStd) Draw.Toggle(ref anchorPoints, "Heighmap Anchor-Points");
								using (Cell.LineStd) Draw.Field(ref pixelSelected, "Pixel Selected");

								if (pixelSelected>=0  &&  pixelSelected<stripe.arr.Length)
									using (Cell.LineStd) Draw.Field(ref stripe.arr[pixelSelected], "Value");

								//using (Cell.LineStd) Draw.Field(ref stripes[s].offset, "Offset");
								//using (Cell.LineStd) Draw.Field(ref stripes[s].scale, "Scale");

								using (Cell.LineStd) 
									if (Draw.Button("Remove"))
										removeAt = s;
							}
				}

				if (removeAt >= 0)
					ArrayTools.RemoveAt(ref stripes, removeAt);

				//Cell.EmptyLine();
			}

			Cell.EmptyRowPx(8);
		}


		public static StripeWindow Show (Stripe stripe, bool newWindow=false, bool openWindow=true, string name=null) 
		{
			StripeWindow window = null;
			StripeWindow[] allWindows = Resources.FindObjectsOfTypeAll<StripeWindow>();

			//if opened as biome via focused graph window - opening as biome
			//if (focusedWindow is StripeWindow focWin)
			//	return focWin;
				//do not continue to ShowWindow?

			//if new window - always open new window
			if (newWindow)
				window = CreateInstance<StripeWindow>();

			//if opened only one window - using it
			if (window == null  &&  allWindows.Length == 1)  
				window = allWindows[0];

			//if window with this stripe currently opened - just focusing it
			if (window == null)
				for (int w=0; w<allWindows.Length; w++)
				{
					int index = allWindows[w].FindStripeIndex(stripe);
					if (index >=0 )
						window = allWindows[w];
				}

			//looking for the window with the same entity name
			if (window == null)
				for (int w=0; w<allWindows.Length; w++)
				{
					int index = allWindows[w].FindNameIndex(name);
					if (index >=0 )
						window = allWindows[w];
				}

			//if several non-related windows opened - using any of those
			if (window == null  &&  !newWindow  &&  allWindows.Length != 0)
				window = allWindows[0];

			//if no window found after all - creating new tab (and trying to load mm biomes)
			if (window == null  &&  openWindow)
				window = CreateInstance<StripeWindow>();

			if (window == null)
				return null;

			
			//once window found - deciding whether to add new entry or re-use exising with same name
			int selected = -1;
			{
				int index = window.FindNameIndex(name);
				if (index >=0 )
					selected = index;
			}


			//adding new stripe if no proper selected
			if (selected < 0)
			{
				ArrayTools.Add(ref window.stripes, new StripeDisplay());
				selected = window.stripes.Length-1;
			}

			//setting stripe
			window.stripes[selected].stripe = stripe;
			window.stripes[selected].name = name;
			//window.stripes[selected].offset.x = stripe.offset;
			//window.stripes[selected].scale.x = stripe.offset;


			window.titleContent.text = "Stripe";

			if (openWindow)	
				FocusOnWindow(window, inTab:false);

			return window;
		}


		public static void ReassignStripe (Stripe stripe, string name) 
		/// Assigns new stripe to window of this name, but only if it is opened
		{
			StripeWindow[] allWindows = Resources.FindObjectsOfTypeAll<StripeWindow>();

			for (int w=0; w<allWindows.Length; w++)
			{
				int index = allWindows[w].FindNameIndex(name);
				if (index >=0 )
				{
					allWindows[w].stripes[index].stripe = stripe;
					allWindows[w].Repaint(); 
				}
			}
		}



		private static void FocusOnWindow (StripeWindow window, bool inTab=false)
		/// Opens the graph window. But it should be created and graph assigned first.
		{
			//Texture2D icon = TexturesCache.LoadTextureAtPath("MapMagic/Icons/Window"); 
			//window.titleContent = new GUIContent("Stripe"); //, icon);

			if (inTab) window.ShowTab();
			else window.Show();
			window.Focus();
			window.Repaint();

			DragDrop.obj = null;
			//window.ScrollZoomOnOpen(); //focusing after window has shown (since it needs window size)
		}
	}
}