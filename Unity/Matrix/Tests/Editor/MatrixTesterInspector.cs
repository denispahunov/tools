﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using UnityEngine.Profiling;

using Den.Tools;
using Den.Tools.Matrices;
using Den.Tools.Matrices.Window;
using Den.Tools.GUI;


namespace Den.Tools.Tests
{
	[CustomEditor(typeof(MatrixTester))]
	public class MatrixTesterInspector : Editor
	{
		const int previewSize = 256;

		MatrixTester tester;
		UI ui = new UI();

		Type[] allTestTypes;
		string[] allTestNames;

		
		public override void  OnInspectorGUI ()
		{
			tester = (MatrixTester)target;
			ui.Draw(DrawGUI, inInspector:true);
		}

		public void DrawGUI ()
		{
			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref tester.guiSrc, "Src", isLeft:true))
					if (tester.guiSrc)
					{
						using (Cell.LinePx(0)) DrawMatrixSource(ref tester.gen, ref tester.src);

						Cell.EmptyLinePx(4);
						using (Cell.LinePx(0)) DrawPreview(ref tester.src, ref tester.srcPreview, postfix:"Src");
					}

			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref tester.guiSrcMask, "SrcMask", isLeft:true))
					if (tester.guiSrcMask)
					{
						using (Cell.LinePx(0)) DrawMatrixSource(ref tester.maskGen, ref tester.srcMask);

						Cell.EmptyLinePx(4);
						using (Cell.LinePx(0)) DrawPreview(ref tester.srcMask, ref tester.srcMaskPreview, postfix:"SrcMask");
					}

			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref tester.guiTest, "Test", isLeft:true))
					if (tester.guiTest)
					{
						using (Cell.LineStd) Draw.TypeSelector(ref tester.test, "Tester", ref allTestTypes, ref allTestNames);

						using (Cell.LinePx(0))
							if (tester.test != null)
							{
								Draw.Class(tester.test);
								Draw.Editor(tester.test);
							}

						bool testPressed = false;
						using (Cell.LineStd) 
							if (Draw.Button("Test")) testPressed = true;

						using (Cell.LineStd) Draw.ToggleLeft(ref tester.autoTest, "Auto-test on change");

						if ((Cell.current.valChanged && tester.autoTest) || testPressed)
						{
							//if (tester.src == null) LoadTexture(tester.tex, ref tester.src);
							//if (tester.test is MatrixTester.IMaskTest && tester.srcMask==null) LoadTexture(tester.maskTex, ref tester.srcMask);

							tester.Test();

							UpdatePreview(ref tester.dstPreview, tester.dst);
							if (tester.dstMaskPreview!=null) UpdatePreview(ref tester.dstMaskPreview, tester.dstMask);
							MatrixWindow.OpenUpdate(tester.dst, tester.name + " Dst");
							Repaint();
						}
					}

			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref tester.guiBenchmark, "Benchmark", isLeft:true))
					if (tester.guiBenchmark)
					{
						using (Cell.LineStd) Draw.Field(ref tester.benchmarkIterations, "Iterations");
						using (Cell.LineStd) Draw.Field(ref tester.benchmarkApproaches, "Approaches");

						using (Cell.LineStd) 
							if (Draw.Button("Benchmark"))
							{
								//if (tester.src == null) LoadTexture(tester.tex, ref tester.src);
								//if (tester is MatrixTester.IMaskTest && tester.srcMask==null) LoadTexture(tester.maskTex, ref tester.srcMask);

								tester.Benchmark();

								UpdatePreview(ref tester.dstPreview, tester.dst);
								MatrixWindow.OpenUpdate(tester.dst, tester.name + " Dst");
								Repaint();
							}

						using (Cell.LineStd) Draw.DualLabel("Min Iteration Time", tester.benchmarkIterationTime.ToString("0.") + " ms");
						using (Cell.LineStd) Draw.DualLabel("MegaPixel Time", tester.benchmarkPerMegapixelTime.ToString("0.") + " ms");
						using (Cell.LineStd) Draw.DualLabel("Total Time", tester.benchmarkTotalTime.ToString("0.") + " ms");
					}


			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref tester.guiDst, "Dst", isLeft:true))
					if (tester.guiDst)
						using (Cell.LinePx(0))
							DrawPreview(ref tester.dst, ref tester.dstPreview, postfix:"Dst");

			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref tester.guiDstMask, "DstMask", isLeft:true))
					if (tester.guiDstMask)
						using (Cell.LinePx(0))
							DrawPreview(ref tester.dstMask, ref tester.dstMaskPreview, postfix:"DstMask");

			if (Cell.current.valChanged)
				EditorUtility.SetDirty(tester);
		}


		private void DrawMatrixSource (ref MatrixTester.MatrixSource gen, ref Matrix matrix)
		{
			using (Cell.LinePx(0))
			{
				using (Cell.LineStd) Draw.Field(ref gen.sourceType, "Type");
				using (Cell.LineStd) Draw.Field(ref gen.resolution, "Resolution");

				Cell.EmptyLinePx(5);

				using (Cell.LineStd) 
				{
					Draw.ObjectField(ref gen.tex, "Texture");
					if (Cell.current.valChanged && gen.tex != null)
						gen.resolution = gen.tex.width;
				}

				using (Cell.LineStd) 
				{
					Draw.ObjectField(ref gen.asset, "Asset");
					if (Cell.current.valChanged && gen.asset != null)
						gen.resolution = gen.asset.matrix.rect.size.x;
				}

				using (Cell.LineStd) Draw.Field(ref gen.intensity, "Intensity");
				using (Cell.LineStd) Draw.Field(ref gen.seed, "Seed");
				using (Cell.LineStd) Draw.Field(ref gen.size, "Size");
				using (Cell.LineStd) Draw.Field(ref gen.rotation, "Rotation");
				using (Cell.LineStd) Draw.Button("Load");

				if (Cell.current.valChanged)
				{
					matrix = gen.Load();
					UpdatePreview(ref tester.srcPreview, tester.src);
				}
			}
		}


		private void DrawPreview (ref Matrix matrix, ref Texture2D previewTex, string postfix=null)
		{
			if (previewTex == null  &&  matrix != null)
				UpdatePreview(ref previewTex, matrix);

			if (previewTex != null)
				using (Cell.LinePx(previewSize))
				{
					Cell.EmptyRow();
					using (Cell.RowPx(previewSize))
						Draw.MatrixPreviewTexture(previewTex);
					Cell.EmptyRow();
				}

			if (Draw.Button(visible:false))
				MatrixWindow.GetCreateWindow(matrix, postfix);
		}


		private static void UpdatePreview (ref Texture2D preview, Matrix matrix)
		{
			if (matrix == null) 
				{ preview = null; return; }

			if (preview == null || preview.width != matrix.rect.size.x || preview.height != matrix.rect.size.z)
			{
				preview = new Texture2D(matrix.rect.size.x, matrix.rect.size.z, textureFormat:TextureFormat.R16, mipChain:false);
				preview.filterMode = FilterMode.Point;
			}
			matrix.ExportTextureRaw(preview);
		}
	}
}