﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

using Den.Tools;
using Den.Tools.Matrices;
using Den.Tools.GUI;

namespace Den.Tools.Tests
{

	[System.Serializable]
	[HelpURL("https://gitlab.com/denispahunov/mapmagic/wikis/home")]
	//[CreateAssetMenu(menuName = "Test/Matrix Test", fileName = "MatrixTest.asset", order = 101)]  
	public class MatrixTester : ScriptableObject, ISerializationCallbackReceiver 
	{
		[Serializable]
		public partial class MatrixSource 
		{
			public enum SourceType { Texture, Asset, Noise, Voronoi, Cone, Sphere, Circle, Pyramid, Gradient, GradientX, GradientZ, Point, Line, AALine };
			public SourceType sourceType = SourceType.Texture;

			public int resolution = 512;  
			public Texture2D tex;
			public MatrixAsset asset; 
			public float intensity = 1f;
			public float size = 200;
			public float rotation = 0;
			public int seed = 12345;

			//public Matrix Load ();
		}

		

		public MatrixSource gen = new MatrixSource();
		public MatrixSource maskGen = new MatrixSource();

		public bool resize;
		public CoordRect resizeRect;

		[NonSerialized] public Matrix src;
		[NonSerialized] public Matrix srcMask;
		[NonSerialized] public ITest test;
		[NonSerialized] public Matrix dst;
		[NonSerialized] public Matrix dstMask;

		[NonSerialized] public Texture2D srcPreview;
		[NonSerialized] public Texture2D srcMaskPreview;
		[NonSerialized] public Texture2D dstPreview;
		[NonSerialized] public Texture2D dstMaskPreview;

		public bool guiLoad;
		public bool guiSrc;
		public bool guiSrcMask;
		public bool guiMask;
		public bool guiTest;
		public bool guiBenchmark;
		public bool guiDst;
		public bool guiDstMask;

		public bool autoTest;

		public int benchmarkIterations = 5;
		public int benchmarkApproaches = 5;
		public double benchmarkIterationTime;
		public double benchmarkTotalTime;
		public double benchmarkPerMegapixelTime;


		public void Test ()
		{
			if (test==null) return;

			if (src == null)
				src = gen.Load();

			if (test is IPrepare prepTest) dst = prepTest.Prepare(src);
			else dst = new Matrix(src);
			if (test is IMaskTest) dstMask = new Matrix(srcMask);

			if (test is ISimpleTest simpleTest) simpleTest.Process(dst);
			if (test is ISrcDstTest srcDstTest) srcDstTest.Process(src, dst);
			if (test is IRefTest refTest) refTest.Process(ref dst);
			if (test is ISrcRefDstTest srcRefDstTest) srcRefDstTest.Process(src, ref dst);
			if (test is IMaskTest maskTest) maskTest.Process(src, srcMask, dst, dstMask);
		}


		public void Benchmark ()
		{
			if (src==null || test==null) return;

			double totalTime = 0;
			double minTime = double.MaxValue;
			for (int a=0; a<benchmarkApproaches; a++)
			{
				Matrix[] dsts = new Matrix[benchmarkIterations];
				Matrix[] dstMasks = new Matrix[benchmarkIterations];

				for (int i=0; i<benchmarkIterations; i++)
				{
					if (test is IPrepare prepTest) dsts[i] = prepTest.Prepare(src);
					else dsts[i] = new Matrix(src);
					if (test is IMaskTest) dstMasks[i] = new Matrix(srcMask);
				}

				Stopwatch timer = new Stopwatch();
				timer.Start();

				for (int i=0; i<benchmarkIterations; i++)
				{
					if (test is ISimpleTest simpleTest) simpleTest.Process(dsts[i]);
					if (test is ISrcDstTest srcDstTest) srcDstTest.Process(src, dsts[i]);
					if (test is IRefTest refTest) refTest.Process(ref dsts[i]);
					if (test is ISrcRefDstTest srcRefDstTest) srcRefDstTest.Process(src, ref dsts[i]);
					if (test is IMaskTest maskTest) maskTest.Process(src, srcMask, dsts[i], dstMasks[i]);
				}

				timer.Stop();
				totalTime += timer.ElapsedMilliseconds;
				if (timer.ElapsedMilliseconds<minTime)
					minTime = timer.ElapsedMilliseconds;

				dst = dsts[dsts.Length-1]; //assigning last result as dst to see it's working
			}

			benchmarkTotalTime = totalTime;
			benchmarkIterationTime = minTime / benchmarkIterations;
			benchmarkPerMegapixelTime = benchmarkIterationTime / (1f*src.rect.Count / 1000000);
		}


		#region Source

			public partial class MatrixSource
			{
				public Matrix Load ()
				{
					if (sourceType == SourceType.Texture)
					{
						if (tex==null) { return null; }
						Matrix matrix = new Matrix( new CoordRect(0,0,tex.width, tex.height) );
						matrix.ImportTextureRaw(tex);

						if (matrix.rect.size.x != resolution) 
							MatrixOps.Resize(matrix, new CoordRect(0,0,resolution, resolution));

						return matrix;
					}


					if (sourceType == SourceType.Asset)
					{
						if (asset==null || asset.matrix==null) { return null; }
						Matrix matrix = new Matrix(asset.matrix);

						if (matrix.rect.size.x != resolution) 
							MatrixOps.Resize(matrix, new CoordRect(0,0,resolution, resolution));

						return matrix;
					}


					if (sourceType == SourceType.Noise)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Noise noise = new Noise(seed);

						int iterations = (int)Mathf.Log(size,2) + 1; //+1 max size iteration

						Coord min = matrix.rect.Min; Coord max = matrix.rect.Max;
						for (int x=min.x; x<max.x; x++)
							for (int z=min.z; z<max.z; z++)
							{ 
								float val = noise.Fractal(x, z, size, iterations, detail:0.5f, turbulence:0, type:2);
								matrix[x,z] = val * intensity;
							}

						return matrix;
					}


					if (sourceType == SourceType.Voronoi)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Noise noise = new Noise(seed);

						CoordRect rect = CoordRect.WorldToPixel((Vector2D)0, (Vector2D)1000, (Vector2D)size);  //will convert to cells as well
						Vector3D matrixPos = new Vector3D(rect.offset.x*size, 0, rect.offset.z*size);  //modifying worldPos/size too
						Vector3D matrixSize = new Vector3D(rect.size.x*size, 250, rect.size.z*size);

						PositionMatrix posMatrix = new PositionMatrix(rect, matrixPos, matrixSize);
						posMatrix.Scatter(0, noise);
						posMatrix = posMatrix.Relaxed();

						Coord min = matrix.rect.Min; Coord max = matrix.rect.Max; 
						for (int x=min.x; x<max.x; x++)
							for (int z=min.z; z<max.z; z++)
							{
								Vector3D closest; Vector3D secondClosest;
								float minDist; float secondMinDist;
								posMatrix.GetTwoClosest(new Vector3D(x/size,0,z/size), out closest, out secondClosest, out minDist, out secondMinDist); 

								matrix[x,z] = (secondMinDist-minDist) / 160 * intensity;
							}

						return matrix;
					}


					if (sourceType == SourceType.Cone  ||  sourceType == SourceType.Circle  ||  sourceType == SourceType.Gradient  ||  sourceType == SourceType.Pyramid  ||  sourceType == SourceType.Sphere)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						
						Vector2D center = (Vector2D)resolution/2f;

						Coord min = matrix.rect.Min; Coord max = matrix.rect.Max;
						for (int x=min.x; x<max.x; x++)
							for (int z=min.z; z<max.z; z++)
							{
								Vector2D formPos = new Vector2D(x,z);
								float val = 0;

								switch (sourceType)
								{
									case SourceType.Cone:
										val = 1 - ((center-formPos).Magnitude)/size;
										if (val<0) val = 0;
										break;
									case SourceType.Sphere:
										val = 1 - ((center-formPos).Magnitude)/size;
										if (val<0) val = 0;
										val = Mathf.Sqrt(2*val - val*val);
										break;
									case SourceType.Circle:
										val = 1 - ((center-formPos).Magnitude)/size;
										if (val<(size/matrix.rect.size.x)) val = 0;
										else val = 1;
										break;
									case SourceType.Pyramid:
										float valX = formPos.x / size; if (valX > 1-valX) valX = 1-valX;
										float valZ = formPos.z / size; if (valZ > 1-valZ) valZ = 1-valZ;
										val = valX<valZ? valX*2 : valZ*2;
										break;
									case SourceType.GradientX:
										val = formPos.x / size;
										break;
									case SourceType.GradientZ:
										val = formPos.z / size;
										break;
								}
								
								matrix[x,z] = val*intensity;
							}

						return matrix;
					}


					if (sourceType == SourceType.Point)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Coord center = (Coord)resolution/2f;
						matrix[center] = 1;
						return matrix;
					}


					if (sourceType == SourceType.Line  ||  sourceType == SourceType.AALine)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Vector2D center = (Vector2D)resolution/2f;
						Vector2D dir = new Vector2D( Mathf.Sin(rotation*Mathf.Deg2Rad), Mathf.Cos(rotation*Mathf.Deg2Rad) );
						dir *= resolution/2;
						dir *= size/100;
						matrix.Line(center-dir, center+dir, antialised:sourceType==SourceType.AALine);
						return matrix;
					}


					return null;
				}
			}

		#endregion


		#region Tests

			public interface ITest { }
			public interface ISimpleTest : ITest  { void Process (Matrix matrix); }
			public interface ISrcDstTest : ITest  { void Process (Matrix src, Matrix dst); }
			public interface ISrcRefDstTest : ITest  { void Process (Matrix src, ref Matrix dst); }
			public interface IRefTest : ITest { void Process (ref Matrix matrix); }
			public interface IMaskTest : ITest  { void Process (Matrix src, Matrix srcMask, Matrix dst, Matrix dstMask); }

			public interface IPrepare { Matrix Prepare (Matrix src); } //prepares the dst in a special way
			
			public interface IBenchmark { void Benchmark (Matrix src); } //special case for benchmark with no additional stuff (and no result returned)


			public class Resize : ISrcDstTest, IPrepare
			{
				[Val("Dst Rect")] public CoordRect dstRect = new CoordRect(0,0,100,100);
				[Val("Fast")] public bool fast = false;

				public Matrix Prepare (Matrix src)
					{ return new Matrix(dstRect); }

				public void Process (Matrix src, Matrix dst) 
				{ 
					if (!fast) MatrixOps.Resize(src, dst); 
					else MatrixOps.ResizeFast(src, dst); 
				}
			}

			public class MipMap : ISrcRefDstTest, IBenchmark
			{
				[Val("Count")] public int count = 4;

				public void Process (Matrix src, ref Matrix dst)
				{
					Matrix[] mips = MatrixOps.GenerateMips(src, count);
					dst = MatrixOps.TestMips(mips);
				}

				public void Benchmark (Matrix src)
					{ MatrixOps.GenerateMips(src, count); }
			}


			public class GaussianBlurTest : ISrcDstTest, IPrepare
			{
				[Val("Blur")] public float blur = 1.5f;
				[Val("Use Self")] public bool useSelf = true;

				public Matrix Prepare (Matrix src)
					{ return new Matrix(src); }

				public void Process (Matrix src, Matrix dst) 
					{ MatrixOps.GaussianBlur(useSelf ? dst : src, dst, blur); }
			}


			public class DownsampleBlurTest : ISrcDstTest, IPrepare
			{
				[Val("Downsample")] public int downsample = 2;
				[Val("Blur")] public float blur = 1;
				[Val("Use Self")] public bool useSelf = true;

				public Matrix Prepare (Matrix src)
					{ return new Matrix(src); }

				public void Process (Matrix src, Matrix dst) 
					{ MatrixOps.DownsampleBlur(useSelf ? dst : src, dst, downsample, blur); }
			}


			public class DownsampleOverblurTest : ISrcDstTest, IPrepare
			{
				[Val("Min Resolution")] public int minResolution = 4;
				[Val("Blur")] public float blur = 1;
				[Val("Escalate")] public float escalate = 2;
				[Val("Use Self")] public bool useSelf = true;

				public Matrix Prepare (Matrix src)
					{ return new Matrix(src); }

				public void Process (Matrix src, Matrix dst) 
					{ MatrixOps.OverblurMipped(useSelf ? dst : src, dst, escalate, minResolution, blur); }
			}


			public class SpreadLinearTest : ISrcDstTest, IPrepare
			{
				[Val("Subtract")] public float subtract = 0.01f;
				[Val("Diagonals")] public bool diagonals;
				[Val("Quarters")] public bool quarters;
				[Val("Bulb")] public bool bulb;

				public Matrix Prepare (Matrix src)
					{ return new Matrix(src); }

				public void Process (Matrix src, Matrix dst) 
					{ MatrixOps.SpreadLinear(src, dst, subtract, diagonals, quarters, bulb); }
			}


			public class PaddingOnePixelTest : IMaskTest
			{
				[Val("Intensity")] public float intensity = 1;

				public void Process (Matrix src, Matrix srcMask, Matrix dst, Matrix dstMask)
					{ MatrixOps.PaddingOnePixel(src, srcMask, dst, dstMask, intensity); }
			}


			public class PaddingMipped : IMaskTest
			{
				[Val("MinVal")] public float minVal = 0.001f;
				[Val("MipCount")] public int mipCount = -1;
				[Val("MipContrast")] public float mipContrast = 2;
				[Val("Mip 1px Padding")] public float mipOnePxPadding = 0.5f;

				public void Process (Matrix src, Matrix srcMask, Matrix dst, Matrix dstMask)
					{ MatrixOps.PaddingMipped(src, srcMask, dst, mipCount, mipContrast, mipOnePxPadding); }
			}


			public class PaddingSpreadTest : IMaskTest
			{
				[Val("Fade")] public float fade = 0.001f;

				public void Process (Matrix src, Matrix srcMask, Matrix dst, Matrix dstMask)
					{ MatrixOps.PaddingSpread(src, dst, srcMask, dstMask, fade, fade); }
			}


			public class Resize_UpscaleFast : IPrepare, ISrcDstTest
			{
				[Val("Dst Offset")] public Coord dstOffset = new Coord(0,0); 
				[Val("Dst Size")] public Coord dstSize = new Coord(100,100); 

				public Matrix Prepare (Matrix src)
					{ return new Matrix(dstOffset, dstSize); }

				public void Process (Matrix src, Matrix dst)
					{ MatrixOps.UpscaleFast(src, dst); }
			}


			public class Resize_DownscaleFast : IPrepare, ISrcDstTest
			{
				[Val("Dst Offset")] public Coord dstOffset = new Coord(100,100); 

				public Matrix Prepare (Matrix src)
					{ return new Matrix(dstOffset, src.rect.size/2); }

				public void Process (Matrix src, Matrix dst)
					{ MatrixOps.DownscaleFast(src, dst); }
			}


			public class Resize_LosslessUpscaleDownscaleTest : ISimpleTest
			{
				[Val("Upscale NearestNeigh")] public bool upscaleNN = false;
				[Val("Downscale NearestNeigh")] public bool downscaleNN = true; 

				public void Process (Matrix src)
				{ 
					Matrix big = new Matrix(src.rect*2);

					if (upscaleNN) MatrixOps.ResizeNearestNeighbor(src, big); 
					else MatrixOps.UpscaleFast(src, big); 

					if (downscaleNN) MatrixOps.ResizeNearestNeighbor(big, src); 
					else MatrixOps.DownscaleFast(big, src); 
				}
			}


			public class LineTest : ISimpleTest
			{
				[Val("Pos1")] public Vector2D pos1 = new Vector2D(10,10);
				[Val("Pos2")] public Vector2D pos2 = new Vector2D(100,10);
				[Val("Pos3")] public Vector2D pos3 = new Vector2D(200,110);
				[Val("Pos4")] public Vector2D pos4 = new Vector2D(209,219);
				[Val("Pos5")] public Vector2D pos5 = new Vector2D(53,167);
				[Val("Pos6")] public Vector2D pos6 = new Vector2D(130,97);
				[Val("Pos7")] public Vector2D pos7 = new Vector2D(90,166);

				public void Process (Matrix dst)
				{
					dst.Line(pos1+(Vector2D)dst.rect.offset, pos2+(Vector2D)dst.rect.offset.Vector2D(), 1,1, true);
					dst.Line(pos2+(Vector2D)dst.rect.offset, pos3+(Vector2D)dst.rect.offset.Vector2D(), 1,1, true);
					dst.Line(pos3+(Vector2D)dst.rect.offset.Vector2D(), pos4+(Vector2D)dst.rect.offset.Vector2D(), 1,1, true);
					dst.Line(pos4+(Vector2D)dst.rect.offset.Vector2D(), pos5+(Vector2D)dst.rect.offset.Vector2D(), 1,1, true);
					dst.Line(pos5+(Vector2D)dst.rect.offset.Vector2D(), pos6+(Vector2D)dst.rect.offset.Vector2D(), 1,1, true);
					dst.Line(pos6+(Vector2D)dst.rect.offset.Vector2D(), pos7+(Vector2D)dst.rect.offset.Vector2D(), 1,0, true);
				}
			}


			public class CavityTest : ISrcDstTest
			{
				[Val("Intensity")] public float intensity = 1;

				public void Process (Matrix src, Matrix dst)
					{ MatrixOps.Cavity(src, dst); }
			}


			public class OverSpreadTest : ISrcDstTest
			{
				[Val("Intensity")] public float intensity = 1;

				public void Process (Matrix src, Matrix dst)
					{ MatrixOps.OverSpread(src, dst, intensity); }
			}






			public class PredictPaddingTest : ISimpleTest
			{
				[Val("Expand Edge")] public float expandEdge = 0.1f;
				[Val("Expand Pixels")] public int expandPixels = 50;

				public void Process (Matrix dst)
				{ 
					Matrix src = new Matrix(dst);
					src.Add(-0.0001f); //to make negative values
					MatrixOps.PredictPadding(src, dst, expandEdge, expandPixels); 
				}
			}



			public class ExpandCircularTest : ISrcDstTest
			{
				[Val("Coord")] public Coord center = new Coord(120,120);
				[Val("Radius")] public int radius = 100;
				[Val("Range")] public int range = 10;
				[Val("Expand Edge")] public float expandEdge = 0.1f;
				[Val("Expand Pixels")] public int expandPixels = 50;

				public void Process (Matrix src, Matrix dst)
					{  MatrixOps.ExtendCircular(dst, center, radius, range, expandPixels); }
			}


			public class ReadWriteLineTest : ISrcDstTest
			{
				[Val("Start")] public Vector2 start = new Vector2(10,10);
				[Val("End")] public Vector2 end = new Vector2(20,30);

				public void Process (Matrix src, Matrix dst)
				{
					Stripe stripe = new Stripe(src.rect.size.x);
					MatrixOps.ReadStrip(stripe, src, start, end);
					stripe.Fill(0);
					MatrixOps.WriteStrip(stripe, dst, start, end);
				}
			}


			public class AddTest : IMaskTest
			{
				[Val("Value")] public float val = 0.05f;
				[Val("Iterations")] public int iterations =10;
				[Val("Native")] public bool native;

				[DllImport ("__Internal", EntryPoint = "Test")]
				private static extern int Test (int input);

				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "CoordTest")]
				private static extern int CoordTest (Coord coord);

				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "CoordRectTest")]
				private static extern int CoordRectTest (CoordRect coordRect);

				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "MatrixAdd")] 
				private static extern void Add (Matrix matrix, Matrix m, float val);

				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "MatrixBlendLayers")] 
				private static extern void Blend (IntPtr layers, float[] opacities, int layersCount);

				public void Process (Matrix src, Matrix srcMask, Matrix dst, Matrix dstMask) 
				{
				//	Matrix[] arr = new Matrix[] {dst, dstMask};
				//	float[] opacities = new float[] { 1, 1};

					//MatrixSet ms = new MatrixSet();
					//ms.arrOfMatrix = arr;
					//arr[0].arr[0] = 42;

				//	GCHandle handle = GCHandle.Alloc(arr, GCHandleType.Pinned);
				//	IntPtr address = handle.AddrOfPinnedObject();

				//	Blend(address, opacities, 2);
					//UnityEngine.Debug.Log( Blend(dst, address, opacities, 2) );

				//	handle.Free();

				//	UnityEngine.Debug.Log( Test(31) );
				

					if (native) Add(dst, dstMask, 0.5f);
					else dst.Add(dstMask, 0.5f);
				}
			}


			public class NativeNoiseTest : ISimpleTest
			{
				[System.NonSerialized] Noise noise = new Noise(12346);
				[Val("Size")] public float size = 10;
				[Val("Native")] public bool native;

				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "NoiseLinear")]
				private static extern float NoiseLinear (Noise thisNoise, float x, float y);

				[DllImport ("NativePlugins", CallingConvention = CallingConvention.Cdecl, EntryPoint = "NoiseFractal")]
				private static extern float NoiseFractal (Noise thisNoise, int x, int y, float size, int iterations=-1, float detail=0.5f, float turbulence=0, int type=2);

				public void Process (Matrix matrix) 
				{
					for (int x=0; x<matrix.rect.size.x; x++)
						for (int z=0; z<matrix.rect.size.z; z++)
						{
							int pos = z*matrix.rect.size.x + x;
							float fx = x / size;
							float fz = z / size;

							float val;
							if (!native) val = noise.Fractal(x,z,size);
							else val = NoiseFractal(noise, x, z,size);

							matrix.arr[pos] = val;
						}
				}
			}

			


			public class ChangeRange : ISimpleTest
			{
				[Val("From")] public Vector2 from = new Vector2(0,1);
				[Val("To")] public Vector2 to = new Vector2(-1,1);

				public void Process (Matrix matrix) 
				{
					matrix.ChangeRange(from.x, from.y, to.x, to.y);
				}
			}


			public class SelectTest : ISrcDstTest
			{
				[Val("Level")] public float level = 1;
				[Val("Antialiasing")] public bool antialiasing = true;

				public void Process (Matrix src, Matrix dst)
					{ MatrixOps.Silhouette(src, dst, level, antialiasing); }
			}


			public class BeachTest : IRefTest
			{
				[Val("Level")] public float level = 1;
				[Val("Spread 1")] public float spread1 = 1;
				[Val("Spread 2")] public float spread2 = 1;
				[Val("Blur")] public float blur = 1;
				[Val("Antialiasing")] public bool antialiasing = true;

				public void Process (ref Matrix src)
				{ 
					Matrix sel = new Matrix(src);
					MatrixOps.Silhouette(src, sel, level, antialiasing:antialiasing); 

					Matrix bea = new Matrix(sel.rect);
					MatrixOps.SpreadLinear(sel, bea, spread1, true); //MatrixOps.Spread(sel, bea, spread1, 1);

					//MatrixOps.GaussianBlur(bea, blur);

					src = bea;
				}
			}


			public class NormalsSetTest : IRefTest
			{
				public enum DisplayAxis { X, Z, Blue };
				[Val("Axis")] public DisplayAxis displayAxis;
				[Val("Size")] public float size = 1000;
				[Val("Height")] public float height = 250;

				public void Process (ref Matrix src)
				{ 
					Matrix normX = new Matrix(src.rect);
					Matrix normZ = new Matrix(src.rect);
					Matrix normBlue = new Matrix(src.rect);

					MatrixOps.NormalsSet(src, normX, normZ, normBlue, size/src.rect.size.x, height);

					switch (displayAxis)
					{
						case DisplayAxis.X: src = normX; break;
						case DisplayAxis.Z: src = normZ; break;
						case DisplayAxis.Blue: src = normBlue; break;
					}
				}
			}

			public class NormalsDirectedTest : ISrcDstTest
			{
				[Val("Hor Angle")] public float horAngle = 0;
				[Val("Vert Angle")] public float vertAngle = 0;
				[Val("Size")] public float size = 1000;
				[Val("Height")] public float height = 250;
				[Val("Intensity")] public float intensity = 1;
				[Val("Wrapping")] public float wrapping = 1;

				public void Process (Matrix src, Matrix dst)
				{ 
					Vector3 dir = new Vector3(Mathf.Sin(horAngle*Mathf.Deg2Rad), Mathf.Tan(vertAngle*Mathf.Deg2Rad), Mathf.Cos(horAngle*Mathf.Deg2Rad));
					dir = dir.normalized;

					MatrixOps.NormalsDir(src, dst, dir, size/src.rect.size.x, height, intensity, wrapping);
				}
			}

		#endregion

		#region Serialize

			public Serializer.Object[] serializedTester = new Serializer.Object[0];

			public void OnBeforeSerialize ()
			{ 
				serializedTester = Serializer.Serialize(test);
			}

			public void OnAfterDeserialize () 
			{
				test = (ITest)Serializer.Deserialize(serializedTester); 
			}

		#endregion

	}
}
