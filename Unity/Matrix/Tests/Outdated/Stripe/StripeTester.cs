
using UnityEngine;

using Den.Tools.Matrices;
using System;

namespace Den.Tools.Tests
{
	//It's for Universal Tester, and doesn't seem to be working
	/*[Test(category="Stripe")]
	public class WindowTest : ITest
	{
		public Stripe stripe = new Stripe();
		public int length = 9;

		public void Test ()
		{
			float[] arr = new float[] { 0.5f, 0, 0.5f, 1, 0.75f, 0.5f, 0.25f, 0, 1,1,1 }; 
			stripe = new Stripe(arr, length);
		}
	}*/


	[CreateAssetMenu(fileName = "StripeTester", menuName = "Test/StripeTester")]
	public class StripeTester : TestAsset, ITestInspectorGUI, ITestRegression  
	/// Remember it should be named same as file!
	{
		//TODO: opened windows with stripes do not refresh!
		//RE-OPEN THEM MANUALLY
		
		public Stripe src = new Stripe( new float[] {0,1, 0,0,0, 1,1, 0,0,0, 1,1, 0,0,0, 1,1, 0,0,0,0, 1,1, 0} );
		public Stripe dst = new Stripe();
		public Stripe photoshop = new Stripe();
		public Stripe reg = new Stripe();

		public enum PhotoshopPreset { None, UpscaleLinear2x, UpscaleLinear25x, UpscaleLinear3x, DownsampleLinear2x, Downsample25x, Downsample3x, UpscaleCubic2x, UpscaleCubic25x, UpscaleCubic3x }
		[Space(10)]
		public PhotoshopPreset photoshopPreset;

		public enum TestFunction { ResampleNN, UpsampleLinear, DownsampleLinear, UpsampleCubic }
		[Space(10)]
		public TestFunction testFunction;


		public override void Test ()
		{
			dst.Clear();

			System.Reflection.MethodInfo testMethod = typeof(Stripe).GetMethod(testFunction.ToString());
			testMethod.Invoke(null, new object[] { src, dst} );
		}


		public bool Regression ()
		{
			Test();
			if (dst is IComparable comDst)
				return comDst.CompareTo(reg) > 0;
			return false;
		}

		public void StoreRegression ()
		{
			if (reg==null)
				reg = new Stripe();
			Stripe.Copy(dst, reg);
		}


		public void OnInspectorGUI ()
		{
			Den.Tools.Matrices.Window.StripeWindow.Show(src, newWindow:false, openWindow:false, name:"src");
			Den.Tools.Matrices.Window.StripeWindow.Show(dst, newWindow:false, openWindow:false, name:"dst");
			if (photoshop != null && photoshop.length != 0)
				Den.Tools.Matrices.Window.StripeWindow.Show(photoshop, newWindow:false, openWindow:false, name:"photoshop");

			if (photoshopPreset != PhotoshopPreset.None)
			{
				src = new Stripe( new float[] {0,1, 0,0,0, 1,1, 0,0,0, 1,1, 0,0,0, 1,1, 0,0,0,0, 1,1, 0} );

				switch (photoshopPreset)
				{
					case PhotoshopPreset.UpscaleLinear2x: photoshop=new Stripe( new float[] {0, 0.25f, 0.75f, 0.75f,0.25f, 0,0,0,0, 0.25f, 0.75f, 1,1,0.75f, 0.25f, 0}, length:48, offset:0, size:24); break;
					case PhotoshopPreset.UpscaleLinear25x: photoshop=new Stripe( new float[] {0, 0.1f, 0.5f, 0.9f, 0.7f, 0.3f, 0,0,0,0,0, 0.1f, 0.5f, 0.9f, 1,1,0.9f,0.5f,0.1f,0}, 60, 0, 24);  break;
					case PhotoshopPreset.UpscaleLinear3x: photoshop=new Stripe( new float[] { 0,0, 0.3333f, 0.6666f, 1, 0.6666f, 0.3333f, 0,0,0,0,0,0,0, 0.3333f, 0.6666f, 1,1,1,1, 0.6666f, 0.3333f, 0}, 72, 0, 24 );  break;

					case PhotoshopPreset.DownsampleLinear2x: photoshop=new Stripe( new float[] {0.5f, 0, 0.5f,0.5f, 0, 1, 0, 0.5f, 0.5f, 0, 0.5f,0.5f}, 0, 24);  break;
					case PhotoshopPreset.Downsample25x: 
						src = new Stripe( new float[] {0,1, 0,0,0, 1,1, 0,0,0, 1,1, 0,0,0, 1,1, 0,0,0,0, 1,1, 0}, 25);
						photoshop=new Stripe( new float[] {0.5f, 0, 1, 0, 1, 0, 1, 0, 0.5f, 0.5f}, 0, 25);  break;
					case PhotoshopPreset.Downsample3x: photoshop=new Stripe( new float[] {0.3333f, 0.3333f, 0.3333f, 0.6666f, 0, 0.6666f, 0, 0.6666f}, 0, 24);  break;

					case PhotoshopPreset.UpscaleCubic2x: photoshop=new Stripe( new float[] {0, 0.26f, 0.88f, 0.88f, 0,0,0,0, 0.23f, 0.77f, 1,1, 0.77f, 0.23f, 0}, 48, 0, 24); break;
					case PhotoshopPreset.UpscaleCubic25x: photoshop=new Stripe( new float[] {0, 0.09f, 0.59f, 0.98f, 0.84f, 0.32f, 0,0,0,0,0, 0.08f,0.5f,0.92f,1,1,0.92f,0.5f,0.08f,0}, 60, 0, 24);  break;
					case PhotoshopPreset.UpscaleCubic3x: photoshop=new Stripe( new float[] { 0,0, 0.37f, 0.79f, 1, 0.79f, 0.37f, 0,0,0,0,0,0,0, 0.32f, 0.68f, 1,1,1,1, 0.68f, 0.32f, 0}, 72, 0, 24);  break;
				}
			}

			photoshopPreset = PhotoshopPreset.None;
		}
	}
}