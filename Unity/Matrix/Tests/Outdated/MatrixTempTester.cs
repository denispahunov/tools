﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

using Den.Tools;
using Den.Tools.Matrices;
using Den.Tools.GUI;

namespace Den.Tools.Tests
{

	[System.Serializable]
	[HelpURL("https://gitlab.com/denispahunov/mapmagic/wikis/home")]
	[CreateAssetMenu(menuName = "Test/Matrix Test Test", fileName = "MatrixTest.asset", order = 101)]  
	public class MatrixTempester : ScriptableObject 
	{
		public float tempFloat2;
		//[SerializeReference] 
		public MatrixWorld matrix = new MatrixWorld(new CoordRect(1,2,3,4), new Vector3D(0,0,0), new Vector3D(1000,1000,1000));
		public MatrixWorld matrix2 = new MatrixWorld(new CoordRect(1,2,3,4), new Vector3D(0,0,0), new Vector3D(1000,1000,1000));
		public float tempFloat;
		public Vector3 tempVec;
		
	}
}
