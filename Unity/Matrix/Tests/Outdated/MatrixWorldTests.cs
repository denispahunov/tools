﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools;
using Den.Tools.Tests;
using Den.Tools.Matrices;
//using Den.Tools.Splines;
using Den.Tools.GUI;
using Den.Tools.Segs;


namespace Den.Tools.Segs.Tests
{
	[Test(category="MatrixWorld")]
	public class GetWorldHeightTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Heights", allowSceneObject=true)] public Texture2D heightsTex;
		[Val("PixelSize")] public float pixelSize = 1;
		[Val("Height")] public float height = 250;

		[NonSerialized] private MatrixWorld heightsMatrix;
		[NonSerialized] private MatrixHeightGizmo heightsMatrixGizmo;

		[Val("Position")] public Vector2D pos = new Vector2D(10,10);
		[Val("Pos Y")] public float posY = 0;


		public void OnSceneGUI ()
		{
			if (heightsMatrixGizmo==null) return;
			heightsMatrixGizmo.Draw();

			pos = DebugGizmos.PositionHandle(pos);

			DebugGizmos.DrawDot("Test", new Vector3(pos.x, posY, pos.z), 6, Color.green);
		}

		public void Test ()
		{
			if (heightsTex==null) return;

			if (heightsMatrix==null || heightsMatrix.rect.size.x!=heightsTex.width || heightsMatrix.rect.size.z!=heightsTex.height)
				heightsMatrix = new MatrixWorld(new CoordRect(0,0,heightsTex.width,heightsTex.height), new Vector2D(), new Vector2D(), 0); 
			heightsMatrix.worldSize = new Vector3D(heightsTex.width*pixelSize, height, heightsTex.height*pixelSize);
			heightsMatrix.ImportTexture(heightsTex);

			if (heightsMatrixGizmo==null) heightsMatrixGizmo = new MatrixHeightGizmo();
			heightsMatrixGizmo.SetMatrixWorld(heightsMatrix, true);

			posY = heightsMatrix.GetWorldInterpolatedValue(pos.x, pos.z) * heightsMatrix.worldSize.y;
		}
	}


	[Test(category="MatrixWorld")]
	public class PixelToWorldTest : ITest, ISceneGUI
	/// Determines whether ball is on left or on right side of the line
	{
		[Val("Heights", allowSceneObject=true)] public MatrixObject matrixObj;
		[Val("Src Position")] public Vector2D srcPos = new Vector2D(10,10);
		[Val("Dst Position")] public Vector2D dstPos = new Vector2D(10,10);

		public void OnSceneGUI ()
		{
			srcPos = DebugGizmos.PositionHandle(srcPos);
			DebugGizmos.DrawDot("PixelToWorld", dstPos.Vector3(), 6, Color.green);
		}

		public void Test ()
		{
			if (matrixObj==null || matrixObj.matrix==null) return;

			Coord coord = matrixObj.MatrixWorld.WorldToPixel(srcPos.x, srcPos.z);
			dstPos = matrixObj.MatrixWorld.PixelToWorld(coord.x, coord.z);

			matrixObj.matrix.Fill(0);
			matrixObj.matrix[coord] = 1;
			matrixObj.RefreshGizmos();
		}
	}
}