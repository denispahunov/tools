using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools.Matrices;
using Den.Tools.GUI;
using static Den.Tools.Matrices.MatrixOps;
using UnityEditor;
using System;
using static Den.Tools.Tests.StripeTester;

namespace Den.Tools.Tests
{
	[CreateAssetMenu(menuName = "Test/Matrix World Tester", fileName = "MatrixWorldTest.asset")]  
	public class MatrixWorldTester : TestAsset, ITestInspectorGUI, ITestRegression  
	/// Remember it should be named same as file!
	{
		public MatrixWorld src = new MatrixWorld(new CoordRect(0,0,10,10), new Vector3D(0,0,0), new Vector3D(100,1,100));
		public MatrixWorld dst = new MatrixWorld(new CoordRect(0,0,10,10), new Vector3D(0,0,0), new Vector3D(100,1,100));
		public MatrixWorld reg = new MatrixWorld(new CoordRect(0,0,10,10), new Vector3D(0,0,0), new Vector3D(100,1,100));

		public MatrixSource srcSource = new MatrixSource();

		public enum TestFunction { Resize, CopyInterpolated, CopyInterpolatedRegion }
		[Space(10)] public TestFunction testFunction;


		public override void Test ()
		{
			dst.Fill(0);

			System.Reflection.MethodInfo testMethod = typeof(MatrixOps).GetMethod(testFunction.ToString(),new Type[] { typeof(Matrix),typeof(Matrix) });
			if (testMethod==null)
				testMethod = typeof(MatrixOps).GetMethod(testFunction.ToString(),new Type[] { typeof(MatrixWorld),typeof(MatrixWorld) });
			if (testMethod == null)
				testMethod = typeof(MatrixWorldTester).GetMethod(testFunction.ToString(), System.Reflection.BindingFlags.Instance, null, Type.EmptyTypes, null);

			testMethod.Invoke(null,new object[] { src,dst });
		}

		public bool Regression ()
		{
			Test();
			if (dst is IComparable comDst)
				return comDst.CompareTo(reg) > 0;
			return false;
		}

		public void StoreRegression ()
		{
			if (reg==null)
				reg = new MatrixWorld();
			MatrixWorld.Copy(dst, reg);
		}


		#region Specific Tests

			/*public void CopyInterpolatedRegion ()
			{
				Vector2D firstOffset = GetAdditionalValue<Vector2D>("First Offset");
				Vector2D firstSize = GetAdditionalValue<Vector2D>("First Size");

				Vector2D secondOffset = GetAdditionalValue<Vector2D>("Second Offset");
				Vector2D secondSize = GetAdditionalValue<Vector2D>("Second Size");

				Matrix tmp = null;
				MatrixOps.CopyInterpolated(src, dst, firstOffset, firstSize, ref tmp);
				MatrixOps.CopyInterpolated(src, dst, secondOffset, secondSize, ref tmp);
			}*/

		#endregion


		#region GUI

		public void OnInspectorGUI ()
			{
				if (srcSource.sourceType != MatrixSource.SourceType.None)
				{
					Matrix m = srcSource.Load();
					if (m != null)
						Matrix.Copy(m, src); //this will preserve src's world coords

					srcSource.sourceType = MatrixSource.SourceType.None;
				}
			}

		#endregion


		#region MatrixSource

			[Serializable]
			public partial class MatrixSource 
			{
				public enum SourceType { None, Texture, Asset, Noise, Voronoi, Cone, Sphere, Circle, Pyramid, Gradient, GradientX, GradientZ, Point, Line, AALine };
				public SourceType sourceType = SourceType.Texture;

				public int resolution = 512;  
				public Texture2D tex;
				public MatrixAsset asset; 
				public float intensity = 1f;
				public float size = 200;
				public float rotation = 0;
				public int seed = 12345;


				public Matrix Load ()
				{
					if (sourceType == SourceType.Texture)
					{
						if (tex==null) { return null; }
						Matrix matrix = new Matrix( new CoordRect(0,0,tex.width, tex.height) );
						matrix.ImportTextureRaw(tex);

						if (matrix.rect.size.x != resolution) 
							MatrixOps.Resize(matrix, new CoordRect(0,0,resolution, resolution));

						return matrix;
					}


					if (sourceType == SourceType.Asset)
					{
						if (asset==null || asset.matrix==null) { return null; }
						Matrix matrix = new Matrix(asset.matrix);

						if (matrix.rect.size.x != resolution) 
							MatrixOps.Resize(matrix, new CoordRect(0,0,resolution, resolution));

						return matrix;
					}


					if (sourceType == SourceType.Noise)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Noise noise = new Noise(seed);

						int iterations = (int)Mathf.Log(size,2) + 1; //+1 max size iteration

						Coord min = matrix.rect.Min; Coord max = matrix.rect.Max;
						for (int x=min.x; x<max.x; x++)
							for (int z=min.z; z<max.z; z++)
							{ 
								float val = noise.Fractal(x, z, size, iterations, detail:0.5f, turbulence:0, type:2);
								matrix[x,z] = val * intensity;
							}

						return matrix;
					}


					if (sourceType == SourceType.Voronoi)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Noise noise = new Noise(seed);

						CoordRect rect = CoordRect.WorldToPixel((Vector2D)0, (Vector2D)1000, (Vector2D)size);  //will convert to cells as well
						Vector3D matrixPos = new Vector3D(rect.offset.x*size, 0, rect.offset.z*size);  //modifying worldPos/size too
						Vector3D matrixSize = new Vector3D(rect.size.x*size, 250, rect.size.z*size);

						PositionMatrix posMatrix = new PositionMatrix(rect, matrixPos, matrixSize);
						posMatrix.Scatter(0, noise);
						posMatrix = posMatrix.Relaxed();

						Coord min = matrix.rect.Min; Coord max = matrix.rect.Max; 
						for (int x=min.x; x<max.x; x++)
							for (int z=min.z; z<max.z; z++)
							{
								Vector3D closest; Vector3D secondClosest;
								float minDist; float secondMinDist;
								posMatrix.GetTwoClosest(new Vector3D(x/size,0,z/size), out closest, out secondClosest, out minDist, out secondMinDist); 

								matrix[x,z] = (secondMinDist-minDist) / 160 * intensity;
							}

						return matrix;
					}


					if (sourceType == SourceType.Cone  ||  sourceType == SourceType.Circle  ||  sourceType == SourceType.Gradient  ||  sourceType == SourceType.Pyramid  ||  sourceType == SourceType.Sphere)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						
						Vector2D center = (Vector2D)resolution/2f;

						Coord min = matrix.rect.Min; Coord max = matrix.rect.Max;
						for (int x=min.x; x<max.x; x++)
							for (int z=min.z; z<max.z; z++)
							{
								Vector2D formPos = new Vector2D(x,z);
								float val = 0;

								switch (sourceType)
								{
									case SourceType.Cone:
										val = 1 - ((center-formPos).Magnitude)/size;
										if (val<0) val = 0;
										break;
									case SourceType.Sphere:
										val = 1 - ((center-formPos).Magnitude)/size;
										if (val<0) val = 0;
										val = Mathf.Sqrt(2*val - val*val);
										break;
									case SourceType.Circle:
										val = 1 - ((center-formPos).Magnitude)/size;
										if (val<(size/matrix.rect.size.x)) val = 0;
										else val = 1;
										break;
									case SourceType.Pyramid:
										float valX = formPos.x / size; if (valX > 1-valX) valX = 1-valX;
										float valZ = formPos.z / size; if (valZ > 1-valZ) valZ = 1-valZ;
										val = valX<valZ? valX*2 : valZ*2;
										break;
									case SourceType.GradientX:
										val = formPos.x / size;
										break;
									case SourceType.GradientZ:
										val = formPos.z / size;
										break;
								}
								
								matrix[x,z] = val*intensity;
							}

						return matrix;
					}


					if (sourceType == SourceType.Point)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Coord center = (Coord)resolution/2f;
						matrix[center] = 1;
						return matrix;
					}


					if (sourceType == SourceType.Line  ||  sourceType == SourceType.AALine)
					{
						Matrix matrix = new Matrix( new CoordRect(0,0, resolution, resolution) );
						Vector2D center = (Vector2D)resolution/2f;
						Vector2D dir = new Vector2D( Mathf.Sin(rotation*Mathf.Deg2Rad), Mathf.Cos(rotation*Mathf.Deg2Rad) );
						dir *= resolution/2;
						dir *= size/100;
						matrix.Line(center-dir, center+dir, antialised:sourceType==SourceType.AALine);
						return matrix;
					}


					return null;
				}
		}

		#endregion


	}
}