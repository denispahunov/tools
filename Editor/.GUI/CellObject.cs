﻿using System;
using System.Reflection;
using System.Collections.Generic;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public static class CellObject 
	{
		private struct CellObj : IEquatable<CellObj>
		{
			public object obj;
			public string id;

			public CellObj (object o) { obj=o; id=null; }
			public CellObj (object o, string i) { obj=o; id=i; }

			public bool Equals (CellObj o2) { return obj==o2.obj && id==o2.id; }
			public override bool Equals (object o2) { if (o2 is CellObj) return Equals((CellObj)o2); else return false; }
			public override int GetHashCode () { return id==null ? 0 : id.GetHashCode() + obj.GetHashCode(); }
		}

		private static Dictionary<Cell,CellObj> cellToObject = new Dictionary<Cell,CellObj>();
		private static Dictionary<CellObj,Cell> objectToCell = new Dictionary<CellObj,Cell>();
		//faster then getkey

		//public static object this[Cell cell] 

		public static void SetObject (Cell cell, object obj, string id=null)
		{
			CellObj cellObj = new CellObj(obj,id);

			if (cellToObject.ContainsKey(cell)) cellToObject[cell] = cellObj;
			else cellToObject.Add(cell, cellObj);

			if (objectToCell.ContainsKey(cellObj)) objectToCell[cellObj] = cell;
			else objectToCell.Add(cellObj, cell);
		}

		public static bool HasObject (Cell cell)
		{
			return cellToObject.ContainsKey(cell);
		}

		public static object GetObject (Cell cell)
		{
			if (cell == null) return null;

			if (cellToObject.ContainsKey(cell)) return cellToObject[cell].obj;
			else return null;
		}

		public static T GetObject<T> (Cell cell) where T : class
		{
			if (cell == null) return null;

			if (cellToObject.ContainsKey(cell)) return (T)(cellToObject[cell].obj);
			else return null;
		}

		public static string GetId (Cell cell)
		{
			if (cell == null) return null;

			if (cellToObject.ContainsKey(cell)) return cellToObject[cell].id;
			else return null;
		}

		public static Cell GetCell (object obj, string id=null)
		{
			CellObj cellObj = new CellObj(obj,id);

			if (objectToCell.ContainsKey(cellObj)) return objectToCell[cellObj];
			else return null;
		}

		public static Cell GetLastObjectCell (List<Cell> cells)
		/// Returns the last cell with any object
		{
			int cellCount = cells.Count;
			for (int i=cellCount-1; i>=0; i--)
			{
				if (!cellToObject.ContainsKey(cells[i])) continue;
				return cells[i];
			}
			return null;
		}

		public static void Clear ()
		{
			cellToObject.Clear();
			objectToCell.Clear();
		}
	}
}