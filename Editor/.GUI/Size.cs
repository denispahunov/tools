﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public struct Dimension
	{
		public enum Type { Auto, Relative, Pixels }
		public Type type;

		public double pixels;	//storing dstSize in doubles to prevent position flickering
		public double relative;

		public Dimension (Type type, double val)
		{
			this.type = type;
			switch (type)
			{
				case Type.Relative: relative = val; pixels = 0; break;
				case Type.Pixels: relative = 0; pixels = val; break;
				default: relative = 0; pixels = 0; break;
			}
		}

		public Dimension (Type type)
		{
			this.type = type; relative = 0; pixels = 0;
		}

		public override string ToString () { return "Size " + type.ToString() + ", " + pixels.ToString("0.000") + "p " + relative.ToString("0.000") + "%"; }
	}

	public struct Size
	{
		public Dimension x;
		public Dimension y;
		public Dimension width;
		public Dimension height;

		public const int lineHeight = 18;

		public static readonly Size fixedLine;
		public static readonly Size line;
		public static readonly Size row;
		public static readonly Size full;
		public static readonly Size center;

		public Vector2 Center {get{ return new Vector2((float)(x.pixels + width.pixels/2), (float)(y.pixels + height.pixels/2)); } }
		public Rect Rect {get{ return new Rect((float)x.pixels, (float)y.pixels, (float)width.pixels, (float)height.pixels); } }
		public Vector2 Position {get{ return new Vector2((float)x.pixels, (float)y.pixels); } set{x.pixels=value.x; y.pixels=value.y;} }
		public Vector2 SizeWidthHeight {get{ return new Vector2((float)width.pixels, (float)height.pixels); } }
		public Vector2 LeftFocus {get{ return new Vector2((float)(x.pixels + height.pixels/2), (float)(y.pixels + height.pixels/2)); }} //left point equidistant from left, top and bottom borders
		public Vector2 RightFocus {get{ return new Vector2((float)(x.pixels + width.pixels/2 - height.pixels/2), (float)(y.pixels + height.pixels/2)); }}

		public bool Contains (Vector2 pos, float margins=0)
		{
			return  pos.x+margins > x.pixels  &&  pos.x-margins < x.pixels+width.pixels &&
					pos.y+margins > y.pixels  &&  pos.y-margins < y.pixels+height.pixels;
		}

		public bool Contains (Vector2 pos, Vector2 size, float margins=0)
		/// Checks if contains all (with size)
		{
			return  pos.x+margins > x.pixels  &&  pos.x+size.x-margins < x.pixels+width.pixels &&
					pos.y+margins > y.pixels  &&  pos.y+size.y-margins < y.pixels+height.pixels;
		}

		public Vector2 Clamp (Vector2 pos, float margins=0)
		{
			if (pos.x < x.pixels-margins) pos.x = (float)(x.pixels-margins);
			if (pos.y < y.pixels-margins) pos.y = (float)(y.pixels-margins);
			if (pos.x > x.pixels+width.pixels+margins) pos.x = (float)(x.pixels+width.pixels+margins);
			if (pos.y > y.pixels+height.pixels+margins) pos.y = (float)(y.pixels+height.pixels+margins);
			return pos;
		}

		public static Size operator * (Size size, double f) { return new Size () { 
				width = new Dimension(Dimension.Type.Pixels, size.width.pixels * f),
				x = new Dimension(Dimension.Type.Pixels, size.x.pixels * f),
				height = new Dimension(Dimension.Type.Pixels, size.height.pixels * f),
				y = new Dimension(Dimension.Type.Pixels, size.y.pixels * f) }; }
		public static Size operator / (Size size, float f) { return new Size () { 
				width = new Dimension(Dimension.Type.Pixels, size.width.pixels / f),
				x = new Dimension(Dimension.Type.Pixels, size.x.pixels / f),
				height = new Dimension(Dimension.Type.Pixels, size.height.pixels / f),
				y = new Dimension(Dimension.Type.Pixels, size.y.pixels / f) }; }

		static Size ()
		{
			fixedLine = new Size () { 
				width = new Dimension(Dimension.Type.Relative, 1),
				x = new Dimension(Dimension.Type.Pixels, 0),
				height = new Dimension(Dimension.Type.Pixels, lineHeight),
				y = new Dimension(Dimension.Type.Auto) };

			line = new Size () { 
				width = new Dimension(Dimension.Type.Relative, 1),
				x = new Dimension(Dimension.Type.Pixels, 0),
				height = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Auto) };

			row = new Size () { 
				width = new Dimension(Dimension.Type.Auto),
				x = new Dimension(Dimension.Type.Auto),
				height = new Dimension(Dimension.Type.Relative, 1),
				y = new Dimension(Dimension.Type.Pixels, 0) };

			full = new Size () { 
				x = new Dimension(Dimension.Type.Pixels, 0),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Relative, 1),
				height = new Dimension(Dimension.Type.Relative, 1) };

			center = new Size () {
				x = new Dimension(Dimension.Type.Relative, 0.5f),
				y = new Dimension(Dimension.Type.Relative, 0.5f),
				width = new Dimension(Dimension.Type.Pixels, 0),
				height = new Dimension(Dimension.Type.Pixels, 0) }; //zero-sized cell (to use with margins) in the center of the parent one
		}

		public static Size Custom (float x, float y, float width, float height)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, x),
				y = new Dimension(Dimension.Type.Pixels, y),
				width = new Dimension(Dimension.Type.Pixels, width),
				height = new Dimension(Dimension.Type.Pixels, height) };
		}

		public static Size Custom (Vector2 pos, Vector2 size)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, pos.x),
				y = new Dimension(Dimension.Type.Pixels, pos.y),
				width = new Dimension(Dimension.Type.Pixels, size.x),
				height = new Dimension(Dimension.Type.Pixels, size.y) };
		}

		public static Size Custom (Size size)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, size.x.pixels),
				y = new Dimension(Dimension.Type.Pixels, size.y.pixels),
				width = new Dimension(Dimension.Type.Pixels, size.width.pixels),
				height = new Dimension(Dimension.Type.Pixels, size.height.pixels) };
		}

		public static Size Custom (Rect rect)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, rect.x),
				y = new Dimension(Dimension.Type.Pixels, rect.y),
				width = new Dimension(Dimension.Type.Pixels, rect.width),
				height = new Dimension(Dimension.Type.Pixels, rect.height) };
		}

		public static Size WidthHeight (float width, float height)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, 0),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, width),
				height = new Dimension(Dimension.Type.Pixels, height) };
		}

		public static Size CenterWidthHeight (float width, float height)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Relative, 0.5f),
				y = new Dimension(Dimension.Type.Relative, 0.5f),
				width = new Dimension(Dimension.Type.Pixels, width),
				height = new Dimension(Dimension.Type.Pixels, height) };
		}

		public static Size OffsetWidth (float offset, float width)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, offset),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, width),
				height = new Dimension(Dimension.Type.Auto) };
		}

		public static Size FullWindow (Rect windowRect)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, 0),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, windowRect.width),
				height = new Dimension(Dimension.Type.Pixels, windowRect.height) };
		}

		public static Size RowPercent (float percent)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Relative, percent),
				height = new Dimension(Dimension.Type.Relative, 1) };
		}

		public static Size LinePercent (float percent)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, 0),
				y = new Dimension(Dimension.Type.Auto),
				width = new Dimension(Dimension.Type.Relative, 1),
				height = new Dimension(Dimension.Type.Relative, percent) };
		}

		public static Size RowPixels (float pixels)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, pixels),
				height = new Dimension(Dimension.Type.Relative, 1) };
		}

		public static Size LinePixels (float pixels)
		{
			return new Size () { 
				x = new Dimension(Dimension.Type.Pixels, 0),
				y = new Dimension(Dimension.Type.Auto),
				width = new Dimension(Dimension.Type.Relative, 1),
				height = new Dimension(Dimension.Type.Pixels, pixels) };
		}
	}

	public struct Padding
	{
		public float left;
		public float right;
		public float top;
		public float bottom;


		public Padding (float left, float right, float top, float bottom)
		{
			this.left = left;
			this.right = right;
			this.top = top;
			this.bottom = bottom;
		}

		public Padding (float hor, float vert)
		{
			this.left = hor;
			this.right = hor;
			this.top = vert;
			this.bottom = vert;
		}

		public Padding (float offset)
		{
			this.left = offset;
			this.right = offset;
			this.top = offset;
			this.bottom = offset;
		}
	}
}