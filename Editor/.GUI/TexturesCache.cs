﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public static class TexturesCache
	{
		private static Dictionary<string,Texture2D> textures = new Dictionary<string,Texture2D>();
		private static Dictionary<string, Dictionary<uint,Texture2D>> colorTextures = new Dictionary<string, Dictionary<uint, Texture2D>>();
		private static Dictionary<uint, Texture2D> blankTextures = new Dictionary<uint, Texture2D>();
		private static Dictionary<Texture2D,GUIStyle> styles = new Dictionary<Texture2D,GUIStyle>();

		private static Dictionary<string,Material> materials = new Dictionary<string, Material>();
		
		public static bool forcePro = false;
		public static bool forceLight = false;


		public static Texture2D GetTexture (string textureName)
		{
			if (textures.ContainsKey(textureName))
				return textures[textureName];
			else
			{
				Texture2D tex = LoadTextureAtPath(textureName); 
				textures.Add(textureName, tex);
				return tex;
			}
		}

		private static uint ColorHash (Color color)
		/// Using manual color hash since color in dictionary (compare) creates garbage for some reason
		{
			uint r = (uint)(color.r*255);
			uint g = (uint)(color.g*255);
			uint b = (uint)(color.b*255);
			uint a = (uint)(color.a*255);

			return r<<24 | g<<16 | b<<8 | a;
		}

		public static Texture2D GetColorizedTexture (string textureName, Color color)
		{
			Dictionary<uint,Texture2D> dict;

			if (colorTextures.ContainsKey(textureName))
				dict = colorTextures[textureName];
			else 
			{
				dict = new Dictionary<uint, Texture2D>();
				colorTextures.Add(textureName, dict);
			}

			uint colorHash = ColorHash(color);

			if (dict.ContainsKey(colorHash)  &&  !(dict[colorHash]==null || dict[colorHash].Equals(null)))  //texture is removed on scene change
				return dict[colorHash];
			
			//else
			{
				Texture2D source = GetTexture(textureName);
				Texture2D clone = source.Clone();
				clone.Multiply(color);

				if (!dict.ContainsKey(colorHash)) dict.Add(colorHash, clone);
				else dict[colorHash] = clone;

				return clone;
			}

			
		}


		public static Texture2D GetBlankTexture (float color) 
			{ return GetBlankTexture( new Color(color, color, color)); }


		public static Texture2D GetBlankTexture (Color color) 
		{
			uint colorHash = ColorHash(color);

			if (blankTextures.ContainsKey(colorHash))
				return blankTextures[colorHash];
			else
			{
				Texture2D tex = new Texture2D(4,4);
				Color[] colors = tex.GetPixels();
				for (int i=0; i<colors.Length; i++) colors[i] = color; //new Color(0.0f, 0.0f, 0.0f, 1);
				tex.SetPixels(colors);
				tex.Apply(true, true);

				blankTextures.Add(colorHash, tex);
				return tex;
			}
		}


		public static Texture2D LoadTextureAtPath (string textureName)
		/// Loads texture dealing with pro name and folders
		{
			Texture2D texture;

			bool isPro = false;
			if (forcePro) isPro = true;
			else if (forceLight) isPro = false;
			#if UNITY_EDITOR
			else isPro = UnityEditor.EditorGUIUtility.isProSkin;
			#endif

			if (!isPro)
			{
				texture = Resources.Load(textureName) as Texture2D;
				//if (texture == null) texture = Resources.Load(proName) as Texture2D;
				//if (texture == null) texture = Resources.Load(proFolderName) as Texture2D;
			}
			else
			{
				string proName = textureName + "_pro";
			
				int sepIndex = textureName.LastIndexOf('/'); if (sepIndex < 0) sepIndex = 0;
				string folderName = textureName.Substring(0,sepIndex);
				string fileName = textureName.Substring(sepIndex+1);
				string proFolderName = folderName + "/pro/" + fileName;

				texture = Resources.Load(proName) as Texture2D;
				if (texture == null) texture = Resources.Load(proFolderName) as Texture2D;
				if (texture == null) texture = Resources.Load(textureName) as Texture2D;
			}

			if (texture == null)
				throw new Exception("Could not find texture " + textureName);

			return texture;
		}


		public static GUIStyle GetElementStyle (string textureName, RectOffset borders=null, RectOffset overflow=null)
		{
			Texture2D tex = GetTexture(textureName); 
			return GetElementStyle(tex, borders, overflow);
		}

		public static GUIStyle GetElementStyle (Texture2D tex, RectOffset borders=null, RectOffset overflow=null)
		{
			if (styles.ContainsKey(tex))
				return styles[tex];

			else
			{
				GUIStyle style = new GUIStyle();
				style.normal.background = tex;

				if (borders == null) style.border = new RectOffset(tex.width/2, tex.width/2, tex.height/2, tex.height/2);
				else style.border = borders;

				if (overflow != null) style.overflow = overflow;

				styles.Add(tex, style);
				return style;
			}
		}

		public static Material GetMaterial (string shaderName)
		{
			if (materials.ContainsKey(shaderName))
			{
				Material mat = materials[shaderName];
				if (mat==null) //destroys material on scene close
				{
					mat = new Material( Shader.Find(shaderName) ); 
					materials[shaderName] = mat;
				}
				return mat;
			}
			
			else
			{
				Material mat = new Material( Shader.Find(shaderName) ); 
				materials.Add(shaderName,mat);
				return mat;
			}
		}
	}
}