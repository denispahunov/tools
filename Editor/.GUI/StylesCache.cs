﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public class StylesCache
	{
		public const int defaultFontSize = 11;
		

		private Dictionary<string,GUIStyle> styles = new Dictionary<string,GUIStyle>();
		private Dictionary<GUIStyle,float> customFontsSize = new Dictionary<GUIStyle, float>();

		public GUIStyle label = null;
		public GUIStyle centerLabel = null;
		public GUIStyle smallLabel = null;
		public GUIStyle bigLabel = null;
		public GUIStyle url = null;
		public GUIStyle foldout = null;
		public GUIStyle field = null;
		public GUIStyle button = null;
		public GUIStyle enumClose = null;
		public GUIStyle enumFar = null;
		public GUIStyle toolbar = null;
		public GUIStyle toolbarButton = null;
		public GUIStyle helpBox = null;

		private static Texture2D fieldTexture = null;
		private static Texture2D fieldTexturePro = null;

		public bool isPro = false;

		private int lastFontSize = 11; //to check before resize
		private float lastZoom = 1;

		public bool Initilized
		{get{
			return label != null && field !=null && foldout != null;
		}}

		public void Init (bool forcePro=false, bool forceLight=false)
		{
			#if UNITY_EDITOR

			//this.isPro = isPro;

			isPro = UnityEditor.EditorGUIUtility.isProSkin;
			if (forcePro) isPro = true;
			if (forceLight) isPro = false;

			Color fontColor = Color.black;
			if (isPro) fontColor = new Color(0.705f, 0.705f, 0.705f);

			label = new GUIStyle(UnityEditor.EditorStyles.label); 
			//label.font = Font.CreateDynamicFontFromOSFont("Verdana", 11); //better for scaling
			label.normal.textColor = label.focused.textColor = label.active.textColor = fontColor; //no focus
			styles.Add("Label", label);

			centerLabel = new GUIStyle(label); 
			centerLabel.alignment = TextAnchor.LowerCenter;
			styles.Add("CenterLabel", centerLabel);

			smallLabel = new GUIStyle(label); 
			smallLabel.fontSize = 9;
			styles.Add("SmallLabel", smallLabel);
			customFontsSize.Add(smallLabel,9);

			bigLabel = new GUIStyle(label); 
			bigLabel.font = Font.CreateDynamicFontFromOSFont("Verdana", 16);
			bigLabel.fontSize = 16;
			//bigLabel.fontStyle = FontStyle.Bold; 
			bigLabel.alignment = TextAnchor.LowerLeft;
			bigLabel.contentOffset = new Vector2(0,-1);
			styles.Add("BigLabel", bigLabel);
			customFontsSize.Add(bigLabel,16.5f);

			url = new GUIStyle(label); 
			url.normal.textColor = new Color(0.3f, 0.5f, 1f); 
			styles.Add("Url", url);

			foldout = new GUIStyle(UnityEditor.EditorStyles.foldout);  
			foldout.fontStyle = FontStyle.Bold; 
			foldout.focused.textColor = Color.black; 
			foldout.active.textColor = Color.black; 
			foldout.onActive.textColor = Color.black;
			foldout.clipOffset = new Vector2(0,0); new RectOffset(0,0,0,0);
			styles.Add("Foldout", foldout);

			button = new GUIStyle("Button"); 
			button.normal.textColor = button.focused.textColor = button.active.textColor = fontColor; //no focus
			styles.Add("Button", button);
					
			toolbar = new GUIStyle(UnityEditor.EditorStyles.toolbar);
			toolbar.overflow = new RectOffset(0,0,0,0);
			toolbar.padding = new RectOffset(0,0,0,0);
			toolbar.fixedHeight = 0;
			styles.Add("Toolbar", toolbar);

			toolbarButton = new GUIStyle(UnityEditor.EditorStyles.toolbarButton);  
			toolbarButton.overflow = new RectOffset(0,0,1,0);
			//toolbarButton.padding = new RectOffset(0,0,10,0);
			//toolbarButton.margin = new RectOffset(0,0,10,0);
			toolbarButton.fixedHeight = 0;
			styles.Add("ToolbarButton", toolbarButton);

			helpBox = new GUIStyle(UnityEditor.EditorStyles.helpBox); 
			styles.Add("HelpBox", helpBox);

			enumClose = new GUIStyle(UnityEditor.EditorStyles.popup);
			enumClose.fixedHeight = 14;
			styles.Add("EnumClose", enumClose);

			enumFar = new GUIStyle("Button"); 
			enumFar.alignment = TextAnchor.MiddleLeft;
			styles.Add("EnumFar", enumFar);


			field = new GUIStyle(UnityEditor.EditorStyles.numberField);

			if (fieldTexture == null) fieldTexture = Resources.Load("DPUI/Backgrounds/Field") as Texture2D;
			if (fieldTexturePro == null) fieldTexturePro = Resources.Load("DPUI/Backgrounds/Field_pro") as Texture2D;

			if (!isPro) field.normal.background = field.active.background = field.focused.background = fieldTexture;
			else field.normal.background = field.active.background = field.focused.background = fieldTexturePro;

			field.border = new RectOffset(4,4,4,4);
			field.normal.textColor = field.focused.textColor = field.active.textColor = fontColor;
			styles.Add("Field", field);
			#endif
		}

		public void Clear ()
		{
			styles.Clear();
			customFontsSize.Clear();

			label = null; 
			centerLabel = null;
			smallLabel = null; 
			bigLabel = null; 
			url = null; 
			foldout = null;  
			button = null; 	
			toolbar = null;
			toolbarButton = null;  
			helpBox = null; 
			enumClose = null;
			enumFar = null; 
			field = null;
		}


		public void Resize (float zoom=-1)
		{
			if (zoom == -1) zoom = lastZoom;
			
			int fontSize = Mathf.FloorToInt(defaultFontSize * zoom);
			if (fontSize == lastFontSize) return;

			foreach (var kvp in styles)
			{
				GUIStyle style = kvp.Value;

				if (customFontsSize.ContainsKey(style))
					style.fontSize = Mathf.RoundToInt(customFontsSize[style] * zoom);
				else 
					style.fontSize = fontSize;
			}

			//field.fontSize = Mathf.RoundToInt(14 * zoom * 0.8f);

			lastFontSize = fontSize;
			lastZoom = lastZoom;
		}

			
		public GUIStyle this[string name] 
		{
			get{ if (styles.ContainsKey(name)) return styles[name]; else return null; }
			set{ 
				if (styles.ContainsKey(name)) styles[name] = value; 
				else styles.Add(name,value); 

				if (value.fontSize != defaultFontSize)
				{
					if (customFontsSize.ContainsKey(value)) customFontsSize[value] = value.fontSize;
					else customFontsSize.Add(value, value.fontSize);
				}

				Resize();
			}
		}

	}
}
