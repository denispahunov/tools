﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public static partial class UI
	{
		[Serializable] //serializing scroll/zoom 
		public class Settings
		{
			[NonSerialized] public static Settings current;

			[NonSerialized] public Cell rootCell = new Cell();
			[SerializeField] public ScrollZoom scrollZoom = new ScrollZoom();
			[NonSerialized] public StylesCache styles = new StylesCache();
			//public TexturesCache currentTextures;

			[NonSerialized] public UnityEditor.Editor editor;
		}

		public static bool repaint = false;

		public static Rect inspectorRect;

		public static Vector2 windowSize;

		public static string lastUndoName;  //the last undo group name (kept to know it on UndoRedoPerformed)


		public static bool IsWithinWindow (Rect rect) 
		{
			Vector2 min = rect.min; Vector2 max = rect.max;
			return max.x >= 0 && max.y >= 0 && min.x <= windowSize.x && min.y <= windowSize.y;
		}


		public static void RemoveFocusOnControl ()
		/// GUI.FocusControl(null) is not reliable, so creating a temporary control and focusing on it
		{
			UnityEngine.GUI.SetNextControlName("Temp");
			UnityEditor.EditorGUI.FloatField(new Rect(-10,-10,0,0), 0);
			UnityEngine.GUI.FocusControl("Temp");
		}

		public static void RepaintAllWindows ()
		/// Usually called on undo
		{
			UnityEditor.EditorWindow[] windows = Resources.FindObjectsOfTypeAll<UnityEditor.EditorWindow>();
			foreach (UnityEditor.EditorWindow win in windows)
				win.Repaint();
		}

		public static void Draw (Action drawAction, Settings settings)
		{
			Settings.current = settings;

			//scroll/zoom
			if (settings.scrollZoom.allowScroll) settings.scrollZoom.Scroll();
			if (settings.scrollZoom.allowZoom) settings.scrollZoom.Zoom();

			//dragging
			DragDrop.CheckDrag();

			//read inspector rect
			#if UNITY_EDITOR
			//if (Event.current.rawType != EventType.MouseUp)
			{
				UnityEditor.EditorGUI.indentLevel = 0;
				inspectorRect = UnityEditor.EditorGUILayout.GetControlRect(GUILayout.Height(0));
				inspectorRect.x -= 10; inspectorRect.width += 10;
			}
			#endif

			//undo/redo
			#if UNITY_EDITOR

			lastUndoName = UnityEditor.Undo.GetCurrentGroupName();

			UnityEditor.Undo.undoRedoPerformed -= RepaintAllWindows;
			UnityEditor.Undo.undoRedoPerformed += RepaintAllWindows;

			if (settings.rootCell != null)
			{
				UnityEditor.Undo.undoRedoPerformed -= settings.rootCell.PerformUndo;
				UnityEditor.Undo.undoRedoPerformed += settings.rootCell.PerformUndo;
			}
			#endif

			//skipping all unnecessary events
			bool skipEvent = false;

			if (Event.current.type == EventType.Layout  ||  Event.current.type == EventType.Used) skipEvent = true; //skip all layouts

			#if UNITY_EDITOR
			if (Event.current.type == EventType.MouseDrag) //skip all mouse drags (except when dragging text selection cursor in field)
			{
				if (!UnityEditor.EditorGUIUtility.editingTextField) skipEvent = true;
				if (UnityEngine.GUI.GetNameOfFocusedControl() == "Temp") skipEvent = true; 
			}
			#endif

			if (!skipEvent)
			{
				//styles
				if (!settings.styles.Initilized) settings.styles.Init();
				


				//setting window frames to cull elements that are not within
				UI.windowSize = new Vector2(Screen.width, Screen.height);


				//drawing
				using (Timer.Start("Draw UI " + Event.current.type))
				{

					UI.repaint = false;

					using (Timer.Start("Pass1 Layout"))
						drawAction();

					using (Timer.Start("Scale Styles"))
						settings.styles.Resize(settings.scrollZoom.zoom);

					using (Timer.Start("Cell Layout"))
					{
						double prevHeight = settings.rootCell.dstSize.height.pixels;

						settings.rootCell.Layout();

						if (settings.rootCell.dstSize.height.pixels != prevHeight && settings.editor!=null)
						{
							Debug.Log("Height Changed. Repainting");
							settings.editor.Repaint();
						}
					}

					using (Timer.Start("Cells under cursor"))
						cellsUnderCursor = GetCellsUnderCursor();

					UI.repaint = true;

					using (Timer.Start("Pass2 Repaint"))
						drawAction();


					using (Timer.Start("Redraw on event use"))
					{
						if (Event.current.type == EventType.Used)
						{
							Event.current = new Event() { type=EventType.Repaint };

							UI.repaint = false;
							drawAction(); //refreshing gui on event use to prevent flushing unused cells
							UI.repaint = true;

							if (Event.current.type != EventType.Repaint)
								Event.current.Use();
						}
					}

					//debugging
					//settings.rootCell.DrawBackground(recursive:true);
					//for (int i=0; i<UI.cellsUnderCursor.Count; i++) cellsUnderCursor[i].DrawBackground(i);

					using (Timer.Start("Flush Unused"))
						settings.rootCell.FlushUnusedChildren();

				}
			}

			//assign inspector rect
			#if UNITY_EDITOR
			UnityEditor.EditorGUILayout.GetControlRect(GUILayout.Height((float)settings.rootCell.dstSize.height.pixels));
			#endif


			//flushing cell objects
			CellObject.Clear();

			Settings.current = null;
		}


		#region Cell under cursor

			public static List<Cell> cellsUnderCursor = new List<Cell>();


			private static List<Cell> GetCellsUnderCursor ()
			{
				List<Cell> intersectingCells = new List<Cell>();
				Vector2 pos = UI.Settings.current.scrollZoom.ToInternal( Event.current.mousePosition );
				Cell root = UI.Settings.current.rootCell;

				FillIntersectingCells(intersectingCells, pos, root);

				return intersectingCells;
			}


			private static void FillIntersectingCells (List<Cell> intersectingList, Vector2 pos, Cell cell)
			{
				if (cell.dstSize.Contains(pos))
				{
					intersectingList.Add(cell);
				}

				//if (cell.dstSize.Contains(pos, margins:50)) //ignoring only cells that are 50 pixels away. Otherwise it will skip cells with offset
				{
					if (cell.children != null) 
					{
						int childCount = cell.children.Count;
						for (int i=0; i<childCount; i++)
							FillIntersectingCells(intersectingList, pos, cell.children[i]);
					}
				}
			}

		#endregion
	}
}