﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public static class DragDrop
	{
		public static object obj; //drag object or id

		public static Vector2 initialMousePos;
		public static object initialValue; //assigned manually on drag start and then not changing
		public static Vector2 mousePos; //to calculate current delta not only on mousedrag
		public static Vector2 totalDelta; //drag pos relative to initial position

		//internal gui parameters
		//public static Vector2 intTotalDelta;
		//public static Rect intInitialRect;
		//public static Vector2 intMousePos;


		public static bool CheckStart (
			object obj,
			Rect rect)
		{
			if (!UI.repaint && GUI2.UI.current.layout) return false; //dragging could be started only in repaint (how we get rect otherwise?)

			Vector2 mousePos = Event.current.mousePosition;
			//mousePos += mousePos - UI.Settings.current.scrollZoom.scroll;

			if (Event.current.type==EventType.MouseDown  &&  Event.current.button==0  &&  rect.Contains(mousePos))
			{
				DragDrop.obj = obj;

				initialMousePos = mousePos;
				totalDelta = new Vector2();
				DragDrop.mousePos = mousePos;

				/*ScrollZoom scrollZoom = UI.Settings.current.scrollZoom;
				intInitialRect = new Rect(
					//(rect.position - scrollZoom.scroll) / scrollZoom.zoom,
					rect.position / scrollZoom.zoom,
					rect.size / scrollZoom.zoom);
				intTotalDelta = new Vector2();
				intMousePos = mousePos / scrollZoom.zoom;*/

				Event.current.Use();

				return true;
			}
			return false;
		}

		public static bool CheckStart (Cell cell)
		{
			if (cell==null || Event.current.type == EventType.Used) return false;
			return CheckStart(cell, cell.GetRect(usePadding:false, pixelPerfect:false));
		}


		public static bool CheckDrag (object obj)
		/// Done before each gui layout. Change the static position values (does not move any cell or something)
		/// Done both in UI.repaint and non-repaint
		{
			if (DragDrop.obj==null  ||  !DragDrop.obj.Equals(obj))
				return false;

			else
			{
				mousePos = Event.current.mousePosition;// - UI.Settings.current.scrollZoom.scroll;

				totalDelta = mousePos - initialMousePos;

				//intTotalDelta = totalDelta / UI.Settings.current.scrollZoom.zoom;
				//intMousePos = mousePos / UI.Settings.current.scrollZoom.zoom;

				//prevMousePos = mousePos;

				#if UNITY_EDITOR
				if (Event.current.rawType != EventType.MouseUp) //don't use mouse up events, will not release otherwise
					if (Event.current.isMouse) Event.current.Use();

				if (UnityEditor.EditorWindow.focusedWindow != null) UnityEditor.EditorWindow.focusedWindow.Repaint(); 
				#endif

				return true;
			}
		}

		public static bool CheckDrag ()
		{
			if (obj==null) return false;
			else return CheckDrag(obj);
		}


		public static bool CheckRelease (object obj)
		{
			if (Event.current.rawType == EventType.MouseUp && (UI.repaint || !GUI2.UI.current.layout)) 
			{
				if (DragDrop.obj==null  ||  !DragDrop.obj.Equals(obj))
				{
					//DragDrop.obj = null;
					return false;
				}

				else
				{
					DragDrop.obj = null;

					#if UNITY_EDITOR
					if (Event.current.isMouse) Event.current.Use();
					if (UnityEditor.EditorWindow.focusedWindow != null) UnityEditor.EditorWindow.focusedWindow.Repaint(); 
					#endif

					return true;
				}
			}

			return false;
		}


		public static void DragCell (Cell cell)
		{
			if (cell==null || Event.current.type == EventType.Used) return;
			Rect rect = cell.GetRect(usePadding:false, pixelPerfect:false);
			DragCell(cell, ref rect);
			cell.srcSize = Size.Custom( UI.Settings.current.scrollZoom.ToInternal(rect) );
		}


		public static void DragCell (object obj, ref Rect rect)
		{
			if (DragDrop.CheckStart(obj, rect))
				DragDrop.initialValue = rect.position;

			if (DragDrop.CheckDrag(obj) && !UI.repaint)
				rect.position = (Vector2)DragDrop.initialValue + DragDrop.totalDelta;

			DragDrop.CheckRelease(obj);
		}


		private struct ObjId { public object i1; public int i2; public ObjId (object o, int i) {i1=o; i2=i;} } 
		//will be used in ResizeCell instead TupleSet <object, int>

		public static bool ResizeCell (
			object obj,
			ref Rect rect,
			int border=6,
			Vector2 minSize = new Vector2())
		{
			//ui.Element(Icons.GetElementStyle("DPLayout_LayerInactive_pro"), cell:cell);

			Vector2 mp = Event.current.mousePosition;

			bool resized = false;
			


			Rect leftRect = new Rect( rect.x - border/2, rect.y + border, border, rect.height-border*2 );
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (leftRect, UnityEditor.MouseCursor.ResizeHorizontal);
			#endif
			ObjId obj1 = new ObjId (obj, 1);
			if (CheckStart(obj1, leftRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj1))
			{ 
				rect.x = ((Rect)initialValue).position.x + totalDelta.x;  
				rect.width = ((Rect)initialValue).width - totalDelta.x; 
				if (rect.width<minSize.x)  { rect.x = ((Rect)initialValue).xMax - minSize.x; rect.width = minSize.x; }
				resized = true; 
			}
			if (CheckRelease(obj1))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(leftRect, Color.red);


			Rect rightRect = new Rect( rect.x - border/2 + rect.width, rect.y + border, border, rect.height-border*2 );
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (rightRect, UnityEditor.MouseCursor.ResizeHorizontal);
			#endif
			ObjId obj2 = new ObjId (obj, 2);
			if (CheckStart(obj2, rightRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj2))
			{ 
				rect.width = ((Rect)initialValue).width + totalDelta.x; 
				if (rect.width<minSize.x)  { rect.width = minSize.x; }
				resized = true; 
			}
			if (CheckRelease(obj2))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(rightRect, Color.red);


			Rect topRect = new Rect( rect.x + border, rect.y - border/2, rect.width-border*2, border );
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (topRect, UnityEditor.MouseCursor.ResizeVertical);
			#endif
			ObjId obj3 = new ObjId (obj, 3);
			if (CheckStart(obj3, topRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj3))
			{ 
				rect.y = ((Rect)initialValue).position.y + totalDelta.y;  
				rect.height = ((Rect)initialValue).height - totalDelta.y; 
				if (rect.height<minSize.y)  { rect.y = ((Rect)initialValue).yMax - minSize.y; rect.height = minSize.y; }
				resized = true; 
			}
			if (CheckRelease(obj3))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(topRect, Color.red);


			Rect bottomRect = new Rect( rect.x + border, rect.y - border/2 + rect.height, rect.width-border*2, border );
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (bottomRect, UnityEditor.MouseCursor.ResizeVertical);
			#endif
			ObjId obj4 = new ObjId (obj, 4);
			if (CheckStart(obj4, bottomRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj4))
			{ 
				rect.height = ((Rect)initialValue).height + totalDelta.y; 
				if (rect.height<minSize.y)  { rect.height = minSize.y; }
				resized = true; 
			}
			if (CheckRelease(obj4))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(bottomRect, Color.red);


			Rect topLeftRect = new Rect( rect.x-border, rect.y-border, border*2, border*2);
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (topLeftRect, UnityEditor.MouseCursor.ResizeUpLeft);
			#endif
			ObjId obj5 = new ObjId (obj, 5);
			if (CheckStart(obj5, topLeftRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj5))
			{ 
				rect.position = ((Rect)initialValue).position + totalDelta;  
				rect.size = ((Rect)initialValue).size - totalDelta; 
				if (rect.width<minSize.x)  { rect.x = ((Rect)initialValue).xMax - minSize.x; rect.width = minSize.x; }
				if (rect.height<minSize.y)  { rect.y = ((Rect)initialValue).yMax - minSize.y; rect.height = minSize.y; }
				resized = true; 
			}
			if (CheckRelease(obj5))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(topLeftRect, Color.red);


			Rect topRightRect = new Rect( rect.x-border + rect.width, rect.y-border, border*2, border*2);
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (topRightRect, UnityEditor.MouseCursor.ResizeUpRight);
			#endif
			ObjId obj6 = new ObjId (obj, 6);
			if (CheckStart(obj6, topRightRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj6))
			{ 
				rect.position = ((Rect)initialValue).position + new Vector2(0,totalDelta.y);  
				rect.size = ((Rect)initialValue).size + new Vector2(totalDelta.x, -totalDelta.y);  
				if (rect.width<minSize.x)  { rect.width = minSize.x; }
				if (rect.height<minSize.y)  { rect.y = ((Rect)initialValue).yMax - minSize.y; rect.height = minSize.y; }
				resized = true; 
			}
			if (CheckRelease(obj6))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(topRightRect, Color.red);


			Rect bottomLeftRect = new Rect( rect.x-border, rect.y-border + rect.height, border*2, border*2);
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (bottomLeftRect, UnityEditor.MouseCursor.ResizeUpRight);
			#endif
			ObjId obj7 = new ObjId (obj, 7);
			if (CheckStart(obj7, bottomLeftRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj7))
			{ 
				rect.position = ((Rect)initialValue).position + new Vector2(totalDelta.x,0);  
				rect.size = ((Rect)initialValue).size + new Vector2(-totalDelta.x, totalDelta.y); 
				if (rect.width<minSize.x)  { rect.x = ((Rect)initialValue).xMax - minSize.x; rect.width = minSize.x; }
				if (rect.height<minSize.y)  { rect.height = minSize.y; }
				resized = true; 
			}
			if (CheckRelease(obj7))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(bottomLeftRect, Color.red);


			Rect bottomRightRect = new Rect( rect.x-border + rect.width, rect.y-border + rect.height, border*2, border*2);
			#if UNITY_EDITOR
			UnityEditor.EditorGUIUtility.AddCursorRect (bottomRightRect, UnityEditor.MouseCursor.ResizeUpLeft);
			#endif
			ObjId obj8 = new ObjId (obj, 8);
			if (CheckStart(obj8, bottomRightRect))
				{ initialValue = rect; resized = true; }
			if (CheckDrag(obj8))
			{ 
				rect.size = ((Rect)initialValue).size + totalDelta;  
				if (rect.width<minSize.x)  { rect.width = minSize.x; }
				if (rect.height<minSize.y)  { rect.height = minSize.y; }
				resized = true; 
			}
			if (CheckRelease(obj8))
				{ resized = true; }
			//UnityEditor.EditorGUI.DrawRect(bottomRightRect, Color.red);

			return resized;
		}

		
		public static bool ResizeCell (Cell cell, int border=6, Vector2 minSize = new Vector2())
		{
			if (cell==null || Event.current.type == EventType.Used) return false;
			Rect rect = cell.GetRect(usePadding:false, pixelPerfect:false);
			if (ResizeCell(cell, ref rect, border, minSize*UI.Settings.current.scrollZoom.zoom))
			{
				cell.srcSize = Size.Custom( UI.Settings.current.scrollZoom.ToInternal(rect) );
				return true;
			}
			return false;
		}
	}
}
