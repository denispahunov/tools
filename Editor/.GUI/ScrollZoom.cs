﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Plugins.GUI
{
	[System.Serializable]
	public class ScrollZoom
	{
		public float zoom = 1;
		//public int zoomStage = 0; //number of scroll wheel ticks from zoom 1
		public float zoomStep = 0.0625f;
		public float minZoom = 0.25f;
		public float maxZoom = 2;
		public bool allowZoom = false; //read externally before zooming
		
		public int scrollWheelStep = 18;

		public Vector2 scroll = new Vector2(0, 0);
		private bool isScrolling = false;
		private	Vector2 clickPos = new Vector2(0,0);
		private Vector2 clickScroll = new Vector2(0,0);
		public int scrollButton = 2;
		public bool roundScroll = true;
		public bool allowScroll = false;

		public void Zoom()
		{
			if (Event.current == null) return;

			//reading control
			#if UNITY_EDITOR_OSX
			bool control = Event.current.command;
			#else
			bool control = Event.current.control;
			#endif

			float delta = 0;
			if (Event.current.type == EventType.ScrollWheel) delta = Event.current.delta.y / 3f;
			//else if (Event.current.type == EventType.MouseDrag && Event.current.button == 0 && control) delta = Event.current.delta.y / 15f;
			//else if (control && Event.current.alt && Event.current.type==EventType.KeyDown && Event.current.keyCode==KeyCode.Equals) delta --;
			//else if (control && Event.current.alt && Event.current.type==EventType.KeyDown && Event.current.keyCode==KeyCode.Minus) delta ++;
			if (Mathf.Abs(delta) < 0.001f) return;

			//calculating current zoom stage - number of scroll wheel ticks from zoom 1
			int zoomStage = 0;
			if (zoom < 0.999f)
				zoomStage = -(int)((1-zoom) / zoomStep);
			else if (zoom > 1.0001f)
			{
				float tempZoom = 1;
				while (tempZoom < zoom)
				{
					tempZoom += zoomStep * (int)(tempZoom / 2 + 1) * 2;
					zoomStage ++;
				}
			}

			//new zoom	
			if (delta > 0) zoomStage--;
			if (delta < 0) zoomStage++;

			float newZoom = 1;
			if (zoomStage < 0)
			{
				int minStage = -(int)((1-minZoom) / zoomStep);
				if (zoomStage < minStage) zoomStage = minStage;
				newZoom = 1 - zoomStep*(-zoomStage);
			}
			else if (zoomStage > 0)
			{
				for (int i=0; i<zoomStage; i++)
				{
					float val = zoomStep * (int)(newZoom / 2 + 1) * 2;
					if (newZoom + val > maxZoom + 0.0001f)
						break;
					newZoom += val;
				}
			}

			Zoom(newZoom, Event.current.mousePosition);
		}

		public void Zoom (float newZoom, Vector2 pivot)
		/// Pivot is usually mouse position
		{
			//record mouse position in worldspace
			Vector2 worldMousePos = (pivot - scroll) / zoom;

			//changing zoom
			float zoomChange = newZoom - zoom;
			zoom = newZoom;

			//scrolling around pivot
			scroll -= worldMousePos * zoomChange;
			
			#if UNITY_EDITOR
			if (UnityEditor.EditorWindow.focusedWindow != null) UnityEditor.EditorWindow.focusedWindow.Repaint();
			#endif

			if (roundScroll) RoundScroll();
		}


		public void ScrollWheel(int step = 3)
		{
			float delta = 0;
			if (Event.current.type == EventType.ScrollWheel) delta = Event.current.delta.y / 3f;
			scroll.y -= delta * scrollWheelStep * step;
		}


		public void Scroll()
		{
			if (Event.current.type == EventType.MouseDown  &&  Event.current.button == scrollButton)
			{
				clickPos = Event.current.mousePosition;
				clickScroll = scroll;
				isScrolling = true;
			}

			if (Event.current.type == EventType.MouseDown  &&  Event.current.button == 0  &&  Event.current.alt)  //alternative way to scroll
			{
				clickPos = Event.current.mousePosition;
				clickScroll = scroll;
				isScrolling = true;
			}

			if (Event.current.rawType == EventType.MouseUp) 
			{
				isScrolling = false;
			}
			
			if (isScrolling)
				Scroll(clickScroll + Event.current.mousePosition - clickPos);
		}


		public void Scroll (Vector2 newScroll)
		{
			scroll = newScroll; 

			if (roundScroll) RoundScroll();

			#if UNITY_EDITOR
			GUI2.UI.RemoveFocusOnControl(); //disabling text typing
			if (UnityEditor.EditorWindow.focusedWindow != null)
				UnityEditor.EditorWindow.focusedWindow.Repaint();
			#endif
		}


		public void RoundScroll ()

		{
			if (scroll.x < 0) scroll.x--;	scroll.x = (int)(float)(scroll.x + 0.50002f);  //adding epsilon to prevent position flickering on clear numbers
			if (scroll.y < 0) scroll.y--;	scroll.y = (int)(float)(scroll.y + 0.50002f);
		}


		public Vector2 ToDisplay(Vector2 pos)
		{
			return new Vector2(pos.x * zoom + scroll.x, pos.y * zoom + scroll.y);
		}


		public Rect ToDisplay(Vector2 offset, Vector2 size, Padding padding=new Padding())
		{
			return new Rect(
				(int)( (offset.x + padding.left) * zoom + scroll.x  + 0.5f), 
				(int)( (offset.y + padding.top) * zoom + scroll.y   + 0.5f), 
				(int)( (size.x - (padding.left+padding.right)) * zoom  + 0.5f), 
				(int)( (size.y - (padding.top+padding.bottom)) * zoom  + 0.5f));
		}

		public Rect ToInternal(Rect rect)
		{
			Vector2 offset = new Vector2(
				(rect.x - scroll.x) / zoom, 
				(rect.y - scroll.y) / zoom );
			Vector2 size = new Vector2(
				rect.width / zoom, 
				rect.height / zoom);
			return new Rect(offset, size);
		}

		public Vector2 ToInternal(Vector2 pos)
		{
			return new Vector2 (
				(pos.x - scroll.x) / zoom,
				(pos.y - scroll.y) / zoom );
		}

		public float ToInternal(float val)
		{
			return val / zoom;
		}

		public Vector2 GetWindowCenter (Rect windowRect)
		/// returns the center of the screen(window) in base coordinates
		{
			return -(scroll - windowRect.size/2) / zoom;
		}

		public void FocusWindowOn (Vector2 center, Rect windowRect)
		{
			Scroll( -center*zoom + windowRect.size/2 );
		}


		public Vector2 RoundToZoom (Vector2 vec)
		/// Queer thing
		{
			vec.x /= zoom;
			vec.x = Mathf.Round(vec.x);
			vec.x *= zoom;

			vec.y /= zoom;
			vec.y = Mathf.Round(vec.y);
			vec.y *= zoom;

			return vec;
		}

		/*public Rect ToDisplay(Rect rect)
		{
			return new Rect(rect.x * zoom + scroll.x, rect.y * zoom + scroll.y, rect.width * zoom, rect.height * zoom);
		}
		
		public Rect ToDisplay(float offsetX, float offsetY, float sizeX, float sizeY)
		{
			return new Rect(offsetX * zoom + scroll.x, offsetY * zoom + scroll.y, sizeX * zoom, sizeY * zoom);
		}

		public Rect ToDisplay(Float2 pos, Float2 size)
		{
			if (paddingBox==null) return new Rect(
				(int)( pos.x * zoom + scroll.x  + 0.5f), 
				(int)( pos.y * zoom + scroll.y  + 0.5f), 
				(int)( size.x * zoom  + 0.5f), 
				(int)( size.y * zoom  + 0.5f) );

			else 
			{
				Padding padding = (Padding)paddingBox;
				return new Rect(
				(int)( (pos.x + padding.left) * zoom + scroll.x  + 0.5f), 
				(int)( (pos.y + padding.top) * zoom + scroll.y   + 0.5f), 
				(int)( (size.x - (padding.left+padding.right)) * zoom  + 0.5f), 
				(int)( (size.y - (padding.top+padding.bottom)) * zoom  + 0.5f));
			}
		}

		public Rect ToInternal(Rect rect)
		{
			return new Rect((rect.x - scroll.x) / zoom, (rect.y - scroll.y) / zoom, rect.width / zoom, rect.height / zoom);
		}

		public Vector2 ToInternal(Vector2 pos)
		{
			return (pos - scroll) / zoom;
		} //return new Vector2( (pos.x-scroll.x)/zoom, (pos.y-scroll.y)/zoom); }


		public void Focus(Cell cell, Vector2 pos)
		{
			throw new System.NotImplementedException();
		}*/
	}
}
