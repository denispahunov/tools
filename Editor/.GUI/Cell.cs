﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	/*public struct Dimension
	{
		public enum Type { Auto, Relative, Pixels }
		public Type type;

		public float pixels;
		public float relative;

		public override string ToString () { return "Size " + type.ToString() + ", " + pixels.ToString("0.000") + "p " + relative.ToString("0.000") + "%"; }
	}*/


	public partial class Cell : IDisposable
	{
		public string name;

		public Size srcSize;
		public Size dstSize; //src size should not be changed during layout, so ising it's duplicate

		public bool disabled = false;
		//public Action onChange = null;

		public Padding margins = new Padding();  //this cell offset from it's parents
		public Padding padding = new Padding(2); //this cell contents offset from it's borders

		public float fieldWidth = 0.5f;

		public static Cell current;
		public Cell prevCurrent = null; //SOON: use static list instead. This way we can assign cell as active several times

		public List<Cell> children = null;
		private int poolChildNum = 0;

		public Cell parent = null;


		public bool Clicked
		{get{
			Event currentEvent = Event.current;
			EventType eventType = currentEvent.type;
			if (eventType != EventType.MouseDown || currentEvent.button != 0) return false;
			
			
			Rect rect = GetRect(usePadding:false);
			Debug.Log(rect);
			return rect.Contains(currentEvent.mousePosition);
		}}

		public override string ToString () { return name + " " + dstSize.x.pixels + "," + dstSize.y.pixels + "," + dstSize.width.pixels + "," + dstSize.height.pixels; }

		#region Rect

		public Rect GetRect (bool usePadding=true, bool pixelPerfect=true, bool exactSize=true, bool useScrollZoom=true)
			{
				double minX = dstSize.x.pixels;
				double maxX = dstSize.x.pixels + dstSize.width.pixels;

				double minY = dstSize.y.pixels;
				double maxY = dstSize.y.pixels + dstSize.height.pixels;

				if (usePadding)
				{
					minX += padding.left;
					maxX -= padding.right;
					minY += padding.top;
					maxY -= padding.bottom;
				}

				ScrollZoom scrollZoom = UI.Settings.current.scrollZoom;
				//Vector2 scroll = new Vector2((int)scrollZoom.scroll.x, (int)scrollZoom.scroll.y);
				if (useScrollZoom && scrollZoom != null)
				{
					minX = minX*scrollZoom.zoom + scrollZoom.scroll.x;
					maxX = maxX*scrollZoom.zoom + scrollZoom.scroll.x;
					minY = minY*scrollZoom.zoom + scrollZoom.scroll.y;
					maxY = maxY*scrollZoom.zoom + scrollZoom.scroll.y;
				}

				double sizeX = maxX-minX;
				double sizeY = maxY-minY;

				if (pixelPerfect)
				{
					if (minX < 0) minX--;	minX = (int)(float)(minX + 0.50002f);  //adding epsilon to prevent position flickering on clear numbers
					if (minY < 0) minY--;	minY = (int)(float)(minY + 0.50002f);
					if (sizeX < 0) sizeX--;	sizeX = (int)(float)(sizeX + 0.50002f);
					if (sizeY < 0) sizeY--;	sizeY = (int)(float)(sizeY + 0.50002f);
				}

				return new Rect((float)minX, (float)minY, (float)sizeX, (float)sizeY);
			}

			public void SetRect (Rect rect)
			{
				ScrollZoom scrollZoom = UI.Settings.current.scrollZoom;
				srcSize.x.pixels = (rect.x - scrollZoom.scroll.x) / scrollZoom.zoom; 
				srcSize.y.pixels = (rect.y - scrollZoom.scroll.y) / scrollZoom.zoom; 
				srcSize.width.pixels = rect.width / scrollZoom.zoom; 
				srcSize.height.pixels = rect.height / scrollZoom.zoom; 
			}

			

		#endregion


		#region Pool

			public static Cell GetCell () { using (Timer.Start("Get Cell"))
			/// Gets the next pooled cell from Cell.current - it is the only way to create new cells
			{
				//Profiler.BeginSample("Get Cell");

				if (Event.current.type == EventType.Used)
				{
					//current = null;
					//Profiler.EndSample();
					//return null;
				}

				Cell cell;
				
				if (current.children == null || current.poolChildNum >= current.children.Count)
				//creating new cell
				{
					cell = new Cell();

					if (current.children == null) current.children = new List<Cell>();
					current.children.Add(cell);
				}

				else
				//using the cell from the pool
				{
					cell = current.children[current.poolChildNum];

					cell.poolChildNum = 0; //resetting new cell child pool counter
				}

				current.poolChildNum++;

				cell.disabled = current.disabled;
				cell.valChanged = false;
				cell.parent = current;

				cell.margins = new Padding();
				cell.padding = new Padding(2);
				cell.fieldWidth = current.fieldWidth;

				//Profiler.EndSample();
				return cell;
			}}

			public void ResetChildCount () { poolChildNum = 0; } //to reset root cell children

			public int GetNum (Cell cell)
			{
				for (int i=0; i<children.Count; i++)
					if (children[i] == cell) return i;
				return -1;
			}

			public static readonly Cell tempCell = new Cell(); //universal cell for all used events

		#endregion


		#region Active

			public void SetActive ()
			{
				//if (Cell.current == this) return; //yes, it should be assigned as active on every using!
				prevCurrent = current;
				current = this;
			}

			public void Dispose ()
			{
				if (current != this  &&  Event.current.type != EventType.Used)
					throw new Exception("Cell is not assigned as active on Dispose");

				current = prevCurrent;
			}

		#endregion


		#region Layout

			public void Layout ()
			{
				SaveSizes();
				//CalcMinimumSizes();
				LayoutPixelSized();
				CalcChildrenPixelSizes();
				CalcChildrenPixelOffsets();
				//FlushUnusedChildren();
			}


			private void SaveSizes ()
			{
				dstSize = srcSize;

				if (children == null) return; //for the empty root cell
				int childCount = children.Count;

				//recursive
				for (int i=0; i<childCount; i++)
					children[i].SaveSizes();
			}


			private void LayoutPixelSized ()
			/// Does the minimum-sized layout of only the pixel-sized cells to calculate the minimum cell pixel size
			/// Calculated from child to parent
			{
				if (children == null) return; //for the empty root cell
				int childCount = children.Count;

				//recursive
				for (int i=0; i<childCount; i++)
				{

					Cell child = children[i];
					if (child.children != null)
						child.LayoutPixelSized();
				}

				//Horizontal sizes:
				{
					double offset = 0;
					double maxSize = 0;

					for (int i=0; i<childCount; i++)
					{
						Cell child = children[i];
						//if (child.dstSize.width.type != Dimension.Type.Pixels) continue; //could be pixel cell inside a relative one

						if (child.dstSize.x.type == Dimension.Type.Pixels)
						{
							if (child.dstSize.x.pixels + child.dstSize.width.pixels > maxSize)
								maxSize = child.dstSize.x.pixels + child.dstSize.width.pixels;
						}

						if (child.dstSize.x.type == Dimension.Type.Auto)
						{
							child.dstSize.x.pixels = offset;
							offset += child.dstSize.width.pixels;

							if (offset > maxSize) maxSize = offset;
						}

						//skipping relative sizes because it's not clear what should we do with them
					}

					if (dstSize.width.pixels < maxSize + margins.left + margins.right) 
						dstSize.width.pixels = maxSize + margins.left + margins.right;
				}

				//Vertical sizes:
				{
					double offset = 0;
					double relativeSum = 0;
					double maxSize = 0;

					for (int i=0; i<childCount; i++)
					{
						Cell child = children[i];
						//if (child.dstSize.height.type != Dimension.Type.Pixels) continue;

						if (child.dstSize.y.type == Dimension.Type.Pixels)
						{
							if (child.dstSize.y.pixels + child.dstSize.height.pixels > maxSize)
								maxSize = child.dstSize.y.pixels + child.dstSize.height.pixels;
						}

						if (child.dstSize.y.type == Dimension.Type.Auto)
						{
							child.dstSize.y.pixels = offset;
							offset += child.dstSize.height.pixels;

							if (offset > maxSize) maxSize = offset;
						}
					}

					if (dstSize.height.pixels < maxSize + margins.top + margins.bottom) 
						dstSize.height.pixels = maxSize + margins.top + margins.bottom;
				}
			}


			private void CalcMinimumSizes ()
			/// Sets the width and height 'pixels' values to the sum of child pixel values.
			/// Does not use type, relative or any offset
			{
				if (children == null) return; //for the empty root cell
				int childCount = children.Count;

				//recursive
				for (int i=0; i<childCount; i++)
				{

					Cell child = children[i];
					if (child.children != null)
						child.CalcMinimumSizes();
				}

				//calc min
				double minWidth = 0;
				double minHeight = 0;
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];

					minWidth += child.dstSize.width.pixels;
					minHeight += child.dstSize.height.pixels;
				}

				if (minWidth > dstSize.width.pixels) dstSize.width.pixels = minWidth;
				if (minHeight > dstSize.height.pixels) dstSize.height.pixels = minHeight;
			}


			private void CalcChildrenPixelSizes ()
			/// Calculates child pixel-sized width and height using this cell pixel size
			{
				if (children == null) return; //for the empty root cell
				int childCount = children.Count;

				//Horizontal:
				{
					double pixelsLeft = dstSize.width.pixels - margins.left - margins.right;
					int autoCellsCount = 0;

					//non-auto children first
					for (int i=0; i<childCount; i++)
					{
						Cell child = children[i];
					
						//skipping all pixel-sized,
						if (child.dstSize.width.type == Dimension.Type.Pixels)
							pixelsLeft -= child.dstSize.width.pixels;

						//sizing all relative
						if (child.dstSize.width.type == Dimension.Type.Relative)
						{
							child.dstSize.width.pixels = (this.dstSize.width.pixels-margins.left-margins.right) * child.dstSize.width.relative;
							pixelsLeft -= child.dstSize.width.pixels;
						}

						//counting all auto
						if (child.dstSize.width.type == Dimension.Type.Auto)
							autoCellsCount++;
					}
			
					//spreading remaining pixels among auto-sized
					if (autoCellsCount != 0)
					{
						//excluding cells that have minimum sizes
						for (int i=0; i<childCount; i++)
						{
							Cell child = children[i];
							if (child.dstSize.width.type != Dimension.Type.Auto) continue;

							pixelsLeft -= child.dstSize.width.pixels;
						}

						//adding to all auto-sized the remaining pixels
						double autoSize = pixelsLeft / autoCellsCount;
						for (int i=0; i<childCount; i++)
						{
							Cell child = children[i];

							if (child.dstSize.width.type == Dimension.Type.Auto)
								child.dstSize.width.pixels += autoSize;
						}
					}
				}

				//Vertical
				{
					double pixelsLeft = dstSize.height.pixels - margins.top - margins.bottom;
					int autoCellsCount = 0;

					//non-auto children first
					for (int i=0; i<childCount; i++)
					{
						Cell child = children[i];

						//skipping all pixel-sized,
						if (child.dstSize.height.type == Dimension.Type.Pixels)
							pixelsLeft -= child.dstSize.height.pixels;

						//sizing all relative
						if (child.dstSize.height.type == Dimension.Type.Relative)
						{
							double pixels = (this.dstSize.height.pixels-margins.top-margins.bottom) * child.dstSize.height.relative;
							if (pixels > child.dstSize.height.pixels) child.dstSize.height.pixels = pixels;
							pixelsLeft -= child.dstSize.height.pixels;
						}

						//counting all auto
						if (child.dstSize.height.type == Dimension.Type.Auto)
							autoCellsCount++;
					}
			
					//spreading remaining pixels among auto-sized
					if (autoCellsCount != 0)
					{
						//excluding cells that have minimum sizes
						for (int i=0; i<childCount; i++)
						{
							Cell child = children[i];
							if (child.dstSize.height.type != Dimension.Type.Auto) continue;

							pixelsLeft -= child.dstSize.height.pixels;
						}

						//adding to all auto-sized the remaining pixels
						double autoSize = pixelsLeft / autoCellsCount;
						for (int i=0; i<childCount; i++)
						{
							Cell child = children[i];

							if (child.dstSize.height.type == Dimension.Type.Auto)
								child.dstSize.height.pixels += autoSize;
						}
					}
				}

				//recursive
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];
					if (child.children != null)
						child.CalcChildrenPixelSizes();
				}
			}


			private void CalcChildrenPixelOffsets ()
			/// Adding cell offset to pixel offsets, and placing relative and auto offsets
			{
				if (children == null) return; //for the empty root cell
				int childCount = children.Count;

				//Horizontal
				double autoOffset = 0;
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];

					//placing relative
					if (child.dstSize.x.type == Dimension.Type.Relative)
						child.dstSize.x.pixels = child.dstSize.x.relative * (this.dstSize.width.pixels-margins.left-margins.right);

					//placing auto
					if (child.dstSize.x.type == Dimension.Type.Auto)
						child.dstSize.x.pixels = autoOffset;

					child.dstSize.x.pixels += this.dstSize.x.pixels + margins.left;
					autoOffset += child.dstSize.width.pixels;
				}

				//Vertical
				autoOffset = 0;
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];

					//placing relative
					if (child.dstSize.y.type == Dimension.Type.Relative)
						child.dstSize.y.pixels = child.dstSize.y.relative * (this.dstSize.height.pixels-margins.top-margins.bottom);

					//placing auto
					if (child.dstSize.y.type == Dimension.Type.Auto)
						child.dstSize.y.pixels = autoOffset;

					child.dstSize.y.pixels += this.dstSize.y.pixels + margins.top;
					autoOffset += child.dstSize.height.pixels;
				}

				//recursive
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];
					if (child.children != null)
						child.CalcChildrenPixelOffsets();
				}
			}


			public void FlushUnusedChildren ()
			{
				if (children == null) return; //for the empty root cell
				int childCount = children.Count;

				//recursive
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];
					if (child.children != null)
						child.FlushUnusedChildren();
				}

				if (childCount > poolChildNum)
				{
					Debug.Log("Flushing " + (childCount-poolChildNum) + " (" + childCount + "-" + poolChildNum + ")"  + " children in " + Event.current.type + " " + name);
					children.RemoveRange(poolChildNum, childCount-poolChildNum); 
				}
			}

		#endregion


		#region Change

			public bool valChanged = false;
			public bool ignoreChange = false;

			public UnityEngine.Object undoObject;
			public string undoName;

			public Action undoAction;

			public void WriteUndo ()
			{
				#if UNITY_EDITOR
				if (undoObject!=null) UnityEditor.Undo.RecordObject(undoObject, undoName);
				#endif

				if (parent != null) parent.WriteUndo();
			}


			public void SetChange () 
			/// Enables 'changed' toggle recursively (and sets dirty state btw)
			{ 
				#if UNITY_EDITOR
				//graph.setDirty = !graph.setDirty;  //no need to do it since 
				if (undoObject!=null) UnityEditor.EditorUtility.SetDirty(undoObject); //doesn't really matter if is set dirty before the actual value change or after
				#endif

				valChanged = true;  
				if (parent != null) parent.SetChange();
			}


			public void PerformUndo ()
			/// Calling undo action on all cells
			/// Subscribing in UI.Start
			{
				if (undoAction != null) undoAction();

				if (children != null)
				{
					int childCount = children.Count;
					for (int i=0; i<childCount; i++)
					{
						Cell child = children[i];
						child.PerformUndo();
					}
				}
			}

		#endregion


		#region Static Constructors

			public static Cell Line
			{get{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					width = new Dimension(Dimension.Type.Relative, 1),
					x = new Dimension(Dimension.Type.Pixels, 0),
					height = new Dimension(Dimension.Type.Auto),
					y = new Dimension(Dimension.Type.Auto) };

				cell.SetActive();
				return cell;
			}}


			public static Cell LinePx (float size)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					width = new Dimension(Dimension.Type.Relative, 1),
					x = new Dimension(Dimension.Type.Pixels, 0),
					height = new Dimension(Dimension.Type.Pixels, size),
					y = new Dimension(Dimension.Type.Auto) };

				cell.SetActive();
				return cell;
			}

			public static Cell LineRel (float size)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Pixels, 0),
					y = new Dimension(Dimension.Type.Auto),
					width = new Dimension(Dimension.Type.Relative, 1),
					height = new Dimension(Dimension.Type.Relative, size) };

				cell.SetActive();
				return cell;
			}


			public static Cell LineStd
			{get{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					width = new Dimension(Dimension.Type.Relative, 1),
					x = new Dimension(Dimension.Type.Pixels, 0),
					height = new Dimension(Dimension.Type.Pixels, Size.lineHeight),
					y = new Dimension(Dimension.Type.Auto) };

				cell.SetActive();
				return cell;
			}}


			public static Cell Row
			{get{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					width = new Dimension(Dimension.Type.Auto),
					x = new Dimension(Dimension.Type.Auto),
					height = new Dimension(Dimension.Type.Relative, 1),
					y = new Dimension(Dimension.Type.Pixels, 0) };

				cell.SetActive();
				return cell;
			}}


			public static Cell RowPx (float size)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, size),
				height = new Dimension(Dimension.Type.Relative, 1) };

				cell.SetActive();
				return cell;
			}


			public static Cell RowRel (float size)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Relative, size),
				height = new Dimension(Dimension.Type.Relative, 1) };

				cell.SetActive();
				return cell;
			}


			public static Cell Custom (float sizeX, float sizeY)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Pixels, 0),
					y = new Dimension(Dimension.Type.Pixels, 0),
					width = new Dimension(Dimension.Type.Pixels, sizeX),
					height = new Dimension(Dimension.Type.Pixels, sizeY) };
			
				cell.SetActive();
				return cell;
			}


			public static Cell Custom (float x, float y, float width, float height)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;

				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Pixels, x),
					y = new Dimension(Dimension.Type.Pixels, y),
					width = new Dimension(Dimension.Type.Pixels, width),
					height = new Dimension(Dimension.Type.Pixels, height) };
			
				cell.SetActive();
				return cell;
			}


			public static void EmptyLinePx (float pixels)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return;
				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Pixels, 0),
					y = new Dimension(Dimension.Type.Auto),
					width = new Dimension(Dimension.Type.Relative, 1),
					height = new Dimension(Dimension.Type.Pixels, pixels) };
			}


			public static void EmptyLineRel (float percent)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return;
				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Pixels, 0),
					y = new Dimension(Dimension.Type.Auto),
					width = new Dimension(Dimension.Type.Relative, 1),
					height = new Dimension(Dimension.Type.Relative, percent) };
			}


			public static void EmptyRowPx (float pixels)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return;
				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Auto),
					y = new Dimension(Dimension.Type.Pixels, 0),
					width = new Dimension(Dimension.Type.Pixels, pixels),
					height = new Dimension(Dimension.Type.Relative, 1) };
			}


			public static void EmptyRowRel (float percent)
			{
				Cell cell = Cell.GetCell();
				if (cell == null) return;
				cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Relative, percent),
				height = new Dimension(Dimension.Type.Relative, 1) };
			}


			public static Cell Full
			{get{
				Cell cell = UI.Group(Cell.current.srcSize);
				return cell;
			}}


		#endregion


		public virtual void DrawBackground (int level=0, bool recursive=false)
		{
			Rect dispRect = GetRect(usePadding:false); //PixelPerfectRect_NoPadding;			

			Color color = new Color(level/7f, 1-level/7f, 0, 0.5f);

			if (dispRect.width < 0 || dispRect.height < 0)
				color = new Color(0, 0, 0, 0.5f);

			#if UNITY_EDITOR
			UnityEngine.GUI.DrawTexture(dispRect, TexturesCache.GetTexture("DPUI/Backgrounds/Gradient"), ScaleMode.StretchToFill);
			UnityEditor.EditorGUI.DrawRect(dispRect, color);
			//if (caption && name != null) 
			//	UnityEditor.EditorGUI.LabelField( new Rect(rect.x, rect.y+rect.height/2-9, rect.width, 18), name, style:Styles.light.centerLabel);

			ScrollZoom scrollZoom = UI.Settings.current.scrollZoom;
			//Logging position to prevent flickering
			//UnityEditor.EditorGUI.LabelField(dispRect, 
			//	"r:" + dispRect.y + 
			//	" c:" + dstSize.y.pixels);

			#endif

			if (recursive && children != null)
			{
				int childCount = children.Count;
				for (int i=0; i<childCount; i++)
				{
					Cell child = children[i];
					child.DrawBackground(level+1, true);
				}
			}
		}





		/*public void Dispose ()
		{
			active = parent;
		}*/
	}
}