﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{

	public static partial class UI
	{
		public static float origLabelValue;

		private static readonly float[] roundSteps = { 0.05f,	0.1f,	0.5f,	1f,		5f,		10f };
		private static readonly float[] roundVals =  { 0.5f,	1,		10,		40,		200,	500 };


		private static bool DragLabel (
			ref float val, 
			string label, 
			Texture2D icon = null,
			Cell cell = null,
			float minStep = 0.01f,
			string tooltip = null)
		{
			//Profiler.BeginSample("DragLabel");

			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;

			#if UNITY_EDITOR
			if (repaint) 
			{
				Rect rect = cell.GetRect();
				rect.height += 2;
				if (UI.IsWithinWindow(rect))
				{
					//cursor
					if (!cell.disabled)
						UnityEditor.EditorGUIUtility.AddCursorRect (rect, UnityEditor.MouseCursor.SlideArrow);

					//drawing
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);
					
						if (icon == null)
							using (Timer.Start("LabelField"))
								UnityEditor.EditorGUI.LabelField(rect, label, style:UI.Settings.current.styles.label);
						else
						{
							Vector2 center = new Vector2(rect.x + icon.width/2, rect.y + rect.size.y/2);
							Icon(icon,center);
						
							Rect labelRect = new Rect(rect.x+icon.width, rect.y, rect.width-icon.width, rect.height);
							using (Timer.Start("LabelField"))
								UnityEditor.EditorGUI.LabelField(labelRect, label, style:UI.Settings.current.styles.label);
						}
					
					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();

					float newVal = val;

					if (!cell.disabled)
					{
						//starting drag
						if (DragDrop.CheckStart( cell, rect ))
							origLabelValue = val;

						//dragging
						if (DragDrop.CheckDrag(cell))
						{
							newVal = RoundValue(origLabelValue, DragDrop.totalDelta.x/5, minStep);

							if (UnityEditor.EditorWindow.focusedWindow!=null) UnityEditor.EditorWindow.focusedWindow.Repaint(); 
							//UnityEditor.EditorGUI.FocusTextInControl(null);
							UnityEngine.GUI.FocusControl(null);  
						
						}

						DragDrop.CheckRelease(cell);
						
						//on change
						if (newVal-val > Mathf.Epsilon  ||  newVal-val < -Mathf.Epsilon)
						{
							cell.WriteUndo();
							val = newVal;
							cell.SetChange();

							return true;
						}
					}
				}
			}
			#endif

			//Profiler.EndSample();

			return false;
		}


		private static float RoundValue (float orig, float delta, float minStep = 0.01f, float min=0, float max=0)
		{
			float val = orig;
			int steps = Mathf.RoundToInt(delta);

			for (int i=0; i<Mathf.Abs(steps); i++)
			{
				float absVal = val>=0? val : -val;

				float step = 0.01f;
				//if (absVal > 0.99f) step=0.02f; if (absVal > 1.99f) step=0.1f;   if (absVal > 4.999f) step = 0.2f; if (absVal > 9.999f) step=0.5f;
				//if (absVal > 39.999f) step=1f;  if (absVal > 99.999f) step = 2f; if (absVal > 199.999f) step = 5f; if (absVal > 499.999f) step = 10f; 
				if (absVal > 0.5f) step=0.05f;  if (absVal > 0.999f) step=0.1f;   if (absVal > 9.999f) step=0.5f;
				if (absVal > 39.999f) step=1f;   if (absVal > 199.999f) step = 5f; if (absVal > 499.999f) step = 10f; 
				if (step < minStep) step = minStep;

				val = steps>0? val+step : val-step;
				val = Mathf.Round(val*10000)/10000f;

				if (Mathf.Abs(min)>0.001f && val<min) val=min;
				if (Mathf.Abs(max)>0.001f && val>max) val=max;
			}

			return val;
		}

	}
}