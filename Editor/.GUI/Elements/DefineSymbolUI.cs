﻿using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{
		public static bool DefineSymbol (
			string keyword,
			string label,
			Cell cell = null)
		{
			#if UNITY_EDITOR
			UnityEditor.BuildTargetGroup buildGroup = UnityEditor.EditorUserBuildSettings.selectedBuildTargetGroup;
			string defineSymbols = UnityEditor.PlayerSettings.GetScriptingDefineSymbolsForGroup(buildGroup);
				
			bool enabled = false;
			if (defineSymbols.Contains(keyword+";") || defineSymbols.EndsWith(keyword)) enabled = true;
				
			using (Cell.LineStd)
			if (UI.Field(ref enabled, label, cell:Cell.current))
			{
				if (enabled)
				{
					defineSymbols += (defineSymbols.Length!=0? ";" : "") + keyword;
				}
				else
				{
					defineSymbols = defineSymbols.Replace(keyword,""); 
					defineSymbols = defineSymbols.Replace(";;", ";"); 
				}
				
				UnityEditor.PlayerSettings.SetScriptingDefineSymbolsForGroup(buildGroup, defineSymbols);

				return true;
			}
			#endif

			return false;
		}
	}
}
