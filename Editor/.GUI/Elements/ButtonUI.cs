﻿using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{
		public static bool Button (
			string label, 
			Cell cell,
			Texture2D icon = null, 
			GUIStyle style = null,
			string tooltip = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "Button";

			bool dst = false;

			if (repaint) 
			{
				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					if (style == null) style = UI.Settings.current.styles.button;

					#if UNITY_EDITOR
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);

					dst = UnityEngine.GUI.Button(rect, label, style);
					//UnityEditor.EditorGUI.LabelField(rect,  "" + (rect.center.x-370));

					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}

				if (icon != null) 
					Icon(icon, cell:cell);
			}

			if (dst) 
			{
				//cell.WriteUndo(); ?
				//cell.SetChange(); //Note that pressing the button does not sets change!
			}

			return dst;
		}


		public static bool CheckButton (
			ref bool src, 
			string label, 
			Texture2D icon = null, 
			Cell cell = null,
			GUIStyle style = null,
			string tooltip = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "CheckButton";

			if (style == null) style = UI.Settings.current.styles.button;

			bool dst = src;

			if (repaint) 
			{
				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);

					dst = UnityEngine.GUI.Toggle(rect, src, label, style);

					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}

				if (icon != null) 
					Icon(icon, cell:cell);
			}

			if (src != dst) 
			{
				cell.WriteUndo();
				src = dst;
				cell.SetChange();
				return true;
			}

			return false;
		}

		
		public static bool IconButton (
			bool src, 
			Texture2D icon,  
			Cell cell = null,
			GUIStyle style = null,
			string tooltip = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return src;

			if (style == null) style = GUIStyle.none;

			bool dst = src;

			if (repaint) 
			{
				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);

					dst = UnityEngine.GUI.Toggle(rect, src, text:null, style:style);

					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}
			}

			else UI.Icon(icon, cell:cell);

			if (src != dst) 
			{
				cell.SetChange();
			}

			return dst;
		}


		public static bool IconToggle (
			bool src, 
			Texture2D onIcon, 
			Texture2D offIcon, 
			Cell cell = null,
			GUIStyle style = null,
			string tooltip = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return src;

			if (style == null) style = GUIStyle.none;

			bool dst = src;

			if (repaint) 
			{
				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);

					dst = UnityEngine.GUI.Toggle(rect, src, text:null, style:style);

					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}
			}

			if (dst) UI.Icon(onIcon, cell:cell);
			else UI.Icon(offIcon, cell:cell);

			if (src != dst) 
			{
				cell.SetChange();
			}

			return dst;
		}

	}
}