﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace Plugins.GUI
{

	public static partial class UI
	{

		public static bool Field<T> (
			ref T val, 
			Cell cell,
			Type type = null,
			bool allowSceneObject = false, //for unity objects
			string tooltip = null)
		{
			Profiler.BeginSample("Field");

			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) 
			{
				Profiler.EndSample();
				return false;
			}

			//drawing
			if (type == null) type = typeof(T);
			T newVal = val;
			bool change = false;

			#if UNITY_EDITOR
			if (repaint) 
			{
				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					//finding if filed is aimed to re-draw it on drag
					//if (Event.current.type == EventType.MouseDown  &&  Event.current.button == 0  &&  rect.Contains(Event.current.mousePosition))
					//	Event.current.Use();
					//	{ Debug.Log("Field Clicked"); UI.clickedField = true; }
					//if (Event.current.rawType == EventType.MouseUp) 
					//	UI.clickedField = false;


					//drawing
					if (Cell.current.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);

					if (type == typeof(float))
					{
						float src = (float)(object)val;
						float dst;
						using (Timer.Start("FloatField"))
							dst = UnityEditor.EditorGUI.FloatField(rect, src, style:UI.Settings.current.styles.field);//, style:style??styles.field); 
						//dst = DragDrop.FieldChange(labelCell, dst);
						change = dst-src > Mathf.Epsilon  ||  dst-src < -Mathf.Epsilon;
						newVal = (T)(object)dst;
					}

					else if (type == typeof(int))
					{
						int src = (int)(object)val;
						int dst;
						using (Timer.Start("IntField"))
							dst = UnityEditor.EditorGUI.IntField(rect, src, UI.Settings.current.styles.field);//, style:style??styles.field);
						//if (label != null) newVal = Mathf.RoundToInt(DragDrop.FieldChange(this, labelCell, newVal, minStep:1));
						change = src!=dst;
						newVal = (T)(object)dst;
					}

					else if (type == typeof(bool))
					{
						bool src = (bool)(object)val;
						bool dst;
						using (Timer.Start("Toggle"))
						{
							if (UI.Settings.current.scrollZoom==null || UI.Settings.current.scrollZoom.zoom > 0.8f)
								dst = UnityEditor.EditorGUI.Toggle(rect, src);
							else
								dst = UnityEditor.EditorGUI.Toggle(rect, src, style:UI.Settings.current.styles.button);
						}
						change = src!=dst;
						newVal = (T)(object)dst;
					}

					else if (type == typeof(string))
					{
						string src = (string)(object)val;
						string dst = UnityEditor.EditorGUI.TextField(rect, src, UI.Settings.current.styles.field);//, style:style??styles.field);
						change = src!=dst;
						newVal = (T)(object)dst;
					}

					else if (type == typeof(Color))
					{
						Color src = (Color)(object)val;
						Color dst = UnityEditor.EditorGUI.ColorField(rect, src);
						change = src!=dst;
						newVal = (T)(object)dst;
					}

					else if (type == typeof(Texture2D)) //should be a separate type from object
					{
						Texture2D src = (Texture2D)(object)val;
						Texture2D dst = (Texture2D)UnityEditor.EditorGUI.ObjectField(rect, src, typeof(Texture2D), allowSceneObject);
						change = src!=dst;
						newVal = (T)(object)dst;
					}

					else if (type == typeof(Transform)) //to assign prefab (it's a GameOObject by default)
					{
						Transform src = (Transform)(object)val;
						Transform dst = ((GameObject)UnityEditor.EditorGUI.ObjectField(rect, src.gameObject, typeof(GameObject), allowSceneObject)).transform;
						change = src!=dst;
						newVal = (T)(object)dst;
					}

					else if (type.IsSubclassOf(typeof(UnityEngine.Object)))
					{
						UnityEngine.Object src = (UnityEngine.Object)(object)val;
						UnityEngine.Object dst = UnityEditor.EditorGUI.ObjectField(rect, src, type, allowSceneObject);
						change = src!=dst;
						newVal = (T)(object)dst;

						//code from previous UI. Could be useful. ScriptOps moved to ScriptableAssets extension
						/* #if UNITY_EDITOR
						UnityEngine.Object oldVal = (UnityEngine.Object)val;
						UnityEngine.Object newVal = UnityEditor.EditorGUI.ObjectField(fieldRect, oldVal, type, allowSceneObject);

						//showing object selector
						if (openObjectSelector) ScriptOps.ShowObjectSelector(type, id, allowSceneObject);

						//waiting for object selector close
						if (Event.current.type == EventType.ExecuteCommand && 
							Event.current.commandName == "ObjectSelectorUpdated" && 
							ScriptOps.GetObjectSelectorId() == id)
								newVal = (UnityEngine.Object)ScriptOps.GetObjectSelectorObject();

						change = oldVal!=newVal;
						val = newVal; */
					}

					else if (type.IsEnum) 
					{
						Enum src = (Enum)(object)val;
						Enum dst;
						using (Timer.Start("EnumPopup"))
						{
							if (UI.Settings.current.scrollZoom==null || UI.Settings.current.scrollZoom.zoom > 0.9f)
								dst = UnityEditor.EditorGUI.EnumPopup(rect, src, UI.Settings.current.styles.enumClose); 
							else
								dst = UnityEditor.EditorGUI.EnumPopup(rect, src, UI.Settings.current.styles.enumFar);
						}
						
						change = !src.Equals(dst);  //!EqualityComparer<Enum>.Default.Equals(oldVal, newVal);
						newVal = (T)(object)dst;
					}

					if (Cell.current.disabled) UnityEditor.EditorGUI.EndDisabledGroup();

					//if (Event.current.type == EventType.MouseDown  &&  Event.current.button == 0  &&  rect.Contains(Event.current.mousePosition))
					//	Event.current.Use();
				}

			}
			#endif

			if (change)
			{
				cell.WriteUndo();
				val = newVal;
				cell.SetChange();
			}

			Profiler.EndSample();

			return change;
		}


		public static bool Field<T> (
			ref T val, 
			string label, 
			Cell cell,
			Texture2D icon = null,
			bool isLeft = false,
			Type type = null,
			bool allowSceneObject = false, //for unity objects
			float fieldWidth = -1,
			string tooltip = null)
		{
			using (Timer.Start("Get Field Type"))
			{
				if (type == null) type = typeof(T);
				if (type == typeof(object)) type = val.GetType();
			}

			if (type == typeof(Vector2) || type == typeof(Vector3) || type == typeof(Vector4) || type == typeof(Coord) || type == typeof(CoordRect))
				using (Timer.Start("Multiline Field"))
					return MultilineLabelField(ref val, label, cell:cell, type:type, tooltip:tooltip);

			using (Timer.Start("Get Field Cell"))
			{
				if (cell == null) cell = Empty(Size.fixedLine);
				if (cell == null) return false;
				cell.name = "LabelField";
			}

			if (fieldWidth == -1) fieldWidth = cell.fieldWidth;

			bool change = false;

			bool disposeNeeded = false;
			if (Cell.current != cell) 
			{
				disposeNeeded = true;
				cell.SetActive();
			}

			//using (cell)
			using (Timer.Start("Field Cells"))
			{
				//creating cells in needed order
				Cell labelCell; Cell fieldCell;
				if (!isLeft)
				{
					labelCell = Cell.GetCell();
					fieldCell = Cell.GetCell();
					
				}
				else 
				{
					fieldCell = Cell.GetCell();
					labelCell = Cell.GetCell();
				}

				if (labelCell == null) { if (disposeNeeded) cell.Dispose(); return false; }
				if (fieldCell == null) { if (disposeNeeded) cell.Dispose(); return false; }

				//setting cell sizes
				
				if (type == typeof(bool)) fieldCell.srcSize = Size.RowPixels(20);
				else if (type == typeof(Color)) fieldCell.srcSize = Size.RowPixels(50);
				else if (fieldWidth >= 0) fieldCell.srcSize = Size.RowPercent(fieldWidth);
				else fieldCell.srcSize = Size.row; 

				labelCell.srcSize = Size.row;

				//drawing
				
				if (UI.repaint)
				{
					Rect cellRect = cell.GetRect();
					if (UI.IsWithinWindow(cellRect))
					using (Timer.Start("Drawing Field"))
					{
						if (type == typeof(float) && val!=null) //val could be null on auto class layout
						{
							float fval = (float)(object)val;
							using (Timer.Start("Drag Label"))
								change = DragLabel(ref fval, label, icon:icon, cell:labelCell, tooltip:tooltip);
							val = (T)(object)fval;
						}
						else if (type == typeof(int) && val!=null)
						{
							int ival = (int)(object)val;
							float fval = (float)ival;
							using (Timer.Start("Drag Label"))
								change = DragLabel(ref fval, label, icon:icon, cell:labelCell, minStep:1f, tooltip:tooltip);
							ival = (int)fval;
							val = (T)(object)ival;
						}
						else Label(label, icon:icon, cell:labelCell, tooltip:tooltip);

						change = Field<T>(ref val, cell:fieldCell, type:type, allowSceneObject:allowSceneObject, tooltip:tooltip)  ||  change;
					}
				}
			}
			if (disposeNeeded) cell.Dispose();

			return change;
		}


		public static T Field<T> (  //for properties
			T val, 
			string label,
			Cell cell,
			Texture2D icon = null,
			bool isLeft = false,
			Type type = null,
			bool allowSceneObject = false,
			float fieldWidth = 0.5f,
			string tooltip = null)
		{
			Field(ref val, label, cell, icon, isLeft, type, allowSceneObject, fieldWidth, tooltip);
			return val;
		}

		public static bool MultilineLabelField<T> (
			ref T val, 
			string label, 
			Cell cell = null,
			Type type = null,
			float fieldWidth = 0.5f,
			string tooltip = null)
		{
			if (type == null) type = typeof(T);

			//preapring temp vector
			bool useX=false; bool useY=false; bool useZ=false; bool useW=false;
			Vector4 src = new Vector4(); 

			if (type == typeof(Vector2)) { useX=true; useY=true;	if (val!=null) src = (Vector2)(object)val; } //null on class auto layout
			else if (type == typeof(Coord)) { useX=true; useZ=true;		if (val!=null)src = ((Coord)(object)val).vector4; }
			else if (type == typeof(Vector3)) { useX=true; useY=true; useZ=true;	if (val!=null)src = (Vector3)(object)val; }
			else if (type == typeof(Vector4)) { useX=true; useY=true; useZ=true; useW=true;		if (val!=null) src = (Vector4)(object)val; }
			else if (type == typeof(CoordRect)) { useX=true; useY=true; useZ=true; useW=true;	 if (val!=null)src = ((CoordRect)(object)val).vector4; }

			Vector4 dst = src;

			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "MultilineField";

			//cell.SetActive();
			//using (cell)
			bool disposeNeeded = false;
			if (Cell.current != cell) 
			{
				disposeNeeded = true;
				cell.SetActive();
			}

			{
				//label
				using (UI.Group(Size.row))
				{
					Cell labelCell = Cell.GetCell();
					if (labelCell == null) { if (disposeNeeded) cell.Dispose(); return false; }

					labelCell.srcSize = Size.fixedLine;
					#if UNITY_EDITOR
					if (repaint) 
					{
						Rect labelRect = labelCell.GetRect();
						if (UI.IsWithinWindow(labelRect)) 
							UnityEditor.EditorGUI.LabelField(labelRect, label, style:UI.Settings.current.styles.label);
					}
					#endif
				}

				//XYZ
				using (UI.Group(Size.RowPixels(18)))
				{
					if (useX) DragLabel(ref dst.x, "X");
					if (useY) DragLabel(ref dst.y, "Y");
					if (useZ) DragLabel(ref dst.z, "Z");
					if (useW) DragLabel(ref dst.w, "W");
				}

				//Fields
				Size fieldSize = fieldWidth >= 0  ? 
					Size.RowPercent(fieldWidth) :
					Size.row; 
				using (UI.Group(fieldSize))
				{
					if (useX) using (Cell.LineStd) Field(ref dst.x, cell:Cell.current);
					if (useY) using (Cell.LineStd) Field(ref dst.y, cell:Cell.current);
					if (useZ) using (Cell.LineStd) Field(ref dst.z, cell:Cell.current);
					if (useW) using (Cell.LineStd) Field(ref dst.w, cell:Cell.current);
				}

				//change
				if (Math.Abs(dst.x - src.x) > Mathf.Epsilon  ||  Math.Abs(dst.y - src.y) > Mathf.Epsilon || Math.Abs(dst.z - src.z) > Mathf.Epsilon || Math.Abs(dst.w - src.w) > Mathf.Epsilon)
				{
					cell.WriteUndo();

					if (type == typeof(Vector2)) val = (T)(object)(new Vector2(dst.x, dst.y));
					else if (type == typeof(Coord)) val = (T)(object)(new Coord((int)dst.x, (int)dst.z));
					else if (type == typeof(Vector3)) val = (T)(object)(new Vector3(dst.x, dst.y, dst.z));
					else if (type == typeof(Vector4)) val = (T)(object)dst;
					else if (type == typeof(CoordRect)) val = (T)(object)(new CoordRect((int)dst.x, (int)dst.y, (int)dst.z, (int)dst.w));

					cell.SetChange();

					if (disposeNeeded) cell.Dispose();
					return true;
				}
			}

			if (disposeNeeded) cell.Dispose();

			return false;
		}


		public static bool CustomField<T> (
			ref T val, 
			Func<T, Rect, T> drawField, 
			Func<T, T, bool> changeFunc=null,
			Cell cell = null,
			Type type = null,
			string tooltip = null)
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;

			//drawing
			if (type == null) type = typeof(T);
			T newVal = val;
			bool change = false;

			#if UNITY_EDITOR
			if (repaint) 
			{
				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					newVal = drawField(val, rect);

					if (changeFunc != null) change = changeFunc(val, newVal);
					else change = (object)val != (object)newVal;  //val.Equals(newVal);

					val = newVal;
				}
			}
			#endif

			if (change)
			{
				cell.WriteUndo();
				val = newVal;
				cell.SetChange();
			}

			//Profiler.EndSample();

			return change;
		}
	}
}