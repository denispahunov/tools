﻿using System;
using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{

		public static Cell Root (Size size)
		///Creates a root group trying to re-use the given cell
		{
			Cell cell = Settings.current.rootCell;
			cell.ResetChildCount(); //this is usually done in GetChildCell, but root cell has no parent

			cell.SetActive();
			cell.srcSize = size;
			cell.name = "Root";
			cell.valChanged = false;

			//if (cell.styles == null) cell.styles = new StylesCache();
			//if (!cell.styles.Initilized) cell.styles.Init();
			//cell.styles.Resize(cell.scrollZoom.zoom);

			return cell;
		}


		public static Cell Line ()
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			cell.srcSize = new Size () { 
				width = new Dimension(Dimension.Type.Relative, 1),
				x = new Dimension(Dimension.Type.Pixels, 0),
				height = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Auto) };

			return cell;
		}


		public static Cell Line (float size)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			if (size < 1)
				cell.srcSize = new Size () { 
					x = new Dimension(Dimension.Type.Pixels, 0),
					y = new Dimension(Dimension.Type.Auto),
					width = new Dimension(Dimension.Type.Relative, 1),
					height = new Dimension(Dimension.Type.Relative, size) };
			else
				cell.srcSize = new Size () { 
					width = new Dimension(Dimension.Type.Relative, 1),
					x = new Dimension(Dimension.Type.Pixels, 0),
					height = new Dimension(Dimension.Type.Pixels, size),
					y = new Dimension(Dimension.Type.Auto) };

			return cell;
		}


		public static Cell LineStd ()
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			cell.srcSize = new Size () { 
				width = new Dimension(Dimension.Type.Relative, 1),
				x = new Dimension(Dimension.Type.Pixels, 0),
				height = new Dimension(Dimension.Type.Pixels, Size.lineHeight),
				y = new Dimension(Dimension.Type.Auto) };

			return cell;
		}


		public static Cell LineGroup ()
		{
			Cell cell = Line();
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell LineStdGroup ()
		{
			Cell cell = LineStd();
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell LineGroup (float size)
		{
			Cell cell = Line(size);
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell Row ()
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			cell.srcSize = new Size () { 
				width = new Dimension(Dimension.Type.Auto),
				x = new Dimension(Dimension.Type.Auto),
				height = new Dimension(Dimension.Type.Relative, 1),
				y = new Dimension(Dimension.Type.Pixels, 0) };

			return cell;
		}


		public static Cell Row (float size)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			if (size < 1)
				cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Relative, size),
				height = new Dimension(Dimension.Type.Relative, 1) };
			else
				cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Auto),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, size),
				height = new Dimension(Dimension.Type.Relative, 1) };

			return cell;
		}


		public static Cell RowGroup ()
		{
			Cell cell = Row();
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell RowGroup (float size)
		{
			Cell cell = Row(size);
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell Custom (float width, float height)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Pixels, 0),
				y = new Dimension(Dimension.Type.Pixels, 0),
				width = new Dimension(Dimension.Type.Pixels, width),
				height = new Dimension(Dimension.Type.Pixels, height) };
			
			return cell;
		}


		public static Cell Custom (float x, float y, float width, float height)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;

			cell.srcSize = new Size () { 
				x = new Dimension(Dimension.Type.Pixels, x),
				y = new Dimension(Dimension.Type.Pixels, y),
				width = new Dimension(Dimension.Type.Pixels, width),
				height = new Dimension(Dimension.Type.Pixels, height) };
			
			return cell;
		}


		public static Cell CustomGroup (float width, float height)
		{
			Cell cell = Custom(width, height);
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell CustomGroup (float x, float y, float width, float height)
		{
			Cell cell = Custom(x, y, width, height);
			if (cell == null) return null;
			cell.SetActive();
			return cell;
		}


		public static Cell Group (Size size)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;
			cell.SetActive();
			cell.srcSize = size;
			cell.name = "Group";

			return cell;
		}


		public static Cell FoldoutBackgroundGroup (Size size) 
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;
			cell.SetActive();
			cell.srcSize = size;
			cell.name = "FoldoutBackgroundGroup";

			GUIStyle backStyle = TexturesCache.GetElementStyle("DPUI/Backgrounds/Foldout");
			Element(backStyle, cell, usePadding:false);
			cell.margins = new Padding(5,5,5,5);

			return cell;
		}


		public static Cell Empty (Size size)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;
			cell.srcSize = size;
			return cell;
		}

		public static Cell Empty (Size size, bool disabled)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;
			cell.srcSize = size;
			cell.disabled = disabled;
			return cell;
		}

		public static Cell EmptyLine (int pixels)
		{
			Cell cell = Cell.GetCell();
			if (cell == null) return null;
			cell.srcSize = Size.LinePixels(pixels);
			return cell;
		}


		public static void Element (GUIStyle style, Cell cell, bool usePadding=false)
		{
			Rect cellRect = cell.GetRect(usePadding:usePadding);
			if (UI.IsWithinWindow(cellRect))
			{
				#if UNITY_EDITOR
				if (repaint && Event.current.type == EventType.Repaint) 
					style.Draw(cellRect, false, false, false ,false);
				#endif
			}
		}


		public static void Label (string label, Cell cell, Texture2D icon=null, GUIStyle style=null, string tooltip=null) 
		{ 
			//Profiler.BeginSample("Label");

			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			cell.name = "Label";

			if (style == null) style = UI.Settings.current.styles.label;

			if (repaint) 
			{
				Rect rect = cell.GetRect();
				rect.height += 2; //extending label a bit to include p,y and etc
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);
					
						if (icon == null)
							UnityEditor.EditorGUI.LabelField(rect, label, style:style);
						else
						{
							Vector2 center = new Vector2(rect.x + icon.width/2, rect.y + rect.size.y/2);
							Icon(icon,center);
						
							Rect labelRect = new Rect(rect.x+icon.width, rect.y, rect.width-icon.width, rect.height);
							using (Timer.Start("LabelField"))
								UnityEditor.EditorGUI.LabelField(labelRect, label, style:style);
						}
					
					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}
			}

			//Profiler.EndSample();
		}


		public static void DoubleLabel (
			string label,
			string field, 
			Texture2D icon = null,
			Cell cell = null,
			bool isLeft = false,
			float fieldWidth = 0.5f,
			string tooltip = null)
		/// Label with parameter displayed to look like a field
		{ 
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			cell.name = "DoubleLabel";

			cell.SetActive();
			using (cell)
			{
				//creating cells in needed order
				Cell labelCell = Cell.GetCell();
				Cell fieldCell = Cell.GetCell();
					
				//setting cell sizes
				if (fieldWidth >= 0) fieldCell.srcSize = Size.RowPercent(fieldWidth);
				else fieldCell.srcSize = Size.row; 

				labelCell.srcSize = Size.row;

				Label(label, icon:icon, cell:labelCell, tooltip:tooltip);
				Label(field, cell:fieldCell, tooltip:tooltip);
			}
		}


		public static void URL (string label, string url, Cell cell=null) 
		{ 
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			cell.name = "URL";

			if (repaint) 
			{
				GUIStyle style = UI.Settings.current.styles.url;

				Rect rect = cell.GetRect();
				rect.height += 2; //extending label a bit to include p,y and etc
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);
					
					if (UnityEngine.GUI.Button(rect, label, style)) Application.OpenURL(url); 
					UnityEditor.EditorGUIUtility.AddCursorRect (rect, UnityEditor.MouseCursor.Link);

					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}
			}
		}


		public static bool PopupSelector (ref int selectedIndex, string[] displayedOptions, 
			string label, 
			Cell cell=null,
			string tooltip = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "Popup";

			Rect rect = cell.GetRect();
			int newIndex = selectedIndex;
			if (UI.IsWithinWindow(rect))
			{
				#if UNITY_EDITOR
				newIndex = UnityEditor.EditorGUI.Popup(rect, selectedIndex, displayedOptions);
				#endif
			}

			if (newIndex != selectedIndex)
			{
				cell.WriteUndo();
				selectedIndex = newIndex;
				cell.SetChange();
				return true;
			}

			return false;
		}


		public static bool PopupSelector<T> (
			ref T selectedObject, 
			T[] objects, 
			Func<T,string> nameCallback, 
			string label, 
			Cell cell=null,
			string tooltip = null)
		{
			string[] names = new string[objects.Length];
			int selectedNum = -1;

			for (int i=0; i<objects.Length; i++)
			{
				names[i] = nameCallback(objects[i]);
				if (selectedObject.Equals(objects[i])) selectedNum = i;
			}

			if (PopupSelector(ref selectedNum, names, label, cell, tooltip))
				{ selectedObject = objects[selectedNum]; return true; }

			return false;
		}


		public static bool PopupSelectorWithNone<T> (
			ref T selectedObject, 
			T[] objects, 
			Func<T,string> nameCallback, 
			string label, 
			string nullName = "None",
			Cell cell=null,
			string tooltip = null)
		{
			string[] names = new string[objects.Length+1];
			names[0] = nullName;
			int selectedNum = 0;

			for (int i=0; i<objects.Length; i++)
			{
				names[i+1] = nameCallback(objects[i]);
				if (selectedObject != null && selectedObject.Equals(objects[i])) selectedNum = i+1;
			}

			if (PopupSelector(ref selectedNum, names, label, cell, tooltip))
			{
				if (selectedNum <= 0) selectedObject = default(T);
				else selectedObject = objects[selectedNum-1];
				return true;
			}

			return false;
		}


		public static bool Foldout (
			ref bool src, 
			string label,
			Cell cell = null, 
			int inspectorOffset = 10, //for some reason in inspector it is displayed 10 pixels left, but ok in window
			string tooltip = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "Foldout";

			if (UI.repaint)
			{
				GUIContent content = new GUIContent(label, tooltip);

				Rect rect = cell.GetRect();
				rect.x += inspectorOffset; rect.width -= inspectorOffset;
				bool dst = src;
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					dst = UnityEditor.EditorGUI.Foldout(rect, src, content, true, UI.Settings.current.styles.foldout);
					if (src != dst) UnityEditor.EditorGUI.FocusTextInControl("");
					#endif
				}

				if (src != dst)
				{
					//cell.SetChange();
					src = dst;
					return true;
				}
			}

			return false;
		}

		public static Cell FoldoutGroup (
			ref bool src, 
			string label,
			int inspectorOffset = 10, //for some reason in inspector it is displayed 10 pixels left, but ok in window
			int margins = 3,
			string tooltip = null) 
		{
				Cell cell = Cell.GetCell();
				if (cell == null) return null;
				cell.srcSize = Size.fixedLine;
				cell.name = "FoldoutGroup";
				cell.margins = new Padding(margins);
				if (src) cell.margins.bottom += 3; // new Padding(3,3,3,src? 6:3); //leaving 3 pixels if opened
				cell.padding = new Padding(0,0,0,3);

				Cell internalCell;

				cell.SetActive();
				using (cell)
				{
					GUIStyle backStyle = TexturesCache.GetElementStyle("DPUI/Backgrounds/Foldout");
					if (src) Element(backStyle, cell, usePadding:true);

					Cell foldoutCell = Cell.GetCell();
					if (foldoutCell == null) return null;
					foldoutCell.srcSize = Size.LinePixels(20);
					foldoutCell.padding = new Padding(0,0,0,2);
					UI.Foldout(ref src, label, cell:foldoutCell, inspectorOffset:inspectorOffset);

					//UI.Empty(Size.LinePixels(3));

					//cell.SetActive();

					internalCell = Cell.GetCell();
					if (internalCell==null) return null;
					internalCell.srcSize = Size.LinePixels(0);
					internalCell.margins = new Padding(10,0,0,0);
				}

				internalCell.SetActive();
				return internalCell;
		}


		public static bool LayerChevron (
			int num,
			ref int expanded,
			Cell cell = null) 
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "LayerChevron";

			int newexpanded = expanded;
			if (UI.IconToggle(expanded==num, 
				TexturesCache.GetTexture("DPUI/Chevrons/Down"),  
				TexturesCache.GetTexture("DPUI/Chevrons/Left"), 
				cell:cell, tooltip:"Expand Layer (Will close other expanded)"))
					newexpanded = num;
			else 
				{ if (expanded==num) newexpanded = -1; }

			if (expanded != newexpanded)
			{
				expanded = newexpanded;
				return true;
			}

			return false;
		}


		public static void Rect (Cell cell, Color color, bool isWire=false)
		/// Cell.rect is for debug purpose, so using this
		{
			Rect rect = cell.GetRect(usePadding:false);
			if (UI.IsWithinWindow(rect))
			{
				#if UNITY_EDITOR
				if (!isWire)
					UnityEditor.EditorGUI.DrawRect(rect, color);

				else
					UnityEditor.EditorGUI.DrawRect( new Rect(rect.x, rect.y, 1, rect.height-1), color);
					UnityEditor.EditorGUI.DrawRect( new Rect(rect.x, rect.y, rect.width-1, 1), color);
					UnityEditor.EditorGUI.DrawRect( new Rect(rect.x, rect.y+rect.height-1, rect.width-1, 1), color);
					UnityEditor.EditorGUI.DrawRect( new Rect(rect.x+rect.width-1, rect.y, 1, rect.height-1), color);

				#endif
			}

		}
	}
}