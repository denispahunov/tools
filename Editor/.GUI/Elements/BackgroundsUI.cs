﻿using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{
		public static void Grid (
			Color color,
			Color background = new Color(),
			Cell cell = null,
			int cellsNumX = 4,
			int cellsNumY = 4,
			bool fadeWithZoom = true)
		{
			//if cell is undefined using full window
			if (cell==null) cell = FullWindowCell();
			if (cell == null) return;
			cell.name = "Grid";

			if (!UI.repaint) return;

			Rect displayRect = cell.GetRect(usePadding:false);
			if (!UI.IsWithinWindow(displayRect)) return;

			Material mat = TexturesCache.GetMaterial("Hidden/DPLayout/GridOutdated");

			if (fadeWithZoom)
			{	
				float clampZoom = UI.Settings.current.scrollZoom.zoom;
				if (clampZoom > 1) clampZoom = 1;
				color = color*clampZoom  +  background*(1-clampZoom);
			}

			mat.SetColor("_Color", color);
			mat.SetColor("_Background", background);

			mat.SetInt("_CellsNumX", cellsNumX);
			mat.SetInt("_CellsNumY", cellsNumY); 

			mat.SetVector("_Rect", new Vector4(0,0,displayRect.size.x, displayRect.size.y));

			Texture2D tempTex = TexturesCache.GetTexture("DPUI/Backgrounds/Field"); //doesn't matter what texture it is
			UnityEditor.EditorGUI.DrawPreviewTexture(displayRect, tempTex, mat, ScaleMode.StretchToFill);
		}


		public static void LatticeBackground (
			float gridSize, 
			//Vector2 windowPos,
			//Vector2 windowSize,
			Color color,
			Color background = new Color(),
			Cell cell = null,
			bool fadeWithZoom = true)
		{
			//if cell is undefined using full window
			if (cell==null) cell = FullWindowCell();
			if (cell == null) return;
			cell.name = "LatticeBackground";

			if (!UI.repaint) return;

			ScrollZoom scrollZoom = UI.Settings.current.scrollZoom;
			//Rect displayRect = scrollZoom.ToDisplay(windowPos, windowSize);
			Rect displayRect = cell.GetRect(usePadding:false);
			if (!UI.IsWithinWindow(displayRect)) return;

			Material mat = TexturesCache.GetMaterial("Hidden/DPLayout/LatticeOutdated");

			if (fadeWithZoom)
			{	
				float clampZoom = UI.Settings.current.scrollZoom.zoom;
				if (clampZoom > 1) clampZoom = 1;
				color = color*clampZoom  +  background*(1-clampZoom);
			}

			mat.SetColor("_Color", color);
			mat.SetColor("_Background", background);

			mat.SetFloat("_CellSize", Mathf.RoundToInt(gridSize*scrollZoom.zoom));

			mat.SetFloat("_OffsetX", -scrollZoom.scroll.x - 1); //-1 makes the line pass through 0-1 pixel
			mat.SetFloat("_OffsetY", scrollZoom.scroll.y - displayRect.height);

			mat.SetVector("_Rect", new Vector4(0,0,displayRect.size.x,displayRect.size.y));

			Texture2D tempTex = TexturesCache.GetTexture("DPUI/Backgrounds/Field");
			UnityEditor.EditorGUI.DrawPreviewTexture(displayRect, tempTex, mat, ScaleMode.StretchToFill);
		}


		public static void BackgroundAxis (
			int pos,
			bool isVertical,
			Color color,
			Cell cell = null,
			bool fadeWithZoom = true)
		/// Infinite horizontal or vertical line
		/// Draws 1-pixel rect in base coordinates (or nothing if it is out of cell)
		/// Used to draw axis
		{
			//if cell is undefined using full window
			if (cell==null) cell = FullWindowCell();
			if (cell == null) return;
			cell.name = "LatticeBackground";

			if (!UI.repaint) return;

			Rect displayRect = cell.GetRect(usePadding:false);
			if (!UI.IsWithinWindow(displayRect)) return;

			ScrollZoom scrollZoom = UI.Settings.current.scrollZoom;
			
			Rect lineRect = isVertical ?
				new Rect (
					pos*scrollZoom.zoom + scrollZoom.scroll.x,
					displayRect.y,
					1,
					displayRect.height) :
				new Rect (
					displayRect.x,
					pos*scrollZoom.zoom + scrollZoom.scroll.y,
					displayRect.width,
					1);
			
			if (fadeWithZoom && scrollZoom.zoom < 1) color.a *= scrollZoom.zoom;

			UnityEditor.EditorGUI.DrawRect(lineRect, color);

		}


		public static Cell FullWindowCell()
		{
			Cell backgroundCell = UI.Empty(Cell.current.srcSize);
			if (backgroundCell == null) return null;

			backgroundCell.srcSize.x.pixels = -UI.Settings.current.scrollZoom.scroll.x;
			backgroundCell.srcSize.y.pixels = -UI.Settings.current.scrollZoom.scroll.y;
			backgroundCell.srcSize /= UI.Settings.current.scrollZoom.zoom;
			
			return backgroundCell;
		}
	}
}