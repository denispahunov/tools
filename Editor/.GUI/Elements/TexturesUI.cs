﻿using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{
		public static void Icon (
			Texture2D icon,
			Cell cell,
			Color color = new Color(),
			Vector2 offset = new Vector2())
		/// Draws an icon in the center of cell in native resolution (for zoom 1)
		{
			if (!UI.repaint) return;
			if (cell == null) return; //on used event

			Rect cellRect = cell.GetRect(usePadding:false, pixelPerfect:false);
			Vector2 center = new Vector2(cellRect.x + cellRect.width/2f + offset.x, cellRect.y + cellRect.height/2f + offset.y);

			float zoom = UI.Settings.current.scrollZoom==null? 1 : UI.Settings.current.scrollZoom.zoom;

			Rect iconRect = new Rect(
				Mathf.RoundToInt(center.x - icon.width/2f * zoom + 0.0001f), //adding epsilon to prevent position flickering on clear numbers
				Mathf.RoundToInt(center.y - icon.height/2f * zoom + 0.0001f),
				Mathf.RoundToInt(icon.width * zoom),
				Mathf.RoundToInt(icon.height * zoom));

			//Rect iconRect = new Rect(center.x - icon.width/2f, center.y - icon.height/2f, icon.width, icon.height);

			//iconRect = UI.Settings.current.scrollZoom.ToDisplay(iconRect.position, iconRect.size);

			if (UI.IsWithinWindow(iconRect))
			{
				#if UNITY_EDITOR
				if (color.a==0) UnityEngine.GUI.DrawTexture(iconRect, icon, ScaleMode.ScaleAndCrop);
				else UnityEngine.GUI.DrawTexture(iconRect, icon, ScaleMode.ScaleAndCrop, true, 0, color, 0,0);
				#endif
			}
		}


		public static void Icon (
			Texture2D icon,
			Vector2 center,
			Color color = new Color())
		/// Draws an icon in the given coord in native resolution (for zoom 1)
		{
			if (!UI.repaint) return;

			float zoom = UI.Settings.current.scrollZoom==null? 1 : UI.Settings.current.scrollZoom.zoom;

			Rect iconRect = new Rect(
				Mathf.RoundToInt(center.x - icon.width/2f * zoom  + 0.0001f),
				Mathf.RoundToInt(center.y - icon.height/2f * zoom  + 0.0001f),
				Mathf.RoundToInt(icon.width * zoom),
				Mathf.RoundToInt(icon.height * zoom));

			if (UI.IsWithinWindow(iconRect))
			{
				#if UNITY_EDITOR
				if (color.a==0) UnityEngine.GUI.DrawTexture(iconRect, icon, ScaleMode.ScaleAndCrop);
				else UnityEngine.GUI.DrawTexture(iconRect, icon, ScaleMode.ScaleAndCrop, true, 0, color, 0,0);
				#endif
			}
		}


		public static void IconLabel (
			Texture2D icon,
			string label,
			Cell cell = null,
			Color color = new Color(),
			Vector2 offset = new Vector2())
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			//cell.name = label;

			if (UI.repaint)
			{
				float zoom = UI.Settings.current.scrollZoom==null? 1 : UI.Settings.current.scrollZoom.zoom;

				Rect rect = cell.GetRect(usePadding:false, pixelPerfect:false);
				Vector2 center = new Vector2(rect.x + rect.width/2f + offset.x, rect.y + rect.height/2f + offset.y);

				float pureWidth = icon.width - icon.height; //magic formula to calculate icon width
				if (pureWidth < 0) pureWidth = 0;
				pureWidth *= zoom;
				center.x = rect.x + rect.height/2 + pureWidth/2;

				Rect iconRect = new Rect(
					center.x - icon.width/2f * zoom,
					center.y - icon.height/2f * zoom,
					icon.width * zoom,
					icon.height * zoom);

				Rect labelRect = new Rect(
					rect.x + iconRect.width,
					rect.y,
					rect.width - iconRect.width,
					rect.height);

				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					if (color.a==0) UnityEngine.GUI.DrawTexture(iconRect, icon, ScaleMode.ScaleAndCrop);
					else UnityEngine.GUI.DrawTexture(iconRect, icon, ScaleMode.ScaleAndCrop, true, 0, color, 0,0);

					if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);
					UnityEngine.GUI.Label(rect, label);
					if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();
					#endif
				}
			}
		}


		public static void Texture (
			Texture2D texture,
			Material mat = null,
			ScaleMode scaleMode = ScaleMode.ScaleToFit,
			float size=-1, 
			Cell cell = null)
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;

			if (texture == null) texture = TexturesCache.GetBlankTexture(new Color(0,0,0,1));

			if (repaint && texture != null) 
			{
				Rect cellRect = cell.GetRect();
				if (UI.IsWithinWindow(cellRect))
				{
					#if UNITY_EDITOR
					if (mat != null) 
					{
						if (scaleMode != ScaleMode.ScaleToFit) UnityEditor.EditorGUI.DrawPreviewTexture(cellRect, texture, mat, scaleMode); //leaves 1 pixel from top and bottom for some reason, so using special case for mat only
						else UnityEditor.EditorGUI.DrawPreviewTexture(cellRect, texture, mat);
					}
					else UnityEditor.EditorGUI.DrawPreviewTexture(cellRect, texture);
					#endif	
				}
			}
			
		}


		public static void TextureIcon (
			Texture2D texture,
			Cell cell = null,
			Color color = new Color(),
			int borderRadius = 3)
		/// Draws a rounded texture preview at the cell background (no offset)
		{
			if (cell == null) cell = Empty(Size.full);
			if (cell == null) return;

			if (color.a == 0) color = Color.white;
			if (texture == null) texture = TexturesCache.GetBlankTexture( new Color(0.5f, 0.5f, 0.5f, 1) );

			if (repaint && texture != null) 
			{
				Rect cellRect = cell.GetRect(pixelPerfect:true);
				if (UI.IsWithinWindow(cellRect))
				{
					#if UNITY_EDITOR
					Material mat = TexturesCache.GetMaterial("Hidden/DPLayout/TextureIcon");
					mat.SetFloat("_Borders", 1/cellRect.width);
					Shader.SetGlobalInt("_IsLinear", UnityEditor.PlayerSettings.colorSpace==ColorSpace.Linear ? 1 : 0);

					if (texture == null) 
						texture = TexturesCache.GetTexture("DPUI/Empty");
					UnityEditor.EditorGUI.DrawPreviewTexture(cellRect, texture, mat);
						//UnityEngine.GUI.DrawTexture(cellRect, texture, ScaleMode.ScaleToFit, alphaBlend:false, imageAspect:1, color:new Color(1,1,1,1), borderWidth:1, borderRadius:borderRadius);
					#endif

					/*#if UNITY_EDITOR
					UnityEditor.EditorGUI.DrawRect(cellRect, Color.black);
					cellRect = cellRect.Extended(-1);
					UnityEngine.GUI.DrawTexture(cellRect, texture);
					Debug.Log(cellRect);
					#endif*/
				}
			}
		}


		public static void TextureIcon (
			Texture2DArray textureArr,
			int index,
			Cell cell = null,
			Color color = new Color(),
			int borderRadius = 3)
		/// Draws a rounded texture preview at the cell background (no offset)
		{
			if (textureArr == null) 
				{ TextureIcon(null, cell, color, borderRadius); return; }

			if (cell == null) cell = Empty(Size.full);
			if (cell == null) return;

			if (color.a == 0) color = Color.white;

			if (repaint) 
			{
				Rect cellRect = cell.GetRect(pixelPerfect:true);
				if (UI.IsWithinWindow(cellRect))
				{
					#if UNITY_EDITOR
					Material mat = TexturesCache.GetMaterial("Hidden/DPLayout/TextureArrayIcon");
					mat.SetFloat("_Borders", 1/cellRect.width);
					mat.SetFloat("_Index", index);
					mat.SetTexture("_MainTexArr", textureArr);
					Shader.SetGlobalInt("_IsLinear", UnityEditor.PlayerSettings.colorSpace==ColorSpace.Linear ? 1 : 0);

					Texture2D emptyTex = TexturesCache.GetBlankTexture( new Color(0.5f, 0.5f, 0.5f, 1) );
					UnityEditor.EditorGUI.DrawPreviewTexture(cellRect, emptyTex, mat);
					#endif
				}
			}
		}

	}
}