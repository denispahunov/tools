﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Profiling;

namespace Plugins.GUI
{
	public static partial class UI
	{
		const float stepAsideDist = 10; //the distance other layers step aside when dragging layer

		public static IEnumerable<int> Layers (
			int count,
			Action<int> onAdd = null,
			Action<int> onRemove = null, 
			Action<int,int> onMove = null,
			Cell cell = null,
			bool invert = false,
			string texturesFolder = "DPUI/Layers/")
		{
			if (cell == null) cell = Empty(Size.line);
			if (cell == null) yield break;
			cell.name = "Layers";

			Cell dragCell = null; //preparing dragged cell to drow atop
			int dragNum = -1;
			int dragTo = -1; //to switch layers on drag release
			bool draggedOnRemoveCell = false;

			cell.SetActive();
			using (cell)
			{
				//background
				GUIStyle background = TexturesCache.GetElementStyle(texturesFolder+"AddPanelEmpty");
				UI.Element(background, cell);


				//layers
				Cell layersCell = UI.Group(Size.line);
				using (layersCell)
				{
					//layout - creates the default cell positions
					if (!UI.repaint)
					{
						for (int i=0; i<count; i++)
							using (UI.Group(Size.line))
								yield return i;
					}

					//repaint
					else
					{
						//get all cells
						for (int i=0; i<count; i++)
							//cells.Add(cell.GetChildCell());
							Cell.GetCell(); //performing necessary get applications
						List<Cell> cells = layersCell.children;

						//finding dragged cell (CheckDrag could be called before drawing layer)
						dragNum = -1;
						for (int i=0; i<count; i++)
							if (DragDrop.CheckDrag(cells[i]))
							{
								dragNum = i;
								dragCell = cells[i];
							}
						
						//finding where the cell is dragged
						dragTo = dragNum;
						if (dragCell != null)
						{
							float mousePosY = UI.Settings.current.scrollZoom.ToInternal(Event.current.mousePosition).y;
						
							if (mousePosY < cell.dstSize.y.pixels) dragTo = 0;
							else if (mousePosY > cell.dstSize.y.pixels+cell.dstSize.height.pixels) dragTo = count-1;
							else
							{
								int num = 0; //using counter to skip dragged field
								for (int i=0; i<count; i++)
								{
									if (i == dragNum) continue; //cell is dragged - pos always within this cell

									Cell layerCell = cells[i];

									double start = layerCell.dstSize.y.pixels;
									double mid = layerCell.dstSize.y.pixels + cells[i].dstSize.height.pixels/2;
									double end = layerCell.dstSize.y.pixels + cells[i].dstSize.height.pixels;

									//Cell tmp = UI.Empty(Size.Custom(-30, mousePosY-5, 10, 60));
									//tmp.DrawBackground();
									if (mousePosY >= start-stepAsideDist  &&  mousePosY < mid) { dragTo = num; break; }
									if (mousePosY >= mid  &&  mousePosY < end+stepAsideDist) { dragTo = num+1; break; }

									num++;
								}
							}
						}

						//offset all of the cells from dragTo to dragNum
						if (dragCell != null  &&  dragTo != dragNum)  //&& !draggedOnRemoveCell
						{
							Rect layersRect = layersCell.GetRect(usePadding:false);
							layersRect.height+=10; layersRect.y-=10;
							if (layersRect.Contains(Event.current.mousePosition))
							{
								if (dragNum < dragTo)
								for (int i=dragNum+1; i<dragTo+1; i++)
								{
							

									Cell layerCell = cells[i];
									layerCell.srcSize = Size.Custom(layerCell.dstSize); //this will fix it's calculated size in pixels
									layerCell.srcSize.y.pixels -= stepAsideDist;

									//re-applying layout
									UI.repaint=false; 
									layerCell.Layout(); 
									UI.repaint=true;
								}

								else
								for (int i=dragTo; i<dragNum; i++)
								{
									Cell layerCell = cells[i];
									layerCell.srcSize = Size.Custom(layerCell.dstSize);
									layerCell.srcSize.y.pixels += stepAsideDist;
							
									//re-applying layout
									UI.repaint=false; 
									layerCell.Layout(); 
									UI.repaint=true;
								}
							}
						}

						//drawing non-dragged cells
						for (int i=0; i<count; i++)
						{
							if (i==dragNum) continue;
							Cell layerCell = cells[i];

							layerCell.SetActive();
							using (layerCell)
							{
								//background
								GUIStyle style;
								if (i==0) style = TexturesCache.GetElementStyle(texturesFolder+"Top");
								else if (i==count-1) style = TexturesCache.GetElementStyle(texturesFolder+"Bot");
								else style = TexturesCache.GetElementStyle(texturesFolder+"Mid");

								layerCell.padding = new Padding(0,0,-1,0);
								UI.Element(style, Cell.current, usePadding:true);

								//drawing layer
								yield return !invert ? 
									i : 
									count-1-i;
							}
						}

						//starting dragging cell
						for (int i=0; i<count; i++)
						{
							if (i==dragNum) continue;
							Cell layerCell = cells[i];

							if (DragDrop.CheckStart(layerCell))
								yield break;
						}
					}
				}


				//add/remove
				using (UI.Group(Size.fixedLine))
				{
					Cell buttonCell = UI.Empty(Size.RowPixels(65)); //preparing button cell to avoid creaing/removing it on drag start/stop

					if (dragCell == null) //add when drag is disabled
					{
						GUIStyle style = count!=0 ?
							TexturesCache.GetElementStyle(texturesFolder+"AddPanel") :
							TexturesCache.GetElementStyle(texturesFolder+"AddPanelEmpty");
						UI.Element(style, Cell.current);

						UI.Icon(TexturesCache.GetTexture(texturesFolder+"AddIcon"), buttonCell);
						if (UI.Button(null, cell:buttonCell, style:GUIStyle.none))
						{
							cell.WriteUndo();
							onAdd(count);
							Cell.current.SetChange();

							Event.current.Use(); //gui structure changed. Not necessary since button is used
						}
					}

					else //remove when drag is enabled
					{
						Rect cellRect = Cell.current.GetRect(usePadding:false, pixelPerfect:false);
						draggedOnRemoveCell = cellRect.Contains(Event.current.mousePosition);
						
						GUIStyle style = draggedOnRemoveCell ?
							TexturesCache.GetElementStyle(texturesFolder+"RemovePanelActive") :
							TexturesCache.GetElementStyle(texturesFolder+"RemovePanel");
						UI.Element(style, Cell.current);
						UI.Icon(TexturesCache.GetTexture(texturesFolder+"RemoveIcon"), buttonCell);
					}
				}

				//drawing dragged cell
				if (dragCell != null)
				{
					dragCell.SetActive();
					dragCell.name = "DragCell";
					using (dragCell)
					{
						dragCell.srcSize = Size.Custom(Cell.current.dstSize);
						dragCell.srcSize.x.pixels += DragDrop.totalDelta.x / UI.Settings.current.scrollZoom.zoom;
						dragCell.srcSize.y.pixels += DragDrop.totalDelta.y / UI.Settings.current.scrollZoom.zoom;

						//re-applying layout
						UI.repaint=false; 
						dragCell.Layout(); 
						UI.repaint=true;

						//background
						GUIStyle bkStyle = TexturesCache.GetElementStyle(texturesFolder+"Mid");
						UI.Element(bkStyle, dragCell);

						//drawing layer
						yield return !invert ? 
							dragNum : 
							count-1-dragNum; 
					}
				}

				//releasing drag
				if (dragCell!=null && DragDrop.CheckRelease(dragCell))
				{
						if (draggedOnRemoveCell)
						{
							cell.WriteUndo();
							onRemove(!invert ? 
								dragNum : 
								count-1-dragNum);
							Cell.current.SetChange();

							Event.current.Use(); //to remove extra child in flush

							UI.repaint=false; 
							cell.Layout(); 
							UI.repaint=true;
						}

						else if (onMove != null && dragNum!=dragTo)  //switch is the last - it conflicts with remove
						{
							cell.WriteUndo();
							onMove(//dragNum, dragTo);
								!invert ? dragNum : count-1-dragNum, 
								!invert ? dragTo : count-1-dragTo);
							Cell.current.SetChange();
						}
				}
			}
		}
	}
}
