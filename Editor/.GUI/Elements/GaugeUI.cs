﻿using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace Plugins.GUI
{
	public static partial class UI
	{
			//HACK: disabling updating scene view on material change (otherwise it will repaint scene view after each GUI frame causing lag)
//			#if UNITY_EDITOR
//			List<UnityEditor.SearchableEditorWindow> searchableWindows = 
//				(List<UnityEditor.SearchableEditorWindow>)typeof(UnityEditor.SearchableEditorWindow).GetField("searchableWindows", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null);
//			searchableWindows.Remove(UnityEditor.SceneView.lastActiveSceneView);
//			searchableWindows.Add(UnityEditor.SceneView.lastActiveSceneView);
//			#endif


		public static void ProgressBar (float val, Color color, Cell cell=null,
			string backgroundTexName = "DPUI/ProgressBar/Background",
			string filledTexName = "DPUI/ProgressBar/Fill",
			string fullTexName = "DPUI/ProgressBar/FillFull")
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			cell.name = "ProgressBar";

			ProgressBarBackground(cell, backgroundTexName);
			ProgressBarGauge(val, color, cell, filledTexName, fullTexName);
			ProgressBarLabel(val, cell);
		}


		public static void ProgressBar (float primeVal, float secVal, Color primeColor, Color secColor, Cell cell=null,
			string backgroundTexName = "DPUI/ProgressBar/Background",
			string filledTexName = "DPUI/ProgressBar/Fill",
			string fullTexName = "DPUI/ProgressBar/FillFull")
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			cell.name = "ProgressBar";

			ProgressBarBackground(cell, backgroundTexName);
			ProgressBarGauge(secVal, secColor, cell, filledTexName, fullTexName);
			ProgressBarGauge(primeVal, primeColor, cell, filledTexName, fullTexName);
			ProgressBarLabel(primeVal, cell);
		}


		public static void ProgressBarBackground (Cell cell, string texName="DPUI/ProgressBar/Background")
		{
			if (!UI.repaint) return;

			Texture2D backgroundTex = TexturesCache.GetTexture(texName);
			GUIStyle backgroundStyle = TexturesCache.GetElementStyle(backgroundTex, borders:new RectOffset(2,2,2,2));

			Rect rect = cell.GetRect();
			if (UI.IsWithinWindow(rect))
			{
				#if UNITY_EDITOR
				if (Event.current.type == EventType.Repaint)
					backgroundStyle.Draw(rect, false, false, false, false);
				#endif
			}
		}


		public static void ProgressBarGauge (float val, Color color, Cell cell,
			string filledTexName = "DPUI/ProgressBar/Fill",
			string fullTexName = "DPUI/ProgressBar/FillFull")
		{
			if (!UI.repaint) return;

			Rect cellRect = cell.GetRect();
			if (!UI.IsWithinWindow(cellRect)) return;

			Rect gaugeRect = cellRect;
			gaugeRect.width = gaugeRect.width * val;
			gaugeRect.width = Mathf.RoundToInt(gaugeRect.width);
			if (gaugeRect.width < 2) return;

			Texture2D tex;
			if (gaugeRect.width != cellRect.width)
				tex = TexturesCache.GetColorizedTexture(filledTexName, color); 
			else 
				tex = TexturesCache.GetColorizedTexture(fullTexName, color);
				
			GUIStyle element = TexturesCache.GetElementStyle(tex, borders:new RectOffset(2,2,2,2)); 
			#if UNITY_EDITOR
			if (Event.current.type == EventType.Repaint)
				element.Draw(gaugeRect, false, false, false, false);
			#endif
		}


		public static void ProgressBarLabel (float val, Cell cell)
		{
			GUIStyle rightLabelStyle = UI.Settings.current.styles["RightLabel"];
			if (rightLabelStyle == null)
			{
				rightLabelStyle = new GUIStyle(UI.Settings.current.styles.label);
				rightLabelStyle.fontSize = (int)(StylesCache.defaultFontSize * 0.9f);

				rightLabelStyle.alignment = TextAnchor.LowerRight;
				UI.Settings.current.styles["RightLabel"] = rightLabelStyle;
			}

			if (UI.repaint) 
			{
				Rect rect = cell.GetRect(usePadding:false);
				rect.width-=2; rect.height-=2; rect.x+=1; rect.y+=1; //apply custom padding
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					UnityEditor.EditorGUI.LabelField(rect, ((int)(val*100)).ToString()+"%", style:rightLabelStyle);
					#endif
				}
			}
		}


		public static void Gauge (
			float val, 
			float secondary=0,
			Cell cell = null,
			Color color = new Color())
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return;
			cell.name = "Gauge";

			Texture2D backgroundTex = TexturesCache.GetTexture("DPUI/ProgressBar/Background");
			GUIStyle backgroundStyle = TexturesCache.GetElementStyle("DPUI/Toolbar/GaugeBackground", borders:new RectOffset(2,2,2,2));

			Texture2D mainTex = TexturesCache.GetTexture("DPUI/Toolbar/Gauge");

			
			GUIStyle mainStyle = TexturesCache.GetElementStyle("DPUI/Toolbar/Gauge", borders:new RectOffset(1,1,1,1));

			GUIStyle rightLabelStyle = UI.Settings.current.styles["RightLabel"];
			if (rightLabelStyle == null)
			{
				rightLabelStyle = new GUIStyle(UI.Settings.current.styles.label);
				rightLabelStyle.fontSize = StylesCache.defaultFontSize;

				rightLabelStyle.alignment = TextAnchor.LowerRight;
				UI.Settings.current.styles["RightLabel"] = rightLabelStyle;
			}

			if (UI.repaint) 
			{
				UI.Element(backgroundStyle, cell:cell);

				Rect rect = cell.GetRect();
				if (UI.IsWithinWindow(rect))
				{
					#if UNITY_EDITOR
					Rect secRect = rect;
					secRect.width = secRect.width*secondary;
					if (secRect.width >= 2)
					{
						mainStyle.Draw(secRect, false, false, false, false);
						//UnityEditor.EditorGUI.DrawPreviewTexture(secRect, TexturesCache.GetTexture("DPUI/White"), secMat);
					}

					Rect gaugeRect = rect;
					gaugeRect.width = gaugeRect.width*val;
					if (gaugeRect.width >= 2)
					{
						mainStyle.Draw(gaugeRect, false, false, false, false);
						//UnityEditor.EditorGUI.DrawPreviewTexture(gaugeRect, TexturesCache.GetTexture("DPUI/White"), mainMat);
					}


					UnityEditor.EditorGUI.LabelField(rect, ((int)(val*100)).ToString()+"%", style:rightLabelStyle);
					#endif
				}
			}	
		}
	}
}