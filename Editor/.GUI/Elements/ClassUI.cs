﻿using System;
using System.Reflection;
using System.Collections.Generic;


namespace Plugins.GUI
{

	public static partial class UI
	{
		public static bool ClassFields (
			object obj, 
			string category=null,
			Cell cell = null) 
		{ using (Timer.Start("Class Fields " + obj.GetType().ToString()))
		{
			Type objType;
			using (Timer.Start("GetType"))
				objType = obj.GetType();

			Val[] attributes;
			using (Timer.Start("Get Attributes"))
				attributes = Val.GetAttributes(objType);
			if (attributes==null || attributes.Length == 0) 
				return false;
			

			using (Timer.Start("Get Cell"))
				if (cell == null) cell = Empty(Size.line);
			if (cell == null) 
				return false; 

			//cell.name = objType + "ClassFields";
			
			cell.SetActive();
			using (cell)
			{
				for (int a=0; a<attributes.Length; a++)
				{
					if (attributes[a].field == null) continue; //could be a method
					if (!attributes[a].display) continue;
					if (attributes[a].cat != category) continue; //null category is a category too. Not drawing all when category is null.

					object val = null;
					using (Timer.Start("Get Value"))
						if (UI.repaint) val = attributes[a].field.GetValue(obj);

					Cell fieldCell ;
					using (Timer.Start("Field Cell"))
						fieldCell = Empty(Size.fixedLine);
					if (fieldCell==null) 
						return false; //event.used

					using (Timer.Start("Store Object"))
						CellObject.SetObject(fieldCell, attributes[a].field);

					bool change;
					using (Timer.Start("Field"))
						change = UI.Field(ref val, attributes[a].name, cell:fieldCell, isLeft:attributes[a].isLeft, type:attributes[a].field.FieldType);

					using (Timer.Start("Set Value"))
						if (change) attributes[a].field.SetValue(obj, val); 
				}
			}

			return cell.valChanged;
		}}


		public static void ClassMethods (
			object obj, 
			string category=null,
			Cell cell = null)
		{
			Type objType = obj.GetType();

			Val[] attributes = Val.GetAttributes(objType);

			if (attributes==null || attributes.Length == 0) return;

			if (cell == null) cell = Empty(Size.line);
			if (cell == null) return;
			cell.name = "Class Methods";

			cell.SetActive();
			using (cell)
			{
				for (int a=0; a<attributes.Length; a++)
				{
					if (attributes[a].method == null) continue; //could be a field
					if (!attributes[a].display) continue;
					if (attributes[a].cat != category) continue; //null category is a category too. Not drawing all when category is null.
				
					attributes[a].method.Invoke(obj, null);
				}
			}
		}


		public static void ClassMethods<T> (
			object obj, 
			T argument,
			string category=null,
			Cell cell = null) //should be vertical
		{
			Type objType = obj.GetType();

			Val[] attributes = Val.GetAttributes(objType);
			if (attributes==null || attributes.Length == 0) return;

			if (cell == null) cell = Empty(Size.line);
			if (cell == null) return;
			cell.name = "Class Methods";

			cell.SetActive();
			using (cell)
			{
				for (int a=0; a<attributes.Length; a++)
				{
					if (attributes[a].method == null) continue; //could be a field
					if (!attributes[a].display) continue;
					if (attributes[a].cat != category) continue; //null category is a category too. Not drawing all when category is null.
				
					attributes[a].method.Invoke(obj, new object[]{argument});
				}
			}
		}


		public static void ClassMethods<T1,T2> (
			object obj, 
			T1 argument1,
			T2 argument2,
			string category=null,
			Cell cell = null) //should be vertical 
		{
			Type objType = obj.GetType();

			Val[] attributes = Val.GetAttributes(objType);
			if (attributes==null || attributes.Length == 0) return;

			if (cell == null) cell = Empty(Size.line);
			if (cell == null) return;
			cell.name = "Class Methods";

			cell.SetActive();
			using (cell)
			{
				for (int a=0; a<attributes.Length; a++)
				{
					if (attributes[a].method == null) continue; //could be a field
					if (!attributes[a].display) continue;
					if (attributes[a].cat != category) continue; //null category is a category too. Not drawing all when category is null.

					attributes[a].method.Invoke(obj, new object[]{argument1, argument2});
				}
			}
		}
	}
}
