﻿using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{
		public static Cell currentEditableLabel; //to know what label we are currently editing

		public static string EditableLabel (
			string label, 
			Cell cell=null,
			GUIStyle style=null,
			bool isLeft = false)
		{
			EditableLabel(ref label, cell:cell, style:style, isLeft:isLeft);
			return label;
		}

		public static bool EditableLabel (
			ref string label, 
			Cell cell=null,
			GUIStyle style=null,
			bool isLeft = false)
		/// Note vertical alignment is Middle for this control
		{
			if (cell == null) cell = Empty(Size.fixedLine);
			if (cell == null) return false;
			cell.name = "EditableLabel";

			//cell.SetActive();
			//using (cell)
			{
				//creating cells in needed order
				Cell labelCell; Cell buttonCell;
				if (!isLeft)
				{
					labelCell = UI.Empty(Size.row);
					buttonCell = UI.Empty(Size.RowPixels(20));
					
				}
				else 
				{
					buttonCell = UI.Empty(Size.RowPixels(20));
					labelCell = UI.Empty(Size.row);
				}

				if (labelCell == null) return false;
				if (buttonCell == null) return false;


				#if UNITY_EDITOR
				if (repaint) 
				{
					//drawing button
					Rect buttonRect = buttonCell.GetRect(usePadding:false);
					UnityEditor.EditorGUIUtility.AddCursorRect (buttonRect, UnityEditor.MouseCursor.Link);

					Icon(TexturesCache.GetTexture("DPUI/Icons/Pencil"), cell:buttonCell);
					if (UI.Button(null, cell:buttonCell, style:GUIStyle.none)) 
						currentEditableLabel = cell;

					//aligning label rect to the middle
					Rect labelRect = labelCell.GetRect();
					float center = labelRect.y + labelRect.height/2f;
					float zoom = UI.Settings.current.scrollZoom==null? 1 : UI.Settings.current.scrollZoom.zoom;
					labelRect = new Rect(
						labelRect.x, //adding epsilon to prevent position flickering on clear numbers
						Mathf.RoundToInt(center - 9 * zoom + 0.0001f),
						labelRect.width,
						Mathf.RoundToInt(19 * zoom));

					//drawing label
					if (UI.IsWithinWindow(labelRect))
					{
						#if UNITY_EDITOR

						if (cell.disabled) UnityEditor.EditorGUI.BeginDisabledGroup(true);

						//non-editable
						if (currentEditableLabel != cell)
							UnityEditor.EditorGUI.LabelField(labelRect, label, style:UI.Settings.current.styles.label);

						//editable
						else
						{
							UnityEngine.GUI.SetNextControlName("LayerFoldoutNextFocus"); //to focus in text right after pressing edit button
							label = UnityEditor.EditorGUI.TextField(labelRect, label, style:UI.Settings.current.styles.label);
							UnityEditor.EditorGUI.FocusTextInControl("LayerFoldoutNextFocus"); 
							if (UnityEditor.EditorWindow.focusedWindow != null) UnityEditor.EditorWindow.focusedWindow.Repaint();

							//exit editing
							if (Event.current.keyCode==KeyCode.KeypadEnter || Event.current.keyCode==KeyCode.Return || Event.current.keyCode==KeyCode.Escape || //if enter or esc
								(Event.current.type==EventType.MouseDown && !labelRect.Contains(Event.current.mousePosition))) //if clicked somewhere else
									currentEditableLabel = null;
						}
						if (cell.disabled) UnityEditor.EditorGUI.EndDisabledGroup();

						#endif
					}
				}
				#endif
			}

			return false;
		}
	}
}