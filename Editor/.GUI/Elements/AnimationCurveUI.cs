﻿using System;
using UnityEngine;

namespace Plugins.GUI
{
	public static partial class UI
	{	
		private static AnimationCurve windowCurveRef = null;

		private static Type curveWindowType;
		private static Type GetCurveWindowType ()
		{
			#if UNITY_EDITOR
			if (curveWindowType == null) curveWindowType = typeof(UnityEditor.EditorWindow).Assembly.GetType("UnityEditor.CurveEditorWindow");
			return curveWindowType;
			#else
			return null;
			#endif
		}

		public static void AnimationCurve (
			AnimationCurve src, 
			Cell cell=null, 
			Rect ranges=new Rect(), 
			Color color = new Color())
		{
			if (cell == null) cell = Empty(Size.LinePixels(100));
			if (cell == null) return;
			cell.name = "AnimCurve";

			Rect displayRect = cell.GetRect();
			if (!UI.IsWithinWindow(displayRect)) return;

			if (ranges.width < Mathf.Epsilon && ranges.height < Mathf.Epsilon) { ranges.width = 1; ranges.height = 1; }
			if (color.a == 0) color = Color.white;

			#if UNITY_EDITOR

			//recording undo on change if the curve editor window is opened (and this current curve is selected)
			try
			{
				Type curveWindowType = GetCurveWindowType();
				if (UnityEditor.EditorWindow.focusedWindow != null && UnityEditor.EditorWindow.focusedWindow.GetType() == curveWindowType)
				{
					AnimationCurve windowCurve = curveWindowType.GetProperty("curve").GetValue(UnityEditor.EditorWindow.focusedWindow, null) as AnimationCurve;
					if (windowCurve == src)
					{
						if (windowCurveRef == null) windowCurveRef = windowCurve.Copy();
						if (!windowCurve.IdenticalTo(windowCurveRef))
						{
								
							Keyframe[] tempKeys = windowCurve.keys;
							windowCurve.keys = windowCurveRef.keys;
							cell.WriteUndo();
							cell.SetChange();
								
							windowCurve.keys = tempKeys;

							windowCurveRef = windowCurve.Copy();
						}
					}
				}
				else windowCurveRef = null;
			}
			catch {};

			UnityEditor.EditorGUI.CurveField(displayRect, src, color, ranges); 

			#endif
		}
		
	}
}