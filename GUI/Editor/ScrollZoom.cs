﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEditor;
using UnityEngine;

namespace Den.Tools.GUI
{
	[System.Serializable]
	public class ScrollZoom
	{
		public Vector2 zoom = new Vector2(1,1);
		public int zoomStage = 0; //number of scroll wheel ticks from zoom 1. Negative if zoom out
		public int minZoomStage = -8;
		public int maxZoomStage = 8;
		public bool allowZoomX = false; //read externally before zooming
		public bool allowZoomY = false;
		
		public int scrollWheelStep = 18;

		public Vector2 scroll = new Vector2(0, 0);
		public bool isScrolling = false;
		private	Vector2 clickPos = new Vector2(0,0);
		private Vector2 clickScroll = new Vector2(0,0);
		public int scrollButton = 2;
		public bool roundScroll = true;
		public bool allowScroll = false;

		public float SoftZoom //lerped half-zoom to see small details when zoomed out. Doubles zoom when it lowers
		{get{
			if (zoomStage < 0)
				return 1*zoom.x + 2*(1-zoom.x);
			else
				return zoom.x;
		}}

		public void Zoom () => Zoom(Event.current.mousePosition);

		public void Zoom (Vector2 pivot) 
		{ 
			Vector2 newZoom = zoom;
			bool zoomChanged = false;
			if (allowZoomX && allowZoomY) 
			{
				zoomChanged = MouseZoom(ref newZoom.x);
				newZoom.y = newZoom.x;
			}
			else if (allowZoomX) 
				zoomChanged = MouseZoom(ref newZoom.x);
			else if (allowZoomY) 
				zoomChanged = MouseZoom(ref newZoom.y);

			if (zoomChanged)
			{
				ZoomTo(newZoom, pivot); 
				zoom = newZoom;
			}
		}

		public bool MouseZoom (ref float zoom)
		/// Calculates to zoom with mouse wheel
		{
			if (Event.current == null) return false;

			//reading control
			#if UNITY_EDITOR_OSX
			bool control = Event.current.command;
			#else
			bool control = Event.current.control;
			#endif

			float delta = 0;
			if (Event.current.type == EventType.ScrollWheel) delta = Event.current.delta.y / 3f;
			//else if (Event.current.type == EventType.MouseDrag && Event.current.button == 0 && control) delta = Event.current.delta.y / 15f;
			//else if (control && Event.current.alt && Event.current.type==EventType.KeyDown && Event.current.keyCode==KeyCode.Equals) delta --;
			//else if (control && Event.current.alt && Event.current.type==EventType.KeyDown && Event.current.keyCode==KeyCode.Minus) delta ++;
			if (Mathf.Abs(delta) < 0.001f) return false;

			//new zoom	
			if (delta > 0) zoomStage--;
			if (delta < 0) zoomStage++;

			if (zoomStage < minZoomStage)
				zoomStage = minZoomStage;
			if (zoomStage > maxZoomStage)
				zoomStage = maxZoomStage;

			zoom = ZoomStageToZoom(zoomStage);
			return true;
		}

		public float ZoomStageToZoom (int zoomStage)
		{
			float newZoom = 1;
			if (zoomStage < 0)
				newZoom = Mathf.Pow(0.84089642f, -zoomStage); //magical numbers to repeat 0.25 more often
			else if (zoomStage > 0)
				newZoom = Mathf.Pow(1.1892072f, zoomStage);

			return newZoom;
		}

		public void ZoomTo (Vector2 zoomVal, Vector2 pivot)
		/// Zooms to zoomVal
		/// Pivot is usually mouse position
		{
			//record mouse position in worldspace
			Vector2 worldMousePos = (pivot - scroll) / zoom;

			//changing zoom
			Vector2 zoomChange = zoomVal - zoom;
			zoom = zoomVal;

			//scrolling around pivot
			scroll -= worldMousePos * zoomChange;
			
			#if UNITY_EDITOR
			//UnityEditor.EditorWindow.focusedWindow?.Repaint();
			UI.current?.editorWindow?.Repaint(); 
			#endif

			//if (roundScroll) 
			//	RoundScroll();
		}


		public void ScrollWheel(int step = 3)
		{
			float delta = 0;
			if (Event.current.type == EventType.ScrollWheel) delta = Event.current.delta.y / 3f;
			scroll.y -= delta * scrollWheelStep * step;
		}


		public void Scroll()
		{
			if (!allowScroll) return;

			Vector2 mousePosition = Event.current.mousePosition; //UI.current?.HardwareMousePos ?? Event.current.mousePosition;

			if (Event.current.type == EventType.MouseDown  &&  Event.current.button == scrollButton)
			{
				clickPos = mousePosition;
				clickScroll = scroll;
				isScrolling = true;
			}

			if (Event.current.type == EventType.MouseDown  &&  Event.current.button == 0  &&  Event.current.alt)  //alternative way to scroll
			{
				clickPos = mousePosition;
				clickScroll = scroll;
				isScrolling = true;
			}

			if (UI.MouseUp) //(Event.current.rawType == EventType.MouseUp  ||  Event.current.rawType == EventType.MouseLeaveWindow) 
			{
				isScrolling = false;
			}
			
			if (isScrolling)
				Scroll(clickScroll + mousePosition - clickPos);
		}


		public void Scroll (Vector2 newScroll)
		{
			scroll = newScroll; 

			//if (roundScroll) 
			//	RoundScroll();

			UI.RemoveFocusOnControl(); //disabling text typing
			UI.current?.editorWindow?.Repaint();
		}


		public void RoundScroll ()
		/// Rounding scroll to virtual pixels
		/// If dpi factor is 2 then rounding to 0.5
		/// Actually doesn't make sense since cells are rounded to pixels with any scroll and zoom
		{
			float dpiFactor = UI.current.dpiScaleFactor;
			float zoom = UI.current.scrollZoom != null ? UI.current.scrollZoom.zoom.x : 1;
			float round = dpiFactor;

			if (scroll.x < 0) scroll.x--;   scroll.x = (int)(float)(scroll.x*round) / round;
			if (scroll.y < 0) scroll.y--;	scroll.y = (int)(float)(scroll.y*round) / round;
		}


		public Vector2 GetWindowCenter (Vector2 windowSize)
		/// returns the center of the screen(window) in base coordinates
		{
			//return (windowSize/2 - scroll) / zoom;
			return  (windowSize/2) - scroll/zoom;
		}

		public void FocusWindowOn (Vector2 center, Vector2 windowSize)
		{
			Scroll( -center*zoom + windowSize/2 );
			//Scroll( (windowSize/2 - center) * zoom );
		}


		#region ToScreen/ToInternal

			public Vector2 ToScreen(Vector2 pos) => new Vector2(
				pos.x*zoom.x + scroll.x, 
				pos.y*zoom.y + scroll.y);

			public float ToScreenX (float size) => size * zoom.x;
			public float ToScreenY (float size) => size * zoom.y;

			public Rect ToScreen (Vector2 pos, Vector2 size, bool pixelPerfect=true) => ToScreen(pos.x, pos.y, size.x, size.y, pixelPerfect);
			public Rect ToScreen (Rect rect, bool pixelPerfect=true) => ToScreen(rect.x, rect.y, rect.width, rect.height, pixelPerfect);
			public Rect ToScreen (double minX, double minY, double sizeX, double sizeY,  bool pixelPerfect=false, bool sizePerfect=false)
			{
				//float dpiFactor = UI.current.DpiScaleFactor;
				//dpi scale is applied by Unity after all done
				//draw and EditorGUI.LabelField uses non-scaled fields (but then Unity scales them by itself)
				//so it's not real "to screen" but to screen before apply dpi scale

				minX = minX*zoom.x + scroll.x;
				minY = minY*zoom.y + scroll.y;
				sizeX *= zoom.x;
				sizeY *= zoom.y;

				return new Rect((float)minX, (float)minY, (float)sizeX, (float)sizeY);
			}


			public Rect ToInternal(Rect rect)
			{
				Vector2 offset = new Vector2(
					(rect.x - scroll.x) / zoom.x, 
					(rect.y - scroll.y) / zoom.y );
				Vector2 size = new Vector2(
					rect.width / zoom.x, 
					rect.height / zoom.y);
				return new Rect(offset, size);
			}

			public Vector2 ToInternal(Vector2 pos)
			{
				return new Vector2 (
					(pos.x - scroll.x) / zoom.x,
					(pos.y - scroll.y) / zoom.y );
			}


			public Vector2 RoundToZoom (Vector2 vec)
			/// Queer thing
			{
				vec.x /= zoom.x;
				vec.x = Mathf.Round(vec.x);
				vec.x *= zoom.x;

				vec.y /= zoom.y;
				vec.y = Mathf.Round(vec.y);
				vec.y *= zoom.y;

				return vec;
			}

			public Vector2 RoundToPixels (Vector2 pos)
			/// Transforms pos to screen, rounds it to pixels, and transforms back to internal
			{
				float dpiFactor = UI.current.dpiScaleFactor;
				//despite ToScreen it uses scale factor since we got to round to scaled pixels

				Vector2 screen = (pos*zoom + scroll)*dpiFactor;

				if (screen.x < 0) screen.x--;	screen.x = (int)(screen.x + 0.4999f);  //-epsilon to ensure exact .5 cases will be always rounded
				if (screen.y < 0) screen.y--;	screen.y = (int)(screen.y + 0.4999f);

				pos = (screen/dpiFactor - scroll) / zoom;

				return pos;
			}


		#endregion








		/*public Rect ToDisplay(Rect rect)
		{
			return new Rect(rect.x * zoom + scroll.x, rect.y * zoom + scroll.y, rect.width * zoom, rect.height * zoom);
		}
		
		public Rect ToDisplay(float offsetX, float offsetY, float sizeX, float sizeY)
		{
			return new Rect(offsetX * zoom + scroll.x, offsetY * zoom + scroll.y, sizeX * zoom, sizeY * zoom);
		}

		public Rect ToDisplay(Float2 pos, Float2 size)
		{
			if (paddingBox==null) return new Rect(
				(int)( pos.x * zoom + scroll.x  + 0.5f), 
				(int)( pos.y * zoom + scroll.y  + 0.5f), 
				(int)( size.x * zoom  + 0.5f), 
				(int)( size.y * zoom  + 0.5f) );

			else 
			{
				Padding padding = (Padding)paddingBox;
				return new Rect(
				(int)( (pos.x + padding.left) * zoom + scroll.x  + 0.5f), 
				(int)( (pos.y + padding.top) * zoom + scroll.y   + 0.5f), 
				(int)( (size.x - (padding.left+padding.right)) * zoom  + 0.5f), 
				(int)( (size.y - (padding.top+padding.bottom)) * zoom  + 0.5f));
			}
		}

		public Rect ToInternal(Rect rect)
		{
			return new Rect((rect.x - scroll.x) / zoom, (rect.y - scroll.y) / zoom, rect.width / zoom, rect.height / zoom);
		}

		public Vector2 ToInternal(Vector2 pos)
		{
			return (pos - scroll) / zoom;
		} //return new Vector2( (pos.x-scroll.x)/zoom, (pos.y-scroll.y)/zoom); }


		public void Focus(Cell cell, Vector2 pos)
		{
			throw new System.NotImplementedException();
		}*/
	}
}
