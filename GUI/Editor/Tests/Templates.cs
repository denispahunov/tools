using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEditor;
using Den.Tools.Tasks;

//These are examples on how to use Den.Tools.GUI
//Copy to your code

namespace Den.Tools.GUI.Templates
{
	//Minimalistic Inspector
	//[CustomEditor(typeof(MyType))]
	public class MinimalisticInspector : Editor
	{
		UI ui = new UI();

		public override void OnInspectorGUI ()
		{
			ui.Draw(DrawGUI, inInspector:true);
		}

		public void DrawGUI ()
		{
			using (Cell.LinePx(100)) Draw.Label("Test");
		}
	}


	//Ususal Inspector
	//[CustomEditor(typeof(MyType))]
	public class InspectorWithUndo : Editor
	{
		UI ui = new UI();

		public override void  OnInspectorGUI ()
		{
			//undo
			if (ui.undo == null) ui.undo = new Den.Tools.GUI.Undo();
				//ui.undo.undoAction = GraphWindow.current.RefreshMapMagic;
			ui.undo.undoObject = target;
			ui.undo.undoName = "MyGUI Value Change";

			//drawing
			ui.Draw(DrawGUI, inInspector:true);
		}

		public void DrawGUI ()
		{
			using (Cell.LinePx(100)) 
				Draw.Label("Test");
		}
	}


	//Foldout
	//[CustomEditor(typeof(MyType))]
	public class FoldoutInspector : Editor
	{
		UI ui = new UI();
		bool guiFoldoutExpanded;

		public override void OnInspectorGUI ()
		{
			ui.Draw(DrawGUI, inInspector:true);
		}

		public void DrawGUI ()
		{
			Cell.EmptyLinePx(4);
			using (Cell.LineStd)
				using (new Draw.FoldoutGroup(ref guiFoldoutExpanded, "Tile Settings", isLeft:true))
					if (guiFoldoutExpanded)
					{

					}
		}
	}




	//In a window with scroll group (it's not ScrollZoom!)
	public class GUITesterWindow2 : EditorWindow
	{
		UI ui = new UI();

		float fVal;

		private void OnGUI () 
		{
			using (Timer.Start("Draw GUI " + Event.current.type))
				ui.Draw(DrawScroll, inInspector:false);
		}

		public void DrawScroll ()
		{
			using (Cell.LinePx(0)) //for some reason it all usually goes in a cell
			{
				using (Cell.RowPx(30)) Draw.Label("Row");

				using (Cell.Row)
				{
					using (Cell.LineStd) Draw.Button("Button");

					using (Cell.LinePx(150))
						using (new Draw.ScrollGroup(ref fVal))
						{
							for (int i=0; i<20; i++)
							{
								using (Cell.LineStd) 
								{
									using (Cell.RowPx(35)) Draw.Button("Button " + i);
									using (Cell.Row) Draw.Field(ref fVal, "Value");
								}
							}
						}
				}
			}
		}

	}


	//A short real example on how to create a popup window with that
	public class PixelErrorWindow : EditorWindow
	{
		UI ui = new UI();

		public void OnGUI () => ui.Draw(DrawGUI, inInspector:false);
		
		public void DrawGUI ()
		{
			using (Cell.LinePx(100)) Draw.Label("Splat res");
		}


		public static void ShowWindow ()
		{
			PixelErrorWindow window = new PixelErrorWindow();
			window.position = new Rect(Screen.currentResolution.width/2-100, Screen.currentResolution.height/2-50, 200, 100);
			window.ShowAuxWindow();
		}

		//[MenuItem ("Window/MapMagic/Test")]
		public static void ShowEditor ()
		{
			EditorWindow.GetWindow<PixelErrorWindow>("Pixel Error");
		}
	}


	//MapMagic's about window
	public class AboutWindow : EditorWindow
	{
		UI ui = new UI();

		public void OnGUI ()
		{
			ui.Draw(DrawGUI, inInspector:false);
		}

		public void DrawGUI ()
		{
			using (Cell.Line)
				DrawAbout();
		}

		public static void DrawAbout ()
		{
			using (Cell.RowPx(100))
				Draw.Icon(UI.current.textures.GetTexture("MapMagic/Icons/AssetBig"), scale:0.5f);

			using (Cell.Row)
			{
				string versionName = "0.0.0.0"; // MapMagicObject.version.ToString();
				//versionName = versionName[0]+"."+versionName[1]+"."+versionName[2];
				using (Cell.LineStd) Draw.Label("MapMagic " + versionName);
				using (Cell.LineStd) Draw.Label("by Denis Pahunov");

				Cell.EmptyLinePx(10);

				using (Cell.LineStd) Draw.URL(" - Online Documentation", "https://gitlab.com/denispahunov/mapmagic/wikis/home");
				using (Cell.LineStd) Draw.URL(" - Video Tutorials", url:"https://www.youtube.com/playlist?list=PL8fjbXLqBxvbsJ56kskwA2tWziQx3G05m");
				using (Cell.LineStd) Draw.URL(" - Forum Thread", url:"https://forum.unity.com/threads/released-mapmagic-2-infinite-procedural-land-generator.875470/");
				using (Cell.LineStd) Draw.URL(" - Issues / Ideas", url:"http://mm2.idea.informer.com");
			}
		}


		//[MenuItem ("Window/MapMagic/About")]
		public static void ShowWindow ()
		{
			AboutWindow window = (AboutWindow)GetWindow(typeof (AboutWindow));

			Texture2D icon = TexturesCache.LoadTextureAtPath("MapMagic/Icons/Window");
			window.titleContent = new GUIContent("About MapMagic", icon);

			window.position = new Rect(100,100,300,200);
		}
	}


	//A window with toolbar and scrollzoom
	public abstract class BaseMatrixWindow : EditorWindow
	{
		public abstract Texture2D PreviewTexture { get; }

		UI toolbarUI = new UI();
		UI previewUI = UI.ScrolledZoomedUI(maxZoomStage:0, minZoomStage:8); 

		const int winToolbarWidth = 260; //128+4

		private void OnGUI () 
		{
			titleContent = new GUIContent(name);

			previewUI.Draw(DrawPreview, inInspector:false);
			toolbarUI.Draw(DrawWinToolbar, inInspector:false);
		}


		protected virtual void DrawPreview ()
		{
				//background
				Rect displayRect = new Rect(0, 0, Screen.width, Screen.height);
				float gridBackgroundColor = !UI.current.styles.isPro ? 0.45f : 0.2f;
				float gridColor = !UI.current.styles.isPro ? 0.5f : 0.23f;
				Draw.StaticGrid(
					displayRect: displayRect,
					cellSize:new Vector2(128,128),
					cellOffset:new Vector2(),
					color:new Color(gridColor,gridColor,gridColor), 
					background:new Color(gridBackgroundColor,gridBackgroundColor,gridBackgroundColor),
					fadeWithZoom:true);

				//using (Cell.Custom(MatrixRect))
				//	Draw.MatrixPreviewTexture(PreviewTexture, colorize:colorize, relief:relief, min:min, max:max);

				Draw.StaticAxis(displayRect, 0, false, Color.red);
				Draw.StaticAxis(displayRect, 0, true, Color.blue);

				//foreach (IPlugin plugin in plugins)
				//	plugin.DrawWindow(Matrix, this);
		}


		protected virtual void DrawWinToolbar () //a toolbar window inside main window (not tollbar at top)
		{
				Cell.EmptyRow();
				using (Cell.RowPx(winToolbarWidth)) 
				{
					Cell.EmptyLinePx(6);
					using (Cell.LineStd)
					{
						bool enabled = true;
						using (new Draw.FoldoutGroup(ref enabled, "Toolbar", isLeft:false, style:UI.current.styles.foldoutOpaque, backgroundWhileClosed:true))
						{

						}
					}
				}

				Cell.EmptyRowPx(8);
		}
	}


	//Layers without anything
	//This window is UI of a Direct Texture Holder - a script that is applied with MM's Direct Textures Output
	//[CustomEditor(typeof(DirectTexturesHolder))]
	public class DirectTexturesHolder 
	{
		[NonSerialized] public DictionaryOrdered<string,Texture2D> textures = new DictionaryOrdered<string, Texture2D>();
	}

	public class DirectTexturesHolderInspector : Editor
	{
		UI ui = new UI();

		public override void OnInspectorGUI ()
		{
			ui.Draw(DrawGUI, inInspector:true);
		}

		public void DrawGUI ()
		{
			DirectTexturesHolder holder = null;//(DirectTexturesHolder)target;

			using (Cell.Line)
			{
				Cell layersCell = Cell.current;

				using (Cell.LinePx(0)) 
					LayersEditor.DrawLayersThemselves(Cell.current, 
						holder.textures.Count,
						onDraw:n => DrawLayer(holder.textures.GetKeyByNum(n), holder.textures[n]) );
			}
		}

		private static void DrawLayer (string name, Texture2D texture)
		{
			Cell.EmptyLinePx(4);

			using (Cell.LinePx(64))
			{
				Cell.EmptyRowPx(4);
				using (Cell.RowPx(64)) Draw.TextureIcon(texture); 
				using (Cell.Row) Draw.Label(name);
				Cell.EmptyRowPx(4);
			}

			Cell.EmptyLinePx(4);
		}

	}//class


	//Ways to show simple window
	public class GraphWindow : EditorWindow
	{
		public class Graph { public bool ContainsSubGraph(Graph g) {return false;} }
		public Graph graph;

		//Simplest way to open window from the menu - but no object could be provided
		//[MenuItem ("Window/MapMagic/About")]  //uncomment this
		public static void ShowWindow ()
		{
			GraphWindow window = (GraphWindow)GetWindow(typeof (GraphWindow));
			window.position = new Rect(100,100,300,200);
		}


		public static GraphWindow ShowInNewTab (Graph graph) //this is done with a button from somewhere
		{
			GraphWindow window = CreateInstance<GraphWindow>();

			FocusOnWindow(window, inTab:true);
			return window;
		}


		public static GraphWindow Show (Graph graph) //this is done with a button from somewhere
		{
			GraphWindow window = null;
			GraphWindow[] allWindows = Resources.FindObjectsOfTypeAll<GraphWindow>();

			//if opened as biome via focused graph window - opening as biome
			if (focusedWindow is GraphWindow focWin)
				return focWin;
				//do not continue to ShowWindow?

			//if opened only one window - using it (and trying to load mm biomes)
			if (window == null)
			{
				if (allWindows.Length == 1)  
				{
					window = allWindows[0];
					window.graph = graph;
				}
			}

			//if window with this graph currently opened - just focusing it
			if (window == null)
			{
				for (int w=0; w<allWindows.Length; w++)
					if (allWindows[w].graph == graph)
						window = allWindows[w];
			}

			//if no window found after all - creating new tab (and trying to load mm biomes)
			if (window == null)
			{
				window = CreateInstance<GraphWindow>();
				window.graph = graph;
			}
					
			FocusOnWindow(window, inTab:false);
			return window;
		}


		private static void FocusOnWindow (GraphWindow window, bool inTab=false)
		/// Opens the graph window. But it should be created and graph assigned first.
		{
			Texture2D icon = TexturesCache.LoadTextureAtPath("MapMagic/Icons/Window"); 
			window.titleContent = new GUIContent("MapMagic Graph", icon);

			if (inTab) window.ShowTab();
			else window.Show();
			window.Focus();
			window.Repaint();

			DragDrop.obj = null;
			//window.ScrollZoomOnOpen(); //focusing after window has shown (since it needs window size)
		}


		private static GraphWindow FindReusableWindow (Graph graph)
		/// Finds the most appropriate window among all of all currently opened
		{
			GraphWindow[] allWindows = Resources.FindObjectsOfTypeAll<GraphWindow>();

			//if opened only one window - using it
			if (allWindows.Length == 1)  
				return allWindows[0];

			//if opening from currently active window
			if (focusedWindow is GraphWindow focWin)
				if (focWin.graph.ContainsSubGraph(graph))
					return focWin;
						
			//if window with this graph currently opened
			for (int w=0; w<allWindows.Length; w++)
				if (allWindows[w].graph == graph)
					return allWindows[w];

			//if the window with parent graph currently opened
			for (int w=0; w<allWindows.Length; w++)
				if (allWindows[w].graph.ContainsSubGraph(graph))
					return allWindows[w];

			return null;
		}
	}
}
