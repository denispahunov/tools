Shader "Hidden/DPLayout/Frame"
/// Draws pixel-perfect frame with rounded corners
{
    Properties
    {
		_MainTex ("Texture", 2D) = "white" {}
		_CellRect("Cell Rect", Vector) = (0,0,10,10)
		_ClipRect("Clip Rect", Vector) = (0,0,10,10) //window size, assigned automatically

		_NodeRect("Node Rect", Vector) = (0,0,100,100) //object that casts shadow
		_CornerRounding("Corner Rounding", Range(0, 100)) = 50
		_NodeColor ("Node Color", Color) = (1, 0, 0, 1)

		_FrameWidth("Frame Width", Range(0, 10)) = 1
		_FrameColor ("Frame Color", Color) = (0, 0, 0, 1)

        _DpiScale("DPI Scale", Range(0, 1)) = 0.5
    }

    SubShader
    {
        Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Stencil
		{
			Ref[_Stencil]
			Comp[_StencilComp]
			Pass[_StencilOp]
			ReadMask[_StencilReadMask]
			WriteMask[_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest[unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

            struct appdata_t
            {
                float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
				float2 screenPos : TEXCOORD1; //for window clipping
				float2 stretchedUv : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_TexelSize; // Unity-provided: (1/texWidth, 1/texHeight, texWidth, texHeight)
			float4 _CellRect;
			float4 _ClipRect;

			float4 _NodeRect;
			float _CornerRounding;
			float4 _NodeColor;

			float _FrameWidth;
			float4 _FrameColor;

			float _DpiScale;

            v2f vert (appdata_t v)
            {
                v2f o;

                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				o.screenPos = v.vertex.xy;

                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
				//early clipping
				float alpha = UnityGet2DClipping(i.screenPos.xy, _ClipRect);
				alpha *= step(0, i.screenPos.y);
				clip(alpha - 0.001); 


				//dpi scale affects only the size of _CellRect (and all depending parameters)
				float2 pixel = i.uv * ceil(_CellRect.zw * _DpiScale);
				//return (pixel.y-0.5) % 1;

				_NodeRect *= _DpiScale; //round(_NodeRect*_DpiScale); //not rounded, rounding center instead
				_CornerRounding = round(_CornerRounding*_DpiScale);

				//distance to _NodeRect with rounded corners
				//treating rect like a circle (point to be precise) and shrinking it horizontally and vertically
				float2 center = _NodeRect.xy + _NodeRect.zw*0.5;
				float2 baseMin = _NodeRect.xy + _CornerRounding; 
				float2 baseMax = _NodeRect.xy + _NodeRect.zw - _CornerRounding;
				float2 diff = pixel - center;
				float2 offCenter = pixel + clamp(diff, baseMin-center, baseMax-center) - diff;
				offCenter = round(offCenter+0.0001); //pixel perfect //In some cases rounding 0.5 exactly gives random +-1, so adding epsilon

				float dist = length(offCenter - pixel) + 0.5; // - half-pixel
				//return float4(dist/2, dist/4, dist/10, 1);

				//AA frame
				float innerVal = 1 - clamp(0, 1, dist - _CornerRounding);
				float outerVal = clamp(0, 1, dist - _CornerRounding + _FrameWidth);

				innerVal = sqrt(innerVal); //making AA more smooth
				outerVal = 1 - (1-outerVal)*(1-outerVal);

				float frameVal = min(innerVal,outerVal);


				//blending
				float4 col = _NodeColor * innerVal;
				col = lerp(col, _FrameColor, frameVal); 

				//col.rgb = frameVal;
				//col.a = 1;
				//col.a = max(col.a,0.3);
				return col;

            }
            ENDCG
        }
    }
}
