﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditor.Overlays;
using Den.Tools.SceneEdit;


namespace Den.Tools.GUI.Popup
{
	public class SingleWindow : EditorWindow //PopupWindowContent 
	{
		const bool debug = false;

		public bool sortItems = true;

		const int searchLineHeight = 30;
		const int itemLineHeight = 22;
		const int separatorHeight = 6;
		const int backLineHeight = 22;

		private UI ui = UI.ScrolledUI();

		public List<Item> openedItems = new List<Item>();
		public Item RootItem => openedItems[0];
		public Item TopItem => openedItems[deepness];
		public int deepness = 0; //at which path level we are now (instead path.Count-1, since we've got to have something at the right when scrolling back)

		public static readonly Color highlightColor = new Color(0.6f, 0.7f, 0.9f);
		public static readonly Color highlightColorPro = new Color(0.219f, 0.387f, 0.629f);

		public static readonly float scrollSpeed = 5f;
		private static DateTime lastFrameTime = DateTime.Now;

		public string search;

		public SingleWindow (Item rootItem) => openedItems.Add(rootItem);

		//public override void OnGUI (Rect rect)  =>  ui.Draw(DrawGUI, inInspector:false);
		public void OnGUI ()  =>  ui.Draw(DrawGUI, inInspector:false);

		private void DrawGUI ()
		{
			titleContent = new GUIContent(TopItem.name);
			float width = position.width;
			float height = position.height;

			//background
			Draw.Rect(UI.current.styles.isPro ? new Color(0.1f, 0.1f, 0.1f) : new Color(0.925f, 0.925f, 0.925f));

			//smoothly scrolling
			float deltaTime = (float)(DateTime.Now-lastFrameTime).TotalSeconds;
			lastFrameTime = DateTime.Now;

			float targetScroll = -deepness * width;

			if (!debug  &&  UI.current.layout)
			{
				if (ui.scrollZoom.scroll.x < targetScroll)
				{
					ui.scrollZoom.scroll.x += scrollSpeed * width * deltaTime;
					if (ui.scrollZoom.scroll.x > targetScroll) ui.scrollZoom.scroll.x = targetScroll;
				}
				if (ui.scrollZoom.scroll.x > targetScroll)
				{
					ui.scrollZoom.scroll.x -= scrollSpeed * width * deltaTime;
					if (ui.scrollZoom.scroll.x < targetScroll) ui.scrollZoom.scroll.x = targetScroll;
				}
			}

			
			//drawing search first - otherwise it will loose focus
			using (Cell.LinePx(searchLineHeight)) //reserving the line for search although it uses custom
				using (Cell.Static(6, 6, width-6, 18))
			{
				Draw.SearchLabel(ref search, forceFocus:true);

				if (Cell.current.valChanged)
					PerformSearch(search, openedItems[deepness], deepness);
			}

			//items
			using (Cell.LinePx(0))
				for (int p=0; p<openedItems.Count; p++)
					using (Cell.RowPx(width))
						DrawMenu(openedItems[p], p);

			//refreshing selection frame
			//this.editorWindow.Repaint();
			Repaint();
		}


		private void PerformSearch (string search, Item startingItem, int startingItemNum)
		{
			//removing all other opened items
			openedItems.RemoveAfter(startingItemNum);

			//search erased - removing search tab
			if (search == null || search.Length == 0)
			{
				if (openedItems[openedItems.Count-1].name.StartsWith("Search "))
					openedItems.RemoveAfter(openedItems.Count-2);

				deepness = openedItems.Count-1;
			}

			//search entered/changed
			else
			{
				Item baseItem = null; //item we performing search at
				Item searchItem = null; //item with search results

				if (openedItems[openedItems.Count-1].name.StartsWith("Search ")) //search is already opened
				{
					searchItem = openedItems[openedItems.Count-1];
					baseItem = openedItems[openedItems.Count-2];
				}
				else //new search item
				{
					baseItem = openedItems[openedItems.Count-1];
					searchItem = new Item("Search " + baseItem.name);
					openedItems.Add(searchItem);
				}

				deepness = openedItems.Count-1;

				searchItem.subItems = baseItem.FindAll(search, inSubItems:true, contains:true);
			}
		}


		private void DrawMenu(Item item, int itemDeepness)
		{
			//sub-items
			if (item.subItems != null)
			{
				bool coloredIcons = item.name.StartsWith("Search ");

				int itemsSpace = (int)position.height-searchLineHeight-backLineHeight;
				using (Cell.LinePx(itemsSpace))
					using (new Draw.ScrollGroup(ref item.scroll))
				{
					
					for (int n=0; n<item.subItems.Count; n++)
					{
						using (Cell.LinePx(0)) 
						{
							Item currItem = item.subItems[n];

							//drawing
							bool highlighted = Cell.current.Contains(ui.mousePos);
						
							if (!currItem.isSeparator)
								using (Cell.LinePx(itemLineHeight)) DefaultItemDraw(currItem, n, highlighted, coloredIcons);
							else
								using (Cell.LinePx(separatorHeight)) DrawSeparator();

							//clicking
							bool clicked = highlighted && 
								!currItem.disabled &&
								Event.current.type == EventType.MouseDown && 
								Event.current.button == 0 && 
								!UI.current.layout;

							if (clicked && currItem.subItems != null) 
							{
								if (openedItems.Count-1 > itemDeepness)
									openedItems.RemoveAfter(itemDeepness);
								openedItems.Add(currItem);
								deepness = itemDeepness + 1;
							}
							if (clicked && currItem.onClick != null)
								currItem.onClick();

							if (clicked && currItem.closeOnClick)
								Close();
								//editorWindow.Close();
						}
					}
				}
			}

			Cell.EmptyLine();

			//back item
			if (itemDeepness != 0)
				using (Cell.LinePx(backLineHeight+4)) //+4 to avoid leaving empty pixel line on selection
				{
					if (Cell.current.Contains(ui.mousePos))
						Draw.Rect(UI.current.styles.isPro ? highlightColorPro : highlightColor);	

					Texture2D shveronTex = UI.current.textures.GetTexture("DPUI/Chevrons/TickLeft"); 
					using (Cell.RowPx(30)) 
						Draw.Icon(shveronTex);

					using (Cell.Row) 
						Draw.Label(itemDeepness == 1 ? "Main Menu" : "Back", style:UI.current.styles.boldLabel);

					bool clicked = Cell.current.Contains(ui.mousePos) && Event.current.rawType == EventType.MouseDown && Event.current.button == 0 && !UI.current.layout;
					if (clicked && itemDeepness != 0) 
					{
						deepness = itemDeepness-1;

						search = null;
						UnityEditor.EditorGUI.FocusTextInControl(null); 
					}
				}
		}


		public void DefaultItemDraw (Item item, int num, bool selected, bool colored=false)
		{
			if (selected && !item.disabled)
				Draw.Rect(UI.current.styles.isPro ? highlightColorPro : highlightColor);	

			Cell.current.disabled = item.disabled;

			//icon
			using (Cell.RowPx(30))
			{
				if (colored)
					Draw.Rect(item.color);
				
				if (item.icon!=null) 
					Draw.Icon(item.icon, scale:0.5f);
			}

			//label
			using (Cell.Row) Draw.Label(item.name);

			//chevron
			if (item.subItems != null)
			{
				Texture2D chevronTex = UI.current.textures.GetTexture("DPUI/Chevrons/TickRight");
				using (Cell.RowPx(20)) Draw.Icon(chevronTex);
			}
		}


		public void DrawSeparator ()
		{
			Cell.EmptyRowPx(20);
			using (Cell.Row)
			{
				Cell.EmptyLine();
				using (Cell.LinePx(1)) Draw.Rect(Color.gray);
				Cell.EmptyLine();
			}
			Cell.EmptyRowPx(20);
		}


		//public override Vector2 GetWindowSize() => new Vector2(width*(debug ? 5 : 1), height);


		public void Show (Vector2 pos)
		{
			//RootItem.SortSubItems();

			//SingleWindow window = EditorWindow.GetWindow<SingleWindow>();
			SingleWindow window = new SingleWindow(RootItem);
			//using GetWindow will open it as a regular window


			//PopupWindow.Show(new Rect(pos.x-width/2,pos.y-10,width,0), this);

			//ShowWindow wnd = EditorWindow.GetWindow<ShowWindow>();
			//ShowWindow wnd = new ShowWindow();
			//wnd.titleContent = new GUIContent("ShowWindow");
			//wnd.ShowAsDropDown(new Rect(pos.x-width/2,pos.y-10,width,0), new Vector2(200,200));
			//wnd.ShowNotification(new GUIContent("Test"));
			window.ShowAuxWindow();
			
			//PopupWindow.Show(new Rect(pos.x-width/2,pos.y-10,width,0), this);

			//EditorUtility.DisplayPopupMenu(new Rect(pos.x-width/2,pos.y-10,width,0), "Assets/", null);

			   //Rect popupRect = new Rect(pos.x - width / 2, pos.y - 10, width, height);
    //OverlayCanvas.ShowPopupAtMouse(popupRect);
		}
	}

}